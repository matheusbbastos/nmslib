#cd #/bin/bash
#test for kdr

#test script

DATA_FILE=$1

if [ "$DATA_FILE" = "" ] ; then
  echo "Specify a test file (1st arg)"
  exit 1
fi

if [ ! -f "$DATA_FILE" ] ; then
  echo "The following data file doesn't exist: $DATA_FILE"
  exit 1
fi

SPACE=$2

if [ "$SPACE" = "" ] ; then
  echo "Specify the space (2d arg)"
  exit 1
fi

THREAD_QTY=$3

if [ "$THREAD_QTY" = "" ] ; then
  echo "Specify the number of test threads (3d arg)"
  exit 1
fi

# If you have python, latex, and PGF installed,
# you can set the following variable to 1 to generate a performance plot.
GEN_PLOT=$4
if [ "$GEN_PLOT" = "" ] ; then
  echo "Specify a plot-generation flag: 1 to generate plots (3d arg)"
  exit 1
fi

DATA_NAME=$5
if [ "$DATA_NAME" = "" ] ; then
  echo "Specify name"
  exit 1
fi

TEST_SET_QTY=1
QUERY_QTY=50
K=1
NUM_DATA=1000
LOG_FILE_PREFIX="logs/log_kdr_$K"
BIN_DIR="../similarity_search"
GS_CACHE_DIR="gs_cache"
mkdir -p $GS_CACHE_DIR
prefix="prefix"

#rm -f $RESULT_FILE.*
#rm -f $LOG_FILE_PREFIX.*
#rm -f $CACHE_PREFIX_GS*

COMMON_ARGS="-s $SPACE -i $DATA_FILE -b $TEST_SET_QTY -Q  $QUERY_QTY -k $K --threadTestQty $THREAD_QTY "

LN=1

function do_run {
  DO_APPEND="$1"
  if [ "$DO_APPEND" = "" ] ; then
    echo "Specify DO_APPEND (1st arg of the function do_run)"
    exit 1
  fi
  if [ "$DO_APPEND" = "1" ] ; then
    APPEND_FLAG=" -a 1 "
  fi
  METHOD_NAME="$2"
  if [ "$METHOD_NAME" = "" ] ; then
    echo "Specify METHOD_NAME (2d arg of the function do_run)"
    exit 1
  fi
  INDEX_ARGS="$3"
  if [ "$INDEX_ARGS" = "" ] ; then
    echo "Specify INDEX_ARGS (3d arg of the function do_run)"
    exit 1
  fi
  RECALL_ONLY="$4"
  if [ "$RECALL_ONLY" = "" ] ; then
    echo "Specify the RECALL_ONLY flag (4th argument of the function do_run)"
    exit 1
  fi
  QUERY_ARGS="$5"
  INDEX_NAME="$6"
  DELTA="$7"
  L="$8"
  RESULT_FILE=results/output_file_${DATA_NAME}
  CACHE_PREFIX_GS=$GS_CACHE_DIR/test_run.sp=${SPACE}_data=${DATA_NAME}_tq=$THREAD_QTY_"$7"_"$8"
  INDEX_ARGS="$INDEX_ARGS,file=results/stats/kDR_"$7"_"$8"_$LN.txt"
  #echo "$cmd"

  #  cmd="$BIN_DIR/release/experiment $COMMON_ARGS -m "$METHOD_NAME" $INDEX_ARGS $QUERY_ARGS $APPEND_FLAG -l ${LOG_FILE_PREFIX}.$LN --recallOnly $RECALL_ONLY"
  #  echo "$cmd"
  #  bash -c "$cmd"
  #  if [ "$?" != "0" ] ; then
  #      echo "Failure! 1"
  #      exit 1
  #    fi

  if [ "$INDEX_NAME" = "" ] ; then
    cmd="$BIN_DIR/release/experiment $COMMON_ARGS --outFilePrefix $RESULT_FILE -g $CACHE_PREFIX_GS -D $NUM_DATA -m "$METHOD_NAME" $INDEX_ARGS $QUERY_ARGS $APPEND_FLAG -l ${LOG_FILE_PREFIX}.$LN --recallOnly $RECALL_ONLY"
    echo "$cmd"
    bash -c "$cmd"
    if [ "$?" != "0" ] ; then
      echo "Failure! 1"
      exit 1
    fi
  else
    # Indices are to be deleted manually!
    rm -f ${INDEX_NAME}*

    #      if [[ -e ${INDEX_NAME} ]]; then
    #              echo "ok"
    #              cmd="$BIN_DIR/release/experiment $COMMON_ARGS -L "$INDEX_NAME" -m "$METHOD_NAME" $QUERY_ARGS $APPEND_FLAG -l ${LOG_FILE_PREFIX}_query.$LN --recallOnly $RECALL_ONLY"
    #              echo "$cmd"
    #              bash -c "$cmd"
    #              if [ "$?" != "0" ] ; then
    #                echo "Failure! 3"
    #                exit 1
    #              fi
    #      else

    cmd="$BIN_DIR/release/experiment $COMMON_ARGS --outFilePrefix $RESULT_FILE -g $CACHE_PREFIX_GS -D $NUM_DATA -S "$INDEX_NAME" -m "$METHOD_NAME" $INDEX_ARGS $QUERY_ARGS $APPEND_FLAG -l ${LOG_FILE_PREFIX}.$LN --recallOnly $RECALL_ONLY"
    echo "$cmd"
    bash -c "$cmd"
    if [ "$?" != "0" ] ; then
      echo "Failure! 2"
      exit 1
    fi
    #  fi
  fi
  LN=$((LN+1))

}

# Methods that may create an index (at least for some spaces)
#KNN=15
#do_run 0 "rng" "-c indexThreadQty=1" 0 "-t initSearchAttempts=10,gnns=1 -t initSearchAttempts=20,gnns=1 -t initSearchAttempts=30,gnns=1" "RNG/rng_${SPACE}.index"
#do_run 1 "sw-graph" " -c NN=${KNN},indexThreadQty=1 " 0 " -t efSearch=10 -t efSearch=20" "SW/sw5_${SPACE}.index"
#do_run 1 "knng" "-c NN=${KNN},indexThreadQty=1 " 0 "-t initSearchAttempts=10,gnns=1 -t initSearchAttempts=20,gnns=1 -t initSearchAttempts=30,gnns=1" "KNN/knn5_${SPACE}.index"
#do_run 1 "nndes" "-c NN=${KNN},indexThreadQty=1" 0 "-t initSearchAttempts=10,greedy=1 -t initSearchAttempts=20,greedy=1 -t initSearchAttempts=30,greedy=1"

#parametrizar valor de k e numero de queries
QNUMDATA = 0.07*$NUM_DATA
for h in 40 160 $NUM_DATA
do
  for L in 1 4 8 16 20 24 28 #search attempts (fazer 4 em 4 de acordo com o artigo)
  do
    for delta in 0.4 0.3 0.2 0.1 0.05 0.03 0.02 #erro
    do
      do_run 1 "kdr" "-c indexThreadQty=1,kmax=200,delta=${delta},L=${L},Q=${QNUMDATA},h=${h}" 0 "-t gnns=0" "kDR/kdr_${SPACE}_${L}_${delta}_${DATA_NAME}.index" ${delta} ${L} #L threads for searching (mesmo tanto que utilizado na construcao)
    done
done
done

if [ "$GEN_PLOT" = 1 ] ; then
  ./genplot_configurable.py -i ${RESULT_FILE}_K=${K}.dat -o plot_${K}_NNEff -x 1~norm~Recall -y 1~log~ImprEfficiency -l "1~south west" -t "Improvement in efficiency vs recall" -n "MethodName" -a axis_desc.txt -m meth_desc.txt
  ./genplot_configurable.py -i ${RESULT_FILE}_K=${K}.dat -o plot_${K}_NNQT -x 1~norm~Recall -y 1~log~QueryTime -l "1~south west" -t "Query Time vs recall" -n "MethodName" -a axis_desc.txt -m meth_desc.txt
  ./genplot_configurable.py -i ${RESULT_FILE}_K=${K}.dat -o plot_${K}_NNIT -x 1~norm~Recall -y 1~log~IndexTime -l "1~south west" -t "Index Time vs recall" -n "MethodName" -a axis_desc.txt -m meth_desc.txt
fi
