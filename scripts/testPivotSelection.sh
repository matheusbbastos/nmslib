#cd #/bin/bash
#test for knng and rng

#test script

DATA_FILE=$1

if [ "$DATA_FILE" = "" ] ; then
  echo "Specify a test file (1st arg)"
  exit 1
fi

if [ ! -f "$DATA_FILE" ] ; then
  echo "The following data file doesn't exist: $DATA_FILE"
  exit 1
fi

SPACE=$2

if [ "$SPACE" = "" ] ; then
  echo "Specify the space (2d arg)"
  exit 1
fi

THREAD_QTY=$3

if [ "$THREAD_QTY" = "" ] ; then
  echo "Specify the number of test threads (3d arg)"
  exit 1
fi

# If you have python, latex, and PGF installed,
# you can set the following variable to 1 to generate a performance plot.
GEN_PLOT=$4
if [ "$GEN_PLOT" = "" ] ; then
  echo "Specify a plot-generation flag: 1 to generate plots (3d arg)"
  exit 1
fi

DATA_NAME=$5
if [ "$DATA_NAME" = "" ] ; then
  echo "Specify name"
  exit 1
fi

TEST_SET_QTY=1
QUERY_QTY=100
K=1,10,30 #definicao do numero de vizinhos mais proximos
LOG_FILE_PREFIX="logs/log_hgraph_$DATA_NAME"
BIN_DIR="../similarity_search"
GS_CACHE_DIR="gs_cache"
mkdir -p $GS_CACHE_DIR
prefix="prefix"
COMMONQUERY="-t initSearchAttempts=1 -t initSearchAttempts=5 -t initSearchAttempts=10 -t initSearchAttempts=20 -t initSearchAttempts=40 -t initSearchAttempts=80 -t initSearchAttempts=120 -t initSearchAttempts=240"
SWQUERY="-t initSearchAttempts=1,algoType=gnns -t initSearchAttempts=5,algoType=gnns -t initSearchAttempts=10,algoType=gnns -t initSearchAttempts=20,algoType=gnns -t initSearchAttempts=40,algoType=gnns -t initSearchAttempts=80,algoType=gnns -t initSearchAttempts=120,algoType=gnns -t initSearchAttempts=240,algoType=gnns"
#rm -f $RESULT_FILE.*
#rm -f $LOG_FILE_PREFIX.*
#rm -f $CACHE_PREFIX_GS*

COMMON_ARGS="-s $SPACE -i $DATA_FILE -b $TEST_SET_QTY -Q  $QUERY_QTY -k $K --threadTestQty $THREAD_QTY "

LN=1

function do_run {
  NUM_DATA="$1"
  echo $NUM_DATA
  CACHE_PREFIX_GS=$GS_CACHE_DIR/test_run.sp=${SPACE}_numData=${NUM_DATA}_data=${DATA_NAME}_tq=$THREAD_QTY
  DO_APPEND="$2"
  if [ "$DO_APPEND" = "" ] ; then
    echo "Specify DO_APPEND (1st arg of the function do_run)"
    exit 1
  fi
  if [ "$DO_APPEND" = "1" ] ; then
    APPEND_FLAG=" -a 1 "
  fi
  METHOD_NAME="$3"
  if [ "$METHOD_NAME" = "" ] ; then
    echo "Specify METHOD_NAME (2d arg of the function do_run)"
    exit 1
  fi
  INDEX_ARGS="$4"
  if [ "$INDEX_ARGS" = "" ] ; then
    echo "Specify INDEX_ARGS (3d arg of the function do_run)"
    exit 1
  fi
  RECALL_ONLY="$5"
  if [ "$RECALL_ONLY" = "" ] ; then
    echo "Specify the RECALL_ONLY flag (4th argument of the function do_run)"
    exit 1
  fi
  QUERY_ARGS="$6"
  INDEX_NAME="$7"
  RESULT_FILE=results/output_file_${DATA_NAME}_${NUM_DATA}
  #echo "$cmd"

  #  cmd="$BIN_DIR/release/experiment $COMMON_ARGS -m "$METHOD_NAME" $INDEX_ARGS $QUERY_ARGS $APPEND_FLAG -l ${LOG_FILE_PREFIX}.$LN --recallOnly $RECALL_ONLY"
  #  echo "$cmd"
  #  bash -c "$cmd"
  #  if [ "$?" != "0" ] ; then
  #      echo "Failure! 1"
  #      exit 1
  #    fi

  if [ "$INDEX_NAME" = "" ] ; then
    cmd="$BIN_DIR/release/experiment $COMMON_ARGS --outFilePrefix $RESULT_FILE -g $CACHE_PREFIX_GS -D $NUM_DATA -m "$METHOD_NAME" $INDEX_ARGS $QUERY_ARGS $APPEND_FLAG -l ${LOG_FILE_PREFIX}.$LN --recallOnly $RECALL_ONLY"
    echo "$cmd"
    bash -c "$cmd"
    if [ "$?" != "0" ] ; then
      echo "Failure! 1"
      exit 1
    fi
  else
    # Indices are to be deleted manually!
    rm -f ${INDEX_NAME}*

    #      if [[ -e ${INDEX_NAME} ]]; then
    #              echo "ok"
    #              cmd="$BIN_DIR/release/experiment $COMMON_ARGS -L "$INDEX_NAME" -m "$METHOD_NAME" $QUERY_ARGS $APPEND_FLAG -l ${LOG_FILE_PREFIX}_query.$LN --recallOnly $RECALL_ONLY"
    #              echo "$cmd"
    #              bash -c "$cmd"
    #              if [ "$?" != "0" ] ; then
    #                echo "Failure! 3"
    #                exit 1
    #              fi
    #      else

    cmd="$BIN_DIR/release/experiment $COMMON_ARGS --outFilePrefix $RESULT_FILE -g $CACHE_PREFIX_GS -D $NUM_DATA -S "$INDEX_NAME" -m "$METHOD_NAME" $INDEX_ARGS $QUERY_ARGS $APPEND_FLAG -l ${LOG_FILE_PREFIX}.$LN --recallOnly $RECALL_ONLY"
    echo "$cmd"
    bash -c "$cmd"
    if [ "$?" != "0" ] ; then
      echo "Failure! 2"
      exit 1
    fi
    #  fi
  fi
  LN=$((LN+1))

}

# Methods that may create an index (at least for some spaces)
#KNN=15
#do_run 0 "rng" "-c indexThreadQty=1" 0 "-t initSearchAttempts=10,gnns=1 -t initSearchAttempts=20,gnns=1 -t initSearchAttempts=30,gnns=1" "RNG/rng_${SPACE}.index"
#do_run 1 "sw-graph" " -c NN=${KNN},indexThreadQty=1 " 0 " -t efSearch=10 -t efSearch=20" "SW/sw5_${SPACE}.index"
#do_run 1 "knng" "-c NN=${KNN},indexThreadQty=1 " 0 "-t initSearchAttempts=10,gnns=1 -t initSearchAttempts=20,gnns=1 -t initSearchAttempts=30,gnns=1" "KNN/knn5_${SPACE}.index"
#do_run 1 "nndes" "-c NN=${KNN},indexThreadQty=1" 0 "-t initSearchAttempts=10,greedy=1 -t initSearchAttempts=20,greedy=1 -t initSearchAttempts=30,greedy=1"
#parametrizar valor de k e numero de queries

for NUM in 68040
#for NUM in 25374
#for NUM in 70000
do
  for KNN in 5 10 55
   do
#do_run $NUM 1 "hgraph" "-c levels=1,indexThreadQty=0,NN=${KNN},seeds=1,overlap=0.1,graph=knng" 0 "$COMMONQUERY" "HGRAPH/${NUM}hknng_${SPACE}_${DATA_NAME}_${LEV}_${SEED}_${KNN}_${OVER}.index"
#do_run $NUM 1 "hgraphset" "-c levels=1,indexThreadQty=0,NN=${KNN},seeds=1,overlap=0.1,gtype1=knng" 0 "$COMMONQUERY" "HGRAPH/${NUM}hknng_${SPACE}_${DATA_NAME}_1_1_${KNN}_1.index"
do_run $NUM 1 "knng" "-c NN=${KNN},indexThreadQty=1 " 0 "$COMMONQUERY" "KNN/${NUM}knn${KNN}_${SPACE}_${DATA_NAME}.index"
do_run $NUM 1 "sw-graph" " -c NN=${KNN},efConstruction=${KNN},indexThreadQty=1 " 0 "$SWQUERY" "SW/${NUM}sw${KNN}_${SPACE}_${DATA_NAME}.index"
for LEV in 1000 5000 10000
    #do
    for SEED in 2 5 10
    do
      for OVER in 0.05 0.1
        do
     do_run $NUM 1 "hgraphset" "-c levels=${LEV},indexThreadQty=0,NN=${KNN},seeds=${SEED},overlap=${OVER},gtype1=knng,ptype=random" 0 "$COMMONQUERY" "HGRAPH/${NUM}hknng_${SPACE}_${DATA_NAME}_1_${SEED}_${KNN}_1_rand.index"
    do_run $NUM 1 "hgraphset" "-c levels=${LEV},indexThreadQty=0,NN=${KNN},seeds=${SEED},overlap=${OVER},gtype1=swg,ptype=random" 0 "$COMMONQUERY" "HGRAPH/${NUM}hknngsw_${SPACE}_${DATA_NAME}_1_${SEED}_${KNN}_1_rand.index"
          for N in 0.001 0.01 0.1
          do
      #do_run $NUM 0 "hgraph" "-c levels=${LEV},indexThreadQty=1,NN=1,seeds=${SEED},construction=BU,graph=rng" 0 "$COMMONQUERY" "HGRAPH/${NUM}hrng_${SPACE}_${DATA_NAME}.index"
            #do_run $NUM 1 "hgraph" "-c levels=10000,indexThreadQty=0,NN=${KNN},seeds=${SEED},overlap=${OVER},graph=knng" 0 "$COMMONQUERY" "HGRAPH/${NUM}hknng_${SPACE}_${DATA_NAME}_${LEV}_${SEED}_${KNN}_${OVER}.index"
    do_run $NUM 1 "hgraphset" "-c levels=${LEV},indexThreadQty=0,NN=${KNN},seeds=${SEED},overlap=${OVER},gtype1=knng,ptype=nfft,nrandom=${N}" 0 "$COMMONQUERY" "HGRAPH/${NUM}hknng_${SPACE}_${DATA_NAME}_1_${SEED}_${KNN}_1_fft_${N}.index"
    do_run $NUM 1 "hgraphset" "-c levels=${LEV},indexThreadQty=0,NN=${KNN},seeds=${SEED},overlap=${OVER},gtype1=swg,ptype=nfft,nrandom=${N}" 0 "$COMMONQUERY" "HGRAPH/${NUM}hknngsw_${SPACE}_${DATA_NAME}_1_${SEED}_${KNN}_1_fft_${N}.index"
    do_run $NUM 1 "hgraphset" "-c levels=${LEV},indexThreadQty=0,NN=${KNN},seeds=${SEED},overlap=${OVER},gtype1=knng,ptype=npam,nrandom=${N}" 0 "$COMMONQUERY" "HGRAPH/${NUM}hknng_${SPACE}_${DATA_NAME}_1_${SEED}_${KNN}_1_pam_${N}.index"
    do_run $NUM 1 "hgraphset" "-c levels=${LEV},indexThreadQty=0,NN=${KNN},seeds=${SEED},overlap=${OVER},gtype1=swg,ptype=npam,nrandom=${N}" 0 "$COMMONQUERY" "HGRAPH/${NUM}hknngsw_${SPACE}_${DATA_NAME}_1_${SEED}_${KNN}_1_pam_${N}.index"  
            #do_run $NUM 1 "hgraph" "-c levels=${LEV},indexThreadQty=1,NN=${KNN},seeds=${SEED},overlap=${OVER},construction=BU,graph=knng" 0 "$COMMONQUERY" "HGRAPH/${NUM}hknngBU_${SPACE}_${DATA_NAME}_${LEV}_${SEED}_${KNN}_${OVER}.index"
          done
          done
       done
    done
  done
done


if [ "$GEN_PLOT" = 1 ] ; then
  ./genplot_configurable.py -i ${RESULT_FILE}_K=${K}.dat -o plot_${K}_NNEff -x 1~norm~Recall -y 1~log~ImprEfficiency -l "1~south west" -t "Improvement in efficiency vs recall" -n "MethodName" -a axis_desc.txt -m meth_desc.txt
  ./genplot_configurable.py -i ${RESULT_FILE}_K=${K}.dat -o plot_${K}_NNQT -x 1~norm~Recall -y 1~log~QueryTime -l "1~south west" -t "Query Time vs recall" -n "MethodName" -a axis_desc.txt -m meth_desc.txt
  ./genplot_configurable.py -i ${RESULT_FILE}_K=${K}.dat -o plot_${K}_NNIT -x 1~norm~Recall -y 1~log~IndexTime -l "1~south west" -t "Index Time vs recall" -n "MethodName" -a axis_desc.txt -m meth_desc.txt
fi
