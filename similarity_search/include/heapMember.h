//
// Created by larissa on 8/23/17.
//

#ifndef HEAPMEMBER_H
#define HEAPMEMBER_H
#include <vector>
#include <map>
#include <iostream>
#include <cmath>
#include <string>
#include "logging.h"

using namespace std;


namespace similarity {

template<typename T, typename dist_t>
class heapMember {
public:
    heapMember(T node, map<size_t ,int>::iterator it, dist_t value) {
        node_ = node;
        it_ = it;
        value_ = value;
    }

    heapMember(){
    }

    T getNode() const{
        return node_;
    }

    map<size_t, int>::iterator getIterator() const{
        return it_;
    }

    dist_t getValue() const{
        return value_;
    }

    void setNode(T node) {
        node_ = node;
    }

    void setIterator(map<size_t ,int>::iterator it) {
        it_ = it;
    }

    void setValue(dist_t value) {
        value_ = value;
    }

    bool operator< (const heapMember<T, dist_t> &obj1) const{
        return (value_ < obj1.getValue());
    }

    bool operator> (const heapMember<T, dist_t> &obj) const{
        return (value_ > obj.getValue());
    }

    //const????

private:
    map<size_t , int>::iterator it_; //iterator pro multiset
    T node_; //nó do grafo
    dist_t value_;//valor de ordenacao
};

}

#endif //NONMETRICSPACELIB_HEAPMEMBER_H
