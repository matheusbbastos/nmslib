//
// Created by larissa on 7/31/17.
//

#ifndef _MIN_HEAP_H
#define _MIN_HEAP_H

#include <vector>
#include <iostream>
#include <cmath>
#include <string>
#include "logging.h"
#include "heapMember.h"
#include <map>

using namespace std;

namespace similarity{

    template <typename T, typename dist_t>
    class MinHeap {
    public:
        MinHeap<T, dist_t>(){
        };
        void insert(heapMember<T,dist_t> v, size_t id);
        void PrintPosMap();
        int update(heapMember<T,dist_t> v, size_t id);
        void remove(heapMember<T,dist_t> v, size_t id);
        void increaseKey(size_t id, dist_t dg);
        void decreaseKey(size_t id, dist_t dg);
        void setNewValue(size_t id, dist_t dg);
        void removeId(size_t id);
        int findHeap(heapMember<T,dist_t> v, int i);
        bool contain(size_t id);
        int getMin(int ii, int li, int ri);
        //void heapify(vector<heapMember<T, dist_t>> uarray);
        void bubbleUp(int i);
        void bubbleDown(int i);
        heapMember<T, dist_t> extractMin();
        heapMember<T,dist_t> getId(int i);

        heapMember<T,dist_t> getRoot() { //max value for max heap min value for min heaps
            return heap[0];
        }

        heapMember<T,dist_t> getLeftChild(int i){
            return heap[(2*i)+1];
        }

        heapMember<T,dist_t> getParent(int i){
            return heap[(int)(i-1)/2];
        }

        heapMember<T,dist_t> getRightChild(int i){
            return heap[(2*i) +2];
        }

        int getSize() {
            return heap.size();
        }
        heapMember<T,dist_t> getItem(int i){
            return heap[i];
        }
        int getMapSize(){
            return PosMap.size();
        }
        int getLeftChildIndex(int i){
            return (2*i) + 1;
        }


        int getParentIndex(int i){
            return (int)(i-1)/2;
        }


        int getRightChildIndex(int i){
            return (2*i) + 2;
        }

    private:
        void removeIndex(int i);
        mutable vector<heapMember<T,dist_t>> heap;
        mutable map<size_t, int> PosMap;
    };

}

#endif //_MIN_HEAP_H
