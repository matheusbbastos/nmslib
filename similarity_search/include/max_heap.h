//
// Created by larissa on 7/27/17.
//

#ifndef _MAX_HEAP_H
#define _MAX_HEAP_H

#include <vector>
#include <iostream>
#include <cmath>
#include <string>
#include "logging.h"
#include "heapMember.h"
#include <map>


namespace similarity{

    using namespace std;

    template <typename T, typename dist_t>
    class MaxHeap {//: public Heap<T, dist_t>{

    public:
        void insert(heapMember<T,dist_t> v,size_t id);
        void PrintPosMap();
        //int update(heapMember<T,dist_t> v, size_t id);
        //void remove(heapMember<T,dist_t> v, size_t id);
        void removeId(size_t id);
        int findHeap(heapMember<T,dist_t> v, int i);
        bool contain(size_t id);
        void increaseKey(size_t id, dist_t newValue);
        void decreaseKey(size_t id, dist_t dg);
        void bubbleUp(int i);
        void bubbleDown(int i);
        heapMember<T, dist_t> extractMax();
        int getMax(int i, int l, int r);
        heapMember<T,dist_t> getId(int i);

        heapMember<T,dist_t> getRoot() { //max value for max heap min value for min heaps
            return heap[0];
        }

        heapMember<T,dist_t> getLeftChild(int i){
            return heap[(2*i)+1];
        }

        heapMember<T,dist_t> getParent(int i){
            return heap[(int)(i-1)/2];
        }

        heapMember<T,dist_t> getRightChild(int i){
            return heap[(2*i) +2];
        }

        int getSize() {
            return heap.size();
        }
        heapMember<T,dist_t> getItem(int i){
            return heap[i];
        }
        int getMapSize(){
            return PosMap.size();
        }
        int getLeftChildIndex(int i){
            return (2*i) + 1;
        }


        int getParentIndex(int i){
            return (int)(i-1)/2;
        }


        int getRightChildIndex(int i){
            return (2*i) + 2;
        }
        void printHeap(){
            for(int i=0; i < heap.size();i++){
                heapMember<T, dist_t> u = heap[i];
                cout << "Pos" << i;
                cout << "Node:" << u.getNode() << " Value:" << u.getValue();
                cout << "Iterator:" << u.getIterator()->first << " ItPosition:" << u.getIterator()->second << endl;

            }
        }


    private:
        void removeIndex(int i);
        mutable vector<heapMember<T,dist_t>> heap;
        mutable map<size_t, int> PosMap;
    };


}


#endif //_MAX_HEAP_H
