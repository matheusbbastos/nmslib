//
// Created by larissa on 2-8-19.
//

#ifndef _SPACE_QFD_H_
#define _SPACE_QFD_H_

#include <string>
#include <limits>
#include <map>
#include <stdexcept>
#include <sstream>
#include <boost/assign/std/vector.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/moment.hpp>
#include <boost/accumulators/statistics/covariance.hpp>
#include <boost/accumulators/statistics/variates/covariate.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

//using namespace boost::accumulators;
//using namespace boost::assign;
//using namespace boost::numeric::ublas;

typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean> > localMean;
typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::covariance<double, boost::accumulators::tag::covariate1> > > covariance_XY;
typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean, boost::accumulators::tag::variance> > zscore;

#include "global.h"
#include "object.h"
#include "utils.h"
#include "space.h"
#include "space_vector.h"
#include "distcomp.h"

#define SPACE_QFD "qfd"

namespace similarity {


    template <typename dist_t>
    class SpaceQFD : public VectorSpaceSimpleStorage<dist_t> {
    public:
        explicit SpaceQFD() {}
        virtual ~SpaceQFD() {}


        virtual size_t GetElemQty(const Object* obj1) const{
            return obj1->datalength()/ sizeof(dist_t);
        }

        //virtual vector<dist_t> getDistVector(const Object* obj1, size_t dim) const;
        virtual std::string StrDesc() const;

        void init(ObjectVector &FVs) const {

        }


    protected:
        virtual dist_t HiddenDistance(const Object* obj1, const Object* obj2) const;

    private:
        DISABLE_COPY_AND_ASSIGN(SpaceQFD);
    };


}  // namespace similarity



#endif //_SPACE_QFD_H_