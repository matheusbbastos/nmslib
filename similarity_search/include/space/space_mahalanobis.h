/**
 * Mahalanobis distance impl
 *
 */
#ifndef _SPACE_MAHALANOBIS_H_
#define _SPACE_MAHALANOBIS_H_

#include <string>
#include <limits>
#include <map>
#include <stdexcept>
#include <sstream>
#include <boost/assign/std/vector.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/moment.hpp>
#include <boost/accumulators/statistics/covariance.hpp>
#include <boost/accumulators/statistics/variates/covariate.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

//using namespace boost::accumulators;
//using namespace boost::assign;
//using namespace boost::numeric::ublas;

typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean> > localMean;
typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::covariance<double, boost::accumulators::tag::covariate1> > > covariance_XY;
typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean, boost::accumulators::tag::variance> > zscore;

#include "global.h"
#include "object.h"
#include "utils.h"
#include "space.h"
#include "space_vector.h"
#include "distcomp.h"

#define SPACE_MAHALANOBIS "mahalanobis"


namespace similarity {


    template <typename dist_t>
    class SpaceMahalanobis : public VectorSpaceSimpleStorage<dist_t> {
    public:
        explicit SpaceMahalanobis() {}
        virtual ~SpaceMahalanobis() {}
        bool InvertMatrix (const boost::numeric::ublas::matrix<dist_t>& input, boost::numeric::ublas::matrix<dist_t>& inverse) const {

            typedef boost::numeric::ublas::permutation_matrix<std::size_t> pmatrix;
            boost::numeric::ublas::matrix<dist_t> A(input);
            pmatrix pm(A.size1());

            int res = lu_factorize(A,pm);
            if( res != 0 )
                return false;

            inverse.assign(boost::numeric::ublas::identity_matrix<dist_t>(A.size1()));
            lu_substitute(A, pm, inverse);

            return true;
        }

        virtual size_t GetElemQty(const Object* obj1) const{
            return obj1->datalength()/ sizeof(dist_t);
        }

        //virtual vector<dist_t> getDistVector(const Object* obj1, size_t dim) const;
        virtual std::string StrDesc() const;

        void init(ObjectVector &FVs) const {



            dist_t covariance_tmp, zscore_derivation;

            size_t dim = GetElemQty(FVs[0]);
            size_t num_FV = FVs.size();
            boost::numeric::ublas::matrix<dist_t> matrix(num_FV, dim);
            boost::numeric::ublas::matrix<dist_t> covariance_matrix(dim,dim);

            for(int i = 0; i < num_FV; i++) {
                const dist_t* x = reinterpret_cast<const dist_t*>(FVs[i]->data());
                for (int j = 0; j < dim; j++) {
                    matrix(i, j) = x[j];
                }
            }


            if (num_FV == 0) {
                return;
            }

            /* =====================================================================================
             * Z-score normalization
             ===================================================================================== */
            zscore norm;
            for (size_t i = 0; i < dim; i++) { // loop trought dimensions
                norm = zscore();
                for (size_t j = 0; j < num_FV; j++) { // loop trought FVs
                    norm(matrix(j,i));
                }
                /* variance with N-1 (sample variance) */
                zscore_derivation = sqrt(boost::accumulators::variance(norm) * (dist_t)dim/((dist_t)dim-1));
                for (size_t j = 0; j < num_FV; j++) {
                    matrix(j,i) = (matrix(j,i) - boost::accumulators::mean(norm)) / zscore_derivation;
                    //LOG(LIB_INFO) << "matrix:::" << matrix(j,i);
                }
            }

            /* =====================================================================================
             * Calculate covariance matrix
            ===================================================================================== */
            for (size_t l = 0; l < dim; l++) { //lines
                for (size_t c = 0; c < dim && c <= l; c++) { //columns
                    covariance_XY cov;
                    for (size_t j = 0; j < num_FV; j++)
                        cov(matrix(j, l), boost::accumulators::covariate1 = matrix(j, c)); // loop through feature vectors
                    covariance_tmp = boost::accumulators::covariance(cov) * ( ((dist_t)num_FV + 1) / ((dist_t)num_FV) ); // sample variance (matrix + F1 => num_FV + 1)
                    covariance_matrix(l,c) = covariance_tmp;  // symmetric covariance matrix
                    covariance_matrix(c,l) = covariance_tmp;
                    // LOG(LIB_INFO) << "covariance" << covariance_tmp;
                }
            }


            /* =====================================================================================
             * Invert covariance matrix - LU decomposition algorithm
            ===================================================================================== */
            inverted_matrix2 = new boost::numeric::ublas::matrix<dist_t>(dim, dim);
            bool inv = InvertMatrix(covariance_matrix, (*inverted_matrix2));
            if(inv == false) {
                throw  std::length_error("Cannot invert the matrix");
            }

            mean_vector.resize(dim, 0.0);
            for (size_t i = 0; i < dim; i++) {
                localMean M;
                for (size_t j = 0; j < num_FV; j++) {
                    M(matrix(j, i));
                }
                mean_vector[i] = boost::accumulators::mean(M);
            }
        }

        virtual unique_ptr<DataFileInputState> ReadDataset(ObjectVector& dataset,
                                                                          std::vector<string> &vExternIds,
                                                                          const string &inputFile,
                                                                          const similarity::IdTypeUnsign MaxNumObjects = MAX_DATASET_QTY) const {
            CHECK_MSG(MaxNumObjects >=0, "Bug: MaxNumObjects should be >= 0");
            unique_ptr<DataFileInputState> inpState(VectorSpace<dist_t>::OpenReadFileHeader(inputFile));
            string line;
            LabelType label;
            string externId;
            for (size_t id = 0; id < MaxNumObjects || !MaxNumObjects; ++id) {
                if (!VectorSpace<dist_t>::ReadNextObjStr(*inpState, line, label, externId)) break;
                dataset.push_back(VectorSpace<dist_t>::CreateObjFromStr(id, label, line, inpState.get()).release());
                vExternIds.push_back(externId);
            }
            inpState->Close();
            init(dataset);
            return inpState;
        }


    protected:
        virtual dist_t HiddenDistance(const Object* obj1, const Object* obj2) const;

    private:
        mutable boost::numeric::ublas::matrix<dist_t> *inverted_matrix2;
        mutable std::vector<dist_t> mean_vector;
        DISABLE_COPY_AND_ASSIGN(SpaceMahalanobis);
    };


}  // namespace similarity

#endif