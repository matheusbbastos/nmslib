/**
 * Non-metric Space Library
 *
 * Authors: Bilegsaikhan Naidan (https://github.com/bileg), Leonid Boytsov (http://boytsov.info).
 * With contributions from Lawrence Cayton (http://lcayton.com/) and others.
 *
 * For the complete list of contributors and further details see:
 * https://github.com/searchivarius/NonMetricSpaceLib
 *
 * Copyright (c) 2014
 *
 * This code is released under the
 * Apache License Version 2.0 http://www.apache.org/licenses/.
 *
 */
#ifndef _RNG_H_
#define _RNG_H_

#include <string>
#include <sstream>

#include "index.h"
#include "params.h"
#include <set>
#include <limits>
#include <iostream>
#include <map>
#include <unordered_set>
#include <thread>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <queue>
#include "space.h"

#define METH_RNG             "rng"

namespace similarity {

using std::string;
using std::vector;
using std::thread;
using std::mutex;
using std::unique_lock;
using std::condition_variable;
using std::ref;

/*
 * This is a dummy, i.e., zero-functionality search method,
 * which can be used as a template to create fully functional
 * search method.
 */
//template<typename dist_t>
class RNGNode{
  public:
    RNGNode(const Object *Obj, size_t id){
      data_ = Obj;
      id_ = id;
    }
    ~RNGNode(){};
    void removeAllNeighbors(){
        neighbors.clear();
    };
    void addNeighbor(RNGNode* element){
      auto it = lower_bound(neighbors.begin(), neighbors.end(), element);
      /*if(it == neighbors.end()||(*it) != element){
          neighbors.insert(it,element);
      }*/
      neighbors.insert(it, element);
    }
    bool hasNeighbor(size_t id){
        for(RNGNode* n : neighbors){
            if(n->getId() == id) return true;
        }
        return false;
    }
    const Object* getData(){
      return data_;
    }
    const size_t getId(){
      return id_;
    }
    const vector<RNGNode*> getNeighbors() const{
      return neighbors;
    }
  private:
    const Object* data_;
    size_t id_;
    vector<RNGNode*> neighbors;

};
//---------
//nos com distancia
template <typename dist_t>
class EvaluatedRNGNode{
public:
  EvaluatedRNGNode() {
      distance = 0;
      element = NULL;
  }
  EvaluatedRNGNode(RNGNode* node, dist_t di) {
    distance = di;
    element = node;
  }

  ~EvaluatedRNGNode(){}
  dist_t getDistance() const {return distance;}
  RNGNode* getRNGNode() const {return element;}
  bool operator< (const EvaluatedRNGNode<dist_t> &obj1) const {
    return (distance < obj1.getDistance());
  }

  bool operator> (const EvaluatedRNGNode<dist_t> &obj) const {
    //  cout << "greater" <<endl;
    return (distance > obj.getDistance());
  }

private:
  dist_t distance;
  RNGNode* element;
};


//-------------------------------
template <typename dist_t>
class RNG : public Index<dist_t> {
 public:
  /*
   * The constructor here space and data-objects' references,
   * which are guaranteed to be be valid during testing.
   * So, we can memorize them safely.
   */
   RNG(bool PrintProgress,
                  const Space<dist_t>& space,
                  const ObjectVector& data);
  /*
   * This function is supposed to create a search index (or call a
   * function to create it)!
   */
  void CreateIndex(const AnyParams& IndexParams) override;/* {
    AnyParamManager  pmgr(IndexParams);
    pmgr.GetParamOptional("doSeqSearch",
                          bDoSeqSearch_,
      // One should always specify the default value of an optional parameter!
                          false
                          );
    // Check if a user specified extra parameters, which can be also misspelled variants of existing ones
    pmgr.CheckUnused();
    // Always call ResetQueryTimeParams() to set query-time parameters to their default values
    this->ResetQueryTimeParams();
  }*/
  void IndexOld();
  void IndexMatrix();

  typedef std::vector<RNGNode*> ElementList;
  /*
   * One can implement functions for index serialization and reading.
   * However, this is not required.
   */
   //void add(const Object* data, RNGNode* newElement);
   bool shouldConnect(EvaluatedRNGNode<dist_t>* evNode, RNGNode* newNode);
   bool shouldConnectMatrix(EvaluatedRNGNode<dist_t>* evNode, RNGNode* newElement);
   void link(RNGNode* first, RNGNode* second);
   RNGNode* getInitialVertice() const;
   RNGNode* addCriticalSection(RNGNode* element);
   void add(RNGNode* newElement, size_t idAtual);
   void addIndexMatrix(RNGNode* newElement, size_t idAtual);
   void writeGephi(string file);
    void printGraph(string file);

  // SaveIndex is not necessarily implemented
  virtual void SaveIndex(const string& location) override ;
  // LoadIndex is not necessarily implemented
  virtual void LoadIndex(const string& location) override;/* {
    throw runtime_error("LoadIndex is not implemented for method: " + StrDesc());
  }*/

  ElementList getElList(){
      return ElList_;
  }
  void SetQueryTimeParams(const AnyParams& QueryTimeParams) override;

  ~RNG(){};

  /*
   * Just the name of the method, consider printing crucial parameter values
   */
  const std::string StrDesc() const override {
    stringstream str;
    str << "RNG";// << (bDoSeqSearch_ ? " does seq. search " : "Relative Neighborhood Graph");
    return str.str();
  }

  /*
   *  One needs to implement two search functions.
   */
  void addIndexThreaded(size_t idAtual);
  void nearestNeighbor(KNNQuery<dist_t>* query) const; //de acordo com o artigo do Ocsa
  void nearestNeighborGS(KNNQuery<dist_t>* query) const; //de acordo com o algoritmo guloso de Aoyama
  void GNNS(KNNQuery<dist_t>* query) const;
  void Search(RangeQuery<dist_t>* query, IdType) const override;
  void Search(KNNQuery<dist_t>* query, IdType) const override;
  void RecursiveRange(RNGNode* initialVertice, RangeQuery<dist_t>* query, dist_t mindist, set<RNGNode*> explored) const; //range recursivo do artigo do Ocsa
  void swSearch(KNNQuery<dist_t> *query) const;
  void swSearchV1Merge(KNNQuery<dist_t> *query) const;

  /*
   * In rare cases, mostly when we wrap up 3rd party methods,
   * we simply duplicate the data set. This function
   * let the experimentation code know this, so it could
   * adjust the memory consumption of the index.
   *
   * Note, however, that this method doesn't create any data duplicates.
   */
  virtual bool DuplicateData() const override { return false; }

 private:
  bool                    data_duplicate_;
  //const ObjectVector&     data_;
  //Space<dist_t>&          space_;
  bool                    bDoSeqSearch_;
  //mutable mutex   ElListGuard_;
  //ElementList     ElList_;
  size_t                NN_;
  bool                  gnns;
  bool                  gs;
  bool                  swsearch;
  size_t                efConstruction_;
  size_t                efSearch_;
  size_t                initIndexAttempts_;
  size_t                initSearchAttempts_;
  size_t                indexThreadQty_;
  const Space<dist_t>&  space_;
  ObjectVector          data_; // We copy all the data
  bool                  PrintProgress_;
  vector<vector<dist_t>>                DistanceMatrix; //alocar dinamicamente

  mutable mutex   ElListGuard_;
  ElementList     ElList_;

  enum AlgoType { old, matrix };

  AlgoType      indexAlgoType_;




  // disable copy and assign
  DISABLE_COPY_AND_ASSIGN(RNG);
};

}   // namespace similarity

#endif     // _L_GRAPH_H_
