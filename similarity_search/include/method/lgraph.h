/**
 * Non-metric Space Library
 *
 * Authors: Bilegsaikhan Naidan (https://github.com/bileg), Leonid Boytsov (http://boytsov.info).
 * With contributions from Lawrence Cayton (http://lcayton.com/) and others.
 *
 * For the complete list of contributors and further details see:
 * https://github.com/searchivarius/NonMetricSpaceLib
 *
 * Copyright (c) 2014
 *
 * This code is released under the
 * Apache License Version 2.0 http://www.apache.org/licenses/.
 *
 */
#ifndef _L_GRAPH_H_
#define _L_GRAPH_H_

#include <string>
#include <sstream>

#include "index.h"
#include "params.h"
#include <set>
#include <limits>
#include <iostream>
#include <map>
#include <unordered_set>
#include <thread>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <queue>

#define METH_L_GRAPH             "lgraph"

namespace similarity {

using std::string;
using std::vector;
using std::thread;
using std::mutex;
using std::unique_lock;
using std::condition_variable;
using std::ref;

/*
 * This is a dummy, i.e., zero-functionality search method,
 * which can be used as a template to create fully functional
 * search method.
 */
//template<typename dist_t>
class LGNode{
  public:
    LGNode(const Object *Obj, size_t id){
      data_ = Obj;
      id_ = id;
    }
    ~LGNode(){};
    void removeAllNeighbors(){
        neighbors.clear();
    };
    void addNeighbor(LGNode* element){
      auto it = lower_bound(neighbors.begin(), neighbors.end(), element);
      neighbors.insert(it, element);
    }
    const Object* getData(){
      return data_;
    }
    const size_t getId(){
      return id_;
    }
    const vector<LGNode*>& getNeighbors() const{
      return neighbors;
    }
  private:
    const Object* data_;
    size_t id_;
    vector<LGNode*> neighbors;

};
//---------
//nos com distancia
template <typename dist_t>
class EvaluatedLGNode{
public:
  EvaluatedLGNode() {
      distance = 0;
      element = NULL;
  }
  EvaluatedLGNode(LGNode* node, dist_t di) {
    distance = di;
    element = node;
  }
  ~EvaluatedLGNode(){}
  dist_t getDistance() const {return distance;}
  LGNode* getLGNode() const {return element;}
  bool operator< (const EvaluatedLGNode<dist_t> &obj1) const {
    return (distance < obj1.getDistance());
  }

private:
  dist_t distance;
  LGNode* element;
};


//-------------------------------
template <typename dist_t>
class LGraph : public Index<dist_t> {
 public:
  /*
   * The constructor here space and data-objects' references,
   * which are guaranteed to be be valid during testing.
   * So, we can memorize them safely.
   */
   LGraph(bool PrintProgress,
                  const Space<dist_t>& space,
                  const ObjectVector& data);
  /*
   * This function is supposed to create a search index (or call a
   * function to create it)!
   */
  void CreateIndex(const AnyParams& IndexParams) override;/* {
    AnyParamManager  pmgr(IndexParams);
    pmgr.GetParamOptional("doSeqSearch",
                          bDoSeqSearch_,
      // One should always specify the default value of an optional parameter!
                          false
                          );
    // Check if a user specified extra parameters, which can be also misspelled variants of existing ones
    pmgr.CheckUnused();
    // Always call ResetQueryTimeParams() to set query-time parameters to their default values
    this->ResetQueryTimeParams();
  }*/

  typedef std::vector<LGNode*> ElementList;
  /*
   * One can implement functions for index serialization and reading.
   * However, this is not required.
   */
   //void add(const Object* data, LGNode* newElement);
   bool shouldConnect(EvaluatedLGNode<dist_t>* evNode, LGNode* newNode);
   void link(LGNode* first, LGNode* second);
   LGNode* getInitialVertice() const;
   void addCriticalSection(LGNode* element);
   void add(LGNode* newElement, size_t idAtual);

  // SaveIndex is not necessarily implemented
  virtual void SaveIndex(const string& location) override {
    throw runtime_error("SaveIndex is not implemented for method: " + StrDesc());
  }
  // LoadIndex is not necessarily implemented
  virtual void LoadIndex(const string& location) override {
    throw runtime_error("LoadIndex is not implemented for method: " + StrDesc());
  }

  void SetQueryTimeParams(const AnyParams& QueryTimeParams) override;

  ~LGraph(){};

  /*
   * Just the name of the method, consider printing crucial parameter values
   */
  const std::string StrDesc() const override {
    stringstream str;
    str << "LGraph: " << (bDoSeqSearch_ ? " does seq. search " : " testing (really dummy)");
    return str.str();
  }

  /*
   *  One needs to implement two search functions.
   */
  void Search(RangeQuery<dist_t>* query, IdType) const override{
    throw runtime_error("RangeQuery is not implemented for method: " + StrDesc());
  };
  void Search(KNNQuery<dist_t>* query, IdType) const override;

  /*
   * In rare cases, mostly when we wrap up 3rd party methods,
   * we simply duplicate the data set. This function
   * let the experimentation code know this, so it could
   * adjust the memory consumption of the index.
   *
   * Note, however, that this method doesn't create any data duplicates.
   */
  virtual bool DuplicateData() const override { return false; }

 private:
  bool                    data_duplicate_;
  //const ObjectVector&     data_;
  //Space<dist_t>&          space_;
  bool                    bDoSeqSearch_;
  //mutable mutex   ElListGuard_;
  //ElementList     ElList_;
  size_t                NN_;
  size_t                efConstruction_;
  size_t                efSearch_;
  size_t                initIndexAttempts_;
  size_t                initSearchAttempts_;
  size_t                indexThreadQty_;
  const Space<dist_t>&  space_;
  ObjectVector          data_; // We copy all the data
  bool                  PrintProgress_;

  mutable mutex   ElListGuard_;
  ElementList     ElList_;




  // disable copy and assign
  DISABLE_COPY_AND_ASSIGN(LGraph);
};

}   // namespace similarity

#endif     // _L_GRAPH_H_
