/**
 *kNNG BRUTE FORCE EXATO
 */
#ifndef _kNNG_H_
#define _kNNG_H_

#include <string>
#include <sstream>

#include "index.h"
#include "params.h"
#include "space.h"
#include <set>
#include <limits>
#include <iostream>
#include <map>
#include <unordered_set>
#include <thread>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <queue>

#define METH_kNNG            "knng"

namespace similarity {

using std::string;
using std::vector;
using std::thread;
using std::mutex;
using std::unique_lock;
using std::condition_variable;
using std::ref;

/*
 * This is a dummy, i.e., zero-functionality search method,
 * which can be used as a template to create fully functional
 * search method.
 */
//template<typename dist_t>
class kNNGNode{
  public:
    kNNGNode(const Object *Obj, size_t id){
      data_ = Obj;
      id_ = id;
    }

    ~kNNGNode(){};
    void removeAllNeighbors(){
        neighbors.clear();
    };
    bool hasNeighbor(size_t id){
        for(kNNGNode* n : neighbors){
            if(n->getId() == id) return true;
        }
        return false;
    }
    void addNeighbor(kNNGNode* element){
      //auto it = lower_bound(neighbors.begin(), neighbors.end(), element);
      //neighbors.insert(it, element);
            for(kNNGNode* node: neighbors){
                if(element->getId() == node->getId()){
                    LOG(LIB_INFO) << "blah";
                    return;
                }
            }
       neighbors.push_back(element);
    }

    const Object* getData(){
      return data_;
    }
    const size_t getId(){
      return id_;
    }
    void setId(size_t id){
        id_ = id;
    }
    const vector<kNNGNode*> getNeighbors() const{
      return neighbors;
    }
  private:
    const Object* data_;
    size_t id_;
    vector<kNNGNode*> neighbors;

};

//---------
//nos com distancia
template <typename dist_t>
class EvaluatedkNNGNode{
public:
  EvaluatedkNNGNode() {
      distance = 0;
      element = NULL;
  }
  EvaluatedkNNGNode(kNNGNode* node, dist_t di) {
    distance = di;
    element = node;
  }

  ~EvaluatedkNNGNode(){}
  dist_t getDistance() const {return distance;}
  kNNGNode* getkNNGNode() const {return element;}

  bool operator> (const EvaluatedkNNGNode<dist_t> &obj) const {
    return (distance > obj.getDistance());
  }

  bool operator< (const EvaluatedkNNGNode<dist_t> &obj) const {
      return (distance < obj.getDistance());
  }

  void setkNNGNode(kNNGNode* s){
      element = s;
  }

  void setDistance(dist_t d){
      distance = d;
  }

private:
  dist_t distance;
  kNNGNode* element;
};


//-------------------------------
template <typename dist_t>
class kNNG : public Index<dist_t> {
 public:
  /*
   * The constructor here space and data-objects' references,
   * which are guaranteed to be be valid during testing.
   * So, we can memorize them safely.
   */
   kNNG(bool PrintProgress,
                  const Space<dist_t>& space,
                  const ObjectVector& data);
  /*
   * This function is supposed to create a search index (or call a
   * function to create it)!
   */
  void CreateIndex(const AnyParams& IndexParams) override;/* {
    AnyParamManager  pmgr(IndexParams);
    pmgr.GetParamOptional("doSeqSearch",
                          bDoSeqSearch_,
      // One should always specify the default value of an optional parameter!
                          false
                          );
    // Check if a user specified extra parameters, which can be also misspelled variants of existing ones
    pmgr.CheckUnused();
    // Always call ResetQueryTimeParams() to set query-time parameters to their default values
    this->ResetQueryTimeParams();
  }*/
  void IndexOld();
  void IndexMatrix();

  typedef std::vector<kNNGNode*> ElementList;
  /*
   * One can implement functions for index serialization and reading.
   * However, this is not required.
   */
   //void add(const Object* data, kNNGNode* newElement);
   void link(kNNGNode* first, kNNGNode* second, bool dir);
   kNNGNode* getInitialVertice() const;
   kNNGNode* addCriticalSection(kNNGNode* element);
   void add(kNNGNode* newElement, size_t idAtual);
    void ThreadedAdd(kNNGNode* newElement);
    void ThreadedConnect(size_t id);
   void writeGephi(string file);
    void addEdges();
   // void addEdgesMatrix();

    void printGraph(string file);


  // SaveIndex is not necessarily implemented
  virtual void SaveIndex(const string& location) override ;
  // LoadIndex is not necessarily implemented
  virtual void LoadIndex(const string& location) override ;

  void SetQueryTimeParams(const AnyParams& QueryTimeParams) override;

  ~kNNG(){};

  /*
   * Just the name of the method, consider printing crucial parameter values
   */
  const std::string StrDesc() const override {
    stringstream str;
    str << "Brute-kNNG";// << (bDoSeqSearch_ ? " does seq. search " : "k - Nearest Neighbor Graph (brute force construction algorithm");
    return str.str();
  }

 vector<kNNGNode*> getElList(){
      return ElList_;
  }

  /*
   *  One needs to implement two search functions.
   */
  void nearestNeighborGS(KNNQuery<dist_t>* query) const; //de acordo com o algoritmo guloso de Aoyama
  void Search(RangeQuery<dist_t>* query, IdType) const override{
    throw runtime_error("Range Query is not implemented for method: " + StrDesc());
  }
  void GNNS(KNNQuery<dist_t>* query) const;
  void swSearchV1Merge(KNNQuery<dist_t>* query) const;
  void swSearch(KNNQuery<dist_t>* query) const;
  void Search(KNNQuery<dist_t>* query, IdType) const override;
  kNNGNode* getRandomEntryPoint() const;
  //void RecursiveRange(kNNGNode* initialVertice, RangeQuery<dist_t>* query, dist_t mindist, set<kNNGNode*> explored) const; //range recursivo do artigo do Ocsa

  /*
   * In rare cases, mostly when we wrap up 3rd party methods,
   * we simply duplicate the data set. This function
   * let the experimentation code know this, so it could
   * adjust the memory consumption of the index.
   *
   * Note, however, that this method doesn't create any data duplicates.
   */
  virtual bool DuplicateData() const override { return false; }

 private:
  bool                    data_duplicate_;
  //const ObjectVector&     data_;
  //Space<dist_t>&          space_;
  bool                    bDoSeqSearch_;
    bool                  gnns;
    bool                  swsearch;
    bool                  dir;
    //bool nndes;
  //mutable mutex   ElListGuard_;
  //ElementList     ElList_;
    //float deltaNNDES_;
    //float rho_;
    size_t efSearch_;
  size_t iterationQty_;
  size_t                initIndexAttempts_;
  size_t                initSearchAttempts_;
  size_t                indexThreadQty_;
  size_t                k_;
  const Space<dist_t>&  space_;
  ObjectVector          data_; // We copy all the data
  bool                  PrintProgress_;
  vector<vector<dist_t>>                DistanceMatrix; //alocar dinamicamente

  mutable mutex   ElListGuard_;
  ElementList     ElList_;
    enum AlgoType { old, matrix };

    AlgoType      indexAlgoType_;




  // disable copy and assign
  DISABLE_COPY_AND_ASSIGN(kNNG);
};

}   // namespace similarity

#endif     // _L_GRAPH_H_
