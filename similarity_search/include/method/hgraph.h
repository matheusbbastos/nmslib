/**
 * Non-metric Space Library
 *
 * Authors: Bilegsaikhan Naidan (https://github.com/bileg), Leonid Boytsov (http://boytsov.info).
 * With contributions from Lawrence Cayton (http://lcayton.com/) and others.
 *
 * For the complete list of contributors and further details see:
 * https://github.com/searchivarius/NonMetricSpaceLib
 *
 * Copyright (c) 2014
 *
 * This code is released under the
 * Apache License Version 2.0 http://www.apache.org/licenses/.
 *
 */
#ifndef _HGRAPH_H_
#define _HGRAPH_H_

#include <string>
#include <sstream>

#include "index.h"
#include "params.h"
#include "space.h"
#include <set>
#include <limits>
#include <iostream>
#include <map>
#include <unordered_set>
#include <thread>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <queue>
#include "logging.h"
#include "utils.h"

#define METH_HGRAPH           "hgraph"

namespace similarity {

    using std::string;
    using std::vector;
    using std::thread;
    using std::mutex;
    using std::unique_lock;
    using std::condition_variable;
    using std::ref;

    template <typename dist_t> class HNode_eval;

    template< typename dist_t>
    class HNode{
    public:
        HNode(const Object *Obj, size_t id){
            data_ = Obj;
            id_ = id;
        }

        void addNeighbor(HNode<dist_t>* element, bool cD, dist_t distance){
            if(cD){
                for(HNode_eval<dist_t>* node : neighbors){
                    if(element->getId() == node->getHNode()->getId()){
                        return;
                    }
                }
            }
            //auto it = lower_bound(neighbors.begin(), neighbors.end(), element);
            /*if(it == neighbors.end()||(*it) != element){
                neighbors.insert(it,element);
            }*/
            neighbors.push_back(new HNode_eval<dist_t>(element, distance));
            //neighbors.insert(it, element);
        }

        void addNeighborLower(HNode<dist_t>* element, bool cD, dist_t distance, int NN){
            if(cD) {
                for (HNode_eval<dist_t> *node : neighbors) {
                    if (element->getId() == node->getHNode()->getId()) {
                        return;
                    }
                }
            }
            auto low = std::lower_bound(neighbors.begin(), neighbors.end(), distance,
                                        [](HNode_eval<dist_t>* c, dist_t distance) {
                                            return c->getDistance() < distance;
                                        });
            if(neighbors.size() < NN){
                neighbors.insert(low, new HNode_eval<dist_t>(element, distance));
            }else if(low != neighbors.end() && neighbors.size() >= NN){
                neighbors.insert(low, new HNode_eval<dist_t>(element, distance));
                //neighbors.erase(neighbors.end() -1);
                neighbors.erase(neighbors.begin()+NN, neighbors.end());
            }else return;
        }

        void eraseNeighbors(int NN_){
            neighbors.erase(neighbors.begin()+NN_, neighbors.end());
            return;
        }

        const Object* getData(){
            return data_;
        }

        int getId(){
            return (int) id_;
        }
        vector<HNode_eval<dist_t>*> getNeighbors(){
            return neighbors;
        }
        void setNeighbors(vector<HNode_eval<dist_t>*> n){
            neighbors = n;
        }
        bool isNeighbor(HNode<dist_t>* element){
            for(HNode_eval<dist_t>* node: neighbors) {
                if (element->getId() == node->getHNode()->getId()) {
                    return true;
                }
            }
            return false;
        }

    private:
        const Object* data_;
        size_t id_;
        vector<HNode_eval<dist_t>*> neighbors;
    };


    template <typename dist_t>
    class HNode_eval{
    public:
        HNode_eval(HNode<dist_t>* hnode, dist_t distance){
            node_ = hnode;
            distance_ = distance;
        }

        HNode<dist_t>* getHNode(){
            return node_;
        }

        dist_t getDistance(){
            return distance_;
        }

        void setHNode(HNode<dist_t>* hnode){
            node_ = hnode;
        }

        void setDist(dist_t dist){
            distance_ = dist;
        }

    private:
        HNode<dist_t>* node_;
        dist_t distance_;
    };

    template <typename dist_t>
    class HNodeEvalCompare{
    public:
        bool operator() (HNode_eval<dist_t>* i, HNode_eval<dist_t>* j) {
            return (i->getDistance() < j->getDistance());
        }
    };

    template<typename dist_t>
    class EvaluatedPair{
    public:
        bool operator()(pair<int, dist_t> n1, pair<int, dist_t> n2){
            return n1.second < n2.second; //maior no topo
        }
    };

    template<typename dist_t>
    class EvaluatedPairMin{
    public:
        bool operator()(pair<int, dist_t> n1, pair<int, dist_t> n2){
            return n1.second > n2.second; //menor no topo
        }
    };


    template<typename dist_t>
    class PivotTable{
    public:
        vector<vector<int>> pivotsTable_;
        vector<vector<int>> overlapTable_;
        PivotTable(vector<vector<int>> pivotsTable, vector<vector<int>> overlapTable){
            pivotsTable_ = pivotsTable;
            overlapTable_ = overlapTable;
        }
    };


    template <typename dist_t>
    class HGraph : public Index<dist_t> {
    public:
        typedef std::vector<HNode<dist_t>*> ElementList;

        /*
         * The constructor here space and data-objects' references,
         * which are guaranteed to be be valid during testing.
         * So, we can memorize them safely.
         */
        HGraph(bool PrintProgress, const Space<dist_t> &space, const ObjectVector &data) :
                PrintProgress_(PrintProgress),space_(space), data_(data)  {}


        /*
         * This function is supposed to create a search index (or call a
         * function to create it)!
         */
        void addCriticalSection(HNode<dist_t> *node);
        void CreateIndex(const AnyParams& IndexParams) override;
        vector<int> addNodes(); //adds nodes returns a vector of ids
        //void createIndexRecursive(int lcount, ObjectVector X);
        void createIndexRecursiveBU(int lcount, vector<int> X);
        void createIndexTD(int lcount, vector<int> X, int nP);
        void createIndexTDParallel(int lcount, vector<int> X, int nP);
        void refineGraph(vector<int> levelSeeds, int lcount, string gType);
        void refineGraphSet(set<int> levelSeeds, int lcount, string gtype);
        void pruneEdges();
        void insertSeeds(vector<int> seeds, int lcount);
        void SeedsRNG(vector<int> seeds, bool isPivots);
        //ObjectVector divide(ObjectVector X,int seedId, int size);
        vector<int> divideIndices(vector<int> X,int seedId, int size);
        // vector<int> divideIndicesSeeds(vector<int> X,int seedId, int size);
        void buildNotSelected(int size, vector<int> set);
        vector<int> divideIndicesNotSelected(int size);
        vector<int> divideIndicesNotSelected();
        vector<int> appendOverlap(vector<int> Xs, float overlapRate, vector<vector<int>> pivotsTable);
        vector<int> appendPivots(vector<int> Xs, vector<vector<int>> pivotsTable);
        //int chooseSeed(ObjectVector X);
        vector<int> chooseSeed(vector<int> X, bool useRandom, int S);
        vector<int> BPivotPosition(vector<int> X);
        vector<int> kmedoids(vector<int> X, int iterations);
        vector<vector<int>> partitionSpace(vector<int> seeds, vector<int> X);
        vector<vector<pair<int,dist_t>>> partitionSpaceMedoids(vector<int> seeds, vector<int> X);
        vector<vector<int>> partitionSpaceOver(vector<int> seeds, vector<int> X);
        vector<vector<int>> partitionSpaceOverDist(vector<int> seeds, vector<int> X, float over);
        vector<int> sample(vector<int> Xs, int number);
        //Index<dist_t>* createGraph(ObjectVector Xs);
        void createGraph(vector<int> Xs, int lcount);
        vector<int> getPivots(vector<vector<int>> pivotsTable);
        //void graphUnion(vector<Index<dist_t>*> levelGraphs, int level);
        void knngConstruction(vector<int> Xs, size_t k, bool dir, bool isPivot);
        void rngConstruction(vector<int> Xs, bool isPivot);
        void krngConstruction(vector<int> Xs, size_t k, bool isPivot);
        void completeGraph(vector<int> Xs);
        void link(HNode<dist_t>* node1, HNode<dist_t>* node2, bool dir, dist_t distance, bool isPivot);
        bool shouldConnect(pair<int, dist_t> evNode, int idAtual, vector<int> Xs);
        bool shouldConnectK(pair<int, dist_t> evNode, int idAtual, vector<int> Xs, int K);
        void printSeeds(vector<int> seeds);

        void printGraph(string file);
        void writeGephi(string file);
        /*
         * One can implement functions for index serialization and reading.
         * However, this is not required.
         */

        virtual void SaveIndex(const string& location) override;

        virtual void LoadIndex(const string& location) override;

        void SetQueryTimeParams(const AnyParams& QueryTimeParams) override;

        ~HGraph(){};

        /*
         * Just the name of the method, consider printing crucial parameter values
         */
        const std::string StrDesc() const override {
            stringstream str;
            str << "HGraph";
            return str.str();
        }

        /*
         *  One needs to implement two search functions.
         */
        void Search(RangeQuery<dist_t>* query, IdType) const override;
        void Search(KNNQuery<dist_t>* query, IdType) const override;

        /*
         * In rare cases, mostly when we wrap up 3rd party methods,
         * we simply duplicate the data set. This function
         * let the experimentation code know this, so it could
         * adjust the memory consumption of the index.
         *
         * Note, however, that this method doesn't create any data duplicates.
         */
        virtual bool DuplicateData() const override { return false; }

        bool approximatelyEqual(dist_t a, dist_t b, float epsilon)
        {
            return fabs(a - b) <= ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
        }

        bool essentiallyEqual(dist_t a, dist_t b, float epsilon)
        {
            return fabs(a - b) <= ( (fabs(a) > fabs(b) ? fabs(b) : fabs(a)) * epsilon);
        }

        bool definitelyGreaterThan(dist_t a, dist_t b, float epsilon)
        {
            return (a - b) > ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
        }

        bool definitelyLessThan(dist_t a, dist_t b, float epsilon)
        {
            return (b - a) > ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
        }

    private:
        bool                    PrintProgress_;
        bool                    data_duplicate_;
        const Space<dist_t>&  space_;
        ObjectVector          data_; // We copy all the data
        bool                    bDoSeqSearch_;
        size_t                  L_; //number of levels to partition
        size_t                  S_; //number of seeds
        size_t                  NN_; //nn para quando escolher knng
        string                  addParams_;
        string     indexGraphType_;
        size_t                indexThreadQty_;
        string gtype_;
        ElementList ElList_;
        int                    countRefine;
        size_t                 initSearchAttempts_;
        set<int>             notSelected;
        set<int>            selected;
        float                overRate_;
        float                eps_;
        //vector<vector<int>>  levelSeeds_;
        set<int> levelSeeds_;


        // disable copy and assign
        DISABLE_COPY_AND_ASSIGN(HGraph);
    };

}   // namespace similarity

#endif     // _HGRAPH_METHOD
