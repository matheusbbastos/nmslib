//
// Created by larissa on 8/23/17.
// weighted rng to compare chavez search methods
//

#ifndef RNG_WEIGHTED_H
#define RNG_WEIGHTED_H

//construir rng com pesos baseado no rng (proximity graph)
//implementar busca chavez para ele

#include <string>
#include <sstream>

#include "index.h"
#include "params.h"
#include <set>
#include <limits>
#include <iostream>
#include <map>
#include <unordered_set>
#include <thread>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <queue>
#include "space.h"
#include "heapMember.h"
#include "min_heap.h"
#include "max_heap.h"



#define METH_RNG_w  "rngw"

namespace similarity{

    using namespace std;

    template <typename dist_t> class EvaluatedRNGWNode;

    template<typename dist_t>
    class RNGWNode{
    public:
        RNGWNode(const Object *Obj, size_t id){
            data_ = Obj;
            id_ = id;
        }
        ~RNGWNode(){};
        void removeAllNeighbors(){
            neighbors.clear();
        };
        void addNeighbor(EvaluatedRNGWNode<dist_t>* element){
            auto it = lower_bound(neighbors.begin(), neighbors.end(), element);
            neighbors.insert(it, element);
        }
        const Object* getData(){
            return data_;
        }
        const size_t getId(){
            return id_;
        }
        bool hasNeighbor(size_t id){
            for(EvaluatedRNGWNode<dist_t>* n : neighbors){
                if(n->getRNGWNode()->getId() == id) return true;
            }
            return false;
        }
        const vector<EvaluatedRNGWNode<dist_t>*> getNeighbors() const{
            return neighbors;
        }
        const vector<RNGWNode<dist_t>*> getNeighborsNode() const{
            vector<RNGWNode<dist_t>*> nodes;
            for(EvaluatedRNGWNode<dist_t>* evalNode : neighbors){
                nodes.push_back(evalNode->getRNGWNode());
            }
            return nodes;
        }
        bool operator== (const RNGWNode<dist_t> &obj1){
            return (data_->id() == obj1.data_->id());
        }
    private:
        const Object* data_;
        size_t id_;
        vector<EvaluatedRNGWNode<dist_t>*> neighbors;

    };

//---------

    template <typename dist_t>
    class EvaluatedRNGWNode{
    public:
        EvaluatedRNGWNode() {
            distance = 0;
            element = NULL;
        }
        EvaluatedRNGWNode(RNGWNode<dist_t>* node, dist_t di) {
            distance = di;
            element = node;
        }

        void setDistance(dist_t di){
            distance = di;
        }

        void setNode(RNGWNode<dist_t>* node){
            element = node;
        }

        ~EvaluatedRNGWNode(){}
        dist_t getDistance() const {return distance;}
        RNGWNode<dist_t>* getRNGWNode() const {return element;}
        bool operator< (const EvaluatedRNGWNode<dist_t> &obj) const {
            return (distance < obj.getDistance());
        }

        bool operator> (const EvaluatedRNGWNode<dist_t> &obj) const {
            return (distance > obj.getDistance());
        }

    private:
        dist_t distance;
        RNGWNode<dist_t>* element;
    };

    /*class CompareSet{
    public:
        bool operator()(const pair<size_t, int> a, pair<size_t, int> b){
            return a.first < b.first;
        }
    };*/


    template <typename dist_t>
    class RNGW : public Index<dist_t>{
    public:
        RNGW(bool PrintProgress,
              const Space<dist_t>& space,
              const ObjectVector& data);
        /*
         * This function is supposed to create a search index (or call a
         * function to create it)!
         */
        void CreateIndex(const AnyParams& IndexParams) override;/* {
    AnyParamManager  pmgr(IndexParams);
    pmgr.GetParamOptional("doSeqSearch",
                          bDoSeqSearch_,
      // One should always specify the default value of an optional parameter!
                          false
                          );
    // Check if a user specified extra parameters, which can be also misspelled variants of existing ones
    pmgr.CheckUnused();
    // Always call ResetQueryTimeParams() to set query-time parameters to their default values
    this->ResetQueryTimeParams();
  }*/
        //void IndexOld();
        //void IndexMatrix();

        typedef std::vector<RNGWNode<dist_t>*> ElementList;
        typedef std::priority_queue<EvaluatedRNGWNode<dist_t>, vector<EvaluatedRNGWNode<dist_t>>, less<EvaluatedRNGWNode<dist_t>>> answerQueue;

        /*
         * One can implement functions for index serialization and reading.
         * However, this is not required.
         */
        //void add(const Object* data, RNGWNode* newElement);
        void link(RNGWNode<dist_t>* first, RNGWNode<dist_t>* second, dist_t distance);
        bool shouldConnectMatrix(EvaluatedRNGWNode<dist_t>* evNode, RNGWNode<dist_t>* newEl);
        RNGWNode<dist_t>* getInitialVertice() const;
        //RNGWNode<dist_t>* getExtractedNode(vector<RNGWNode<dist_t>*> C) const;
        //de acordo com Dijkstra
        RNGWNode<dist_t>* addCriticalSection(RNGWNode<dist_t>* element);
        dist_t CoveringRadius(RNGWNode<dist_t>* u) const; //returns covering radius of a node
        //void add(RNGWNode<dist_t>* newElement, size_t idAtual);
        void writeGephi(string file);
        void printGraph(string file);

        // SaveIndex is not necessarily implemented
        virtual void SaveIndex(const string& location) override ;
        // LoadIndex is not necessarily implemented
        virtual void LoadIndex(const string& location) override;

        void SetQueryTimeParams(const AnyParams& QueryTimeParams) override;

        ~RNGW(){};

        /*
         * Just the name of the method, consider printing crucial parameter values
         */
        const std::string StrDesc() const override {
            stringstream str;
            str << "RNGW";// << (bDoSeqSearch_ ? " does seq. search " : "k - Nearest Neighbor Graph (brute force construction algorithm");
            return str.str();
        }

        vector<RNGWNode<dist_t>*> getElList(){
            return ElList_;
        }

        /*
         *  One needs to implement two search functions.
         */
        RNGWNode<dist_t>* nextToReview() const;
        void addIndexMatrix(RNGWNode<dist_t>* newElement, size_t idAtual);
        /*void checkNeighborhood(RangeQuery<dist_t>* query, EvaluatedRNGWNode<dist_t>* p, dist_t dpq) const;
        vector<EvaluatedRNGWNode<dist_t>*> getNeighborsCand(RNGWNode* u) const;
        void useContainerRQ(EvaluatedRNGWNode<dist_t> u,dist_t duq, dist_t radius) const;*/
        pair<size_t, dist_t> traverse(KNNQuery<dist_t>* query, RNGWNode<dist_t> *p, dist_t dpq) const;
        void useContainerNNQ(EvaluatedRNGWNode<dist_t> *u, dist_t duq, dist_t radius) const;
        void extractNode(RNGWNode<dist_t>* p) const ;
        vector<dist_t> Dijkstra(EvaluatedRNGWNode<dist_t>* p, dist_t rexp) const;
        void nearestNeighborGS(KNNQuery<dist_t>* query) const; //de acordo com o algoritmo guloso de Aoyama
        void Search(RangeQuery<dist_t>* query, IdType) const override{
            throw runtime_error("Range Query is not implemented for method: " + StrDesc());
        }
        void Search(KNNQuery<dist_t>* query, IdType) const override;/*{
            throw runtime_error("kNN Query is not implemented for method: " + StrDesc());
        }*/
        const RNGWNode<dist_t>* getElItem(int i) const{
            return ElList_[i];
        }
        //void RecursiveRange(RNGWNode* initialVertice, RangeQuery<dist_t>* query, dist_t mindist, set<RNGWNode*> explored) const; //range recursivo do artigo do Ocsa
        void IndexMatrix();
        void IndexOld() const {
            throw runtime_error("index old not implemented");
        };
        /*
         * In rare cases, mostly when we wrap up 3rd party methods,
         * we simply duplicate the data set. This function
         * let the experimentation code know this, so it could
         * adjust the memory consumption of the index.
         *
         * Note, however, that this method doesn't create any data duplicates.
         */
        virtual bool DuplicateData() const override { return false; }

    private:
        bool                    data_duplicate_;
        //const ObjectVector&     data_;
        //Space<dist_t>&          space_;
        bool                    bDoSeqSearch_;
        //mutable mutex   ElListGuard_;
        //ElementList     ElList_;
        size_t                initIndexAttempts_;
        size_t                initSearchAttempts_;
        size_t                indexThreadQty_;
        size_t                it_;
        const Space<dist_t>&  space_;
        ObjectVector          data_; // We copy all the data
        bool                  PrintProgress_;
        vector<vector<dist_t>>                DistanceMatrix; //alocar dinamicamente

        mutable mutex   ElListGuard_;
        ElementList     ElList_;
        mutable MaxHeap<RNGWNode<dist_t>*, dist_t> C;//max heap para C
        mutable MinHeap<RNGWNode<dist_t>*, dist_t> radii;// min heap para radii
        //mutable MaxHeap<size_t , dist_t> C;
        //mutable MinHeap<size_t , dist_t> radii;
        mutable EvaluatedRNGWNode<dist_t> container;
        mutable answerQueue nnc; //ordered by decreasing distance
        //priority_queue<EvaluatedRNGWNode<dist_t>> nnc;
        //map id do node na ElList e posicao do heap
        enum AlgoType { old, matrix };

        AlgoType      indexAlgoType_;
        // disable copy and assign
        DISABLE_COPY_AND_ASSIGN(RNGW);
    };





}//similarity



#endif //NONMETRICSPACELIB_RNG_WEIGHTED_H
