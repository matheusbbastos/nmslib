/**
 *kDR BRUTE FORCE EXATO
 */
#ifndef _kDR_H_
#define _kDR_H_

#include <string>
#include <sstream>


#include "index.h"
#include "method/nndes.h"
#include "params.h"
#include <set>
#include <limits>
#include <iostream>
#include <map>
#include <unordered_set>
#include <thread>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <queue>
#include "method/knng.h"

#define METH_kDR           "kdr"

namespace similarity {

using std::string;
using std::vector;
using std::thread;
using std::mutex;
using std::unique_lock;
using std::condition_variable;
using std::ref;

//template<typename dist_t>
class kDRNode{
  public:
    kDRNode(const Object *Obj, size_t id){
      data_ = Obj;
      id_ = id;
    }
    ~kDRNode(){};
    void removeAllNeighbors(){
        neighbors.clear();
    };
    void addNeighbor(kDRNode* element){
      if(!contains(element)){
          auto it = lower_bound(neighbors.begin(), neighbors.end(), element);
          neighbors.insert(it, element);
      }else return;
    }
    bool equals(kDRNode* element){
        if(element->getId() == id_){
            return true;
        }else return false;
    }
    bool contains(kDRNode* element){
        for(kDRNode* v : neighbors){
            if(v->getId() == element->getId()){
                return true;
            }
        }
        return false;
    }
    const Object* getData(){
      return data_;
    }
    const size_t getId(){
      return id_;
    }
    const vector<kDRNode*> getNeighbors() const{
      return neighbors;
    }
  private:
    const Object* data_;
    size_t id_;
    vector<kDRNode*> neighbors;

};
//---------
//nos com distancia
template <typename dist_t>
class EvaluatedkDRNode{
public:
  EvaluatedkDRNode() {
      distance = 0;
      element = NULL;
  }
  EvaluatedkDRNode(kDRNode* node, dist_t di) {
    distance = di;
    element = node;
  }

  ~EvaluatedkDRNode(){}
  dist_t getDistance() const {return distance;}
  kDRNode* getkDRNode() const {return element;}
  bool operator< (const EvaluatedkDRNode<dist_t> &obj1) const {
    return (distance < obj1.getDistance());
  }

  bool operator> (const EvaluatedkDRNode<dist_t> &obj) const {
    return (distance > obj.getDistance());
  }

private:
  dist_t distance;
  kDRNode* element;
};


//-------------------------------
template <typename dist_t>
class kDR : public Index<dist_t> {
 public:
  /*
   * The constructor here space and data-objects' references,
   * which are guaranteed to be be valid during testing.
   * So, we can memorize them safely.
   */
   kDR(bool PrintProgress,
                  const Space<dist_t>& space,
                  const ObjectVector& data);
  /*
   * This function is supposed to create a search index (or call a
   * function to create it)!
   */
  void CreateIndex(const AnyParams& IndexParams) override;/* {
    AnyParamManager  pmgr(IndexParams);
    pmgr.GetParamOptional("doSeqSearch",
                          bDoSeqSearch_,
      // One should always specify the default value of an optional parameter!
                          false
                          );
    // Check if a user specified extra parameters, which can be also misspelled variants of existing ones
    pmgr.CheckUnused();
    // Always call ResetQueryTimeParams() to set query-time parameters to their default values
    this->ResetQueryTimeParams();
  }*/
  //metodos para o calculo da probabilidade
  vector<kDRNode*> randomSampling(size_t size){__throw_runtime_error("comentario");}
  float_t successProbability(size_t L, ObjectVector Q);
    float_t Basin(const Object* q, int pos);
  pair<size_t, dist_t> greedySearch(kDRNode* first, KNNQuery<dist_t>* query) const;
    kDRNode* greedySearchBasin(kDRNode* first, const Object* second);
    void SeqSearchTarget();
  bool greedyReachability(kDRNode* ini, kDRNode* fim);
  //kNNG<dist_t>* CreatekMAX(const Space<dist_t>& space_, ObjectVector data_, size_t kmax_);

  typedef std::vector<kDRNode*> ElementList;
   void link(kDRNode* first, kDRNode* second);
    kDRNode* GRC(kDRNode* x, kDRNode* y) const;
   vector<kDRNode*> getInitialVertice(size_t L) const;
   void addCriticalSection(kDRNode* element);
   void writeGephi(string file);
  // void PGS(size_t tid, kDRNode* x, KNNQuery<dist_t>* query) const;
   // NNDescentMethod<dist_t>* CreateNNDES();


  virtual void SaveIndex(const string& location) override;
  virtual void LoadIndex(const string& location) override {
    throw runtime_error("LoadIndex is not implemented for method: " + StrDesc());
  }

    void printGraph(string file);

  void SetQueryTimeParams(const AnyParams& QueryTimeParams) override;

  ~kDR(){};

  const std::string StrDesc() const override {
    stringstream str;
    str << "kDR: " << (nndes ? "NNDES" : "Brute");
    return str.str();
  }

    void printSuccess(string file_, size_t k,float_t pSuccess); //armazenamento do psuccess conforme o crescimento de k
    void CreateIndexK(vector<kNNGNode*> kmaxList); //construcao com parametro k fornecido
    void GNNS(KNNQuery<dist_t>* query) const;
    void nearestNeighborGS(KNNQuery<dist_t>* query) const; //algoritmo guloso de aoyama
    void swSearch(KNNQuery<dist_t>* query) const; //swSearch do small world

  void Search(RangeQuery<dist_t>* query, IdType) const override{
    throw runtime_error("Range Query is not implemented for method: " + StrDesc());
  }
  void Search(KNNQuery<dist_t>* query, IdType) const override;

  virtual bool DuplicateData() const override { return false; }

 private:
  bool                    data_duplicate_;
  //const ObjectVector&     data_;
  //Space<dist_t>&          space_;
  bool                    bDoSeqSearch_;
  bool                    nndes;
  bool                    gnns;
  bool                     sw;
    bool                   gsl;
    bool                    pgs;
  //mutable mutex   ElListGuard_;
  //ElementList     ElList_;
  size_t                initIndexAttempts_;
  size_t                initSearchAttempts_;
    size_t              efSearch_;
  size_t                indexThreadQty_;
  size_t              iterationQty_;
  size_t                kmax_;
  size_t                k_;
    int                 h_; //tamanho de S[j] numeros de vezes de greedy search
  float                 delta_;
  size_t                L_;
  size_t                Q_;
  float                 rho_;
  float                 deltaNNDES_;
  int                   BasinSize_;
  string                file_;
  const Space<dist_t>&  space_;
  ObjectVector          data_; // We copy all the data
  ObjectVector          Ql;
  vector<size_t>        Target;
  bool                  PrintProgress_;
  vector<vector<dist_t>>                DistanceMatrix; //alocar dinamicamente

  mutable mutex   ElListGuard_;
  ElementList     ElList_;




  // disable copy and assign
  DISABLE_COPY_AND_ASSIGN(kDR);
};

}   // namespace similarity

#endif     // kDR_H
