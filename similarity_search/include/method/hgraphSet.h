#ifndef _HGRAPHSET_H_
#define _HGRAPHSET_H_

#include <string>
#include <sstream>

#include "index.h"
#include "params.h"
#include "space.h"
#include <set>
#include <limits>
#include <iostream>
#include <map>
#include <unordered_set>
#include <thread>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <queue>
#include "logging.h"
#include "utils.h"

#define METH_HGRAPHSET   "hgraphset"

namespace similarity {

    using std::string;
    using std::vector;
    using std::thread;
    using std::mutex;
    using std::unique_lock;
    using std::condition_variable;
    using std::ref;

template <typename dist_t> class HNodeSet_eval;
template <typename dist_t> class HNodeSetEvalCompare;

template< typename dist_t>
class HNodeSet{
    public:
        HNodeSet(const Object *Obj, size_t id){
            data_ = Obj;
            id_ = id;
        }

        void addNeighbor(HNodeSet<dist_t>* element, bool cD, dist_t distance){
            if(cD){
                if(isNeighbor(element)) return;
            }
            neighbors.insert(new HNodeSet_eval<dist_t>(element, distance));

            //auto it = lower_bound(neighbors.begin(), neighbors.end(), element);
            /*if(it == neighbors.end()||(*it) != element){
                neighbors.insert(it,element);
            }*/
            //neighbors.insert(it, element);
        }

        void addNeighborCheckNN(HNodeSet<dist_t>* element, bool cD, dist_t distance, int NN){
            if(cD){
                if(isNeighbor(element)) return;
            }
            if(neighbors.size() >= NN){
                typename set<HNodeSet_eval<dist_t>*, HNodeSetEvalCompare<dist_t>>::iterator i = neighbors.end();
                --i;
                HNodeSet_eval<dist_t>* obj = *i;
                if(obj->getDistance() > distance){
                    neighbors.erase(i);
                    addNeighbor(element, false, distance);
                }else return;
            }else{
                addNeighbor(element, false, distance);
            }
        }

        const Object* getData(){
            return data_;
        }

        int getId(){
            return (int) id_;
        }

        typename set<HNodeSet_eval<dist_t>*,HNodeSetEvalCompare<dist_t>>::iterator eraseNeighbor(typename set<HNodeSet_eval<dist_t>*,HNodeSetEvalCompare<dist_t>>::iterator it){
            typename set<HNodeSet_eval<dist_t>*,HNodeSetEvalCompare<dist_t>>::iterator itR = neighbors.erase(it);
            return itR;
        }

        set<HNodeSet_eval<dist_t>*, HNodeSetEvalCompare<dist_t>> getNeighbors(){
            return neighbors;
        }

        bool isNeighbor(HNodeSet<dist_t>* element){
                for(HNodeSet_eval<dist_t>* node: neighbors) {
                    if (element->getId() == node->getHNodeSet()->getId()) {
                        return true;
                    }
                }
                return false;
        }

    private:
        const Object* data_;
        size_t id_;
        set<HNodeSet_eval<dist_t>*,HNodeSetEvalCompare<dist_t>> neighbors;
    };


template <typename dist_t>
class HNodeSet_eval{
public:
    HNodeSet_eval(HNodeSet<dist_t>* HNodeSet, dist_t distance){
        node_ = HNodeSet;
        distance_ = distance;
    }

    HNodeSet<dist_t>* getHNodeSet() const{
        return node_;
    }

    dist_t getDistance() const{
        return distance_;
    }

    void setHNodeSet(HNodeSet<dist_t>* HNodeSet){
        node_ = HNodeSet;
    }

    void setDist(dist_t dist){
        distance_ = dist;
    }

private:
    HNodeSet<dist_t>* node_;
    dist_t distance_;
};

template <typename dist_t>
class HNodeSetEvalCompare{
public:
    bool operator() (HNodeSet_eval<dist_t>* i, HNodeSet_eval<dist_t>* j) {
        return (i->getDistance() < j->getDistance());
    }
};

    template <typename dist_t>
    class HNodeSetEvalCompareReverse{
    public:
        bool operator() (HNodeSet_eval<dist_t>* i, HNodeSet_eval<dist_t>* j) {
            return (i->getDistance() > j->getDistance());
        }
    };


    template<typename dist_t>
    class EvaluatedPairSet{
    public:
        bool operator()(pair<int, dist_t> n1, pair<int, dist_t> n2){
            return n1.second < n2.second; //maior no topo
        }
    };

    template<typename dist_t>
    class EvaluatedPairMinSet{
    public:
        bool operator()(pair<int, dist_t> n1, pair<int, dist_t> n2){
            return n1.second > n2.second; //menor no topo
        }
    };


    template<typename dist_t>
    class PivotTableSet{
    public:
        vector<vector<int>> pivotsTable_;
        vector<vector<int>> overlapTable_;
    PivotTableSet
    (vector<vector<int>> pivotsTable, vector<vector<int>> overlapTable){
        pivotsTable_ = pivotsTable;
        overlapTable_ = overlapTable;
    }
    };


    template <typename dist_t>
    class HGraphSet : public Index<dist_t> {
    public:
        typedef std::vector<HNodeSet<dist_t>*> ElementList;

        /*
         * The constructor here space and data-objects' references,
         * which are guaranteed to be be valid during testing.
         * So, we can memorize them safely.
         */
        HGraphSet(bool PrintProgress, const Space<dist_t> &space, const ObjectVector &data) :
                PrintProgress_(PrintProgress),space_(space), data_(data)  {}


        /*
         * This function is supposed to create a search index (or call a
         * function to create it)!
         */
        void addCriticalSection(HNodeSet<dist_t> *node);
        void CreateIndex(const AnyParams& IndexParams) override;
        void addNodes(vector<int> &pIndices); //adds nodes returns a vector of ids
        //void createIndexRecursive(int lcount, ObjectVector X);
        void createIndexRecursiveBU(int lcount, vector<int> X);
        void createIndexTD(int lcount, vector<int> &X, int nP);
      //  void createIndexTDParallel(int lcount, vector<int> X, int nP);
        void refineGraph(vector<int> levelSeeds, int lcount, string gType);
        //void refineGraphSet(set<int> levelSeeds, int lcount, string gtype);
        void refineGraphSet();
        void knngConstructionSet(set<int> levelSeeds, int nn_);
        void pruneEdgesSet();
        void insertSeeds(vector<int> &seeds, int lcount);
        void SeedsRNG(vector<int> seeds, bool isPivots);
        void SeedsRNGSet(set<int> seeds, bool isPivots);
        bool shouldConnectSet(pair<int, dist_t> evNode, int idAtual, set<int> Xs);
        //ObjectVector divide(ObjectVector X,int seedId, int size);
        vector<int> divideIndices(vector<int> X,int seedId, int size);
       // vector<int> divideIndicesSeeds(vector<int> X,int seedId, int size);
        void buildNotSelected(int size, vector<int> set);
        vector<int> divideIndicesNotSelected(int size);
        vector<int> divideIndicesNotSelected();
        vector<int> appendOverlap(vector<int> Xs, float overlapRate, vector<vector<int>> pivotsTable);
        vector<int> appendPivots(vector<int> Xs, vector<vector<int>> pivotsTable);
        //int chooseSeed(ObjectVector X);
        vector<int> chooseSeed(vector<int> &X, string ptype, int S);
        //void chooseSeed(vector<int> &X, string ptype, int S, vector<int> &levelSeeds);
        vector<int> FFT(vector<int> X, int S);
        vector<int> PAM(vector<int> X, int S);
        vector<int> NRandom(vector<int> X, int S, int n);
        vector<int> BPivotPosition(vector<int> X);
        vector<int> kmedoids(vector<int> X, int iterations);
        vector<vector<int>> partitionSpace(vector<int> seeds, vector<int> X);
        vector<vector<pair<int,dist_t>>> partitionSpaceMedoids(vector<int> seeds, vector<int> X);
        vector<vector<int>> partitionSpaceOver(vector<int> seeds, vector<int> X);
        //vector<vector<int>> partitionSpaceOverDist(vector<int> seeds, vector<int> X, float over, vector<vector<int>> &pivotsTable);
        void partitionSpaceOverDist(vector<int> seeds, vector<int> X, float over, vector<vector<int>> &pivotsTable);
        void partitionExact(vector<int> seeds, vector<int> X, float over, vector<vector<int>> &pivotsTable);
        vector<int> sample(vector<int> Xs, int number);
        //Index<dist_t>* createGraph(ObjectVector Xs);
        void createGraph(vector<int> Xs, int lcount);
        vector<int> getPivots(vector<vector<int>> pivotsTable);
        //void graphUnion(vector<Index<dist_t>*> levelGraphs, int level);
        void knngConstruction(vector<int> &Xs, size_t k, bool dir, bool isPivot);
        void rngConstruction(vector<int> &Xs, bool isPivot);
        void swgConstruction(vector<int> &Xs, size_t k, bool isPivot);
        void swgIndexSearch(const Object *queryObj, vector<int> X, priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPairSet<dist_t> > &resultSet, set<int> &tempNodesSet, vector<int> &tempNodes) const;
        void krngConstruction(vector<int> &Xs, size_t k, bool isPivot);
        void completeGraph(vector<int> &Xs);
        void link(HNodeSet<dist_t>* node1, HNodeSet<dist_t>* node2, bool dir, dist_t distance, bool isPivot, string gtype);
        bool shouldConnect(pair<int, dist_t> evNode, int idAtual, vector<int> Xs);
        bool shouldConnectK(pair<int, dist_t> evNode, int idAtual, vector<int> Xs, int K);
        void printSeeds(vector<int> seeds);

        void printGraph(string file);
        void writeGephi(string file);
        /*
         * One can implement functions for index serialization and reading.
         * However, this is not required.
         */

        virtual void SaveIndex(const string& location) override;

        virtual void LoadIndex(const string& location) override;

        void SetQueryTimeParams(const AnyParams& QueryTimeParams) override;

        ~HGraphSet(){};

        /*
         * Just the name of the method, consider printing crucial parameter values
         */
        const std::string StrDesc() const override {
            stringstream str;
            str << "HGraph";
            return str.str();
        }

        /*
         *  One needs to implement two search functions.
         */
        void Search(RangeQuery<dist_t>* query, IdType) const override;
        void Search(KNNQuery<dist_t>* query, IdType) const override;
        void GNNS(KNNQuery<dist_t>* query) const;
        void SWSearch(KNNQuery<dist_t>* query) const;

        /*
         * In rare cases, mostly when we wrap up 3rd party methods,
         * we simply duplicate the data set. This function
         * let the experimentation code know this, so it could
         * adjust the memory consumption of the index.
         *
         * Note, however, that this method doesn't create any data duplicates.
         */
        virtual bool DuplicateData() const override { return false; }

        bool approximatelyEqual(dist_t a, dist_t b, float epsilon)
        {
            return fabs(a - b) <= ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
        }

        bool essentiallyEqual(dist_t a, dist_t b, float epsilon)
        {
            return fabs(a - b) <= ( (fabs(a) > fabs(b) ? fabs(b) : fabs(a)) * epsilon);
        }

        bool definitelyGreaterThan(dist_t a, dist_t b, float epsilon)
        {
            return (a - b) > ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
        }

        bool definitelyLessThan(dist_t a, dist_t b, float epsilon)
        {
            return (b - a) > ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
        }

    private:
        bool                    PrintProgress_;
        bool                    data_duplicate_;
        const Space<dist_t>&  space_;
        ObjectVector          data_; // We copy all the data
        bool                    bDoSeqSearch_;
        size_t                  L_; //number of levels to partition
        size_t                  S_; //number of seeds
        size_t                  NN_; //nn para quando escolher knng
        string                  addParams_;
        string     indexGraphType_;
        size_t                indexThreadQty_;
        string gtype_;
        string ptype_;
        ElementList ElList_;
        int                    countRefine;
        size_t                 initSearchAttempts_;
        size_t efSearch_;
        size_t efConstruction_;
        set<int>             notSelected;
        set<int>            selected;
        float                overRate_;
        float                eps_;
        float                  nparam_;
        bool                   stopConstruction;
        //vector<vector<int>>  levelSeeds_;
        set<int> levelSeeds_;
        enum AlgoType { gnns, swsearch};

        AlgoType               searchAlgoType_;


        // disable copy and assign
        DISABLE_COPY_AND_ASSIGN(HGraphSet);
    };

}   // namespace similarity

#endif     // _HGRAPH_METHOD
