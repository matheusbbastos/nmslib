//
// Created by Larissa Shimomura on 05/07/17.
//

#ifndef _kNNGW_WEIGHTED_H
#define _kNNGW_WEIGHTED_H

#include <string>
#include <sstream>

#include "index.h"
#include "params.h"
#include <set>
#include <limits>
#include <iostream>
#include <map>
#include <unordered_set>
#include <thread>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <queue>
#include "heapMember.h"
#include "min_heap.h"
#include "max_heap.h"

#define METH_kNNG_w  "knngw"

namespace similarity {

    using std::string;
    using std::vector;
    using std::thread;
    using std::mutex;
    using std::unique_lock;
    using std::condition_variable;
    using std::ref;

    template <typename dist_t> class EvaluatedkNNGWNode;
/*template<typename dist_t>
class NeighborNode{
public:
    NeighborNode(kNNGWNode* neighbor, dist_t weight){
        node_ = neighbor;
        weight_ = weight;
    }

    void setNode(kNNGWNode* node){
        node_ = node;
    }

    void setWeight(dist_t weight){
        weight_ = weight;
    }
private:
    kNNGWNode* node_;
    dist_t weight_;
};*/

//nos com distancia


template<typename dist_t>
    class kNNGWNode{
    public:
        kNNGWNode(const Object *Obj, size_t id){
            data_ = Obj;
            id_ = id;
        }
        ~kNNGWNode(){};
        void removeAllNeighbors(){
            neighbors.clear();
        };
        void addNeighbor(EvaluatedkNNGWNode<dist_t>* element){
            auto it = lower_bound(neighbors.begin(), neighbors.end(), element);
            neighbors.insert(it, element);
        }
        const Object* getData(){
            return data_;
        }
        const size_t getId(){
            return id_;
        }
        const vector<EvaluatedkNNGWNode<dist_t>*> getNeighbors() const{
            return neighbors;
        }
        const vector<kNNGWNode<dist_t>*> getNeighborsNode() const{
            vector<kNNGWNode<dist_t>*> nodes;
            for(EvaluatedkNNGWNode<dist_t>* evalNode : neighbors){
                nodes.push_back(evalNode->getkNNGWNode());
            }
            return nodes;
        }
        bool operator== (const kNNGWNode<dist_t> &obj1){
            return (data_->id() == obj1.data_->id());
        }
    private:
        const Object* data_;
        size_t id_;
        vector<EvaluatedkNNGWNode<dist_t>*> neighbors;

    };

//---------

    template <typename dist_t>
    class EvaluatedkNNGWNode{
    public:
        EvaluatedkNNGWNode() {
            distance = 0;
            element = NULL;
        }
        EvaluatedkNNGWNode(kNNGWNode<dist_t>* node, dist_t di) {
            distance = di;
            element = node;
        }

        void setDistance(dist_t di){
            distance = di;
        }

        void setNode(kNNGWNode<dist_t>* node){
            element = node;
        }

        ~EvaluatedkNNGWNode(){}
        dist_t getDistance() const {return distance;}
        kNNGWNode<dist_t>* getkNNGWNode() const {return element;}
        bool operator< (const EvaluatedkNNGWNode<dist_t> &obj) const {
            return (distance < obj.getDistance());
        }

        bool operator> (const EvaluatedkNNGWNode<dist_t> &obj) const {
            return (distance > obj.getDistance());
        }

    private:
        dist_t distance;
        kNNGWNode<dist_t>* element;
    };

    class CompareSet{
    public:
        bool operator()(const pair<size_t, int> a, pair<size_t, int> b){
            return a.first < b.first;
        }
    };


//-------------------------------
    template <typename dist_t>
    class kNNGW : public Index<dist_t> {
    public:
        /*
         * The constructor here space and data-objects' references,
         * which are guaranteed to be be valid during testing.
         * So, we can memorize them safely.
         */
        kNNGW(bool PrintProgress,
             const Space<dist_t>& space,
             const ObjectVector& data);
        /*
         * This function is supposed to create a search index (or call a
         * function to create it)!
         */
        void CreateIndex(const AnyParams& IndexParams) override;/* {
    AnyParamManager  pmgr(IndexParams);
    pmgr.GetParamOptional("doSeqSearch",
                          bDoSeqSearch_,
      // One should always specify the default value of an optional parameter!
                          false
                          );
    // Check if a user specified extra parameters, which can be also misspelled variants of existing ones
    pmgr.CheckUnused();
    // Always call ResetQueryTimeParams() to set query-time parameters to their default values
    this->ResetQueryTimeParams();
  }*/
        //void IndexOld();
        //void IndexMatrix();

        typedef std::vector<kNNGWNode<dist_t>*> ElementList;
        typedef std::priority_queue<EvaluatedkNNGWNode<dist_t>, vector<EvaluatedkNNGWNode<dist_t>>, less<EvaluatedkNNGWNode<dist_t>>> answerQueue;

        /*
         * One can implement functions for index serialization and reading.
         * However, this is not required.
         */
        //void add(const Object* data, kNNGWNode* newElement);
        void link(kNNGWNode<dist_t>* first, EvaluatedkNNGWNode<dist_t>* second);
        kNNGWNode<dist_t>* getInitialVertice() const;
        //kNNGWNode<dist_t>* getExtractedNode(vector<kNNGWNode<dist_t>*> C) const;
        //de acordo com Dijkstra
        kNNGWNode<dist_t>* addCriticalSection(kNNGWNode<dist_t>* element);
        dist_t CoveringRadius(kNNGWNode<dist_t>* u) const; //returns covering radius of a node
        void add(kNNGWNode<dist_t>* newElement, size_t idAtual);
        void writeGephi(string file);
        void printGraph(string file);

        // SaveIndex is not necessarily implemented
        virtual void SaveIndex(const string& location) override ;
        // LoadIndex is not necessarily implemented
        virtual void LoadIndex(const string& location) override;

        void SetQueryTimeParams(const AnyParams& QueryTimeParams) override;

        ~kNNGW(){};

        /*
         * Just the name of the method, consider printing crucial parameter values
         */
        const std::string StrDesc() const override {
            stringstream str;
            str << "kNNGW";// << (bDoSeqSearch_ ? " does seq. search " : "k - Nearest Neighbor Graph (brute force construction algorithm");
            return str.str();
        }

        vector<kNNGWNode<dist_t>*> getElList(){
            return ElList_;
        }

        /*
         *  One needs to implement two search functions.
         */
        kNNGWNode<dist_t>* nextToReview() const;
        /*void checkNeighborhood(RangeQuery<dist_t>* query, EvaluatedkNNGWNode<dist_t>* p, dist_t dpq) const;
        vector<EvaluatedkNNGWNode<dist_t>*> getNeighborsCand(kNNGWNode* u) const;
        void useContainerRQ(EvaluatedkNNGWNode<dist_t> u,dist_t duq, dist_t radius) const;*/
        pair<size_t, dist_t> traverse(KNNQuery<dist_t>* query, kNNGWNode<dist_t> *p, dist_t dpq) const;
        void useContainerNNQ(EvaluatedkNNGWNode<dist_t> *u, dist_t duq, dist_t radius) const;
        void extractNode(kNNGWNode<dist_t>* p) const ;
        vector<dist_t> Dijkstra(EvaluatedkNNGWNode<dist_t>* p, dist_t rexp) const;
        vector<dist_t> LimDijkstra(EvaluatedkNNGWNode<dist_t>* p, vector<int> visitedBitset, dist_t rexp) const;
        void nearestNeighborGS(KNNQuery<dist_t>* query) const; //de acordo com o algoritmo guloso de Aoyama
        void Search(RangeQuery<dist_t>* query, IdType) const override{
            throw runtime_error("Range Query is not implemented for method: " + StrDesc());
        }
        void Search(KNNQuery<dist_t>* query, IdType) const override;/*{
            throw runtime_error("kNN Query is not implemented for method: " + StrDesc());
        }*/
        void SearchChavez(KNNQuery<dist_t>* query) const;
        void SearchApproximate(KNNQuery<dist_t>* query) const;
        const kNNGWNode<dist_t>* getElItem(int i) const{
            return ElList_[i];
        }
        void addEdges();
        //void RecursiveRange(kNNGWNode* initialVertice, RangeQuery<dist_t>* query, dist_t mindist, set<kNNGWNode*> explored) const; //range recursivo do artigo do Ocsa

        /*
         * In rare cases, mostly when we wrap up 3rd party methods,
         * we simply duplicate the data set. This function
         * let the experimentation code know this, so it could
         * adjust the memory consumption of the index.
         *
         * Note, however, that this method doesn't create any data duplicates.
         */
        virtual bool DuplicateData() const override { return false; }

    private:
        bool                    data_duplicate_;
        //const ObjectVector&     data_;
        //Space<dist_t>&          space_;
        bool                    bDoSeqSearch_;
        //mutable mutex   ElListGuard_;
        //ElementList     ElList_;
        size_t                initIndexAttempts_;
        size_t                initSearchAttempts_;
        size_t                indexThreadQty_;
        size_t                k_;
        size_t                it_;
        const Space<dist_t>&  space_;
        ObjectVector          data_; // We copy all the data
        bool                  PrintProgress_;
        mutable mutex   ElListGuard_;
        ElementList     ElList_;
        mutable MaxHeap<kNNGWNode<dist_t>*, dist_t> C;//max heap para C
        mutable MinHeap<kNNGWNode<dist_t>*, dist_t> radii;// min heap para radii
        //mutable MaxHeap<size_t , dist_t> C;
        //mutable MinHeap<size_t , dist_t> radii;
        mutable EvaluatedkNNGWNode<dist_t> container;
        mutable answerQueue nnc; //ordered by decreasing distance
        //priority_queue<EvaluatedkNNGWNode<dist_t>> nnc;
        //map id do node na ElList e posicao do heap
        bool chavez_, approx_;
        // disable copy and assign
        DISABLE_COPY_AND_ASSIGN(kNNGW);
    };

}   // namespace similarity

#endif //_kNNGW_WEIGHTED_H
