//
// Created by larissa on 8/11/17.
//

/*#ifndef FACTORY_PBKNNG_H
#define FACTORY_PBKNNG_H


#include <method/pbknng.h>

namespace similarity {


// * Creating functions.


    template <typename dist_t>
    Index<dist_t>* CreatePBKNNG(bool PrintProgress,
                              const string& SpaceType,
                              Space<dist_t>& space,
                              const ObjectVector& DataObjects) {

        bool is_valid = false;
        if(SpaceType=="l1"||SpaceType=="l2"||SpaceType=="bit_hamming"||SpaceType=="angulardist"){
            is_valid = true;
        }
        CHECK_MSG(is_valid,"PBKNNG only work for l1, l2, hamming and angle distance")
        char distType = '2';
        if(SpaceType=="l1"){
            distType = '1';
        }else if(SpaceType=="l2"){
            distType = '2';
        }else if(SpaceType=="bit_hamming"){
            distType = 'h';
        }else if(SpaceType=="angulardist"){
            distType = 'a';
        }

        return new PBKNNG<dist_t>(space, DataObjects, distType);
    }

/*
 * End of creating functions.
 */

//}



//#endif //FACTORY_PBKNNG_H
