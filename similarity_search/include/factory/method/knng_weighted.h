//
// Created by Larissa Shimomura on 05/07/17.
//

#ifndef _FACTORY_KNNG_WEIGHTED_H
#define _FACTORY_KNNG_WEIGHTED_H

#include <method/knng_weighted.h>

namespace similarity {

/*
 * Creating functions.
 */

    template <typename dist_t>
    Index<dist_t>* CreatekNNGW(bool PrintProgress,
                             const string& SpaceType,
                             Space<dist_t>& space,
                             const ObjectVector& DataObjects) {
        return new kNNGW<dist_t>(PrintProgress, space, DataObjects);
    }

/*
 * End of creating functions.
 */

}

#endif //_KNNG_WEIGHTED_H
