
#ifndef _FACTORY_HGRAPH_H_
#define _FACTORY_HGRAPH_H_

#include <method/hgraph.h>

namespace similarity {

    template <typename dist_t>
    Index<dist_t>* CreateHGraph(bool PrintProgress,
                             const string& SpaceType,
                             Space<dist_t>& space,
                             const ObjectVector& DataObjects) {
        return new HGraph<dist_t>(PrintProgress, space, DataObjects);
    }

/*
 * End of creating functions.
 */

}

#endif
