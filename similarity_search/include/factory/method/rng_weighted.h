//
// Created by larissa on 8/23/17.
//

#ifndef _FACTORY_RNG_WEIGHTED_H
#define _FACTORY_RNG_WEIGHTED_H

#include <method/rng_weighted.h>

namespace similarity{

    template <typename dist_t>
    Index<dist_t>* CreateRNGW(bool PrintProgress,
                               const string& SpaceType,
                               Space<dist_t>& space,
                               const ObjectVector& DataObjects) {
        return new RNGW<dist_t>(PrintProgress, space, DataObjects);
    }


}


#endif //NONMETRICSPACELIB_RNG_WEIGHTED_H
