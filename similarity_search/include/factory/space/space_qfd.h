//
// Created by larissa on 2-8-19.
//

#ifndef FACTORY_SPACE_QFD_H
#define FACTORY_SPACE_QFD_H

#include "space/space_qfd.h"

namespace similarity {

/*
 * Creating functions.
 */

    template <typename dist_t>
    Space<dist_t>* CreateQFD(const AnyParams& ) {
//        AnyParamManager pmgr(AllParams);
        LOG(LIB_INFO) << " CREATE QFD DISTANCE";
        return new SpaceQFD<dist_t>();
    }

}



#endif //FACTORY_SPACE_QFD_H
