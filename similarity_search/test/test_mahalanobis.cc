//
// Created by Larissa Shimomura on 28/06/19.
//

#include <memory>

#include "space/space_mahalanobis.h"
#include "bunit.h"
#include "testdataset.h"

namespace similarity {

#define FLOAT_TYPE double

class VectorDataset1 : public TestDataset {
 public:
  VectorDataset1() {
    FLOAT_TYPE arr[8][5] = {
      {0.459, 0.04, 0.086, 0.599, 0.555},
      {0.842, 0.572, 0.801, 0.136, 0.87},
      {0.42, 0.773, 0.554, 0.198, 0.461},
      {0.958, 0.057, 0.376, 0.663, 0.419},
      {0.261, 0.312, 0.7, 0.108, 0.588},
      {0.079, 0.663, 0.921, 0.901, 0.564},
      {0.463, 0.806, 0.672, 0.388, 0.225},
      {0.174, 0.884, 0.801, 0.563, 0.092}
    };

    for (int i = 0; i < 8; ++i) {
      dataobjects_.push_back(new Object(i + 1, -1, 5 * sizeof(FLOAT_TYPE), &arr[i]));
    }
  }
};


TEST(MahalanobisDistance){
  VectorDataset1 dataset;
  const ObjectVector& dataobjects = dataset.GetDataObjects();

  unique_ptr<Space<FLOAT_TYPE>> space(new SpaceMahalanobis<FLOAT_TYPE>());
  const FLOAT_TYPE expected[8][8] = {
          { 0.000, 3.432, 2.996, 3.159, 3.487, 2.501, 3.666, 3.219},
          { 3.432, 0.000, 3.477, 3.378, 3.624, 3.503, 3.152, 3.305},
          { 2.996, 3.477160, 0.000, 2.250, 3.635, 3.460, 2.918, 3.628},
              { 3.159, 3.378, 2.250, 0.000, 2.850, 2.831, 2.534, 1.831},
                  { 3.487, 3.624, 3.635, 2.850, 0.000, 3.296, 3.418, 3.645},
                      { 2.501, 3.503, 3.460820, 2.831879, 3.296249, 0.000000, 1.921, 2.970},
                          { 3.666, 3.152, 2.918, 2.534, 3.418, 1.921, 0.000, 3.321},
                              { 3.219, 3.305, 3.628, 1.831, 3.645, 2.970, 3.321, 0.000}
        };

        for (size_t i = 0; i < 8; ++i) {
            for (size_t j = 0; j < 8; ++j) {
                const FLOAT_TYPE d = space->IndexTimeDistance(dataobjects[i], dataobjects[j]);
                EXPECT_EQ_EPS(expected[i][j], d, 1e-5);
            }
        }

}



}  // namespace similarity

