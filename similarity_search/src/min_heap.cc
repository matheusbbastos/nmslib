//min implementation

#include <cmath>
#include <memory>
#include <iostream>

#include "space.h"
#include "min_heap.h"
#include "heapMember.h"

#include <vector>

#include <set>
#include <map>
#include <sstream>
#include <typeinfo>

using namespace std;

namespace similarity{


    template<typename T, typename dist_t>
    void MinHeap<T, dist_t>::insert(heapMember<T, dist_t> v, size_t id) {
        pair<size_t , int> pairMap(id, heap.size());
        pair<std::map<size_t,int>::iterator,bool> it = PosMap.insert(pairMap);
        if(it.second == true){
            v.setIterator(it.first);
            heap.push_back(v);
            bubbleUp(heap.size()-1);
        }
    }

    template<typename T, typename dist_t>
    void MinHeap<T, dist_t>::remove(heapMember<T, dist_t> v, size_t id) {
        int i = findHeap(v, 0);
        removeIndex(i);
        //PosMap.erase(v.getIterator());
    }

    template<typename T, typename dist_t>
    void MinHeap<T, dist_t>::removeId(size_t id){
        map<size_t, int>::iterator findID = PosMap.find(id);
        if(findID != PosMap.end()){
            //PosMap.erase(findID);
            removeIndex(findID->second);
        }
    };

    template<typename T, typename dist_t>
    int MinHeap<T, dist_t>::update(heapMember<T, dist_t> v, size_t id) {
        //return new position
        return -1;
    }

    template <typename T,typename dist_t>
    heapMember<T, dist_t> MinHeap<T, dist_t>::getId(int id) {
        std::map<size_t ,int>::iterator it;
        /*cout << "PosMap contents" << PosMap.size();
        for (it=PosMap.begin(); it!=PosMap.end(); ++it)
            cout << it->first << " => " << it->second << '\n';*/
        map<size_t, int>::iterator idIterator = PosMap.find(id);
        heapMember<T, dist_t> heapId = heap[idIterator->second];
        return heapId;
    }

    template <typename T, typename dist_t>
    void MinHeap<T, dist_t>::increaseKey(size_t id, dist_t dg) {
        std::map<size_t ,int>::iterator it;
        map<size_t, int>::iterator idIterator = PosMap.find(id);
        heapMember<T, dist_t> heapId = heap[idIterator->second];
        dist_t newValue = heapId.getValue() + dg;
        heap[idIterator->second].setValue(newValue);
        bubbleDown(idIterator->second);
    }

    template <typename T, typename dist_t>
    void MinHeap<T, dist_t>::decreaseKey(size_t id, dist_t dg){
        std::map<size_t ,int>::iterator it;
        map<size_t, int>::iterator idIterator = PosMap.find(id);
        heapMember<T, dist_t> heapId = heap[idIterator->second];
        dist_t newValue = heapId.getValue() - dg;
        heap[idIterator->second].setValue(newValue);
        bubbleUp(idIterator->second);
    }

    template <typename T, typename dist_t>
    void MinHeap<T, dist_t>::setNewValue(size_t id, dist_t dg){
        std::map<size_t ,int>::iterator it;
        map<size_t, int>::iterator idIterator = PosMap.find(id);
        heapMember<T, dist_t> heapId = heap[idIterator->second];
        dist_t newValue = dg;
        heap[idIterator->second].setValue(newValue);
        bubbleUp(idIterator->second);
        bubbleDown(idIterator->second);
    };


    template<typename T, typename dist_t>
    void MinHeap<T, dist_t>::removeIndex(int i){
        if(heap.size() == 1 && PosMap.size()==1) {
            heap.pop_back();
            PosMap.clear();
            // LOG(LIB_INFO) << heap.size();
            return;
        }
        if(i == -1) return;
        swap(heap[i], heap[heap.size()-1]);
        heap[i].getIterator()->second = i;
        PosMap.at(heap[i].getIterator()->first) = i;
        PosMap.erase(heap[heap.size()-1].getIterator());
        heap.resize(heap.size()-1);
        //heap.pop_back();
        bubbleDown(i);
        bubbleUp(i);
    }

    template<typename T, typename dist_t>
    int MinHeap<T, dist_t>::getMin(int ii, int lli, int rri){
        dist_t i = heap[ii].getValue();
        dist_t li = heap[lli].getValue();
        dist_t ri;
        if(rri > heap.size()-1){
            ri = INFINITY;
        }else ri = heap[rri].getValue();
        if(i <= li && i <= ri){
            return ii;
        }
        if(li <= i && li <= ri){
            return lli;
        }
        if(ri <= i && ri <= li){
            return rri;
        }
        else return ii;
    }

    /*template <typename T, typename dist_t>
    void MinHeap<T, dist_t>::heapify(vector<heapMember<T, dist_t>> uarray){
        cout << uarray.size() << endl;
        for(int i =0; i < uarray.size(); i++){
            heap.push_back(uarray[i]);
        }
        for(int i = uarray.size()-1; i > 0; i--){
            cout << i << endl;
            bubbleDown(i);
        }
    }*/

    template <typename T, typename dist_t>
    bool MinHeap<T, dist_t>::contain(size_t id) {
        return (PosMap.find(id) != PosMap.end());
    }

    template<typename T, typename dist_t>
    int MinHeap<T, dist_t>::findHeap(heapMember<T,dist_t> v, int i){
        if(i > heap.size()) return -1;
        if(v.getValue() > heap[i].getValue()) return -1;
        if(heap[i].getIterator() == v.getIterator()) return i;
        int childIndex = this->getLeftChildIndex(i);
        i = -1;
        if(childIndex != -1){
            i = max(findHeap(v, childIndex), findHeap(v, childIndex+1));
        }
        return i; //retorna posicao
    };

    template <typename T, typename dist_t>
    void MinHeap<T, dist_t>::bubbleUp(int i){ //subir um item
        int parentIndex = this->getParentIndex(i);
        if(parentIndex == -1) return ;
        if(heap[parentIndex].getValue() > heap[i].getValue()){
            map<size_t, int>::iterator itParent = heap[parentIndex].getIterator();
            map<size_t, int>::iterator itI = heap[i].getIterator();
            PosMap.at(itParent.operator*().first) = i;
            PosMap.at(itI.operator*().first) == parentIndex;
            itParent->second = i;
            itI->second = parentIndex;
            swap(heap[parentIndex], heap[i]);
            bubbleUp(parentIndex);
        }
    }

    template <typename T, typename dist_t>
    void MinHeap<T, dist_t>::PrintPosMap() {
        std::map<size_t,int>::iterator it;
        // LOG(LIB_INFO) << "PosMap contents" << PosMap.size();
        for (it=PosMap.begin(); it!=PosMap.end(); ++it)
            // LOG(LIB_INFO) << it->first << " => " << it->second << '\n';
            cout << it->first << " => " << it->second << '\n';
    }

    template <typename T, typename dist_t>
    void MinHeap<T, dist_t>::bubbleDown(int i){
        int leftIndex = this->getLeftChildIndex(i);
        if(leftIndex == -1) return;
        if(leftIndex >= heap.size()) return;
        //cout << "i" << i << "left" << leftIndex << endl;
        int minIndex = getMin(i, leftIndex, leftIndex+1); //menor entre o elemento e seus dois filhos
        if(minIndex != i){
           // cout << "min index" << minIndex << endl;
            map<size_t, int>::iterator itMin = heap[minIndex].getIterator();
            map<size_t, int>::iterator itI = heap[i].getIterator();
           // cout << "it min index" << itMin.operator*().first << endl;
           // cout << "HEAP contents" <<  heap.size() << endl;
           // for(int j=0; j < heap.size();j++){
           //     cout << "node:" << heap[j].getNode() << "value:" << heap[j].getValue() << "iterator:" << heap[j].getIterator().operator*().first << "j:" << j << endl;
           // }
            //cout << PosMap.size() << endl;
            //PrintPosMap();
            PosMap.at(itMin.operator*().first) = i;
            PosMap.at(itI.operator*().first) = minIndex;
            itMin->second = i;
            itI->second = minIndex;
            swap(heap[minIndex], heap[i]);
            bubbleDown(minIndex);
        }
    }




    template <typename T, typename dist_t>
    heapMember<T, dist_t> MinHeap<T, dist_t>::extractMin(){
        heapMember<T,dist_t> min;
        if(heap.size() == 0) return min;
        min = heap[0];
        removeIndex(0);
        return min;
     /*   heapMember<T,dist_t> min;
        if(heap.size() == 0) return min;
        min = heap[0];
        removeIndex(0);
        //PosMap.erase(min.getIterator());
        return min;*/
    };

}