#include "distcomp.h"
#include "string.h"
#include "logging.h"
#include "utils.h"
#include "pow.h"
#include "portable_intrinsics.h"

#include <cstdlib>
#include <limits>
#include <algorithm>
#include <cmath>
#include <boost/assign/std/vector.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/moment.hpp>
#include <boost/accumulators/statistics/covariance.hpp>
#include <boost/accumulators/statistics/variates/covariate.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

//using namespace boost::accumulators;
//using namespace boost::assign;
//using namespace boost::numeric::ublas;

typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean> > localMean;
typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::covariance<double, boost::accumulators::tag::covariate1> > > covariance_XY;
typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean, boost::accumulators::tag::variance> > zscore;


namespace similarity {
    using namespace std;
/*
 * Mahalanobis
 *
 */
    template<class T>
    bool InvertMatrix (const boost::numeric::ublas::matrix<T>& input, boost::numeric::ublas::matrix<T>& inverse){
        typedef boost::numeric::ublas::permutation_matrix<std::size_t> pmatrix;
        boost::numeric::ublas::matrix<T> A(input);
        pmatrix pm(A.size1());

        int res = lu_factorize(A,pm);
        if( res != 0 )
            return false;

        inverse.assign(boost::numeric::ublas::identity_matrix<T>(A.size1()));
        lu_substitute(A, pm, inverse);

        return true;
    }

    template<class T>
    T mean_vector(std::vector<T> v, size_t size){
        T sum = 0;
        for(int i = 0; i < size; i++){
            sum = sum + v[i];
        }
        return sum/size;
    }

    template<class T>
    T covar_vector(std::vector<T> v1, std::vector<T> v2, size_t size){
        T mean1, mean2, sum = 0;
        mean1 = mean_vector(v1, size);
        mean2 = mean_vector(v2, size);

        for(int i = 0; i < size; i++){
            sum = sum + ((v1[i] - mean1) * (v2[i] - mean2));
        }
        return sum/(size-1);
    }

    template<class T>
    void covar(const boost::numeric::ublas::matrix<T>& matrix, boost::numeric::ublas::matrix<T>& covar_matrix, size_t size){
        std::vector<T> tmp1(5), tmp2(5); // vetores que armazenarão as colunas

        for(int i = 0; i < 3; i++){
            for(int j = i; j < 3; j++){
                // esvaziar tmps?
                for(int k = 0; k < 5; k++){
                    tmp1[k] = matrix(k,j);
                    tmp2[k] = matrix(k,i);
                }
                covar_matrix(i, j) = covar_vector(tmp1,tmp2,size);
                covar_matrix(j, i) = covar_vector(tmp1,tmp2, size);
            }
        }
    }

    template<class T>
    T Mahalanobis(
            const T *p1,
            const T *p2,
            size_t qty,
            boost::numeric::ublas::matrix<T> *inverted_matrix2,
            std::vector<T> mean_vector)
    {
        T result;
        std::vector<double> dif_mean(qty), tmp_vector(qty);

        for (size_t i = 0; i < qty; i++) {
            //dif_mean[i] = p2[i] - mean_vector[i];
            dif_mean[i] = p2[i] - p1[i];
        }

        for (size_t i = 0; i < qty; i++) {
            tmp_vector[i] = 0;
            for (size_t j = 0; j < qty; j++) {
                tmp_vector[i] += dif_mean[j] * (*inverted_matrix2)(j,i);
            }
        }
        result = 0;
        for (size_t i = 0; i < qty; i++) {
            result += tmp_vector[i] * dif_mean[i];
        }

        return sqrt(result);

    }

    template float Mahalanobis<float>(const float* pVect1, const float* pVect2, size_t qty, boost::numeric::ublas::matrix<float> *inverted_matrix2, std::vector<float> mean_vector);
    template double Mahalanobis<double>(const double* pVect1, const double* pVect2, size_t qty, boost::numeric::ublas::matrix<double> *inverted_matrix2, std::vector<double> mean_vector);


    template<class T>
    T Mahalanobis(
            const T *p1,
            const T *p2,
            size_t qty,
            boost::numeric::ublas::matrix<T> *inverted_matrix2)
    {
        T result;
        std::vector<double> dif_mean(qty), tmp_vector(qty);

        for (size_t i = 0; i < qty; i++) {
            //dif_mean[i] = p1[i] - p2[i];
            dif_mean[i] = p2[i] - p1[i];
        }

        for (size_t i = 0; i < qty; i++) {
            tmp_vector[i] = 0;
            for (size_t j = 0; j < qty; j++) {
                tmp_vector[i] += dif_mean[j] * inverted_matrix2->operator()(j,i);//*inverted_matrix2)(j,i);
            }
        }
        result = 0;
        for (size_t i = 0; i < qty; i++) {
            result += tmp_vector[i] * dif_mean[i];
        }
        if(result < 0) result = result*(-1);
        return sqrt(result);

    }

    template float Mahalanobis<float>(const float* pVect1, const float* pVect2, size_t qty, boost::numeric::ublas::matrix<float> *inverted_matrix2);
    template double Mahalanobis<double>(const double* pVect1, const double* pVect2, size_t qty, boost::numeric::ublas::matrix<double> *inverted_matrix2);

}