/**
 * Non-metric Space Library
 *
 * Authors: Bilegsaikhan Naidan (https://github.com/bileg), Leonid Boytsov (http://boytsov.info).
 * With contributions from Lawrence Cayton (http://lcayton.com/) and others.
 *
 * For the complete list of contributors and further details see:
 * https://github.com/searchivarius/NonMetricSpaceLib
 *
 * Copyright (c) 2014
 *
 * This code is released under the
 * Apache License Version 2.0 http://www.apache.org/licenses/.
 *
 */

#include <cmath>
#include <fstream>
#include <string>
#include <sstream>

#include "space/space_qfd.h"
#include "experimentconf.h"
#include "distcomp.h"

namespace similarity {


    template <typename dist_t>
    std::string SpaceQFD<dist_t>::StrDesc() const {
        std::stringstream stream;
        stream << "--Quadratic Form Distance!---";
        return stream.str();
    }


    template <typename dist_t>
    dist_t SpaceQFD<dist_t>::HiddenDistance(const Object* obj1, const Object* obj2) const {
        //  LOG(LIB_INFO) << "Calculating the distance between objects: " << obj1->label() << " and " << obj2->label() << endl;
        CHECK(obj1->datalength() > 0);
        CHECK(obj1->datalength() == obj2->datalength());
        const size_t dimension = GetElemQty(obj1);
        const dist_t* x = reinterpret_cast<const dist_t*>(obj1->data());
        const dist_t* y = reinterpret_cast<const dist_t*>(obj2->data());
        //if(obj1->id() == obj2->id()) return 0;
        return QFD(x, y, dimension);
    }

    template class SpaceQFD<float>;
    template class SpaceQFD<double>;

}  // namespace similarity
