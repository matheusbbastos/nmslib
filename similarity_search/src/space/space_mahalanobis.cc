/**
 * Non-metric Space Library
 *
 * Authors: Bilegsaikhan Naidan (https://github.com/bileg), Leonid Boytsov (http://boytsov.info).
 * With contributions from Lawrence Cayton (http://lcayton.com/) and others.
 *
 * For the complete list of contributors and further details see:
 * https://github.com/searchivarius/NonMetricSpaceLib
 *
 * Copyright (c) 2014
 *
 * This code is released under the
 * Apache License Version 2.0 http://www.apache.org/licenses/.
 *
 */

#include <cmath>
#include <fstream>
#include <string>
#include <sstream>

#include "space/space_mahalanobis.h"
#include "experimentconf.h"
#include "distcomp.h"

namespace similarity {

//    template<typename dist_t>
//    bool SpaceMahalanobis<dist_t>::InvertMatrix(const boost::numeric::ublas::matrix<dist_t>& input, boost::numeric::ublas::matrix<dist_t>& inverse) const{
//
//        typedef boost::numeric::ublas::permutation_matrix<std::size_t> pmatrix;
//        boost::numeric::ublas::matrix<dist_t> A(input);
//        pmatrix pm(A.size1());
//
//        int res = lu_factorize(A,pm);
//        if( res != 0 )
//            return false;
//
//        inverse.assign(boost::numeric::ublas::identity_matrix<dist_t>(A.size1()));
//        lu_substitute(A, pm, inverse);
//
//        return true;
//    }

    template <typename dist_t>
    std::string SpaceMahalanobis<dist_t>::StrDesc() const {
        std::stringstream stream;
        stream << "---Mahalanobis distance!---";
        return stream.str();
    }


    template <typename dist_t>
    dist_t SpaceMahalanobis<dist_t>::HiddenDistance(const Object* obj1, const Object* obj2) const {
      //  LOG(LIB_INFO) << "Calculating the distance between objects: " << obj1->label() << " and " << obj2->label() << endl;
        CHECK(obj1->datalength() > 0);
        CHECK(obj1->datalength() == obj2->datalength());
        const size_t dimension = GetElemQty(obj1);
        const dist_t* x = reinterpret_cast<const dist_t*>(obj1->data());
        const dist_t* y = reinterpret_cast<const dist_t*>(obj2->data());
        //LOG(LIB_INFO) << "START MATRIX PRINT";
       /*cout << "START MATRIX PRINT" << endl;
        for (unsigned i = 0; i < inverted_matrix2->size1(); ++ i)
            for (unsigned j = 0; j < inverted_matrix2->size2 (); ++ j)
                //LOG(LIB_INFO) << inverted_matrix2->operator()(i,j);
                cout << inverted_matrix2->operator()(i,j) << " ";
        //LOG(LIB_INFO) << "END MATRIX PRINT";
        cout << "END MATRIX PRINT" << endl;*/
        //return Mahalanobis(x, y, dimension, inverted_matrix2);
        return Mahalanobis(x, y, dimension, inverted_matrix2, mean_vector);
    }

    template class SpaceMahalanobis<float>;
    template class SpaceMahalanobis<double>;

}  // namespace similarity
