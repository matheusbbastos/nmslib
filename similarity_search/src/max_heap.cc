//min implementation

#include <cmath>
#include <memory>
#include <iostream>

#include "space.h"
#include "max_heap.h"

#include <vector>
#include <map>
#include <sstream>
#include <typeinfo>

namespace similarity{

    using namespace std;

    template<typename T, typename dist_t>
    void MaxHeap<T, dist_t>::insert(heapMember<T, dist_t> v, size_t id) {
        pair<size_t , int> pairMap(id, heap.size());
        pair<std::map<size_t,int>::iterator,bool> it = PosMap.insert(pairMap);
        if(it.second){
            v.setIterator(it.first);
            heap.push_back(v);
            bubbleUp(heap.size()-1);
        }
    }

    template <typename T,typename dist_t>
    heapMember<T, dist_t> MaxHeap<T, dist_t>::getId(int id) {
        std::map<size_t ,int>::iterator it;
        /*cout << "PosMap contents" << PosMap.size();
        for (it=PosMap.begin(); it!=PosMap.end(); ++it)
            cout << it->first << " => " << it->second << '\n';*/
        map<size_t, int>::iterator idIterator = PosMap.find(id);
        heapMember<T, dist_t> heapId = heap[idIterator->second];
        return heapId;
    }

   /* template<typename T, typename dist_t>
    void MaxHeap<T, dist_t>::remove(heapMember<T, dist_t> v, size_t id) {
        int i = findHeap(v, 0);
        removeIndex(i);
        PosMap.erase(v.getIterator());
    }*/

    template<typename T, typename dist_t>
    void MaxHeap<T, dist_t>::removeId(size_t id){
        map<size_t, int>::iterator findID = PosMap.find(id);
        //int index = findID->second;
        if(findID != PosMap.end()){
            //LOG(LIB_INFO) << "findID" << findID->second;
            removeIndex(findID->second);
           // PosMap.erase(findID);
        }
    };

    /*template<typename T, typename dist_t>
    int MaxHeap<T, dist_t>::update(heapMember<T, dist_t> v, size_t id) {
        //return new position
        return -1;
    }*/

    template <typename T, typename dist_t>
    void MaxHeap<T, dist_t>::PrintPosMap() {
        std::map<size_t,int>::iterator it;
       // LOG(LIB_INFO) << "PosMap contents" << PosMap.size();
        for (it=PosMap.begin(); it!=PosMap.end(); ++it)
           // LOG(LIB_INFO) << it->first << " => " << it->second << '\n';
            cout << it->first << " => " << it->second << '\n';
    }

    /*template <typename T, typename dist_t>
    void MaxHeap<T, dist_t>::increaseKey(size_t id, dist_t dg){
        std::map<size_t,int>::iterator it;
        cout << "PosMap contents" << PosMap.size();
        for (it=PosMap.begin(); it!=PosMap.end(); ++it)
            cout << it->first << " => " << it->second << '\n';
        cout << "increase key id" << id;
        map<size_t, int>::iterator idIterator = PosMap.find(id);
        cout << "size" << PosMap.size();
        cout << "idIterator" << idIterator->first;
        heapMember<T, dist_t> heapId = heap[idIterator->second];
        dist_t newValue = heapId.getValue() + dg;
        //heapId.setValue(newValue);
        heapMember<T, dist_t> newHeapMember;
        newHeapMember.setNode(heapId.getNode());
        newHeapMember.setValue(newValue);
        cout << "iterator value" << heapId.getValue();
        PosMap.erase(idIterator);
        removeIndex(idIterator->second);
        insert(newHeapMember, id);
    };*/

    template <typename T, typename dist_t>
    void MaxHeap<T, dist_t>::increaseKey(size_t id, dist_t dg) {
        std::map<size_t ,int>::iterator it;
        /*cout << "PosMap contents" << PosMap.size();
        for (it=PosMap.begin(); it!=PosMap.end(); ++it)
            cout << it->first << " => " << it->second << '\n';*/
        map<size_t, int>::iterator idIterator = PosMap.find(id);
        heapMember<T, dist_t> heapId = heap[idIterator->second];
        dist_t newValue = heapId.getValue() + dg;
        heap[idIterator->second].setValue(newValue);
        bubbleUp(idIterator->second);
        //cout << "bubble up done";
    }

    template <typename T, typename dist_t>
    void MaxHeap<T, dist_t>::decreaseKey(size_t id, dist_t dg){
        std::map<size_t ,int>::iterator it;
        map<size_t, int>::iterator idIterator = PosMap.find(id);
        heapMember<T, dist_t> heapId = heap[idIterator->second];
        dist_t newValue = heapId.getValue() - dg;
        heap[idIterator->second].setValue(newValue);
        bubbleDown(idIterator->second);
    }


    template <typename T, typename dist_t>
    bool MaxHeap<T, dist_t>::contain(size_t id) {
        if(PosMap.size()==0 && heap.size()==0) return false;
        else return (PosMap.find(id) != PosMap.end());
    }

    template<typename T, typename dist_t>
    int MaxHeap<T, dist_t>::findHeap(heapMember<T,dist_t> v, int i){
        if(i > heap.size()) return -1;
        if(v.getValue() > heap[i].getValue()) return -1;
        if(heap[i].getIterator() == v.getIterator()) return i;
        int childIndex = this->getLeftChildIndex(i);
        i = -1;
        if(childIndex != -1){
            i = min(findHeap(v, childIndex), findHeap(v, childIndex+1));
        }
        return i; //retorna posicao
    };

    template <typename T, typename dist_t>
    heapMember<T, dist_t> MaxHeap<T, dist_t>::extractMax() {
        /*heapMember<T,dist_t> max;
        if(heap.size() == 0) return max;
        max = heap[0];
        cout << PosMap.size();
        if(max.getIterator().operator*().second == 0) LOG(LIB_INFO) << max.getNode() << "ok" << endl;
        PosMap.erase(max.getIterator());
        cout << "iterator";
        removeIndex(0);
        return max;*/
        heapMember<T,dist_t> max;
        if(heap.size() == 0) return max;
        max = heap[0];
        removeIndex(0);
        return max;
    }


    template <typename T, typename dist_t>
    void MaxHeap<T, dist_t>::removeIndex(int i) {
        //LOG(LIB_INFO) << heap.size();
        if(heap.size() == 1 && PosMap.size()==1) {
            heap.pop_back();
            PosMap.clear();
           // LOG(LIB_INFO) << heap.size();
            return;
        }
        swap(heap[i], heap[heap.size()-1]);
        heap[i].getIterator()->second = i;
        PosMap.at(heap[i].getIterator()->first) = i;
        //heap[heap.size()-1].getIterator()->second = heap.size() -1;
        //map<size_t ,int>::iterator Iite = heap[heap.size()-1].getIterator();
        //cout << heap[heap.size()-1].getIterator()->first << endl;
        //cout << heap[0].getValue() << endl;
        //cout << heap[heap.size()-1].getNode() << endl;
        PosMap.erase(heap[heap.size()-1].getIterator());
        //heap.pop_back();
        heap.resize(heap.size()-1);
        bubbleDown(i);
        bubbleUp(i);
    }

    template <typename T, typename dist_t>
    void MaxHeap<T, dist_t>::bubbleUp(int i) {
        int parentIndex = this->getParentIndex(i);
        if(parentIndex == -1) return ;
        if(heap[parentIndex].getValue() < heap[i].getValue()){
            map<size_t, int>::iterator itParent = heap[parentIndex].getIterator();
            map<size_t, int>::iterator itI = heap[i].getIterator();
            //heap[parentIndex].getIterator()->second = i;
            //heap[i].getIterator()->second = parentIndex;
            itParent-> second = i;
            itI->second = parentIndex;
            //cout << "it parent" << itParent->first << "itI" << itI.operator*().first << endl;
            PosMap.at(itParent.operator*().first) = i;
            PosMap.at(itI.operator*().first) == parentIndex;
            //map<size_t, int>::iterator it = PosMap.find(heap[i].getNode());
            //if(it->second == parentIndex) cout << "OK BUBBLE" << endl;
            //cout << "swap" << itParent->first << "com " << itI->first;
            swap(heap[parentIndex], heap[i]);
            bubbleUp(parentIndex);
        }
    }

    template <typename T, typename dist_t>
    void MaxHeap<T, dist_t>::bubbleDown(int i) {
        int leftIndex = this->getLeftChildIndex(i);
        if(leftIndex == -1) return;
        if(leftIndex >= heap.size()) return;
        int maxIndex = getMax(i, leftIndex, leftIndex+1); //menor entre o elemento e seus dois filhos
        if(maxIndex != i){
            map<size_t, int>::iterator itMax = heap[maxIndex].getIterator();
            map<size_t, int>::iterator itI = heap[i].getIterator();
            //heap[maxIndex].getIterator()->second = i;
            //heap[i].getIterator()->second = maxIndex;
            PosMap.at(itMax.operator*().first) = i;
            PosMap.at(itI.operator*().first) = maxIndex;
            itMax->second = i;
            itI->second = maxIndex;
            swap(heap[maxIndex], heap[i]);
            bubbleDown(maxIndex);
        }
    }

    template <typename T, typename dist_t>
    int MaxHeap<T, dist_t>::getMax(int i, int l, int r){
        dist_t ii = heap[i].getValue();
        dist_t li = heap[l].getValue();
        dist_t ri;
        if(r > heap.size()-1){
            ri = -1;
        }else ri = heap[r].getValue();
        if(ii >= li && ii >= ri){
            return i;
        }
        if(li >= ii && li >= ri){
            return l;
        }
        if(ri >= ii && ri >= li){
            return r;
        }
        else return i;
    }

}