#include <iostream>
#include <boost/filesystem.hpp>
#include <method/rng.h>
#include <method/small_world_rand.h>
#include <sstream>
#include <factory/method/small_world_rand.h>
#include <factory/method/rng.h>
#include <fstream>
#include <min_heap.h>
#include <max_heap.h>
#include "max_heap.cc"
#include "min_heap.cc"
#include <cmath>
#include <distcomp.h>
#include "omp.h"

using namespace similarity;
using namespace boost::filesystem;

int getK(const string loc) {
  int sum = 0;
  int k;
  std::ifstream inFile(loc);
  int total = 0;
  CHECK_MSG(inFile, "Cannot open file '" + loc + "' for reading");
  inFile.exceptions(std::ios::badbit);
  for (unsigned pass = 0; pass < 2; ++pass) {
    std::ifstream inFile(loc);
    CHECK_MSG(inFile, "Cannot open file '" + loc + "' for reading");
    inFile.exceptions(std::ios::badbit);
    size_t lineNum = 1;
    string methDesc;
    ReadField(inFile, METHOD_DESC, methDesc);
    lineNum++;
    //ReadField(inFile, "NN", NN_);
    //lineNum++;
    ReadField(inFile, "NN", k);
    return k;
  }
}

pair<float,int> getEdges(const string loc, int i){
  int sum = 0;
  int total=0;
  int k;
  std::ifstream inFile(loc);
  CHECK_MSG(inFile, "Cannot open file '" + loc + "' for reading");
  inFile.exceptions(std::ios::badbit);
  for (unsigned pass = 0; pass < 2; ++pass) {
    std::ifstream inFile(loc);
    CHECK_MSG(inFile, "Cannot open file '" + loc + "' for reading");
    inFile.exceptions(std::ios::badbit);
    size_t lineNum = 1;
    string methDesc;
    ReadField(inFile, METHOD_DESC, methDesc);
    lineNum++;
    //ReadField(inFile, "NN", NN_);
    //lineNum++;
    if(methDesc != "RNG")    ReadField(inFile, "NN", k);
    string line;
    while (getline(inFile, line)) {
      if (line.empty()) {
        lineNum++;
        break;
      }
      stringstream str(line);
      str.exceptions(std::ios::badbit);
      char c1, c2;
      IdType nodeID, objID;
      CHECK_MSG((str >> nodeID) && (str >> c1) && (str >> objID) && (str >> c2),
                "Bug or inconsitent data, wrong format, line: " + ConvertToString(lineNum)
      );
      CHECK_MSG(c1 == ':' && c2 == ':',
                string("Bug or inconsitent data, wrong format, c1=") + c1 + ",c2=" + c2 +
                " line: " + ConvertToString(lineNum)
      );
      int sumNeighbors=0;
      if (pass == 0) {
          sumNeighbors += 0;
      } else {
        IdType nodeFriendID;
        while (str >> nodeFriendID) {
          sumNeighbors += 1;
        }
      }
      sum += (sumNeighbors/i);
      ++lineNum;
      total = (int)nodeID;
    }
    //LOG(LIB_INFO) << "ElList size" << ElList_.size();
    inFile.close();
  }  //LOG(LIB_INFO) << "ElList size" << ElList_.size();
  total++;
  return make_pair((float)sum / (float) total, total);
}


int main(void) {
    cout << "média de arestas por grafo" << endl;
    std::ofstream outFile("/home/larissa/Documents/NonMetricSpaceLib/arestasUSCities.txt");
    path rn("/home/larissa/Documents/NonMetricSpaceLib/rng/");
    for (auto i = directory_iterator(rn); i != directory_iterator(); i++) {
        int sum = 0;
        string str = "/home/larissa/Documents/NonMetricSpaceLib/rng/" + i->path().filename().string();
        pair<float, int> media = getEdges(str, 2);
        outFile << "RNG" << "," << media.second << "," << media.first << ", " << 0 << endl;
    }
    //for arquivos do sw
    path sw("/home/larissa/Documents/NonMetricSpaceLib/sw/");
    for (auto i = directory_iterator(sw); i != directory_iterator(); i++) {
        string str = "/home/larissa/Documents/NonMetricSpaceLib/sw/" + i->path().filename().string();
        pair<float, int> media = getEdges(str, 1);
        outFile << "sw-graph" << "," << media.second << "," << media.first << ", " << getK(str)
                << endl;
    }
    //end for
    outFile.close();
    cout << "Done" << endl;
}


/*std::string remove_extension(const std::string& filename) {
    size_t lastdot = filename.find_last_of(".");
    if (lastdot == std::string::npos) return filename;
    return filename.substr(0, lastdot);
}


void writeFileGephiRNG(string loc, string out) {
    int sum = 0;
    int total = 0;
    int k;
    ifstream inFile(loc);
    CHECK_MSG(inFile, "Cannot open file '" + loc + "' for reading");
    inFile.exceptions(std::ios::badbit);
    for (unsigned pass = 0; pass < 2; ++pass) {
        ifstream inFile(loc);
        CHECK_MSG(inFile, "Cannot open file '" + loc + "' for reading");
        inFile.exceptions(std::ios::badbit);
        size_t lineNum = 1;
        string methDesc;
        ReadField(inFile, METHOD_DESC, methDesc);
        lineNum++;
        //size_t lineAfterReadField = lineNum;
        //ReadField(inFile, "NN", NN_);
        //lineNum++;
        if (methDesc != "RNG") ReadField(inFile, "NN", k);
        string line;
        ofstream outFile(out);
        CHECK_MSG(outFile, "Cannot open file '" + out + "' for writing");
        outFile.exceptions(std::ios::badbit);
//size_t lineNum = 0;

//WriteField(outFile, METHOD_DESC, StrDesc()); lineNum++;
//  WriteField(outFile, "NN", NN_); lineNum++;
        outFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                << "<gexf xmlns=\"http://www.gexf.net/1.2draft\" version=\"1.2\">\n"
                << "<meta>\n"
                << "<creator>nmslib.dummyapp</creator>\n"
                << "<description></description>\n"
                << "</meta>\n"
                << "<graph mode=\"static\" defaultedgetype=\"directed\">\n";
        outFile << "<nodes>\n";
        while (getline(inFile, line)) {
            if (line.empty()) {
                lineNum++;
                break;
            }
            stringstream str(line);
            str.exceptions(std::ios::badbit);
            char c1, c2;
            IdType nodeID, objID;
            CHECK_MSG((str >> nodeID) && (str >> c1) && (str >> objID) && (str >> c2),
                      "Bug or inconsitent data, wrong format, line: " + ConvertToString(lineNum)
            );
            CHECK_MSG(c1 == ':' && c2 == ':',
                      string("Bug or inconsitent data, wrong format, c1=") + c1 + ",c2=" + c2 +
                      " line: " + ConvertToString(lineNum)
            );
//outFile << nodeID << ":" << pNode->getData()->id() << ":";
            outFile << "<node id=\"" << nodeID
                    << "\" label=\"" << objID << "\" />\n";
        }
        outFile << "</nodes>\n";
        outFile << "<edges>\n";
        //LOG(LIB_INFO) << "ElList size" << ElList_.size();
        inFile.clear();
        inFile.seekg(0, ios::beg);
        lineNum = 1;
        methDesc;
        ReadField(inFile, METHOD_DESC, methDesc);
        lineNum++;
        int i = 0;
        while (getline(inFile, line)) {
            if (line.empty()) {
                lineNum++;
                break;
            }
            stringstream str(line);
            str.exceptions(std::ios::badbit);
            char c1, c2;
            IdType nodeID, objID;
            CHECK_MSG((str >> nodeID) && (str >> c1) && (str >> objID) && (str >> c2),
                      "Bug or inconsitent data, wrong format, line: " + ConvertToString(lineNum)
            );
            CHECK_MSG(c1 == ':' && c2 == ':',
                      string("Bug or inconsitent data, wrong format, c1=") + c1 + ",c2=" + c2 +
                      " line: " + ConvertToString(lineNum)
            );
            IdType nodeFriendID;
            str >> nodeFriendID;
            outFile << "<edge id=\"" << i++ << "\" source=\"" << nodeID << "\" "
                    << "target=\"" << nodeFriendID << "\" />\n";
        }
        outFile << "</edges>\n";
        outFile << "</graph>\n";
        outFile << "</gexf>";
        outFile.close();
        inFile.close();
    }
}

int main(void){
  cout << "writeGephi --> grafos" << endl;
  path rn("/home/larissa/Documents/NonMetricSpaceLib/rng/");
  for (auto i = directory_iterator(rn); i != directory_iterator(); i++) {
    int sum = 0;
    string str = "/home/larissa/Documents/NonMetricSpaceLib/rng/"+ i->path().filename().string();
    string strGephi = "/home/larissa/Documents/NonMetricSpaceLib/rng/" + remove_extension(i->path().filename().string()) + "gephi.gexf";
    writeFileGephiRNG(str, strGephi);
  }
string str = "/home/larissa/Documents/NonMetricSpaceLib/rng/colorHisto/600rng_l2_colorHistogramL2.index_0";
    string strGephi = remove_extension(str) + "gephi.gexf";
    writeFileGephiRNG(str, strGephi);
  //for arquivos do sw
  path sw("/home/larissa/Documents/NonMetricSpaceLib/sw/");
  for (auto i = directory_iterator(sw); i != directory_iterator(); i++) {
    string str = "/home/larissa/Documents/NonMetricSpaceLib/sw/"+ i->path().filename().string();
    string strGephi = "/home/larissa/Documents/NonMetricSpaceLib/sw/" + remove_extension(i->path().filename().string()) + "gephi.gexf";
    writeFileGephiSW(str, strGephi);
  }
 cout << "Done" << endl;

}*/



