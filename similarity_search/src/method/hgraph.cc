#include "space.h"
#include "knnquery.h"
#include "knnqueue.h"
#include "rangequery.h"
#include "ported_boost_progress.h"
#include "method/hgraph.h"
//#include "sort_arr_bi.h"

#include <vector>

#include <set>
#include <map>
#include <sstream>
#include <typeinfo>
#include <method/knng.h>
#include <method/rng.h>
#include <method/nndes.h>

#define MERGE_BUFFER_ALGO_SWITCH_THRESHOLD 100

#define USE_BITSET_FOR_INDEXING 1

#define RATE_OVERLAP 0.02

#define NOT_EXACT_BASE_GRAPH 0

#define DEFINED_LCOUNT_GRAPH 4

#define START_PIVOTS 0

#define WITH_LONG_EDGES 1

namespace similarity {

    template <typename dist_t>
    struct IndexThreadParamsHGraph {
        const Space<dist_t>&                        space_;
        HGraph<dist_t>&                             index_;
        const ObjectVector&                         data_;
        size_t                                      index_every_;
        size_t                                      out_of_;
        size_t                                      L_;
        size_t                                      S_;
        size_t                                      NN_;
        string                                      indexGraphType_;
        string                                      gtype_;
        float                                       overRate_;
        // float                                       eps_;
        int                                         nPivots_;
        vector<int>                                 pivotsTable_;
        int                                         lcount_;

        IndexThreadParamsHGraph(
                const Space<dist_t>&                        space,
                HGraph<dist_t>&                             index,
                const ObjectVector&                         data,
                size_t                                      index_every,
                size_t                                      out_of,
                size_t                  L, //number of levels to partition
                size_t                  S, //number of seeds
                size_t                  NN, //nn para quando escolher knng
                string                  indexGraphType,
                string                  gtype,
                float                   overRate,
                //float                   eps,
                int                     nPivots,
                vector<int>             pivotsTable,
                int                     lcount
        ) :
                space_(space),
                index_(index),
                data_(data),
                index_every_(index_every),
                out_of_(out_of),
                L_(L),
                S_(S),
                NN_(NN),
                indexGraphType_(indexGraphType),
                gtype_(gtype),
                overRate_(overRate),
                //    eps_(eps)
                nPivots_(nPivots),
                pivotsTable_(pivotsTable),
                lcount_(lcount)
        { }
    };

    template <typename dist_t>
    struct IndexThreadHGraph {
        void operator()(IndexThreadParamsHGraph<dist_t>& prm) {
            LOG(LIB_INFO) << "entrou funcao";
            /* for (size_t id = 0; id < prm.data_.size(); ++id) {
                 if (prm.index_every_ == id % prm.out_of_) {
                   //  HNode<dist_t>* node = new HNode<dist_t>(prm.data_[id], id);
                   //  prm.index_.add(node); //verificar aqui como adicionar
                     LOG(LIB_INFO) << "parallel Pivot::" << prm.nPivots_;
                     prm.index_.createIndexTDParallel(prm.lcount_, prm.pivotsTable_, prm.nPivots_);
                     LOG(LIB_INFO) << "parallel Pivot 2::" << prm.nPivots_;
                 }
             }*/
        }
    };


    template<typename dist_t>
    vector<int> HGraph<dist_t>::addNodes(){
        vector<int> pIndices;
        for(int i=0; i < data_.size();i++){
            addCriticalSection(new HNode<dist_t>(data_[i], i));
            pIndices.push_back(i);
        }
        return pIndices;
    }

    template<typename dist_t>
    void HGraph<dist_t>::addCriticalSection(HNode<dist_t> *node){
        ElList_.push_back(node);
    }

    template<typename dist_t>
    void HGraph<dist_t>::printSeeds(vector<int> seeds){
        for(int i=0; i < seeds.size();i++){
            LOG(LIB_INFO) << "i:" << i << " seeds:" << seeds[i] << "id:" << ElList_[seeds[i]]->getData()->id();
        }
    }

    template<typename dist_t>
    void HGraph<dist_t>::CreateIndex(const AnyParams &IndexParams)  {
        AnyParamManager  pmgr(IndexParams);
        pmgr.GetParamOptional("levels", L_, 2);
        pmgr.GetParamOptional("seeds", S_, 2);
        pmgr.GetParamOptional("graph",indexGraphType_,"knng");//mesmo nome que o mÃ©todo
        pmgr.GetParamOptional("overlap", overRate_,0.1);
        pmgr.GetParamOptional("eps", eps_, 0);
        pmgr.GetParamOptional("NN", NN_, 5);
        pmgr.GetParamOptional("indexThreadQty", indexThreadQty_, 0);
        pmgr.GetParamOptional("refine", gtype_, "knng");
        pmgr.CheckUnused();
        this->ResetQueryTimeParams();
        //vector<vector<int>> v(L_, vector<int>(1));
        //vector<set<int>> v(L_);
        //levelSeeds_ = v; //otimizar com alguma coisa do C++
        vector<int> pIndices = addNodes();//adiciona todos os nÃ³s no grafo
        if(indexThreadQty_ == 0){
            int lcount = 0;
            //inicializar Set X_
            // notSelected.insert(pIndices.begin(), pIndices.end());
            LOG(LIB_INFO) << "Top Down Construction";
            if(indexGraphType_=="knng" && data_.size()/S_ < NN_){
                LOG(LIB_WARNING) << "This choice of parameters may not give you the approximated graph wanted";
            }
            if(L_ == 1) createGraph(pIndices, 1);
            else{
                createIndexTD(lcount, pIndices, S_);
            }
        }else{
            int lcount=0;
            createIndexTDParallel(lcount, pIndices, S_);
            //  createIndexTd --> cada recursÃ£o um processo em paralelo --> numeros de threads -> numero de seeds
        }
#if WITH_LONG_EDGES
        if(L_ != 1){ //se 1 --> knng ou grqaph normal
            //for(int i=0; i < L_; i++){//ultimo nÃ£o precisa de refine
            //  printSeeds(levelSeeds_[L_-1]);
            //  refineGraph(levelSeeds_[i], i);
            // refineGraph(levelSeeds_[L_-1], L_-1, gtype_);
            refineGraphSet(levelSeeds_, L_ - 1, gtype_);
            // vector<int> seeds(levelSeeds_.begin(), levelSeeds_.end());
            // refineGraph(seeds, L_-1, gtype_);
            //}
        } //aqui refine graph*
#endif
        /*for(int i =0; i < ElList_.size(); i++){
            if(ElList_[i]->getNeighbors().size()==0){
                LOG(LIB_INFO) << "Ellist 0 id:" << i;
            }
        }*/
        //LOG(LIB_INFO) << "prune Edges";
        //pruneEdges();
        //printGraph("/home/larissa/Documents/NonMetricSpaceLib/hgraph/testover1h.txt");
        //   writeGephi("/home/larissa/Documents/NonMetricSpaceLib/hgraph/test500.gexf");
        // LOG(LIB_INFO) << "count Refine::::" << countRefine;
    }

    template<typename dist_t>
    void HGraph<dist_t>::createIndexTDParallel(int lcount, vector<int> X, int nP) {
        //bool base = true;
        lcount++;
        LOG(LIB_INFO) << "lcount" << lcount << " tam:" << X.size();
#if NOT_EXACT_BASE_GRAPH
        if (X.size() <= L_) {
            LOG(LIB_INFO) << "create graph";
            createGraph(X, lcount);
            LOG(LIB_INFO) << "create graph ok!";
            return;
        }
#else
        if(lcount == 4){
            LOG(LIB_INFO) << "create graph";
            createGraph(X, lcount);
            LOG(LIB_INFO) << "create graph ok!";
            return;
        }
#endif
        else {
            // base = false;
            vector<int> levelSeeds = chooseSeed(X, true, nP);//funcao para escolha de seeds
            // printSeeds(levelSeeds);
            insertSeeds(levelSeeds, lcount);//estrutura que guarda as seeds de cada nivel --> refine depois
            LOG(LIB_INFO) << levelSeeds_.size();
            //vector<vector<int>> pivotsTable = partitionSpaceOver(levelSeeds, X);
            //float over = adjustOverlap(overRate, lcount, X.size());
            if (nP == 1) { //se partiÃ§Ã£o vier apenas com um pivot --> colocar no minimo dois (forÃ§ar particionar)
                int proporcao = round((float) data_.size() / (pow(S_, lcount)));
                int numberPivots;
                if (proporcao == 0) numberPivots = 2;
                else numberPivots = round((float) X.size() / proporcao);
                if (numberPivots <= 0) numberPivots = 1;
                //LOG(LIB_INFO) << "number of pivots::: " << numberPivots;
                return createIndexTDParallel(lcount, X, numberPivots);
            }//nao precisa passar por todos os processos
            LOG(LIB_INFO) << "pivotsTable";
            vector<vector<int>> pivotsTable = partitionSpaceOverDist(levelSeeds, X, overRate_);
            //createGraph(levelSeeds, lcount);
            LOG(LIB_INFO) << "pivotsTable ok";
#if WITH_LONG_EDGES
            if (levelSeeds.size() > NN_) knngConstruction(levelSeeds, 2 * NN_, false, true);
            else knngConstruction(levelSeeds, NN_, false, true);//aqui knng construction nas seeds*/
#endif
            int proporcao = round((float) data_.size() / (pow(S_, lcount)));
            //LOG(LIB_INFO) << "proporcao:::" << proporcao;
            vector<int> nPivots(levelSeeds.size());
            for (int i = 0; i < levelSeeds.size(); i++) {
                int numberPivots;
                if (proporcao == 0) numberPivots = 2;
                else numberPivots = round((float) pivotsTable[i].size() / proporcao);
                if (numberPivots == 0) numberPivots = 1;
                nPivots[i] = numberPivots;
            }
            LOG(LIB_INFO) << "antes thread";
            vector<thread>   threads(levelSeeds.size());
            vector<shared_ptr<IndexThreadParamsHGraph<dist_t>>>   threadParamsHGraph;
            for (size_t i = 0; i < levelSeeds.size(); ++i) { //arrumar threadParams
                threadParamsHGraph.push_back(shared_ptr<IndexThreadParamsHGraph<dist_t>>(new IndexThreadParamsHGraph<dist_t>(space_, *this, data_, i, levelSeeds.size(),
                                                                                                                             L_, S_, NN_, indexGraphType_, gtype_, overRate_, nPivots[i], pivotsTable[i], lcount)));
            }
            LOG(LIB_INFO) << "declaracao ok";
            for (size_t i = 0; i < levelSeeds.size(); ++i) {
                LOG(LIB_INFO) << "ok:::" << i;
                threads[i] = thread(IndexThreadHGraph<dist_t>(), ref(*threadParamsHGraph[i]));
            }
            for (size_t i = 0; i < levelSeeds.size(); ++i) {
                threads[i].join();
            }
        }
    }


    template <typename dist_t>
    void HGraph<dist_t>::createIndexTD(int lcount, vector<int> X, int nP) {
        //bool base = true;
        lcount++;
        LOG(LIB_INFO) << "lcount" << lcount << " tam:" << X.size();
#if NOT_EXACT_BASE_GRAPH
        if (X.size() <= L_) {
            LOG(LIB_INFO) << "create graph";
            createGraph(X, lcount);
            LOG(LIB_INFO) << "create graph ok!";
            return;
        }
#else
        if(lcount == DEFINED_LCOUNT_GRAPH){
            LOG(LIB_INFO) << "create graph";
            createGraph(X, lcount);
            LOG(LIB_INFO) << "create graph ok!";
            return;
        }
#endif
        else {
            // base = false;
            vector<int> levelSeeds = chooseSeed(X, true, nP);//funcao para escolha de seeds
            // printSeeds(levelSeeds);
            insertSeeds(levelSeeds, lcount);//estrutura que guarda as seeds de cada nivel --> refine depois
            LOG(LIB_INFO) << levelSeeds_.size();
            //vector<vector<int>> pivotsTable = partitionSpaceOver(levelSeeds, X);
            //float over = adjustOverlap(overRate, lcount, X.size());
            if (nP == 1) { //se partiÃ§Ã£o vier apenas com um pivot --> colocar no minimo dois (forÃ§ar particionar)
                int proporcao = round((float) data_.size() / (pow(S_, lcount)));
                int numberPivots;
                if (proporcao == 0) numberPivots = 2;
                else numberPivots = round((float) X.size() / proporcao);
                if (numberPivots <= 0) numberPivots = 1;
                //LOG(LIB_INFO) << "number of pivots::: " << numberPivots;
                return createIndexTD(lcount, X, numberPivots);
            }//nao precisa passar por todos os processos --> apenas uma partiÃ§Ã£o
            LOG(LIB_INFO) << "pivotsTable";
            vector<vector<int>> pivotsTable = partitionSpaceOverDist(levelSeeds, X, overRate_);
            //createGraph(levelSeeds, lcount);
            LOG(LIB_INFO) << "pivotsTable ok";
#if WITH_LONG_EDGES
            if (levelSeeds.size() > NN_) knngConstruction(levelSeeds, 2 * NN_, false, true);
            else knngConstruction(levelSeeds, NN_, false, true);//aqui knng construction nas seeds*/
#endif
            //completeGraph(levelSeeds);
            //printSeeds(levelSeeds);
            // refine(); -->refinar as seeds com outras partiÃ§Ãµes
            /*estimar numero de pivots para cada partiÃ§Ã£o*/
            //------
            //proporÃ§Ã£o para X.size()
            int proporcao = round((float) data_.size() / (pow(S_, lcount)));
            //LOG(LIB_INFO) << "proporcao:::" << proporcao;
            vector<int> nPivots(levelSeeds.size());
            for (int i = 0; i < levelSeeds.size(); i++) {
                int numberPivots;
                if (proporcao == 0) numberPivots = 2;
                else numberPivots = round((float) pivotsTable[i].size() / proporcao);
                if (numberPivots == 0) numberPivots = 1;
                nPivots[i] = numberPivots;
            }
            for (int i = 0; i < levelSeeds.size(); i++) {
                //createGraph(pivotsTable[i], lcount);
                //LOG(LIB_INFO) << "size:" << pivotsTable[i].size() << " npivots" << nPivots[i] << " lcount:" << lcount;
                createIndexTD(lcount, pivotsTable[i], nPivots[i]);//recursivo
            }
        }
        /*if(!base){
            LOG(LIB_INFO) << "Base ok";
            for(int i=0; i < L_; i++){//ultimo nÃ£o precisa de refine
                refineGraph(levelSeeds_[i], i);
            }
        }*/
    }

    template<typename dist_t>
    vector<int> HGraph<dist_t>::chooseSeed(vector<int> X, bool useRandom, int S) {
//        LOG(LIB_INFO) << X.size();
        vector<int> seeds;
        set<int> randomNum;
        srand(time(NULL));
        int i = 0;
        if(X.size() == S ) return X;
        if(X.size() != data_.size()){
            seeds.push_back(X[0]);//garantir a primeira seed de antes tambÃ©m
            S = S -1;
        }
        /* if(useRandom) {
             while (i < S) {
                 int id = rand() % X.size();
                 if (X[id] != X[0]) {
                     seeds.push_back(X[id]);
                     X.erase(X.begin() + id);
                     i++;
                 }
             }*/
        if(useRandom){
            while(randomNum.size() < S){
                int id = rand() % X.size();
                if(id != 0) randomNum.insert(id);
            }
            set<int>::iterator it = randomNum.begin();
            while(it != randomNum.end()){
                LOG(LIB_INFO) << "randomInt:" << *it;
                seeds.push_back(X[*it]);
                it++;
            }
        }else{
            // seeds = BPivotPosition(X);
            seeds = kmedoids(X, 100);
            //omni pivot
        }
        return seeds; //retorna o id das seeds na ElList
    }


    template<typename dist_t>
    vector<dist_t> totalCost(vector<vector<pair<int,dist_t>>> pivotsTable){
        vector<dist_t> cost;
        for(vector<pair<int,dist_t>> v : pivotsTable){
            dist_t sum = 0;
            for(pair<int, dist_t> p : v){
                sum += p.second;
            }
            cost.push_back(sum);
        }
        return cost;
    }

    template<typename dist_t>
    vector<int> newSeeds(vector<vector<pair<int,dist_t>>> pivotsTable, vector<int> medoids){
        srand(time(NULL));
        int id = rand() % medoids.size();
        int size = pivotsTable[id].size();
        int id2 = rand() % size;
        medoids[id] = pivotsTable[id][id2].first;
        return medoids;
    }


    template<typename dist_t>
    vector<int> HGraph<dist_t>::kmedoids(std::vector<int> X, int iterations) {
        vector<int> medoids;
        vector<vector<pair<int,dist_t>>> pivotsTable;
        vector<vector<pair<int,dist_t>>> bestPivotsTable;
        bool initialMedoids = true;
        bool change = true;
        dist_t pastCost = 0;
        int it = 0;
        vector<int> bestMedoids;
        while(it < iterations){
            //   LOG(LIB_INFO) << "aqui";
            if(initialMedoids){
                medoids = chooseSeed(X, true, (int)S_);
                bestMedoids = medoids;
            }else{
                //change one medoid
                medoids = newSeeds(bestPivotsTable, bestMedoids);
            }
            //associate medoids with points
            //    LOG(LIB_INFO) << "aqui 2";
            //      LOG(LIB_INFO) << medoids.size();
            pivotsTable = partitionSpaceMedoids(medoids, X);
            if(initialMedoids) bestPivotsTable = pivotsTable;
//            LOG(LIB_INFO) << "aqui 3";
            vector<dist_t> cost = totalCost(pivotsTable);
            dist_t totalSum  = 0;
            for(int i=0; i < cost.size(); i++){
                totalSum += cost[i];
            }
            if(totalSum - pastCost < 0){
                bestMedoids = medoids;
                bestPivotsTable = pivotsTable;
            }
            initialMedoids = false;
            pastCost = totalSum;
            it++;
        }
        return bestMedoids;
    }

    template <typename dist_t>
    vector<vector<pair<int,dist_t>>> HGraph<dist_t>::partitionSpaceMedoids(vector<int> seeds, vector<int> X){
        vector<vector<pair<int,dist_t>>> pivotsTable(seeds.size(), vector<pair<int, dist_t>>(1, make_pair(0,0)));
//        LOG(LIB_INFO) << seeds.size();
        for(int i=0; i < seeds.size();i++){
            //  LOG(LIB_INFO) << i;
            pivotsTable[i][0] = make_pair(seeds[i],0);
        }
        for(int x : X){
            dist_t menor = INFINITY;
            int pos = 0;
            bool equal = false; //para nao inserir igual a seed
            for(int j=0; !equal && j < seeds.size();j++){
                if(x == seeds[j]){
                    equal = true;
                }
                dist_t pj = space_.IndexTimeDistance(ElList_[seeds[j]]->getData(), ElList_[x]->getData());
                if(pj < menor){
                    menor = pj;
                    pos = j;
                }
            }
            if(!equal){
                //LOG(LIB_INFO) << "ppos" << pivotsTable.size();
                pivotsTable[pos].push_back(std::make_pair(x,menor));
                //LOG(LIB_INFO) << "pairs 2";
            }
        }
        return pivotsTable;
    }

    template<typename dist_t>
    vector<int> HGraph<dist_t>::BPivotPosition(std::vector<int> X) {
        int n_l;
        if(X.size() < S_*NN_){
            n_l = X.size();
        }else n_l = S_*NN_;
        int i=0;
        vector<int> seeds;
        seeds.push_back(X[0]);//garantir a primeira seed de antes tambÃ©ms
        while (i < n_l - 1) {
            int id = rand() % X.size();
            if (X[id] != X[0]) {
                seeds.push_back(X[id]);
                X.erase(X.begin() + id);
                i++;
            }
        }
        while(seeds.size() > S_){
            // LOG(LIB_INFO) << "dentro while" << seeds.size();
            vector<dist_t> sum(seeds.size(), 0);
            vector<vector<dist_t>> distMat(seeds.size(), vector<dist_t>(X.size()));
            for(int i=0; i < X.size(); i++){
                for(int j=0; j < seeds.size(); j++){
                    distMat[j][i] = space_.IndexTimeDistance(ElList_[i]->getData(), ElList_[seeds[j]]->getData());
                    sum[j] += distMat[j][i];
                }
            }
            //calcular avg standar deviation
            dist_t menor = INFINITY;
            int p = 0;
            for(int j=0; j < seeds.size();j++){
                dist_t avg = sum[j]/X.size();
                dist_t std=0;
                for(int i=0; i < X.size(); i++){
                    std += pow(distMat[j][i] - avg, 2);
                }
                std = std/(X.size()-1);
                std = sqrt(std);
                if(std < menor){
                    p = j;
                }
            }
            //LOG(LIB_INFO) << "p::" << p << " seeds::" << seeds.size();
            seeds.erase(seeds.begin() + p);
            sum.clear();
            distMat.clear();
        }
        return seeds;
    }


    template <typename dist_t>
    vector<int> HGraph<dist_t>::getPivots(vector<vector<int>> pivotsTable){
        vector<int> pivots;
        for(int i=0; i < S_;i++){
            pivots.push_back(pivotsTable[i][0]);
        }
        //LOG(LIB_INFO) << "size::" << pivotsTable.size();
        return pivots;
    }

    template<typename dist_t>
    vector<int> HGraph<dist_t>::appendPivots(std::vector<int> Xs, std::vector<std::vector<int>> pivotsTable) {
        for(vector<int> p: pivotsTable){
            Xs.push_back(p[0]);
        }
        return Xs;
    }

    template<typename dist_t>
    vector<int> HGraph<dist_t>::appendOverlap(vector<int> Xs, float overlapRate, vector<vector<int>> pivotsTable) {
        for(vector<int> p : pivotsTable){
            Xs.push_back(p[0]);
            int numberOverlap = ceil(overlapRate*p.size());
            int i=0;
            p.erase(p.begin()); //erase local
            if(p.size() >= 1){
                vector<int> overlap = sample(p, numberOverlap);
                Xs.insert(Xs.end(), overlap.begin(),overlap.end());
            }
        }
        return Xs;
    }

    template<typename dist_t>
    vector<int> HGraph<dist_t>::sample(vector<int> p, int number) {
        vector<int> overlap;
        srand(time(NULL));
        for(int i=0; i < number;i++){
            int id = rand()%p.size();
            overlap.push_back(p[id]);
            p.erase(p.begin()+id);
        }
        return overlap;
    }

    template <typename dist_t>
    vector<vector<int>> HGraph<dist_t>::partitionSpace(vector<int> seeds, vector<int> X){
        vector<vector<int>> pivotsTable(seeds.size(), vector<int>(1));
        for(int i=0; i < seeds.size();i++){
            pivotsTable[i][0] = seeds[i];
        }
        for(int x : X){
            dist_t menor = INFINITY;
            int pos = 0;
            bool equal = false; //para nao inserir igual a seed
            for(int j=0; !equal && j < seeds.size();j++){
                if(x == seeds[j]){
                    equal = true;
                }
                dist_t pj = space_.IndexTimeDistance(ElList_[seeds[j]]->getData(), ElList_[x]->getData());
                if(pj < menor){
                    menor = pj;
                    pos = j;
                }
            }
            if(!equal) pivotsTable[pos].push_back(x);
        }
        return pivotsTable;
    }

    template <typename dist_t>
    vector<vector<int>> HGraph<dist_t>:: partitionSpaceOverDist(vector<int> seeds, vector<int> X, float over){
        vector<vector<int>> pivotsTable(seeds.size(), vector<int>(1));//, vector<int>(1));
        //calcular dist pairwise pivots
        //seeds not repeated -- ok!
        dist_t pivotsDist[seeds.size()][seeds.size()];
        //int avgSizeOverlap = (int) ceil(((float)X.size()/seeds.size())*overRate_);
        //if(avgSizeOverlap == 0) avgSizeOverlap = 1;
        int avgSize = ceil(X.size()*over);
        if(avgSize <= 0) avgSize = 1;
        //vector<int> overLapSize(seeds.size(), 0);
        for(int i=0; i < seeds.size();i++){
            pivotsTable[i][0] = seeds[i]; //inicializar o primeiro elemento como o pivÃ´ da subdivisÃ£o
            //  LOG(LIB_INFO) << "seeds i:" << i << " value:" << seeds[i];
            /*for(int j=0; j < i; j++){
                //if(pivotsDist[i][j] >= 0) continue;
                if(i==j){
                    pivotsDist[i][j] = 0;
                    pivotsDist[j][i] = 0;
                }else{
                    dist_t d = space_.IndexTimeDistance(ElList_[seeds[i]]->getData(), ElList_[seeds[j]]->getData());
                    pivotsDist[i][j] = d;
                    pivotsDist[j][i] = d;
                    //LOG(LIB_INFO) << "i:" << i << " j:" << j << "dist" << pivotsDist[j][i];
                }
            }*/
        }//matriz de distancias entre pivos

        /*for(int i=0; i < seeds.size(); i++){
            for(int j=0; j < seeds.size(); j++){
                LOG(LIB_INFO) << "i:" << i << " j:" << j << "dist" << pivotsDist[i][j];
            }
        }*/
        //vector<priority_queue<pair<int,dist_t>,vector<pair<int,dist_t>>, EvaluatedPair<dist_t>> > overlapHeaps(seeds.size());
        /*for(int i=0; i < seeds.size(); i++){
            LOG(LIB_INFO) << "Size pivotsTable i" << i << ":::" << pivotsTable[i].size();
        }*/
        //ate aqui ok!!
        vector<priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPair<dist_t> >> resultSet(seeds.size());
        //vector<priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPairMin<dist_t> >> resultSet(seeds.size());
        //matriz de priority queue --> ordenar por diferenca de distancia
        //vector<vector<dist_t>> distTableX(X.size(), vector<dist_t>(seeds.size()));
        //matriz de distancia x
        vector<pair<int,dist_t>> PivotsX(X.size());
        //matriz de pares pivo e distancia seguindo a ordem de x
        int t=0;
        for(int x : X){
            vector<dist_t> xSeeds(seeds.size(), 0);
            //dist_t xSeeds[seeds.size()];
            dist_t menor = INFINITY;
            int pos = 0;
            // bool equal = false;
            for(int j=0; j < seeds.size();j++){
                //  equal = false;
                if(x == seeds[j]){
                    // equal = true;
                    xSeeds[j] = 0;
                    //distTableX[t][j] = -1; //senÃ£o distancia dele com ele mesmo entra como overlap
                    //-1 para nao verificar
                    //distTableX[j][t] = 0;
                }else{
                    dist_t pj = space_.IndexTimeDistance(ElList_[seeds[j]]->getData(), ElList_[x]->getData());
                    xSeeds[j] = pj;
                    //distTableX[t][j] = pj;
                    //distTableX[j][t] = pj;
                    if(pj <= menor){
                        menor = pj;
                        pos = j;
                    }
                }
            }//calcula qual o pivo mais proximo, salva a distancia para todos os pivos para xSeeds
            PivotsX.push_back(make_pair(pos, menor));//par com o pivo e a distancia para o pivo
            //if(!equal){
            if(x != seeds[pos]){ //igual a seed ja foi adicionado
                pivotsTable[pos].push_back(x); //adiciona no seu hiperplano -> pos
                //distTableX[t][pos] = -1; //ja adicionou, nao precisa verificar de novo
                //distTableX[pos][t] = -1;
            }
            for(int i=0; i < xSeeds.size(); i++){
                if(xSeeds[i] != 0 && i != pos){
                    resultSet[i].push(make_pair(x, menor - xSeeds[i])); //constroi priority queue com a diferenca das distancias
                }
            }
            t++;
        }//particionado + calculado a diferencia das distancias
        /*LOG(LIB_INFO) << distTableX.size();
        for(int i=0; i < X.size(); i++){
            for(int j=0; j < seeds.size(); j++){
                LOG(LIB_INFO) << "i::" << i << " j::" << j << " dist::" << distTableX[i][j];
            }
        }*/
        //atÃ© aqui ok!!!! -- overlap tambÃ©m funcionando
        //  LOG(LIB_INFO) << "ok1";
        /* for(int i=0; i < seeds.size(); i++){
             LOG(LIB_INFO) << "i:::::::" << i ;
             printSeeds(pivotsTable[i]);
         }*/
        //:::::como calcular o overlap::::
        /*vector<int> numberElements(seeds.size());
        for(int i=0; i < seeds.size(); i++){
            int n = ceil(pivotsTable[i].size()*over);
            if(n > 1) numberElements[i] = n;
            else numberElements[i] = 1;
        }*/
        for(int i =0; i < seeds.size();i++){
            //for(int j=0; j < numberElements[i]; j++){
            for(int j=0; j < avgSize ; j++){
                pair<int, dist_t> value = resultSet[i].top();
                resultSet[i].pop();
                // LOG(LIB_INFO) << "i::" << i << " value::" << value.first << " dist::" << value.second;
                pivotsTable[i].push_back(value.first);
                //  LOG(LIB_INFO) << "ok!";
            }
            vector<int>::iterator ip = std::unique(pivotsTable[i].begin(), pivotsTable[i].end());
            pivotsTable[i].resize(std::distance(pivotsTable[i].begin(), ip));
        }
        resultSet = vector<priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPair<dist_t> >>();
        return pivotsTable;
    }

    template <typename dist_t>
    vector<vector<int>> HGraph<dist_t>::partitionSpaceOver(vector<int> seeds, vector<int> X){
        vector<vector<int>> pivotsTable(seeds.size(), vector<int>(1));
        for(int i=0; i < seeds.size();i++){
            pivotsTable[i][0] = seeds[i];
        }
        int avgSizeOverlap = ceil(((float)X.size()/seeds.size())*overRate_);
        vector<priority_queue<pair<int,dist_t>,vector<pair<int,dist_t>>, EvaluatedPair<dist_t>> > overlapHeaps(seeds.size());
        for(int x : X){
            dist_t menor = INFINITY;
            int pos = 0;
            int pos2 = 0; //segunda seed mais perto
            int seeds2 = seeds[0];
            dist_t menor2 = INFINITY;
            bool equal = false; //para nao inserir igual a seed
            for(int j=0; !equal && j < seeds.size();j++){
                if(x == seeds[j]){
                    equal = true;
                }
                dist_t pj = space_.IndexTimeDistance(ElList_[seeds[j]]->getData(), ElList_[x]->getData());
                if(pj < menor){
                    //LOG(LIB_INFO) << "pos" << pos << " pos2" << pos2;
                    menor2 = menor;
                    seeds2 = seeds[j];
                    pos2 = pos;//atualiza o segundo menor para overlap
                    menor = pj;
                    pos = j;
                }
            }
            if(!equal){
                pivotsTable[pos].push_back(x);
                if(avgSizeOverlap > 0 && pos2 != pos){ //cria uma overlapHeap de size para cada pos
                    if(overlapHeaps[pos2].size() < avgSizeOverlap){
                        overlapHeaps[pos2].push(pair<int,dist_t>(x,menor2));
                    }else if(overlapHeaps[pos2].top().second >= menor2){
                        overlapHeaps[pos2].pop();
                        overlapHeaps[pos2].push(pair<int,dist_t>(x,menor2));
                    }
                }
                //LOG(LIB_INFO) << "pivots Table: pos"<<pos;
                //printSeeds(pivotsTable[pos]);
            }
        }
        for(int i=0; i < seeds.size();i++){
            // printSeeds(pivotsTable[i]);
            while(overlapHeaps[i].size() > 0){ //adiciona no subset
                pivotsTable[i].push_back(overlapHeaps[i].top().first);
                overlapHeaps[i].pop();
            }
            //pivotsTable[i].insert(pivotsTable[i].end(), overlapHeaps[i].begin().first, overlapHeaps[i].end().first);
        }
        return pivotsTable;
    }

    template<typename dist_t>
    void HGraph<dist_t>::refineGraphSet(set<int> levelSeeds, int lcount, string gtype) {
        countRefine = 0;
        priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPair<dist_t> > resultSet;
        int nNeighbors = NN_;
        /*if(levelSeeds_.size() <= NN_){

        }*/
        for (auto idAtual : levelSeeds_) {
            for (auto i : levelSeeds_) {
                if (i == idAtual) continue;//&& !ElList_[idAtual]->isNeighbor(ElList_[i])
                dist_t d = space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData());
                if (resultSet.size() < nNeighbors) {
                    // LOG(LIB_INFO) << "add n neighbors";
                    resultSet.push(make_pair(i, d));
                } else {
                    if (resultSet.top().second > d) {
                        resultSet.pop();
                        resultSet.push(make_pair(i, d));
                    }//else delete evNode;
                }
            }
            for (int i = 0; i < nNeighbors; i++) {
                pair<int, dist_t> resultNode = resultSet.top();
                resultSet.pop();
                //link(ElList_[idAtual], ElList_[resultNode.first], false);
                link(ElList_[idAtual], ElList_[resultNode.first], false, resultNode.second,true);//nao direcionado para a seed
                countRefine++;
            }
            // LOG(LIB_INFO) << "idAtual 2::" << idAtual;
            resultSet = priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPair<dist_t> >();
        }
    }


    template<typename dist_t>
    void HGraph<dist_t>::refineGraph(std::vector<int> levelSeeds, int lcount, string gtype) {
        //LOG(LIB_INFO) << "GraphSeeds:::" << levelSeeds.size() << " Level:" << lcount;
        //SeedsRNG(levelSeeds); //occlusion rule in fanng como no rng (rng entre os nao conectados)
        LOG(LIB_INFO) << "::::::SEEDS::::::" << levelSeeds.size();
        //printSeeds(levelSeeds);
        LOG(LIB_INFO) << ":::::REFINE GRAPH:::::";
        if (lcount < 1) lcount = 1;
        if(gtype == "knng"){
            //int upperK = NN_;//((data_.size()/S_)/lcount);
            int upperK = NN_;
            knngConstruction(levelSeeds, upperK, false, true);
        }else if(gtype == "krng"){
            int upperK = NN_;//((data_.size()/S_)/lcount);
            krngConstruction(levelSeeds, upperK, true);
        }else if(gtype == "rng"){
            SeedsRNG(levelSeeds, true);
        }
        //krngConstruction(levelSeeds, NN_);
        //completeGraph(levelSeeds);
    }


    template <typename dist_t>
    void HGraph<dist_t>::pruneEdges() {
        //eliminar edges --> se 10NN elimina os outros
        // HNodeEvalCompare<dist_t> compareFunction;
        for(HNode<dist_t>* v : ElList_){
            LOG(LIB_INFO) << "prune edges of v::" << v->getId();
            if(levelSeeds_.find(v->getId()) != levelSeeds_.end()){
                continue;
            }
            //para cada vÃ©rtice: ordenar as arestas pela distancia e manter apenas as NN_ mais proximas
            //vector<HNode_eval<dist_t>*> neighbors = v->getNeighbors();
            //sort(neighbors.begin(), neighbors.end(), HNodeEvalCompare<dist_t>());
            //LOG(LIB_INFO) << "ok!!!";
            //apenas para teste
            //neighbors.erase(neighbors.begin() + (int)NN_, neighbors.end());
            //LOG(LIB_INFO) << "neighbors size " << neighbors.size();
            //v->setNeighbors(neighbors);
            //v->getNeighbors().erase(v->getNeighbors().begin()+(int)NN_, v->getNeighbors().end());
            //v->eraseNeighbors((int)NN_);
            int i=0;
            for(HNode_eval<dist_t>* n : v->getNeighbors()){
                LOG(LIB_INFO) << "Neighbors:: " << n->getHNode()->getId() << " distance:" << n->getDistance() ;
                i++;
            }
        }
        return;
    }


    template<typename dist_t>
    void HGraph<dist_t>::SeedsRNG(vector<int> seeds, bool isPivots){
        for(int idAtual : seeds){
            for(int i: seeds){
                if(i != idAtual){
                    if(!ElList_[idAtual]->isNeighbor(ElList_[i])){ //caso nÃ£o tiver nenhuma conexao verifica
                        dist_t d = space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData());
                        pair<int, dist_t> evNode = make_pair(i, d);
                        bool connect = shouldConnect(evNode, idAtual, seeds);
                        if(connect){
                            link(ElList_[evNode.first], ElList_[idAtual], false, d, isPivots);
                        }
                    }
                }
            }
        }
    }


    template<typename dist_t>
    void HGraph<dist_t>::insertSeeds(std::vector<int> seeds, int lcount) {
        for(int s : seeds){
            levelSeeds_.insert(s);
            //levelSeeds_[lcount].push_back(s);
        }
    }


    template<typename dist_t>
    vector<int> HGraph<dist_t>::divideIndices(vector<int> X, int seedId, int size) {
        vector<int> Xs;
        while(Xs.size() <= size){
            int randId = RandomInt()%X.size();
            Xs.push_back(X[randId]);
            notSelected.erase(X[randId]);
            selected.insert(X[randId]);
            X.erase(X.begin()+randId); //erase aqui permite overlap para outras regiÃµes --tratamento de bordas?
        }
        return Xs;
    }


    /*template <typename dist_t>
    vector<int> HGraph<dist_t>::divideIndicesSeeds(vector<int> X, int seedId, int size) {
        vector<int> Xs;
        priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPair<dist_t> > resultSet;

        //ideias com random escolhe 4 seeds, um for para calcular a dist de cada elemento para cada seed
        //particionar pelo os que estao mais perto de cada seed
        //kmeans?
        return Xs;
    }*/


    //pega uma seed e particiona por treshold (maior dist /numero de seeds) --> problema sizes diferentes em particoes

    template<typename dist_t>
    void HGraph<dist_t>::createGraph(vector<int> Xs, int lcount) {
        //um caso para cada tipo de grafo
        LOG(LIB_INFO) << "tamanho:::" << Xs.size();
        if(Xs.size() <= 1) return;
        //LOG(LIB_INFO) << "LCOUNT:" << lcount << " Xs Size:" << Xs.size();
        if(indexGraphType_=="knng"){
            //   LOG(LIB_INFO) << "knng";
            knngConstruction(Xs, NN_, true, false);
            //knngConstruction(Xs, NN_, false);
            //LOG(LIB_INFO) << "knng done";
            //funcao para construir knng local
        }else if(indexGraphType_ == "rng"){
            //funcao para construir rng
            rngConstruction(Xs, false);
        }
    }

    template<typename dist_t>
    void HGraph<dist_t>::link(HNode<dist_t>* node1, HNode<dist_t>* node2, bool dir, dist_t distance, bool isPivot) {
        if(isPivot){
            if(dir){
                node1->addNeighbor(node2, true, distance); //direcionado
            }else{
                node1->addNeighbor(node2, true, distance); //direcionado
                node2->addNeighbor(node1, true, distance); //direcionado
            }
        }else{
            if(dir){
                //node1->addNeighbor(node2, true, distance); //direcionado
                node1->addNeighborLower(node2, true, distance, NN_*(1+eps_));
            }else{
                // node1->addNeighbor(node2, true, distance); //direcionado
                node1->addNeighborLower(node2, true, distance, NN_*(1+eps_));
                //node2->addNeighbor(node1, true, distance); //direcionado
                node2->addNeighborLower(node1, true, distance, NN_*(1+eps_));
            }
        }
    }

    template<typename dist_t> //min size tem que ser NN
    void HGraph<dist_t>::knngConstruction(vector<int> Xs, size_t k, bool dir, bool isPivot) {
        priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPair<dist_t> > resultSet;
        int nNeighbors = k;
        //LOG(LIB_INFO) << "Number of elements:" << Xs.size();
        if(Xs.size()-1 == k){
            completeGraph(Xs);
            return;
        }
        if(Xs.size() <= k){
            //nNeighbors = Xs.size()-1;
            completeGraph(Xs);
            return;
        }
        //LOG(LIB_INFO) << "xxxx";
        for(int idAtual : Xs){
            for (int i : Xs) {
                // LOG(LIB_INFO) << "no loop::" << i;
                // if(i==idAtual) printSeeds(Xs);
                if (i == idAtual) continue;//&& !ElList_[idAtual]->isNeighbor(ElList_[i])
                dist_t d = space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData());
                if(resultSet.size() < nNeighbors){
                    // LOG(LIB_INFO) << "add n neighbors";
                    resultSet.push(make_pair(i, d));
                } else {
                    if (resultSet.top().second > d) {
                        resultSet.pop();
                        resultSet.push(make_pair(i, d));
                    }//else delete evNode;
                }
            }
            //LOG(LIB_INFO) << "idAtual::" << idAtual;
            //LOG(LIB_INFO) << "nNeighbors" << resultSet.size();
            for (int i = 0; i < nNeighbors; i++) {
                pair<int, dist_t> resultNode = resultSet.top();
                resultSet.pop();
                //link(ElList_[idAtual], ElList_[resultNode.first], false);
                if(idAtual==Xs[0]){
                    link(ElList_[idAtual], ElList_[resultNode.first], false, resultNode.second, isPivot);//nao direcionado para a seed
                }else{
                    link(ElList_[idAtual], ElList_[resultNode.first], dir, resultNode.second, isPivot);//de acordo com dir para outro
                }
            }
            // LOG(LIB_INFO) << "idAtual 2::" << idAtual;
            resultSet = priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPair<dist_t> >();
        }
    }

    template<typename dist_t>
    void HGraph<dist_t>::completeGraph(vector<int> Xs){
        for(int idAtual : Xs){
            for(int i: Xs){
                if(i != idAtual){
                    link(ElList_[idAtual], ElList_[i], true, space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData()), false);
                }
            }
        }
    }

    template<typename dist_t>
    void HGraph<dist_t>::rngConstruction(vector<int> Xs, bool isPivot) {
        for(int idAtual : Xs){
            for(int i: Xs){
                if(i != idAtual){
                    HNode<dist_t> *evaluated = ElList_[i];
                    dist_t d = space_.IndexTimeDistance(ElList_[idAtual]->getData(), evaluated->getData());
                    pair<int, dist_t> evNode = make_pair(i, d);
                    bool connect = shouldConnect(evNode, idAtual, Xs);
                    if(connect){
                        link(ElList_[evNode.first], ElList_[idAtual], false, evNode.second,isPivot);
                    }
                }
            }
        }
    }

    template<typename dist_t>
    void HGraph<dist_t>::krngConstruction(std::vector<int> Xs, size_t k, bool isPivot) {
        LOG(LIB_INFO) << Xs.size();
        for(int idAtual : Xs){
            for(int i: Xs){
                if(i != idAtual && !ElList_[idAtual]->isNeighbor(ElList_[i])){
                    HNode<dist_t> *evaluated = ElList_[i];
                    dist_t d = space_.IndexTimeDistance(ElList_[idAtual]->getData(), evaluated->getData());
                    pair<int, dist_t> evNode = make_pair(i, d);
                    bool connect = shouldConnectK(evNode, idAtual, Xs, k);
                    if(connect){
                        // LOG(LIB_INFO) << "Link krng:" << evNode.first << "--" << idAtual;
                        link(ElList_[evNode.first], ElList_[idAtual], false, evNode.second, isPivot);
                    }
                }
            }
        }
    }


    template<typename dist_t>
    bool HGraph<dist_t>::shouldConnect(pair<int, dist_t> evNode, int idAtual, vector<int> Xs){
        if(idAtual != evNode.first){
            for(int i : Xs){
                dist_t dNewElement = space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData());
                dist_t dEvaluated = space_.IndexTimeDistance(ElList_[evNode.first]->getData(), ElList_[i]->getData());
                if(dNewElement < evNode.second && dEvaluated < evNode.second){
                    return false;
                    break;
                }
            }
            return true;
        }
        return false;
    }

    template<typename dist_t>
    bool HGraph<dist_t>::shouldConnectK(pair<int, dist_t> evNode, int idAtual, vector<int> Xs, int K){
        if(idAtual != evNode.first){
            int count = 0;
            for(int i : Xs){
                dist_t dNewElement = space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData());
                dist_t dEvaluated = space_.IndexTimeDistance(ElList_[evNode.first]->getData(), ElList_[i]->getData());
                if(dNewElement < evNode.second && dEvaluated < evNode.second){
                    count++;
                    if(count > K) return false;
                    //cpnta numero de k
                    //return false;
                    //break;
                }
            }
            if (count <= K) return true;
            else return false;
        }
        return false;
    }

    template <typename dist_t>
    void HGraph<dist_t>::Search(RangeQuery<dist_t>* query, IdType) const {
        if (bDoSeqSearch_) {
            for (size_t i = 0; i < data_.size(); ++i) {
                query->CheckAndAddToResult(data_[i]);
            }
        } else {
            for (int i =0; i < 100000; ++i);
        }
    }

    template <typename dist_t>
    void HGraph<dist_t>::Search(KNNQuery<dist_t>* query, IdType) const {
        //LOG(LIB_INFO) << "gnns";
        for (size_t i = 0; i < initSearchAttempts_; i++) {
#if START_PIVOTS
            int random = RandomInt() % levelSeeds_.size();
            int j=0;
            int curr;
            for (std::set<int>::iterator it=levelSeeds_.begin(); it!=levelSeeds_.end(); ++it){
                if(j == random){
                    curr = *it;
                    LOG(LIB_INFO) << curr;
                    break;
                }
                j++;
            }
#else
            size_t curr = RandomInt() % ElList_.size();
            //seeds to start search
#endif
            dist_t currDist = query->DistanceObjLeft(ElList_[curr]->getData());
            query->CheckAndAddToResult(currDist, ElList_[curr]->getData());
            size_t currOld;
            do {
                currOld = curr;
                //LOG(LIB_INFO) << "current::" << curr;
                for (HNode_eval<dist_t> *e : ElList_[currOld]->getNeighbors()) {
                    size_t currNew = e->getHNode()->getId();
                    //if(currNew != bad){
                    dist_t currDistNew = query->DistanceObjLeft(ElList_[currNew]->getData());
                    query->CheckAndAddToResult(currDistNew, ElList_[currNew]->getData());
                    if (currDistNew < currDist) {
                        curr = currNew;
                        currDist = currDistNew;
                    }
                    //}
                }
            } while (currOld != curr);
        }
    }

    template <typename dist_t>
    void
    HGraph<dist_t>::SetQueryTimeParams(const AnyParams& QueryTimeParams) {
        // Check if a user specified extra parameters, which can be also misspelled variants of existing ones
        AnyParamManager pmgr(QueryTimeParams);
        pmgr.GetParamOptional("initSearchAttempts", initSearchAttempts_, 1);
        pmgr.CheckUnused();
        LOG(LIB_INFO) << "Set Hgraph query-time params";
    }

    template <typename dist_t>
    void HGraph<dist_t>::printGraph(string file) {
        ofstream outFile(file);
        CHECK_MSG(outFile, "Cannot open file");
        outFile.exceptions(std::ios::badbit);
        for (HNode<dist_t> *node : ElList_) {
            for (HNode_eval<dist_t> *neighbor : node->getNeighbors()) {
                //outFile << node->getId() << ", ";
                //outFile << neighbor->getId() << endl;
                outFile << node->getId() << " ";
                outFile << neighbor->getHNode()->getId() << endl;
            }
        }
        outFile.close();
    }


    template<typename dist_t>
    void HGraph<dist_t>::writeGephi(string file) { //arrumar
        ofstream outFile(file);
        CHECK_MSG(outFile, "Cannot open file '" + file + "' for writing");
        outFile.exceptions(std::ios::badbit);
        outFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                << "<gexf xmlns=\"http://www.gexf.net/1.2draft\" version=\"1.2\">\n"
                << "<meta>\n"
                << "<creator>larissa.nmslib</creator>\n"
                << "<description></description>\n"
                << "</meta>\n"
                << "<graph mode=\"static\" defaultedgetype=\"directed\">\n";
        outFile << "<nodes>\n";
        for (HNode<dist_t> *pNode: ElList_) {
            size_t nodeID = pNode->getId();
            CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                      "Bug: unexpected node ID " + ConvertToString(nodeID) +
                      " for object ID " + ConvertToString(pNode->getData()->id()) +
                      "data_.size() = " + ConvertToString(data_.size()));
            outFile << "<node id=\"" << nodeID
                    << "\" label=\"" << pNode->getData()->id() << "\" />\n";
        }
        outFile << "</nodes>\n";
        outFile << "<edges>\n";
        int i = 0;
        for (HNode<dist_t> *pNode: ElList_) {
            size_t nodeId = pNode->getId();
            for (HNode_eval<dist_t> *pNodeFriend: pNode->getNeighbors()) {
                IdType nodeFriendID = pNodeFriend->getHNode()->getId();
                CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                          "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                          " for object ID " + ConvertToString(pNodeFriend->getHNode()->getData()->id()) +
                          "data_.size() = " + ConvertToString(data_.size()));
                outFile << "<edge id=\"" << i++ << "\" source=\"" << nodeId << "\" "
                        << "target=\"" << nodeFriendID << "\" />\n";
            }
        }
        outFile << "</edges>\n";
        outFile << "</graph>\n";
        outFile << "</gexf>";
        outFile.close();
    }

    template<typename dist_t>
    void HGraph<dist_t>::SaveIndex(const string &location) {
        string gephi = location + "gephi.gexf";
        writeGephi(gephi);
        ofstream outFile(location);
        CHECK_MSG(outFile, "Cannot open file '" + location + "' for writing");
        outFile.exceptions(std::ios::badbit);
        size_t lineNum = 0;

        WriteField(outFile, METHOD_DESC, StrDesc());
        lineNum++;
        WriteField(outFile, "NN", NN_); lineNum++;
        WriteField(outFile, "seeds", S_); lineNum++;
        WriteField(outFile, "levels", L_); lineNum++;
        for (HNode<dist_t> *pNode: ElList_) {
            size_t nodeID = pNode->getId();
            CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                      "Bug: unexpected node ID " + ConvertToString(nodeID) +
                      " for object ID " + ConvertToString(pNode->getData()->id()) +
                      "data_.size() = " + ConvertToString(data_.size()));
            outFile << nodeID << ":" << pNode->getData()->id() << ":";
            for (HNode_eval<dist_t> *pNodeFriend: pNode->getNeighbors()) {
                IdType nodeFriendID = pNodeFriend->getHNode()->getId();
                CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                          "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                          " for object ID " + ConvertToString(pNodeFriend->getHNode()->getData()->id()) +
                          "data_.size() = " + ConvertToString(data_.size()));
                outFile << ' ' << nodeFriendID;
            }
            outFile << endl;
            lineNum++;
        }
        outFile << endl;
        lineNum++; // The empty line indicates the end of data entries
        //WriteField(outFile, LINE_QTY, lineNum+1 /* including this line */);
        outFile << LINE_QTY << ":" << lineNum+1;
        outFile.close();
    }


    template<typename dist_t>
    void HGraph<dist_t>::LoadIndex(const string &location) {
        cout << "LOAD INDEX " << endl;
        LOG(LIB_INFO) << "LOAD INDEX";
        vector<HNode<dist_t> *> ptrMapper(data_.size());

        for (unsigned pass = 0; pass < 2; ++pass) {
            ifstream inFile(location);
            CHECK_MSG(inFile, "Cannot open file '" + location + "' for reading");
            inFile.exceptions(std::ios::badbit);

            size_t lineNum = 1;
            string methDesc;
            ReadField(inFile, METHOD_DESC, methDesc);
            lineNum++;
            CHECK_MSG(methDesc == StrDesc(),
                      "Looks like you try to use an index created by a different method: " + methDesc);
            ReadField(inFile, "NN", NN_);
            lineNum++;
            ReadField(inFile, "seeds", S_);
            lineNum++;
            ReadField(inFile, "levels", L_);
            lineNum++;

            string line;
            while (getline(inFile, line)) {
                if (line.empty()) {
                    lineNum++;
                    break;
                }
                stringstream str(line);
                str.exceptions(std::ios::badbit);
                char c1, c2;
                IdType nodeID, objID;
                CHECK_MSG((str >> nodeID) && (str >> c1) && (str >> objID) && (str >> c2),
                          "Bug or inconsitent data, wrong format, line: " + ConvertToString(lineNum)
                );
                CHECK_MSG(c1 == ':' && c2 == ':',
                          string("Bug or inconsitent data, wrong format, c1=") + c1 + ",c2=" + c2 +
                          " line: " + ConvertToString(lineNum)
                );
                CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                          DATA_MUTATION_ERROR_MSG + " (unexpected node ID " + ConvertToString(nodeID) +
                          " for object ID " + ConvertToString(objID) +
                          " data_.size() = " + ConvertToString(data_.size()) + ")");
                CHECK_MSG(data_[nodeID]->id() == objID,
                          DATA_MUTATION_ERROR_MSG + " (unexpected object ID " + ConvertToString(data_[nodeID]->id()) +
                          " for data element with ID " + ConvertToString(nodeID) +
                          " expected object ID: " + ConvertToString(objID) + ")"
                );
                if (pass == 0) {
                    unique_ptr<HNode<dist_t>> node(new HNode<dist_t>(data_[nodeID], nodeID));
                    ptrMapper[nodeID] = node.get();
                    ElList_.push_back(node.release());
                } else {
                    HNode<dist_t> *pNode = ptrMapper[nodeID];
                    CHECK_MSG(pNode != NULL,
                              "Bug, got NULL pointer in the second pass for nodeID " + ConvertToString(nodeID));
                    IdType nodeFriendID;
                    while (str >> nodeFriendID) {
                        CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                                  "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                                  "data_.size() = " + ConvertToString(data_.size()));
                        HNode<dist_t> *pFriendNode = ptrMapper[nodeFriendID];
                        CHECK_MSG(pFriendNode != NULL,
                                  "Bug, got NULL pointer in the second pass for nodeID " +
                                  ConvertToString(nodeFriendID));
                        pNode->addNeighbor(pFriendNode, false, space_.IndexTimeDistance(data_[nodeID], data_[nodeFriendID]));//, false /* don't check for duplicates */);
                    }
                    CHECK_MSG(str.eof(),
                              "It looks like there is some extract erroneous stuff in the end of the line " +
                              ConvertToString(lineNum));
                }
                ++lineNum;
            }
            //LOG(LIB_INFO) << "ElList size" << ElList_.size();
            size_t ExpLineNum;
            ReadField(inFile, LINE_QTY, ExpLineNum);
            CHECK_MSG(lineNum == ExpLineNum,
                      DATA_MUTATION_ERROR_MSG + " (expected number of lines " + ConvertToString(ExpLineNum) +
                      " read so far doesn't match the number of read lines: " + ConvertToString(lineNum) + ")");
            inFile.close();
        }
    }

    template class HGraph<float>;
    template class HGraph<double>;
    template class HGraph<int>;

}
