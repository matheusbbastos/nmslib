#include <cmath>
#include <memory>
#include <iostream>
#include <mmintrin.h>
#include <immintrin.h>
//#include <smmintrin.h>
//#include <tmmintrin.h>

#include "space.h"
#include "knnquery.h"
#include "knnqueue.h"
#include "methodfactory.h"
#include "rangequery.h"
#include "ported_boost_progress.h"
#include "method/kdr.h"
#include "sort_arr_bi.h"
#include "factory/method/knn.h"
#include "method/knng.h"

#include <vector>

#include <set>
#include <map>
#include <sstream>
#include <thread>
#include <typeinfo>
#include <method/nndes.h>
#include <method/kdr.h>

#define MERGE_BUFFER_AkDRO_SWITCH_THRESHOLD 100

#define USE_BITSET_FOR_INDEXING 1
//#define USE_ALTERNATIVE_FOR_INDEXING

namespace similarity {
    using namespace std;

    template<typename dist_t>
    struct IndexkDRParams {
        //dados sobre o que colocar no grafo
        const Space<dist_t> &space_;
        kDR<dist_t> &index_;
        const ObjectVector &data_;
        size_t index_every_;
        size_t out_of_;
        ProgressDisplay *progress_bar_;
        mutex &display_mutex_;
        size_t progress_update_qty_;

        IndexkDRParams(
                const Space<dist_t> &space,
                kDR<dist_t> &index,
                const ObjectVector &data,
                size_t index_every,
                size_t out_of,
                ProgressDisplay *progress_bar,
                mutex &display_mutex,
                size_t progress_update_qty
        ) :
                space_(space),
                index_(index),
                data_(data),
                index_every_(index_every),
                out_of_(out_of),
                progress_bar_(progress_bar),
                display_mutex_(display_mutex),
                progress_update_qty_(progress_update_qty) {}


    };

    template<typename dist_t>
    kDR<dist_t>::kDR(bool PrintProgress, const Space<dist_t> &space, const ObjectVector &data) :
            space_(space), data_(data), PrintProgress_(PrintProgress) {
        vector<vector<dist_t> > matrix(data_.size(), vector<dist_t>(data_.size(), 0));
        DistanceMatrix = matrix;
    }


    template<typename dist_t>
    void kDR<dist_t>::addCriticalSection(kDRNode *element) {
        //unique_lock<mutex> lock(ElListGuard_);
        ElList_.push_back(element);
        //LOG(LIB_INFO) << "Critical Section::: " << element->getId();
    }

/*template <typename dist_t>
struct IndexkDR {
  void operator()(IndexkDRParams<dist_t>& prm) {
    ProgressDisplay*  progress_bar = prm.progress_bar_;
    mutex&            display_mutex(prm.display_mutex_);
    //primeiro elemento já adicionado
    size_t nextQty = prm.progress_update_qty_;
    for (size_t id = 0; id < prm.data_.size(); ++id) {//prm.data_.size()
      if (prm.index_every_ == id % prm.out_of_) {
        kDRNode* node = new kDRNode(prm.data_[id], id);
        //kDRNode* newElement = addCriticalSection(node);
      //  LOG(LIB_INFO) << "Nó inserido atualmente:" << newElement->getId();
        prm.index_.add(node, id);
        if ((id + 1 >= min(prm.data_.size(), nextQty)) && progress_bar) { //prm.data_.size()
          unique_lock<mutex> lock(display_mutex);
          (*progress_bar) += (nextQty - progress_bar->count());
          nextQty += prm.progress_update_qty_;
        }
      }
    }
    if (progress_bar) {
      unique_lock<mutex> lock(display_mutex);
      (*progress_bar) += (progress_bar->expected_count() - progress_bar->count());
    }
  }
};

template <typename dist_t>
struct IndexThreadkDR {
  void operator()(IndexkDRParams<dist_t>& prm) {
    ProgressDisplay*  progress_bar = prm.progress_bar_;
    mutex&            display_mutex(prm.display_mutex_);
     // Skip the first element, it was added already
    size_t nextQty = prm.progress_update_qty_;
    for (size_t id = 0; id < prm.data_.size(); ++id) { //prm.data_.size()
      if (prm.index_every_ == id % prm.out_of_) {
        kDRNode* node = new kDRNode(prm.data_[id], id);
        //kDRNode* newElement = addCriticalSection(node);
      //  LOG(LIB_INFO) << "Nó inserido atualmente thread:" << newElement->getId();
        prm.index_.add(node, id);
        if ((id + 1 >= min(prm.data_.size(), nextQty)) && progress_bar) {//prm.data_.size()
          unique_lock<mutex> lock(display_mutex);
          (*progress_bar) += (nextQty - progress_bar->count());
          nextQty += prm.progress_update_qty_;
        }
      }
    }
    if (progress_bar) {
      unique_lock<mutex> lock(display_mutex);
      (*progress_bar) += (progress_bar->expected_count() - progress_bar->count());
    }
  }
};*/

    template<typename dist_t>
    bool kDR<dist_t>::greedyReachability(kDRNode *ini, kDRNode *fim) {
        //LOG(LIB_INFO) << "Fim" << fim->getId();
        if (fim->getNeighbors().size() == 0) return false;
        kDRNode *gsResult = GRC(ini, fim);
        //LOG(LIB_INFO) << "GS RESULT" << gsResult->getId();
        if (space_.IndexTimeDistance(ini->getData(), gsResult->getData()) <=
            space_.IndexTimeDistance(ini->getData(), fim->getData())) {
            return true;
        } else return false; //rever greedy search
    }

    template<typename dist_t>
    kDRNode *kDR<dist_t>::GRC(kDRNode *x, kDRNode *y) const {
        priority_queue<EvaluatedkDRNode<dist_t>, vector<EvaluatedkDRNode<dist_t>>, greater<EvaluatedkDRNode<dist_t>>> neighbors;
        for (kDRNode *neighbor : y->getNeighbors()) {
            //dist_t neighborD = space_.IndexTimeDistance(ElList_[x->getId()]->getData(), ElList_[neighbor->getId()]->getData());
            dist_t neighborD = space_.IndexTimeDistance(x->getData(), neighbor->getData());
            EvaluatedkDRNode<dist_t> *neighborNode = new EvaluatedkDRNode<dist_t>(neighbor, neighborD);
            neighbors.push(*neighborNode);
        }
        //kDRNode* z = neighbors.top().getkDRNode();
        //delete neighbors;
        //return z;
        return neighbors.top().getkDRNode();
    }


/*template <typename dist_t>
vector<kDRNode*> kDR<dist_t>::randomSampling(size_t size){
    vector<kDRNode*> sample;
    vector<unsigned int> indices(ElList_.size());
    iota(indices.begin(), indices.end(), 0);
    random_shuffle(indices.begin(), indices.end());
    for(int i=0; i < size; i++){
        sample.push_back(ElList_[indices[i]]);
    }
    return sample;
}*/

    template<typename dist_t>
    float_t kDR<dist_t>::successProbability(size_t L, ObjectVector Q) {
        LOG(LIB_INFO) << "Success Probability";
        float_t ret;
        //LOG(LIB_INFO) << "Size:" << Q.size();
        float sumI = 0;
        float sumQ = 0;
        int pos = 0;
        for (const Object *q : Q) {
            int I = 0;
            vector<kDRNode *> S = getInitialVertice(h_);
            LOG(LIB_INFO) << "Objeto de busca:" << q->label();
            for (kDRNode *s : S) { //h starts para cada q
                LOG(LIB_INFO) << "Start node greedy search:" << s->getId();
                kDRNode *result = greedySearchBasin(s, q);
                LOG(LIB_INFO) << "Result greedy search:" << result->getId();
                LOG(LIB_INFO) << "Target:" << Target[pos];
                if (result->getId() == Target[pos]) {
                    I++;
                }
            }
            sumI = (((float) I) / S.size());
            LOG(LIB_INFO) << "Sum I:" << sumI;
            //sumI = Basin(q, pos);
            //somatoria 1 -(1-p~(q'[j];Tk'DR)^L
            sumQ += 1 - pow(1 - sumI, L_);
            pos++;
        }
        LOG(LIB_INFO) << "Success:" << sumQ / Q.size();
        ret = (sumQ / Q.size());
        return ret;
    }

/*template <typename dist_t>
float_t kDR<dist_t>::Basin(kDRNode* q){
    LOG(LIB_INFO) << "Basin Function";
    int basin = 0;
    for(kDRNode* x : ElList_){
        kDRNode* result = greedySearch(x, q);
        if(result->getId() == q->getId()){
            basin++;
        }
    }
    LOG(LIB_INFO) << "Basin:" << basin/ElList_.size();
    return (((float)basin)/ElList_.size());
}*/

    template<typename dist_t>
    void kDR<dist_t>::SeqSearchTarget() {
        for (const Object *q : Ql) {
            pair<size_t, dist_t> nn;
            nn.first = ElList_[0]->getId();
            nn.second = space_.IndexTimeDistance(ElList_[0]->getData(), q);
            for (kDRNode *vertex : ElList_) {
                dist_t distance = space_.IndexTimeDistance(vertex->getData(), q);
                if (distance < nn.second) {
                    nn.second = distance;
                    nn.first = vertex->getId();
                    // LOG(LIB_INFO) << nn.first;
                }
            }
            Target.push_back(nn.first);
        }
        /*for(int i=0; i < Target.size(); i++){
            LOG(LIB_INFO) << "Target::" << i << "::"<<Target[i];
        }*/
    }


    template<typename dist_t>
    float_t kDR<dist_t>::Basin(const Object *q, int pos) {
        int basin = 0;
        vector<kDRNode *> S = getInitialVertice(h_);
        for (kDRNode *x : S) { //substituir por S[j]
            kDRNode *result = greedySearchBasin(x, q);
            if (result->getId() == Target[pos]) {
                basin++;
            }
        }
        //LOG(LIB_INFO) << "I::" << basin;
        return (((float) basin) / h_);
    }


    template<typename dist_t>
    kNNG<dist_t> *CreatekMAX(const Space<dist_t> &space_, ObjectVector data_, size_t kmax_) {
        const AnyParams IndexParams(
                {
                        "NN=" + to_string(kmax_),
                        "dir=0", //grafo knng direcionado para construir como se fosse uma lista (LIST KMAX)
                        "initIndexAttempts=1",
                        "indexThreadQty=1" /* 1 indexing threads */
                });

        //AnyParams QueryTimeParams( { "initSearchAttempts=1" });
        kNNG<dist_t> *kmaxNN = new kNNG<dist_t>(true, space_, data_);
        // Creating an index
        kmaxNN->CreateIndex(IndexParams);
        LOG(LIB_INFO) << "Created knn index";
        LOG(LIB_INFO) << "kmax neighbors knn" << kmaxNN->getElList()[0]->getNeighbors().size();
        //kmaxNN->printGraph("/home/larissa/Documents/knn.txt");
        return kmaxNN;
    }

    template<typename dist_t>
    NNDescentMethod<dist_t> *
    CreateNNDES(const Space<dist_t> &space_, ObjectVector data_, size_t kmax_, float rho_, float deltaNNDES_,
                size_t iterationQty_) {
        const AnyParams IndexParams(
                {
                        "NN=" + to_string(kmax_),
                        "iterationQty=" + to_string(iterationQty_),
                        "rho=" + to_string(rho_),
                        "delta=" + to_string(deltaNNDES_)
                });
        NNDescentMethod<dist_t> *nndes = new NNDescentMethod<dist_t>(true, space_, data_);
        nndes->CreateIndex(IndexParams);
        LOG(LIB_INFO) << "Created nndes";
        return nndes;
    }

    template<typename dist_t>
    void kDR<dist_t>::printGraph(string file) {
        ofstream outFile;
        outFile.open(file);
        CHECK_MSG(outFile, "Cannot open file");
        outFile.exceptions(std::ios::badbit);
        for (kDRNode *node : ElList_) {
            for (kDRNode *neighbor : node->getNeighbors()) {
                outFile << node->getId() << " ";
                outFile << neighbor->getId() << endl;
            }
        }
        outFile.close();
    }

    template<typename dist_t>
    kDRNode *kDR<dist_t>::greedySearchBasin(kDRNode *first, const Object *second) {
        if (ElList_.empty()) return NULL;
        kDRNode *currNode = first;
        bool hasNearestNeighbor = true;
        dist_t d = space_.IndexTimeDistance(first->getData(), second);
        kDRNode *nearestNeighbor = currNode;
        while (hasNearestNeighbor) {
            //vector<RNGNode*> neighbors = currNode.getNeighbors();
            //LOG(LIB_INFO) << "while";
            hasNearestNeighbor = false;
            //LOG(LIB_INFO) << currNode->getNeighbors().size();
            if (currNode->getNeighbors().size() <= 0) break;
            for (kDRNode *neighbor : currNode->getNeighbors()) {
                size_t neighborId = neighbor->getId();
                const Object *currObj = neighbor->getData();
                dist_t dNeighbor = space_.IndexTimeDistance(currObj, second);
                if (dNeighbor < d) {
                    // LOG(LIB_INFO) << "if";
                    nearestNeighbor = neighbor;
                    d = dNeighbor;
                    hasNearestNeighbor = true;
                }
            }
            currNode = nearestNeighbor;
        }
        // LOG(LIB_INFO) << "nearestNeighbor" << nearestNeighbor->getId();
        return nearestNeighbor;
    }


    template<typename dist_t>
    void kDR<dist_t>::link(kDRNode *first, kDRNode *second) { //não direcionado
        //LOG(LIB_INFO) << "LINK:" << first->getId() << " e " << second->getId();
        first->addNeighbor(second);
        second->addNeighbor(first);
    }


    template<typename dist_t>
    void kDR<dist_t>::printSuccess(string file_, size_t k, float_t pSuccess) {
        ofstream outFile;
        outFile.open(file_, ofstream::in | ofstream::out | ofstream::app);
        CHECK_MSG(outFile, "Cannot open file -- printSuccess");
        outFile.exceptions(std::ios::badbit);
        outFile << k << "," << pSuccess << "," << L_ << "," << Q_ << "," << delta_ << endl;
        outFile.close();
    }


    template<typename dist_t>
    void kDR<dist_t>::CreateIndexK(vector<kNNGNode *> kmaxList) { //create index para kdr com k definido (apenas diminui do knn)
        LOG(LIB_INFO) << kmaxList.size();
        for (int kL = 0; kL < k_; kL++) {
            for (int i = 0; i < ElList_.size(); i++) {
                //verificar se neighbor[kL] é greedyReachable do ElList[i]
                int numEl = kmaxList[i]->getNeighbors().size() - 1;
                LOG(LIB_INFO) << numEl;
                kNNGNode *second = kmaxList[i]->getNeighbors()[numEl - kL];
                if (!greedyReachability(ElList_[i], ElList_[second->getId()])) {
                    link(ElList_[i], ElList_[second->getId()]);
                }
                /*kDRNode* nearestNeighbor = greedySearchBasin(ElList_[i], second->getData());
                if(nearestNeighbor->getId() != second->getId()){
                    //LOG(LIB_INFO) << "LINK i:" << i << " second:" << second->getId();
                    link(ElList_[i], ElList_[second->getId()]);
                }*/
            }
            //LOG(LIB_INFO) << "K atual:" << kL;
        }
      //  printGraph("/Users/larissashimomura/Documents/mestrado/SurveyGraph/results/images/kdrFixo5.txt");
    }


//<<<<<<< HEAD
    template<typename dist_t>
    void kDR<dist_t>::CreateIndex(const AnyParams &IndexParams) {
        //construir um kDR
        AnyParamManager pmgr(IndexParams);
        string tmp;
        int rand;
        string quasiqueries;
        vector<string> Qlids;
        pmgr.GetParamOptional("initIndexAttempts", initIndexAttempts_, 1);
        pmgr.GetParamOptional("qlRand", rand, 0);
        pmgr.GetParamOptional("indexThreadQty", indexThreadQty_, thread::hardware_concurrency());
        pmgr.GetParamOptional("iterationQty", iterationQty_, 100); //for nndes
        pmgr.GetParamOptional("NN", k_, 0); //valor de k, default 0 (se zero -->com basin, sem zero sem basin)
        pmgr.GetParamOptional("kmax", kmax_, 200); //fixed value on 2011 paper
        pmgr.GetParamOptional("delta", delta_, 0.1);
        pmgr.GetParamOptional("h", h_, 1);
        pmgr.GetParamOptional("L", L_, 1);
        pmgr.GetParamOptional("Q", Q_, 3);
        pmgr.GetParamOptional("nndes", nndes, 0);
        pmgr.GetParamOptional("file", file_, "../kdrStats2.txt");
        pmgr.GetParamOptional("rho", rho_, 1.0); // Fast rho is 0.5, 1.0 is from Wei Dong's code
        pmgr.GetParamOptional("deltaNNDES", deltaNNDES_, 0.001); // default value from Wei Dong's code
        pmgr.GetParamOptional("quasi", quasiqueries, " ");
        LOG(LIB_INFO) << "QL SIZE" << Ql.size();
        if (k_ > data_.size()) {
            LOG(LIB_INFO) << "Invalid k value!";
            throw runtime_error("Invalid k value!");
        }
        if (h_ > data_.size()) {
            LOG(LIB_INFO) << "S[j] size bigger than data size";
            __throw_runtime_error("S[j] bigger than data size");
        }
        if (kmax_ > data_.size()) {
            kmax_ = data_.size();
            LOG(LIB_INFO) << "kmax bigger than data size! Considering kmax == data size";
        }
        if (k_ > kmax_) {
            LOG(LIB_INFO) << "kmax < k! Not allowed!";
            throw runtime_error("kmax < k! Not allowed!");
        }
        LOG(LIB_INFO) << "initIndexAttempts   = " << initIndexAttempts_;
        LOG(LIB_INFO) << "indexThreadQty      = " << indexThreadQty_;
        LOG(LIB_INFO) << "kNN value          =" << k_;
        LOG(LIB_INFO) << "kmax value         =" << kmax_;
        LOG(LIB_INFO) << "L search attempts  =" << L_;
        LOG(LIB_INFO) << "Sampling size Q (quasi queries) =" << Q_;
        if (k_ == 0) {
            if (quasiqueries != " ") {
                space_.ReadDataset(Ql, Qlids, quasiqueries, Q_);
            } else {
                if (rand) {
                    for (int i = 0; i < Q_; i++) {
                        int pos = RandomInt() % data_.size();
                        Ql.push_back(data_[pos]);
                        data_.erase(data_.begin() + pos);
                    }
                } else {
                    Ql.assign(data_.begin(), data_.begin() + Q_); //quasi queries
                    data_.erase(data_.begin(),
                                data_.begin() + Q_);//tira ql do dataset (vai ser utilizado para a probabilidade)
                }
            }
        }
//=======
 /*
  pmgr.CheckUnused();
  SetQueryTimeParams(getEmptyParams());
  if (data_.empty()) return;
  if(indexThreadQty_ <= 1) {
      size_t k = 0;
      //computar a lista dos kmax mais proximos
      vector<kNNGNode *> kmaxList;
      if (!nndes) {
          LOG(LIB_INFO) << "not nndes";
          kNNG<dist_t> *kmaxNN = CreatekMAX(space_, data_, kmax_);
          //kmaxNN->printGraph("/home/larissa/Documents/NonMetricSpaceLib/knnKDR");
          kmaxList = kmaxNN->getElList();
      } else {
          LOG(LIB_INFO) << "nndes method";
          NNDescentMethod<dist_t> *nndes = CreateNNDES(space_, data_, kmax_, rho_, deltaNNDES_, iterationQty_);
          //const vector<KNN> &nn = nndes->returnNNDESObj()->getNN();
          const vector<KNN> &nn = nndes->nndesObj_->getNN();
          for (int i = 0; i < nn.size(); i++) {
              kNNGNode *node = new kNNGNode(data_[i], i);
              kmaxList.push_back(node);
          }
          for (int i = 0; i < nn.size(); i++) {
              for (const KNNEntry &e : nn[i]) {
                  kmaxList[i]->addNeighbor(kmaxList[e.key]);
              }
          }
      }
      LOG(LIB_INFO) << "Create Index kDR";
      for (size_t i = 0; i < data_.size(); i++) {
          //data_size();
          //kDRNode* vertex = new kDRNode(data_[i], i);
          addCriticalSection(new kDRNode(data_[i], i));    //colocar todos os vértices no grafo -- ok
      }
      LOG(LIB_INFO) << "Vertex added to kDR";
      if (k_ != 0) {
        CreateIndexK(kmaxList);
      } else {
          SeqSearchTarget();//calcular seqsearch de modo separado independente
          LOG(LIB_INFO) << "Target set done";
          float_t pSuccess = successProbability(L_, Ql); //calcular pSuccess inicial
          while (1 - delta_ >= pSuccess ) {
              if(k >= kmax_){
                  LOG(LIB_INFO) << "Valor de k não suficiente para estes parâmetros";
                  __throw_runtime_error("Valor de k superou kmax, construção não convergiu");
              }
              printSuccess(file_, k, pSuccess);
              k++;
              for (size_t i = 0; i < ElList_.size(); i++) { //data_.size() //todos elementos do dataset
                  //kDRNode* y = new kDRNode(kmaxList[i]->getNeighbors()[k-1]->getData(), kmaxList[i]->getNeighbors()[k-1]->getId());
                  int numEl = kmaxList[i]->getNeighbors().size();
                  size_t yId = kmaxList[i]->getNeighbors()[numEl - k]->getId();
                  //LOG(LIB_INFO) << "y Data" << y->getId();
                  // x--> i
                  //y == kNNy
                  //bool reach = greedyReachability(ElList_[i], ElList_[y->getId()]);
                  bool reach = greedyReachability(ElList_[i], ElList_[yId]);
                  //LOG(LIB_INFO) << "greedy result" << reach;
                  if (!reach) {
                      //kDRNode* kNNNeighbor = new kDRNode(y->getData(), y->getId());
                      //link(ElList_[i], ElList_[y->getId()]);//verificar ponteiros
                      // LOG(LIB_INFO) << "LINK I" << i << " YID" << yId;
                      link(ElList_[i], ElList_[yId]);
                  }
              }
              pSuccess = successProbability(L_, Ql);
              LOG(LIB_INFO) << "pSuccess:" << pSuccess << " k:" << k;
          }
          k_ = k; //atribui o valor de k apos a construcao
      }
  }
 // printGraph("/home/larissa/Documents/survey/kdrPrint.txt");
 printGraph("/Users/larissashimomura/Documents/mestrado/SurveyGraph/results/images/kdrProbNovo.txt");
}*/
//>>>>>>> a8fe5c65bdbc9a5abfde3ffa689776e30098a42d

        pmgr.CheckUnused();

        SetQueryTimeParams(getEmptyParams());
        if (data_.empty()) return;
        if (indexThreadQty_ <= 1) {
            size_t k = 0;
            //computar a lista dos kmax mais proximos
            vector<kNNGNode *> kmaxList;
            if (!nndes) {
                LOG(LIB_INFO) << "not nndes";
                kNNG<dist_t> *kmaxNN = CreatekMAX(space_, data_, kmax_);
                //kmaxNN->printGraph("/home/larissa/Documents/NonMetricSpaceLib/knnKDR");
                kmaxList = kmaxNN->getElList();
            } else {
                LOG(LIB_INFO) << "nndes method";
                NNDescentMethod<dist_t> *nndes = CreateNNDES(space_, data_, kmax_, rho_, deltaNNDES_, iterationQty_);
                //const vector<KNN> &nn = nndes->returnNNDESObj()->getNN();
                const vector<KNN> &nn = nndes->nndesObj_->getNN();
                for (int i = 0; i < nn.size(); i++) {
                    kNNGNode *node = new kNNGNode(data_[i], i);
                    kmaxList.push_back(node);
                }
                for (int i = 0; i < nn.size(); i++) {
                    for (const KNNEntry &e : nn[i]) {
                        kmaxList[i]->addNeighbor(kmaxList[e.key]);
                    }
                }
            }
            LOG(LIB_INFO) << "Create Index kDR";
            for (size_t i = 0; i < data_.size(); i++) {
                //data_size();
                //kDRNode* vertex = new kDRNode(data_[i], i);
                addCriticalSection(new kDRNode(data_[i], i));    //colocar todos os vértices no grafo -- ok
            }
            LOG(LIB_INFO) << "Vertex added to kDR";
            if (k_ != 0) {
                LOG(LIB_INFO) << ElList_.size();
                CreateIndexK(kmaxList);
            } else {
                SeqSearchTarget();//calcular seqsearch de modo separado independente
                LOG(LIB_INFO) << "Target set done";
                float_t pSuccess = successProbability(L_, Ql); //calcular pSuccess inicial
                while (1 - delta_ >= pSuccess) {
                    if (k >= kmax_) {
                        LOG(LIB_INFO) << "Valor de k não suficiente para estes parâmetros";
                        __throw_runtime_error("Valor de k superou kmax, construção não convergiu");
                    }
                    printSuccess(file_, k, pSuccess);
                    k++;
                    for (size_t i = 0; i < ElList_.size(); i++) { //data_.size() //todos elementos do dataset
                        //kDRNode* y = new kDRNode(kmaxList[i]->getNeighbors()[k-1]->getData(), kmaxList[i]->getNeighbors()[k-1]->getId());
                        int numEl = kmaxList[i]->getNeighbors().size();
                        size_t yId = kmaxList[i]->getNeighbors()[numEl - k]->getId();
                        //LOG(LIB_INFO) << "y Data" << y->getId();
                        // x--> i
                        //y == kNNy
                        //bool reach = greedyReachability(ElList_[i], ElList_[y->getId()]);
                        bool reach = greedyReachability(ElList_[i], ElList_[yId]);
                        //LOG(LIB_INFO) << "greedy result" << reach;
                        if (!reach) {
                            //kDRNode* kNNNeighbor = new kDRNode(y->getData(), y->getId());
                            //link(ElList_[i], ElList_[y->getId()]);//verificar ponteiros
                            // LOG(LIB_INFO) << "LINK I" << i << " YID" << yId;
                            link(ElList_[i], ElList_[yId]);
                        }
                    }
                    pSuccess = successProbability(L_, Ql);
                    LOG(LIB_INFO) << "pSuccess:" << pSuccess << " k:" << k;
                }
                k_ = k; //atribui o valor de k apos a construcao
            }
        }
        // printGraph("/home/larissa/Documents/survey/kdrPrint.txt");
    }

    template<typename dist_t>
    void kDR<dist_t>::writeGephi(string file) { //arrumar
        ofstream outFile(file);
        CHECK_MSG(outFile, "Cannot open file '" + file + "' for writing");
        outFile.exceptions(std::ios::badbit);
        size_t lineNum = 0;

        //WriteField(outFile, METHOD_DESC, StrDesc()); lineNum++;
//  WriteField(outFile, "NN", NN_); lineNum++;
        outFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                << "<gexf xmlns=\"http://www.gexf.net/1.2draft\" version=\"1.2\">\n"
                << "<meta>\n"
                << "<creator>larissa</creator>\n"
                << "<description></description>\n"
                << "</meta>\n"
                << "<graph mode=\"static\" defaultedgetype=\"directed\">\n";
        outFile << "<nodes>\n";
        for (kDRNode *pNode: ElList_) {
            size_t nodeID = pNode->getId();
            CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                      "Bug: unexpected node ID " + ConvertToString(nodeID) +
                      " for object ID " + ConvertToString(pNode->getData()->id()) +
                      "data_.size() = " + ConvertToString(data_.size()));
            //outFile << nodeID << ":" << pNode->getData()->id() << ":";
            outFile << "<node id=\"" << nodeID
                    << "\" label=\"" << pNode->getData()->id() << "\" />\n";
        }
        outFile << "</nodes>\n";
        outFile << "<edges>\n";
        int i = 0;
        for (kDRNode *pNode: ElList_) {
            for (kDRNode *pNodeFriend: pNode->getNeighbors()) {
                IdType nodeFriendID = pNodeFriend->getId();
                CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                          "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                          " for object ID " + ConvertToString(pNodeFriend->getData()->id()) +
                          "data_.size() = " + ConvertToString(data_.size()));
                outFile << "<edge id=\"" << i++ << "\" source=\"" << pNode->getData()->id() << "\" "
                        << "target=\"" << pNodeFriend->getData()->id() << "\" />\n";
            }
        }
        outFile << "</edges>\n";
        outFile << "</graph>\n";
        outFile << "</gexf>";
        outFile.close();
    }

    template<typename dist_t>
    void kDR<dist_t>::SetQueryTimeParams(const AnyParams &QueryTimeParams) {
        AnyParamManager pmgr(QueryTimeParams);
        pmgr.GetParamOptional("initSearchAttempts", initSearchAttempts_, 1);
        pmgr.GetParamOptional("efSearch", efSearch_, 1);
        pmgr.GetParamOptional("gnns", gnns, 0);
        pmgr.GetParamOptional("sw", sw, 0);
        pmgr.GetParamOptional("pgs", pgs, 0);
        pmgr.GetParamOptional("gsl", gsl, 1); //default;
        pmgr.CheckUnused();
        LOG(LIB_INFO) << "Set kDR query-time parameters:";
        LOG(LIB_INFO) << "initSearchAttempts(for GNNS) =" << initSearchAttempts_;
        LOG(LIB_INFO) << "GNNS=" << gnns;
    }

    template<typename dist_t>
    vector<kDRNode *> kDR<dist_t>::getInitialVertice(size_t h) const {
        vector<kDRNode *> initialVertices;
        vector<unsigned int> indices(ElList_.size());
        iota(indices.begin(), indices.end(), 0);
        random_shuffle(indices.begin(), indices.end());
        for (int i = 0; i < h; i++) {
            initialVertices.push_back(ElList_[indices[i]]);
        }
        return initialVertices;
    }


    template<typename dist_t>
    void PGS(size_t tid, kDRNode *x, KNNQuery<dist_t> *query) {
        //LOG(LIB_INFO) << "PGS";
        kDRNode *z = x;
        vector<kDRNode *> c;
        c.push_back(x);
        dist_t Dmin = query->DistanceObjLeft(z->getData());
        while (z->equals(x)) {
            dist_t DminNeighbor = query->DistanceObjLeft(z->getNeighbors()[0]->getData());
            kDRNode *closestNeighbor = z->getNeighbors()[0];
            for (kDRNode *neighbor : z->getNeighbors()) {
                dist_t neighborDist = query->DistanceObjLeft(neighbor->getData());
                c.push_back(neighbor);
                //adicionar todos os neighbors de z em c
                if (neighborDist < DminNeighbor) {
                    closestNeighbor = neighbor;
                    DminNeighbor = neighborDist;
                }
            }
            x = closestNeighbor;
            if (DminNeighbor < Dmin) {
                Dmin = DminNeighbor;
                z = x;
            }
        }
        query->CheckAndAddToResult(Dmin, z->getData()); //z é o mais proximo
    }

    template<typename dist_t>
    void kDR<dist_t>::GNNS(KNNQuery<dist_t> *query) const {
        LOG(LIB_INFO) << "GNNS";
        for (size_t i = 0; i < initSearchAttempts_; i++) {
            size_t curr = RandomInt() % ElList_.size();
            dist_t currDist = query->DistanceObjLeft(ElList_[curr]->getData());
            query->CheckAndAddToResult(currDist, ElList_[curr]->getData());
            size_t currOld;
            do {
                currOld = curr;
                for (kDRNode *e : ElList_[currOld]->getNeighbors()) {
                    size_t currNew = e->getId();
                    //if(currNew != bad){
                    dist_t currDistNew = query->DistanceObjLeft(ElList_[currNew]->getData());
                    query->CheckAndAddToResult(currDistNew, ElList_[currNew]->getData());
                    if (currDistNew < currDist) {
                        curr = currNew;
                        currDist = currDistNew;
                    }
                    //}
                }
            } while (currOld != curr);
        }
    }


    template<typename dist_t>
    pair<size_t, dist_t> kDR<dist_t>::greedySearch(kDRNode *first, KNNQuery<dist_t> *query) const {
        pair<size_t, dist_t> answer;
        if (ElList_.empty()) return answer;
        kDRNode *currNode = first;
        bool hasNearestNeighbor = true;
        dist_t d = query->DistanceObjLeft(currNode->getData());
        kDRNode *nearestNeighbor = currNode;
        while (hasNearestNeighbor) {
            hasNearestNeighbor = false;
            //LOG(LIB_INFO) << currNode->getNeighbors().size();
            if (currNode->getNeighbors().size() <= 0) break;
            for (kDRNode *neighbor : currNode->getNeighbors()) {
                size_t neighborId = neighbor->getId();
                dist_t dNeighbor = query->DistanceObjLeft(neighbor->getData());
                if (dNeighbor < d) {
                    nearestNeighbor = ElList_[neighborId];
                    d = dNeighbor;
                    hasNearestNeighbor = true;
                }
            }
            currNode = nearestNeighbor;
        }
        answer.first = nearestNeighbor->getId();
        answer.second = d;
        query->CheckAndAddToResult(answer.second, nearestNeighbor->getData());
        return answer;
    }

    template<typename dist_t>
    void kDR<dist_t>::swSearch(KNNQuery<dist_t> *query) const {
        if (ElList_.empty()) return;
        CHECK_MSG(efSearch_ > 0, "efSearch should be > 0");
/*
 * The trick of using large dense bitsets instead of unordered_set was
 * borrowed from Wei Dong's kgraph: https://github.com/aaalgo/kgraph
 *
 * This trick works really well even in a multi-threaded mode. Indeed, the amount
 * of allocated memory is small. For example, if one has 8M entries, the size of
 * the bitmap is merely 1 MB. Furthermore, setting 1MB of entries to zero via memset would take only
 * a fraction of millisecond.
 */
        vector<bool>                        visitedBitset(ElList_.size());

        for (size_t i=0; i < initSearchAttempts_; i++) {
            /**
             * Search of most k-closest elements to the query.
             */
#ifdef START_WITH_E0_AT_QUERY_TIME
            kNNGNode* provider = i ? getRandomEntryPoint(): ElList_[0];
#else
            //kDRNode* provider = getRandomEntryPoint();
            kDRNode* provider = getInitialVertice(1).begin().operator*();
#endif
            //MSWNode* provider = getRandomEntryPoint();

            priority_queue <dist_t>                          closestDistQueue; //The set of all elements which distance was calculated
            priority_queue<EvaluatedkDRNode<dist_t> , vector<EvaluatedkDRNode<dist_t> >, greater<EvaluatedkDRNode<dist_t>>> candidateQueue;
            //the set of elements which we can use to evaluate

            const Object* currObj = provider->getData();
            dist_t d = query->DistanceObjLeft(currObj);
            query->CheckAndAddToResult(d, currObj); // This should be done before the object goes to the queue: otherwise it will not be compared to the query at all!

            //EvaluatedkNNGNode<dist_t> ev(provider, d);
            candidateQueue.push(EvaluatedkDRNode<dist_t>(provider, d));
            closestDistQueue.emplace(d);

            size_t nodeId = provider->getId();
            // data_.size() is guaranteed to be equal to ElList_.size()
            if (nodeId >= data_.size()) {
                stringstream err;
                err << "Bug: nodeId > data_size()";
                LOG(LIB_INFO) << err.str();
                throw runtime_error(err.str());
            }
            visitedBitset[nodeId] = true;

            while(!candidateQueue.empty()){

                auto iter = candidateQueue.top(); // This one was already compared to the query
                const EvaluatedkDRNode<dist_t>& currEv = iter;

                // Did we reach a local minimum?
                if (currEv.getDistance() > closestDistQueue.top()) {
                    break;
                }

                for (kDRNode* neighbor : (currEv.getkDRNode())->getNeighbors()) {
                    _mm_prefetch(reinterpret_cast<const char*>(const_cast<const Object*>(neighbor->getData())), _MM_HINT_T0);
                }
                for (kDRNode* neighbor : (currEv.getkDRNode())->getNeighbors()) {
                    _mm_prefetch(const_cast<const char*>(neighbor->getData()->data()), _MM_HINT_T0);
                }

                const vector<kDRNode*>& neighbor = (currEv.getkDRNode())->getNeighbors();

                // Can't access curEv anymore! The reference would become invalid
                candidateQueue.pop();

                //calculate distance to each neighbor
                for (auto iter = neighbor.begin(); iter != neighbor.end(); ++iter){
                    nodeId = (*iter)->getId();
                    // data_.size() is guaranteed to be equal to ElList_.size()
                    if (nodeId >= data_.size()) {
                        stringstream err;
                        err << "Bug: nodeId > data_size()";
                        LOG(LIB_INFO) << err.str();
                        throw runtime_error(err.str());
                    }
                    if (!visitedBitset[nodeId]) {
                        currObj = (*iter)->getData();
                        d = query->DistanceObjLeft(currObj);
                        visitedBitset[nodeId] = true;

                        if (closestDistQueue.size() < efSearch_ || d < closestDistQueue.top()) {
                            closestDistQueue.emplace(d);
                            if (closestDistQueue.size() > efSearch_) {
                                closestDistQueue.pop();
                            }

                            candidateQueue.emplace(*iter, d);
                        }

                        query->CheckAndAddToResult(d, currObj);
                    }
                }
            }
        }
    }

    template<typename dist_t>
    void kDR<dist_t>::nearestNeighborGS(KNNQuery<dist_t> *query) const {
        pair<size_t, dist_t> closest;
        closest.second = INFINITY;
        vector<kDRNode *> Ini = getInitialVertice(L_); //L inicios
        for (kDRNode *x : Ini) {
            pair<size_t, dist_t> p = greedySearch(x, query);
            if (p.second < closest.second) {
                closest = p;
            }
        }
        query->CheckAndAddToResult(closest.second, ElList_[(int) closest.first]->getData());
        return;
    }

    template<typename dist_t>
    void kDR<dist_t>::Search(KNNQuery<dist_t> *query, IdType) const {
        if (gnns) {
            GNNS(query);
            return;
        }
        if(sw) swSearch(query);
        if (query->GetK() == 1) {
            if (pgs) {
                vector<kDRNode *> x = getInitialVertice(L_); //L inicios
                vector<thread> threads;
                for (size_t i = 0; i < L_; ++i) {
                    threads.push_back(thread(PGS<dist_t>, ref(i), ref(x[i]), ref(query)));
                    // threads[i] = thread(PGS , ref(i), ref(x[i]), ref(query));
                }
                for (size_t i = 0; i < threads.size(); ++i) {
                    threads[i].join();
                }
            } else if (gsl) {
                nearestNeighborGS(query);
            } else {
                vector<unsigned int> indices(ElList_.size());
                iota(indices.begin(), indices.end(), 0);
                random_shuffle(indices.begin(), indices.end());
                greedySearch(ElList_[indices[0]], query);
            }
        } else CHECK_MSG(!gnns && !sw && query->GetK() > 1, "No search algorithm selected");
    }


    template<typename dist_t>
    void kDR<dist_t>::SaveIndex(const string &location) {
        ofstream outFile(location);
        CHECK_MSG(outFile, "Cannot open file '" + location + "' for writing");
        outFile.exceptions(std::ios::badbit);
        size_t lineNum = 0;

        WriteField(outFile, METHOD_DESC, StrDesc());
        lineNum++;
        WriteField(outFile, "NN", k_);
        lineNum++;
        WriteField(outFile, "kmax", kmax_);
        lineNum++;
        WriteField(outFile, "L", L_);
        lineNum++;
        WriteField(outFile, "delta", delta_);
        lineNum++;
        WriteField(outFile, "Q", Q_);
        lineNum++;

        for (kDRNode *pNode: ElList_) {
            size_t nodeID = pNode->getId();
            CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                      "Bug: unexpected node ID " + ConvertToString(nodeID) +
                      " for object ID " + ConvertToString(pNode->getData()->id()) +
                      "data_.size() = " + ConvertToString(data_.size()));
            outFile << nodeID << ":" << pNode->getData()->id() << ":";
            for (kDRNode *pNodeFriend: pNode->getNeighbors()) {
                IdType nodeFriendID = pNodeFriend->getId();
                CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                          "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                          " for object ID " + ConvertToString(pNodeFriend->getData()->id()) +
                          "data_.size() = " + ConvertToString(data_.size()));
                outFile << ' ' << nodeFriendID;
            }
            outFile << endl;
            lineNum++;
        }
        outFile << endl;
        lineNum++; // The empty line indicates the end of data entries
        WriteField(outFile, LINE_QTY, lineNum + 1 /* including this line */);
        outFile.close();
    }


    template
    class kDR<double>;

    template
    class kDR<int>;

    template
    class kDR<float>;

}
