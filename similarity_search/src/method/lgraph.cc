#include <cmath>
#include <memory>
#include <iostream>
#include <mmintrin.h>
#include <immintrin.h>
//#include <smmintrin.h>
//#include <tmmintrin.h>

#include "space.h"
#include "knnquery.h"
#include "knnqueue.h"
#include "rangequery.h"
#include "ported_boost_progress.h"
#include "method/lgraph.h"
#include "sort_arr_bi.h"

#include <vector>
#include <set>
#include <map>
#include <sstream>
#include <typeinfo>


#define MERGE_BUFFER_ALGO_SWITCH_THRESHOLD 100

#define USE_BITSET_FOR_INDEXING 1
//#define USE_ALTERNATIVE_FOR_INDEXING

namespace similarity {
using namespace std;

template<typename dist_t>
struct IndexLGraphParams{
  //dados sobre o que colocar no grafo
  const Space<dist_t>&                        space_;
  LGraph<dist_t>&                             index_;
  const ObjectVector&                         data_;
  size_t                                      index_every_;
  size_t                                      out_of_;
  ProgressDisplay*                            progress_bar_;
  mutex&                                      display_mutex_;
  size_t                                      progress_update_qty_;

  IndexLGraphParams(
                     const Space<dist_t>&             space,
                     LGraph<dist_t>&                  index,
                     const ObjectVector&              data,
                     size_t                           index_every,
                     size_t                           out_of,
                     ProgressDisplay*                 progress_bar,
                     mutex&                           display_mutex,
                     size_t                           progress_update_qty
                      ) :
                     space_(space),
                     index_(index),
                     data_(data),
                     index_every_(index_every),
                     out_of_(out_of),
                     progress_bar_(progress_bar),
                     display_mutex_(display_mutex),
                     progress_update_qty_(progress_update_qty)
                     { }


};

template <typename dist_t>
struct IndexLGraph {
  void operator()(IndexLGraphParams<dist_t>& prm) {
    ProgressDisplay*  progress_bar = prm.progress_bar_;
    mutex&            display_mutex(prm.display_mutex_);
    //primeiro elemento já adicionado
    size_t nextQty = prm.progress_update_qty_;
    for (size_t id = 1; id < prm.data_.size(); ++id) {
      if (prm.index_every_ == id % prm.out_of_) {
        LGNode* node = new LGNode(prm.data_[id], id);
        prm.index_.add(node, id);
        if ((id + 1 >= min(prm.data_.size(), nextQty)) && progress_bar) {
          unique_lock<mutex> lock(display_mutex);
          (*progress_bar) += (nextQty - progress_bar->count());
          nextQty += prm.progress_update_qty_;
        }
      }
    }
    if (progress_bar) {
      unique_lock<mutex> lock(display_mutex);
      (*progress_bar) += (progress_bar->expected_count() - progress_bar->count());
    }
  }
};

template <typename dist_t>
struct IndexThreadLG {
  void operator()(IndexLGraphParams<dist_t>& prm) {
    ProgressDisplay*  progress_bar = prm.progress_bar_;
    mutex&            display_mutex(prm.display_mutex_);
    /*
     * Skip the first element, it was added already
     */
    size_t nextQty = prm.progress_update_qty_;
    for (size_t id = 1; id < prm.data_.size(); ++id) {
      if (prm.index_every_ == id % prm.out_of_) {
        LGNode* node = new LGNode(prm.data_[id], id);
        prm.index_.add(node, id);

        if ((id + 1 >= min(prm.data_.size(), nextQty)) && progress_bar) {
          unique_lock<mutex> lock(display_mutex);
          (*progress_bar) += (nextQty - progress_bar->count());
          nextQty += prm.progress_update_qty_;
        }
      }
    }
    if (progress_bar) {
      unique_lock<mutex> lock(display_mutex);
      (*progress_bar) += (progress_bar->expected_count() - progress_bar->count());
    }
  }
};



template <typename dist_t>
LGraph<dist_t>::LGraph(bool PrintProgress,
                                       const Space<dist_t>& space,
                                       const ObjectVector& data) :
                                       space_(space), data_(data), PrintProgress_(PrintProgress) {}


template <typename dist_t>
void LGraph<dist_t>::addCriticalSection(LGNode* element){
  unique_lock<mutex> lock(ElListGuard_);
  ElList_.push_back(element);
}


template <typename dist_t>
void LGraph<dist_t>::CreateIndex(const AnyParams& IndexParams){
  //construir um RNG
  AnyParamManager pmgr(IndexParams);
  pmgr.GetParamOptional("initIndexAttempts",  initIndexAttempts_,   1);
  pmgr.GetParamOptional("indexThreadQty",     indexThreadQty_,      thread::hardware_concurrency());
  LOG(LIB_INFO) << "initIndexAttempts   = " << initIndexAttempts_;
  LOG(LIB_INFO) << "indexThreadQty      = " << indexThreadQty_;

  pmgr.CheckUnused();

  SetQueryTimeParams(getEmptyParams());

  if (data_.empty()) return;

  // 2) One entry should be added before all the threads are started, or else add() will not work properly
  addCriticalSection(new LGNode(data_[0], 0 /* id == 0 */));

  unique_ptr<ProgressDisplay> progress_bar(PrintProgress_ ?
                                new ProgressDisplay(data_.size(), cerr)
                                :NULL);

  if (indexThreadQty_ <= 1) {
    // Skip the first element, one element is already added
    if (progress_bar) ++(*progress_bar);
    for (size_t id = 1; id < data_.size(); ++id) {
      LGNode* node = new LGNode(data_[id], id);
      add(node, id); //verificar e inserir os vizinhos
      if (progress_bar) ++(*progress_bar);
    }
  } else {
    vector<thread>                                    threads(indexThreadQty_);
    vector<shared_ptr<IndexLGraphParams<dist_t>>>   threadParams;
    mutex                                             progressBarMutex;

    for (size_t i = 0; i < indexThreadQty_; ++i) {
      threadParams.push_back(shared_ptr<IndexLGraphParams<dist_t>>(
                              new IndexLGraphParams<dist_t>(space_, *this, data_, i, indexThreadQty_,
                                                              progress_bar.get(), progressBarMutex, 200)));
    }
    for (size_t i = 0; i < indexThreadQty_; ++i) {
      threads[i] = thread(IndexThreadLG<dist_t>(), ref(*threadParams[i]));
    }
    for (size_t i = 0; i < indexThreadQty_; ++i) {
      threads[i].join();
    }
    if (ElList_.size() != data_.size()) {
      stringstream err;
      err << "Bug: ElList_.size() (" << ElList_.size() << ") isn't equal to data_.size() (" << data_.size() << ")";
      LOG(LIB_INFO) << err.str();
      throw runtime_error(err.str());
    }
    LOG(LIB_INFO) << indexThreadQty_ << " indexing threads have finished";
  }
}


template <typename dist_t>
void LGraph<dist_t>::add(LGNode* newElement, size_t idAtual){
  newElement->removeAllNeighbors();
  bool isEmpty = false;
  {
    unique_lock<mutex> lock(ElListGuard_);
    isEmpty = ElList_.empty();
  }
  if(isEmpty){
  // Before add() is called, the first node should be created!
    LOG(LIB_INFO) << "Bug: the list of nodes shouldn't be empty!";
    throw runtime_error("Bug: the list of nodes shouldn't be empty!");
  }
  {
    priority_queue<EvaluatedLGNode<dist_t>> resultSet;
    for (size_t i = ++idAtual; i < data_.size(); i++) {
        LGNode* evaluated = new LGNode(data_[idAtual], idAtual);
        //calcular distancia
        dist_t d = space_.IndexTimeDistance(newElement->getData(), evaluated->getData()); //ERRO!!!!
        EvaluatedLGNode<dist_t>* evNode = new EvaluatedLGNode<dist_t>(evaluated, d);
        if(shouldConnect(evNode, newElement)){
          LOG(LIB_INFO) << "shouldConnect";
          link(evNode->getLGNode(), newElement);
        }
    }
  }
  addCriticalSection(newElement);
}

template <typename dist_t>
bool LGraph<dist_t>::shouldConnect(EvaluatedLGNode<dist_t>* evNode, LGNode* newNode){
  for(size_t i = 0; i < data_.size(); i++){
    dist_t dNewElement = space_.IndexTimeDistance(newNode->getData(), data_[i]);
    dist_t dEvaluated = space_.IndexTimeDistance(evNode->getLGNode()->getData(), data_[i]);
    if(dNewElement <= evNode->getDistance() && dEvaluated <= evNode->getDistance()){
      return false;
    }
  }
  return true;
}

template <typename dist_t>
void LGraph<dist_t>::link(LGNode* first, LGNode* second){
  first->addNeighbor(second);
  second->addNeighbor(first);
}

template <typename dist_t>
LGNode* LGraph<dist_t>::getInitialVertice() const{
  size_t size = ElList_.size();

  if(!ElList_.size()) {
    return NULL;
  } else {
    size_t num = RandomInt()%size;
    return ElList_[num];
  }
}


template <typename dist_t>
void LGraph<dist_t>::Search(KNNQuery<dist_t>* query, IdType) const {//1 nn aproximacao espacial
  if (ElList_.empty()) return ;
  //vector<bool>     visitedBitset(ElList_.size());
  LGNode* currNode = getInitialVertice();
  bool hasNearestNeighbor = false;
  dist_t d = query->DistanceObjLeft(currNode->getData());
  LGNode* nearestNeighbor;
  while(hasNearestNeighbor){
    //vector<LGNode*> neighbors = currNode.getNeighbors();
    hasNearestNeighbor = false;
    for(LGNode* neighbor : currNode->getNeighbors()){
      size_t neighborId = neighbor->getId();
      const Object* currObj = neighbor->getData();
      dist_t dNeighbor = query->DistanceObjLeft(currObj);
      if(dNeighbor < d){
        nearestNeighbor = neighbor;
        d = dNeighbor;
        hasNearestNeighbor = true;
      }
    }
    currNode = nearestNeighbor;
  }
  query->CheckAndAddToResult(d, currNode->getData());

}

template <typename dist_t>
void LGraph<dist_t>::SetQueryTimeParams(const AnyParams& QueryTimeParams) {
  AnyParamManager pmgr(QueryTimeParams);
  pmgr.GetParamOptional("initSearchAttempts", initSearchAttempts_,  1);
  pmgr.CheckUnused();
  LOG(LIB_INFO) << "Set LGraph query-time parameters:";
  LOG(LIB_INFO) << "initSearchAttempts =" << initSearchAttempts_;
}


template class LGraph<float>;
template class LGraph<double>;
template class LGraph<int>;

}
