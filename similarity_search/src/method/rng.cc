#include <cmath>
#include <memory>
#include <iostream>
#include <mmintrin.h>
#include <immintrin.h>
//#include <smmintrin.h>
//#include <tmmintrin.h>

#include "space.h"
#include "knnquery.h"
#include "knnqueue.h"
#include "rangequery.h"
#include "ported_boost_progress.h"
#include "method/rng.h"
#include "sort_arr_bi.h"

#include <vector>

#include <set>
#include <map>
#include <sstream>
#include <typeinfo>


#define MERGE_BUFFER_ALGO_SWITCH_THRESHOLD 100

#define USE_BITSET_FOR_INDEXING 1
//#define USE_ALTERNATIVE_FOR_INDEXING

namespace similarity {
    using namespace std;

    template<typename dist_t>
    struct IndexRNGParams {
        //dados sobre o que colocar no grafo
        const Space<dist_t> &space_;
        RNG<dist_t> &index_;
        const ObjectVector &data_;
        size_t index_every_;
        size_t out_of_;
        ProgressDisplay *progress_bar_;
        mutex &display_mutex_;
        size_t progress_update_qty_;

        IndexRNGParams(
                const Space<dist_t> &space,
                RNG<dist_t> &index,
                const ObjectVector &data,
                size_t index_every,
                size_t out_of,
                ProgressDisplay *progress_bar,
                mutex &display_mutex,
                size_t progress_update_qty
        ) :
                space_(space),
                index_(index),
                data_(data),
                index_every_(index_every),
                out_of_(out_of),
                progress_bar_(progress_bar),
                display_mutex_(display_mutex),
                progress_update_qty_(progress_update_qty) {}


    };

    template<typename dist_t>
    RNG<dist_t>::RNG(bool PrintProgress, const Space<dist_t> &space, const ObjectVector &data) :
            space_(space), data_(data), PrintProgress_(PrintProgress) {
    }


    template<typename dist_t>
    RNGNode *RNG<dist_t>::addCriticalSection(RNGNode *element) {
        //unique_lock<mutex> lock(ElListGuard_);
        ElList_.push_back(element);
        return ElList_.back();
        //LOG(LIB_INFO) << "Critical Section::: " << element->getId();
    }

/*template <typename dist_t>
struct IndexThreadRNGOld {
  void operator()(IndexRNGParams<dist_t>& prm) {
    ProgressDisplay*  progress_bar = prm.progress_bar_;
    mutex&            display_mutex(prm.display_mutex_);
     // Skip the first element, it was added already
    size_t nextQty = prm.progress_update_qty_;
    for (size_t id = 0; id < prm.data_.size(); ++id) { //prm.data_.size()
      if (prm.index_every_ == id % prm.out_of_) {
        RNGNode* node = new RNGNode(prm.data_[id], id);
        //RNGNode* newElement = addCriticalSection(node);
      //  LOG(LIB_INFO) << "Nó inserido atualmente thread:" << newElement->getId();
        prm.index_.add(node, id);
        if ((id + 1 >= min(prm.data_.size(), nextQty)) && progress_bar) {//prm.data_.size()
          unique_lock<mutex> lock(display_mutex);
          (*progress_bar) += (nextQty - progress_bar->count());
          nextQty += prm.progress_update_qty_;
        }
      }
    }
    if (progress_bar) {
      unique_lock<mutex> lock(display_mutex);
      (*progress_bar) += (progress_bar->expected_count() - progress_bar->count());
    }
  }
};*/

    template<typename dist_t>
    struct IndexThreadRNGMatrix {
        void operator()(IndexRNGParams<dist_t> &prm) {
            ProgressDisplay *progress_bar = prm.progress_bar_;
            mutex &display_mutex(prm.display_mutex_);
            // Skip the first element, it was added already
            size_t nextQty = prm.progress_update_qty_;
            for (size_t id = 0; id < prm.data_.size(); ++id) { //prm.data_.size()
                if (prm.index_every_ == id % prm.out_of_) {
                    //RNGNode* node = new RNGNode(prm.data_[id], id);
                    prm.index_.addIndexThreaded(id);
                    if ((id + 1 >= min(prm.data_.size(), nextQty)) && progress_bar) {//prm.data_.size()
                        unique_lock<mutex> lock(display_mutex);
                        (*progress_bar) += (nextQty - progress_bar->count());
                        nextQty += prm.progress_update_qty_;
                    }
                }
            }
            if (progress_bar) {
                unique_lock<mutex> lock(display_mutex);
                (*progress_bar) += (progress_bar->expected_count() - progress_bar->count());
            }
        }
    };

    template<typename dist_t>
    void RNG<dist_t>::addIndexThreaded(size_t idAtual) {
        RNGNode *newElement = ElList_[idAtual];
        for (size_t i = 0; i < ElList_.size(); i++) {
            if (newElement->getId() == idAtual) continue;
            else {
                RNGNode *evaluated = ElList_[i];
                dist_t d;
                //calcular distancia
                d = space_.IndexTimeDistance(newElement->getData(), evaluated->getData());
                EvaluatedRNGNode<dist_t> *evNode = new EvaluatedRNGNode<dist_t>(evaluated, d);
                bool connect = shouldConnect(evNode, newElement);
                if (connect) {
                    LOG(LIB_INFO) << "Link::::" << evNode->getRNGNode()->getId();
                    link(evNode->getRNGNode(), newElement);
                }
                //  LOG(LIB_INFO) << newElement->getNeighbors().size() << "Neighbors";
            }
        }
    }


    template<typename dist_t>
    void RNG<dist_t>::CreateIndex(const AnyParams &IndexParams) {
        //construir um RNG
        AnyParamManager pmgr(IndexParams);
        string tmp;
        pmgr.GetParamOptional("initIndexAttempts", initIndexAttempts_, 1);
        pmgr.GetParamOptional("indexThreadQty", indexThreadQty_, thread::hardware_concurrency());
        pmgr.GetParamOptional("algoType", tmp, "old");
        ToLower(tmp);
        if (tmp == "old") indexAlgoType_ = old;
        else if (tmp == "matrix") indexAlgoType_ = matrix;
        else {
            throw runtime_error("algoType should be one of the following: old, matrix");
        }
        LOG(LIB_INFO) << "initIndexAttempts   = " << initIndexAttempts_;
        LOG(LIB_INFO) << "indexThreadQty      = " << indexThreadQty_;
        LOG(LIB_INFO) << "algoType           =" << indexAlgoType_;

        pmgr.CheckUnused();

        SetQueryTimeParams(getEmptyParams());

        if (data_.empty()) return;
        if(indexAlgoType_ == old){
            IndexOld();
        }else if(indexAlgoType_ == matrix && indexThreadQty_ ==1) IndexMatrix();
        //printGraph("/home/larissa/Documents/survey/testData/rngDataOld.txt");
        //printGraph("/Users/larissashimomura/Documents/mestrado/SurveyGraph/results/rngw/rngNormal30.txt");
    }

    template<typename dist_t>
    void RNG<dist_t>::IndexMatrix() {//n^2 calculos de distancia  --assumindo o triangulo isosceles
        vector<vector<dist_t> > matrix(data_.size(), vector<dist_t>(data_.size(), 0));
        DistanceMatrix = matrix;
        unique_ptr<ProgressDisplay> progress_bar(PrintProgress_ ? new ProgressDisplay(data_.size(), cerr) : NULL);
        if (indexThreadQty_ <= 1) {
           // RNGNode *node = new RNGNode(data_[0], 0);
            //addIndexMatrix(node, 0);
            if (progress_bar) ++(*progress_bar);
            for (size_t id = 0; id < data_.size(); ++id) { //data_.size()
                RNGNode *node = new RNGNode(data_[id], id);
                addCriticalSection(node);
                //LOG(LIB_INFO) << "Número de elementos::" << ElList_.size();
            }
            for(size_t id=0; id < data_.size();id++){
                addIndexMatrix(ElList_[id], id);
            }
        } else { //mais de uma thread
            for (int i = 0; i < data_.size(); i++) {
                RNGNode *node = new RNGNode(data_[i], i);
                addCriticalSection(node);
            }
            vector<thread> threads(indexThreadQty_);
            vector<shared_ptr<IndexRNGParams<dist_t>>> threadParams;
            mutex progressBarMutex;

            for (size_t i = 1; i < indexThreadQty_; ++i) {
                threadParams.push_back(shared_ptr<IndexRNGParams<dist_t>>(
                        new IndexRNGParams<dist_t>(space_, *this, data_, i, indexThreadQty_,
                                                   progress_bar.get(), progressBarMutex, 200)));
            }
            for (size_t i = 0; i < indexThreadQty_; ++i) {
                threads[i] = thread(IndexThreadRNGMatrix<dist_t>(), ref(*threadParams[i]));
            }
            for (size_t i = 0; i < indexThreadQty_; ++i) {
                threads[i].join();
            }
            if (ElList_.size() != data_.size()) {
                stringstream err;
                err << "Bug: ElList_.size() (" << ElList_.size() << ") isn't equal to data_.size() (" << data_.size()
                    << ")";
                LOG(LIB_INFO) << err.str();
                throw runtime_error(err.str());
            }
            LOG(LIB_INFO) << indexThreadQty_ << " indexing threads have finished";
        }
        //writeGephi("gephiIndexMatrix.gexf");
    }


    template<typename dist_t>
    void RNG<dist_t>::printGraph(string file) {
        ofstream outFile(file);
        CHECK_MSG(outFile, "Cannot open file");
        outFile.exceptions(std::ios::badbit);
        for (RNGNode *node : ElList_) {
            for (RNGNode *neighbor : node->getNeighbors()) {
                outFile << node->getId()+1 << ", ";
                outFile << neighbor->getId()+1 << endl;
            }
        }
        outFile.close();
    }

    template<typename dist_t>
    bool RNG<dist_t>::shouldConnectMatrix(EvaluatedRNGNode<dist_t> *evNode, RNGNode *newElement) {
        if (newElement->getId() == evNode->getRNGNode()->getId()) return false;
        for (size_t i = 0; i < data_.size(); i++) { //data_.size();
            dist_t dNewElement, dEvaluated;
            if (DistanceMatrix[newElement->getId()][i] == 0 && DistanceMatrix[i][newElement->getId()] == 0) {
                dNewElement = space_.IndexTimeDistance(newElement->getData(), data_[i]);
                ElListGuard_.lock();
                DistanceMatrix[newElement->getId()][i] = dNewElement;
                DistanceMatrix[i][newElement->getId()] = dNewElement;
                ElListGuard_.unlock();
            } else dNewElement = DistanceMatrix[newElement->getId()][i];
            if (DistanceMatrix[evNode->getRNGNode()->getId()][i] == 0 &&
                DistanceMatrix[i][evNode->getRNGNode()->getId()] == 0) {
                dEvaluated = space_.IndexTimeDistance(evNode->getRNGNode()->getData(), data_[i]);
                ElListGuard_.lock();
                DistanceMatrix[evNode->getRNGNode()->getId()][i] = dEvaluated;
                DistanceMatrix[i][evNode->getRNGNode()->getId()] = dEvaluated;
                ElListGuard_.unlock();
            } else dEvaluated = DistanceMatrix[evNode->getRNGNode()->getId()][i];
            if (dNewElement < evNode->getDistance() && dEvaluated < evNode->getDistance()) {
                return false;
                break;
            }
        }
        return true;
    }

    template<typename dist_t>
    void RNG<dist_t>::addIndexMatrix(RNGNode *newElement, size_t idAtual) {
        bool isEmpty = false;
        {
            unique_lock<mutex> lock(ElListGuard_);
            isEmpty = ElList_.empty();
        }
        if (isEmpty) {
            // Before add() is called, the first node should be created!
            LOG(LIB_INFO) << "Bug: the list of nodes shouldn't be empty!";
            throw runtime_error("Bug: the list of nodes shouldn't be empty!");
        }
        for (size_t i = 0; i < data_.size(); i++) {//data_.size()
            if (i == idAtual) continue;
            else {
                RNGNode *evaluated = ElList_[i];
                //calcular distancia
                dist_t d;
                if (DistanceMatrix[idAtual][i] == 0 && DistanceMatrix[i][idAtual] == 0) {
                    d = space_.IndexTimeDistance(newElement->getData(), evaluated->getData());
                    DistanceMatrix[idAtual][i] = d;
                    DistanceMatrix[i][idAtual] = d;
                } else {
                    d = DistanceMatrix[idAtual][i];
                }
                EvaluatedRNGNode<dist_t> *evNode = new EvaluatedRNGNode<dist_t>(evaluated, d);
                bool connect = shouldConnectMatrix(evNode, newElement);
                if (connect) {
                    //LOG(LIB_INFO) << "Link::::" << evNode->getRNGNode()->getId();
                    link(evNode->getRNGNode(), newElement);
                }
                delete evNode;
            }
        }
    }

    template<typename dist_t>
    void RNG<dist_t>::IndexOld() { //n^3 brute force geral para todas dimensões e todas métricas
        LOG(LIB_INFO) << "Index old";
        unique_ptr<ProgressDisplay> progress_bar(PrintProgress_ ?
                                                 new ProgressDisplay(data_.size(), cerr)
                                                                : NULL);
        if (indexThreadQty_ <= 1) {
            // Skip the first element, one element is already added
            if (progress_bar) ++(*progress_bar);
            for (size_t id = 0; id < data_.size(); id++) { //data_.size()
                RNGNode *node = new RNGNode(data_[id], id);
                addCriticalSection(node);
            }
            for(size_t id=0; id < data_.size();id++){
                add(ElList_[id], id);
            }
        } else {
            //size_t id = 0;
            //RNGNode* node = new RNGNode(data_[id], id);
            //add(node, id);
            for (int i = 0; i < data_.size(); i++) {
                RNGNode *node = new RNGNode(data_[i], i);
                addCriticalSection(node);
            }
            vector<thread> threads(indexThreadQty_);
            vector<shared_ptr<IndexRNGParams<dist_t>>> threadParams;
            mutex progressBarMutex;

            for (size_t i = 0; i < indexThreadQty_; ++i) {
                threadParams.push_back(shared_ptr<IndexRNGParams<dist_t>>(
                        new IndexRNGParams<dist_t>(space_, *this, data_, i, indexThreadQty_,
                                                   progress_bar.get(), progressBarMutex, 200)));
            }
            for (size_t i = 0; i < indexThreadQty_; ++i) {
                threads[i] = thread(IndexThreadRNGMatrix<dist_t>(), ref(*threadParams[i]));
            }
            for (size_t i = 0; i < indexThreadQty_; ++i) {
                threads[i].join();
            }
            if (ElList_.size() != data_.size()) {
                stringstream err;
                err << "Bug: ElList_.size() (" << ElList_.size() << ") isn't equal to data_.size() (" << data_.size()
                    << ")";
                LOG(LIB_INFO) << err.str();
                throw runtime_error(err.str());
            }
            LOG(LIB_INFO) << indexThreadQty_ << " indexing threads have finished";
        }
        LOG(LIB_INFO) << "list size" << ElList_.size();
        writeGephi("rngraphGephiMatrix.gexf");
    }


    template<typename dist_t>
    void RNG<dist_t>::add(RNGNode *newElement, size_t idAtual) {
//  newElement->removeAllNeighbors();
        newElement = addCriticalSection(newElement);
        bool isEmpty = false;
        {
            unique_lock<mutex> lock(ElListGuard_);
            isEmpty = ElList_.empty();
        }
        if (isEmpty) {
            // Before add() is called, the first node should be created!
            LOG(LIB_INFO) << "Bug: the list of nodes shouldn't be empty!";
            throw runtime_error("Bug: the list of nodes shouldn't be empty!");
        }
        //priority_queue<EvaluatedRNGNode<dist_t>> resultSet;
        for (size_t i = 0; i < data_.size(); i++) {//data_.size()
            if (i != idAtual) {
                // LOG(LIB_INFO) << "id::" << i;
                RNGNode *evaluated = ElList_[i];
                //calcular distancia
                dist_t d = space_.IndexTimeDistance(newElement->getData(), evaluated->getData());
                EvaluatedRNGNode<dist_t> *evNode = new EvaluatedRNGNode<dist_t>(evaluated, d);
                bool connect = shouldConnect(evNode, newElement);
                if (connect) {
                    //LOG(LIB_INFO) << "Link::::" << evNode->getRNGNode()->getId();
                    link(evNode->getRNGNode(), newElement);
                }
                delete evNode;
                //  LOG(LIB_INFO) << newElement->getNeighbors().size() << "Neighbors";
            }
        }
    }

    template<typename dist_t>
    bool RNG<dist_t>::shouldConnect(EvaluatedRNGNode<dist_t> *evNode, RNGNode *newNode) {
        if (newNode->getId() != evNode->getRNGNode()->getId()) {
            for (size_t i = 0; i < data_.size(); i++) { //data_.size();
                dist_t dNewElement = space_.IndexTimeDistance(newNode->getData(), data_[i]);
                dist_t dEvaluated = space_.IndexTimeDistance(evNode->getRNGNode()->getData(), data_[i]);
                if (dNewElement < evNode->getDistance() && dEvaluated < evNode->getDistance()) {
                    return false;
                    break;
                }
            }
            return true;
        }
        return false;
    }

    template<typename dist_t>
    void RNG<dist_t>::link(RNGNode *first, RNGNode *second) {
        if(first->hasNeighbor(second->getId())) return;
        first->addNeighbor(second);
        second->addNeighbor(first);
    }

    template<typename dist_t>
    void RNG<dist_t>::SaveIndex(const string &location) {
        ofstream outFile(location);
        CHECK_MSG(outFile, "Cannot open file '" + location + "' for writing");
        outFile.exceptions(std::ios::badbit);
        size_t lineNum = 0;

        WriteField(outFile, METHOD_DESC, StrDesc());
        lineNum++;
//  WriteField(outFile, "NN", NN_); lineNum++;

        for (RNGNode *pNode: ElList_) {
            size_t nodeID = pNode->getId();
            CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                      "Bug: unexpected node ID " + ConvertToString(nodeID) +
                      " for object ID " + ConvertToString(pNode->getData()->id()) +
                      "data_.size() = " + ConvertToString(data_.size()));
            outFile << nodeID << ":" << pNode->getData()->id() << ":";
            for (RNGNode *pNodeFriend: pNode->getNeighbors()) {
                IdType nodeFriendID = pNodeFriend->getId();
                CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                          "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                          " for object ID " + ConvertToString(pNodeFriend->getData()->id()) +
                          "data_.size() = " + ConvertToString(data_.size()));
                outFile << ' ' << nodeFriendID;
            }
            outFile << endl;
            lineNum++;
        }
        outFile << endl;
        lineNum++; // The empty line indicates the end of data entries
        //WriteField(outFile, LINE_QTY, lineNum+1 /* including this line */);
        outFile << LINE_QTY << ":" << lineNum+1;
        outFile.close();
    }

    template<typename dist_t>
    RNGNode *RNG<dist_t>::getInitialVertice() const {
        if (!ElList_.size()) {
            return NULL;
        } else {
            size_t num = RandomInt()%(ElList_.size());
            //size_t num = 8;
            return ElList_[num];
        }
    }

    template<typename dist_t>
    class EvaluatedComparator {
    public:
        bool operator()(EvaluatedRNGNode<dist_t> *n1, EvaluatedRNGNode<dist_t> *n2) {
            //comparison code here
            return n1->getDistance() > n2->getDistance();
        }
    };

    template<typename dist_t>
    //algoritmo do Ocsa
    void RNG<dist_t>::nearestNeighbor(KNNQuery<dist_t> *query) const {
        if (ElList_.empty()) return;
        RNGNode *a = getInitialVertice();
        dist_t d = query->DistanceObjLeft(a->getData());
        //RNGNode *nearestNeighbor;
        priority_queue<EvaluatedRNGNode<dist_t> *, vector<EvaluatedRNGNode<dist_t>>, greater<EvaluatedRNGNode<dist_t>>> candidateQueue;
        //set<RNGNode*> explored;
        set<size_t> exploredId;
        //LOG(LIB_INFO) << "valor inicial de d:" << d;
        while (exploredId.empty() || exploredId.count(a->getId()) == 0) {
            //LOG(LIB_INFO) << "Explored" << explored.size();
            //LOG(LIB_INFO) << "numero de neighbors:" << a->getNeighbors().size();
            priority_queue<EvaluatedRNGNode<dist_t> *, vector<EvaluatedRNGNode<dist_t>>, greater<EvaluatedRNGNode<dist_t>>> Neighbors;
            for (RNGNode *neighbor : a->getNeighbors()) {
                //size_t neighborId = neighbor->getId();
                const Object *currObj = neighbor->getData();
                dist_t dNeighbor = query->DistanceObjLeft(currObj);
                EvaluatedRNGNode<dist_t> eval(neighbor,
                                              dNeighbor);// = *(new EvaluatedRNGNode<dist_t>(neighbor, dNeighbor));
                Neighbors.push(eval);
                //LOG(LIB_INFO) << "Neighbors" << Neighbors.top().getRNGNode()->getId();
                //LOG(LIB_INFO) << "Neighbors dist" << Neighbors.top().getDistance();
            }
            if (Neighbors.top().getDistance() > d) {
                //LOG(LIB_INFO) << "entrou no if";
                const EvaluatedRNGNode<dist_t> closestNeighbor = *(new EvaluatedRNGNode<dist_t>(a, d));
                candidateQueue.push(closestNeighbor);
                //LOG(LIB_INFO) << "candidateQueue" << candidateQueue.top().getRNGNode()->getId();
            }
            exploredId.insert(a->getId());
            a = ElList_[Neighbors.top().getRNGNode()->getId()];
            //a = Neighbors.top().getRNGNode();
            d = Neighbors.top().getDistance();
            // LOG(LIB_INFO) << "Novo valor de a" << a->getId();
        }
        LOG(LIB_INFO) << candidateQueue.top().getRNGNode()->getId();
        query->CheckAndAddToResult(candidateQueue.top().getDistance(),
                                   candidateQueue.top().getRNGNode()->getData());//*/
        //LOG(LIB_INFO) << "Add to result" << candidateQueue.top().getRNGNode()->getId();
    }

    template<typename dist_t>
    void RNG<dist_t>::nearestNeighborGS(KNNQuery<dist_t> *query) const {
        if (ElList_.empty()) return;
        //vector<bool>     visitedBitset(ElList_.size());
        RNGNode *currNode = getInitialVertice();
        LOG(LIB_INFO) << "Initial Vertex:" << currNode->getId();
        //LOG(LIB_INFO) << "Label initial:" << currNode->getData()->label();
        bool hasNearestNeighbor = true;
        size_t nearestId = currNode->getId();
        dist_t d = query->DistanceObjLeft(currNode->getData());
        RNGNode *nearestNeighbor;
        while (hasNearestNeighbor) {
            //vector<RNGNode*> neighbors = currNode.getNeighbors();
            //LOG(LIB_INFO) << "while";
            hasNearestNeighbor = false;
            //LOG(LIB_INFO) << currNode->getNeighbors().size();
            for (RNGNode *neighbor : currNode->getNeighbors()) {
                size_t neighborId = neighbor->getId();
                const Object *currObj = neighbor->getData();
                dist_t dNeighbor = query->DistanceObjLeft(currObj);
                if (dNeighbor < d) {
                    //LOG(LIB_INFO) << "if";
                    nearestNeighbor = neighbor;
                    nearestId = neighborId;
                    d = dNeighbor;
                    hasNearestNeighbor = true;
                }
            }
            // currNode = nearestNeighbor;
            currNode = ElList_[nearestId];
            //LOG(LIB_INFO) << "currNode:" << nearestId;
        }
        //LOG(LIB_INFO) << "result" << currNode->getId();
        query->CheckAndAddToResult(d, currNode->getData());
    }

    template<typename dist_t>
    void RNG<dist_t>::GNNS(KNNQuery<dist_t> *query) const {
        //LOG(LIB_INFO) << "GNNS";
        for (size_t i = 0; i < initSearchAttempts_; i++) {
            size_t curr = RandomInt() % ElList_.size();
            dist_t currDist = query->DistanceObjLeft(ElList_[curr]->getData());
            query->CheckAndAddToResult(currDist, ElList_[curr]->getData());
            size_t currOld;
            do {
                currOld = curr;
                for (RNGNode *e : ElList_[currOld]->getNeighbors()) {
                    size_t currNew = e->getId();
                    //if(currNew != bad){
                    dist_t currDistNew = query->DistanceObjLeft(ElList_[currNew]->getData());
                    query->CheckAndAddToResult(currDistNew, ElList_[currNew]->getData());
                    if (currDistNew < currDist) {
                        curr = currNew;
                        currDist = currDistNew;
                    }
                    //}
                }
            } while (currOld != curr);
        }
    }


    template<typename dist_t>
    void RNG<dist_t>::Search(KNNQuery<dist_t> *query, IdType) const {//1 nn aproximacao espacial
        if(gnns){
            GNNS(query);
        }else if(swsearch){
            swSearch(query);
        }else if(query->GetK()==1){
            if(gs) nearestNeighborGS(query);
            else nearestNeighbor(query);
        }else __throw_runtime_error("No search method for these parameters");
    }

    template<typename dist_t>
    void RNG<dist_t>::Search(RangeQuery<dist_t> *query, IdType) const { //recursivo
        if (ElList_.empty()) return;
        //vector<bool>     visitedBitset(ElList_.size());
        RNGNode *initialVertice = getInitialVertice();
        set<RNGNode *> explored;
        dist_t mindist = query->DistanceObjLeft(initialVertice->getData());
        return RecursiveRange(initialVertice, query, mindist, explored);
        //range query do ocsa
    }

    template<typename dist_t>
    //testar mais
    void RNG<dist_t>::RecursiveRange(RNGNode *initialVertice, RangeQuery<dist_t> *query, dist_t mindist,
                                     set<RNGNode *> explored) const {
        //algoritmo do Ocsa
        if (explored.count(initialVertice) == 0) {
            EvaluatedRNGNode<dist_t> *evalInitial = new EvaluatedRNGNode<dist_t>(initialVertice, query->DistanceObjLeft(
                    initialVertice->getData()));
            if (evalInitial->getDistance() <= query->Radius()) {
                //adicionar ao result
                query->CheckAndAddToResult(evalInitial->getDistance(), evalInitial->getRNGNode()->getData());//*/
            }
            priority_queue<EvaluatedRNGNode<dist_t> *, vector<EvaluatedRNGNode<dist_t>>, greater<EvaluatedRNGNode<dist_t>>> distanceList;
            for (RNGNode *neighbor : initialVertice->getNeighbors()) {
                EvaluatedRNGNode<dist_t> evalNeighbor(neighbor, query->DistanceObjLeft(neighbor->getData()));
                distanceList.push(evalNeighbor);
            }
            mindist = min(mindist, distanceList.top().getDistance());
            dist_t comparison = mindist + (2 * query->Radius());
            //verificar a distancia com o mindist //quando calcula o mindist?
            //for(EvaluatedRNGNode<dist_t> checkNeighbor : distanceList){ //iterar sobre distance list
            for (size_t i = 0; i < distanceList.size(); i++) {
                const EvaluatedRNGNode<dist_t> *top = &distanceList.top();
                distanceList.pop();
                if (top->getDistance() <= comparison) {
                    RecursiveRange(top->getRNGNode(), query, mindist, explored);
                }
            }
            //}
            explored.insert(initialVertice);
        }
    }

    template<typename dist_t>
    void RNG<dist_t>::writeGephi(string file) { //arrumar
        ofstream outFile(file);
        CHECK_MSG(outFile, "Cannot open file '" + file + "' for writing");
        outFile.exceptions(std::ios::badbit);
        //size_t lineNum = 0;

        //WriteField(outFile, METHOD_DESC, StrDesc()); lineNum++;
//  WriteField(outFile, "NN", NN_); lineNum++;
        outFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                << "<gexf xmlns=\"http://www.gexf.net/1.2draft\" version=\"1.2\">\n"
                << "<meta>\n"
                << "<creator>info.debatty.java.graphs.Graph</creator>\n"
                << "<description></description>\n"
                << "</meta>\n"
                << "<graph mode=\"static\" defaultedgetype=\"directed\">\n";
        outFile << "<nodes>\n";
        for (RNGNode *pNode: ElList_) {
            size_t nodeID = pNode->getId();
            CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                      "Bug: unexpected node ID " + ConvertToString(nodeID) +
                      " for object ID " + ConvertToString(pNode->getData()->id()) +
                      "data_.size() = " + ConvertToString(data_.size()));
            //outFile << nodeID << ":" << pNode->getData()->id() << ":";
            outFile << "<node id=\"" << nodeID
                    << "\" label=\"" << pNode->getData()->id() << "\" />\n";
        }
        outFile << "</nodes>\n";
        outFile << "<edges>\n";
        int i = 0;
        for (RNGNode *pNode: ElList_) {
            for (RNGNode *pNodeFriend: pNode->getNeighbors()) {
                IdType nodeFriendID = pNodeFriend->getId();
                CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                          "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                          " for object ID " + ConvertToString(pNodeFriend->getData()->id()) +
                          "data_.size() = " + ConvertToString(data_.size()));
                outFile << "<edge id=\"" << i++ << "\" source=\"" << pNode->getData()->id() << "\" "
                        << "target=\"" << pNodeFriend->getData()->id() << "\" />\n";
            }
        }
        outFile << "</edges>\n";
        outFile << "</graph>\n";
        outFile << "</gexf>";
        outFile.close();
    }

    template<typename dist_t>
    void RNG<dist_t>::LoadIndex(const string &location) {
        //cout << "LOAD INDEX " << endl;
        //cout << "LOAD INDEX " << endl;
        LOG(LIB_INFO) << "LOAD INDEX";
        vector<RNGNode *> ptrMapper(data_.size());

        for (unsigned pass = 0; pass < 2; ++pass) {
            ifstream inFile(location);
            CHECK_MSG(inFile, "Cannot open file '" + location + "' for reading");
            inFile.exceptions(std::ios::badbit);

            size_t lineNum = 1;
            string methDesc;
            ReadField(inFile, METHOD_DESC, methDesc);
            lineNum++;
            CHECK_MSG(methDesc == StrDesc(),
                      "Looks like you try to use an index created by a different method: " + methDesc);
            //ReadField(inFile, "NN", NN_);
            //lineNum++;

            string line;
            while (getline(inFile, line)) {
                if (line.empty()) {
                    lineNum++;
                    break;
                }
                stringstream str(line);
                str.exceptions(std::ios::badbit);
                char c1, c2;
                IdType nodeID, objID;
                CHECK_MSG((str >> nodeID) && (str >> c1) && (str >> objID) && (str >> c2),
                          "Bug or inconsitent data, wrong format, line: " + ConvertToString(lineNum)
                );
                CHECK_MSG(c1 == ':' && c2 == ':',
                          string("Bug or inconsitent data, wrong format, c1=") + c1 + ",c2=" + c2 +
                          " line: " + ConvertToString(lineNum)
                );
                CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                          DATA_MUTATION_ERROR_MSG + " (unexpected node ID " + ConvertToString(nodeID) +
                          " for object ID " + ConvertToString(objID) +
                          " data_.size() = " + ConvertToString(data_.size()) + ")");
                CHECK_MSG(data_[nodeID]->id() == objID,
                          DATA_MUTATION_ERROR_MSG + " (unexpected object ID " + ConvertToString(data_[nodeID]->id()) +
                          " for data element with ID " + ConvertToString(nodeID) +
                          " expected object ID: " + ConvertToString(objID) + ")"
                );
                if (pass == 0) {
                    unique_ptr<RNGNode> node(new RNGNode(data_[nodeID], nodeID));
                    ptrMapper[nodeID] = node.get();
                    ElList_.push_back(node.release());
                } else {
                    RNGNode *pNode = ptrMapper[nodeID];
                    CHECK_MSG(pNode != NULL,
                              "Bug, got NULL pointer in the second pass for nodeID " + ConvertToString(nodeID));
                    IdType nodeFriendID;
                    while (str >> nodeFriendID) {
                        CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                                  "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                                  "data_.size() = " + ConvertToString(data_.size()));
                        RNGNode *pFriendNode = ptrMapper[nodeFriendID];
                        CHECK_MSG(pFriendNode != NULL,
                                  "Bug, got NULL pointer in the second pass for nodeID " +
                                  ConvertToString(nodeFriendID));
                        pNode->addNeighbor(pFriendNode);//, false /* don't check for duplicates */);
                    }
                    CHECK_MSG(str.eof(),
                              "It looks like there is some extract erroneous stuff in the end of the line " +
                              ConvertToString(lineNum));
                }
                ++lineNum;
            }
            //LOG(LIB_INFO) << "ElList size" << ElList_.size();
            size_t ExpLineNum;
            ReadField(inFile, LINE_QTY, ExpLineNum);
            CHECK_MSG(lineNum == ExpLineNum,
                      DATA_MUTATION_ERROR_MSG + " (expected number of lines " + ConvertToString(ExpLineNum) +
                      " read so far doesn't match the number of read lines: " + ConvertToString(lineNum) + ")");
            inFile.close();
        }
    }


    template<typename dist_t>
    void RNG<dist_t>::SetQueryTimeParams(const AnyParams &QueryTimeParams) {
        AnyParamManager pmgr(QueryTimeParams);
        pmgr.GetParamOptional("initSearchAttempts", initSearchAttempts_, 1);
        pmgr.GetParamOptional("gnns", gnns, 0);
        pmgr.GetParamOptional("gs", gs, 0);
        pmgr.GetParamOptional("sw", swsearch, 0);
        pmgr.GetParamOptional("efSearch", efSearch_, 5);
        pmgr.CheckUnused();
        LOG(LIB_INFO) << "Set RNG query-time parameters:";
        LOG(LIB_INFO) << "initSearchAttempts =" << initSearchAttempts_;
        LOG(LIB_INFO) << "gnns" << gnns;
        LOG(LIB_INFO) << "sw" << swsearch;
        LOG(LIB_INFO) << "efSearch" << efSearch_;
    }

    template<typename dist_t>
    void RNG<dist_t>::swSearch(KNNQuery<dist_t> *query) const {
     if (ElList_.empty()) return;
            CHECK_MSG(efSearch_ > 0, "efSearch should be > 0");
/*
 * The trick of using large dense bitsets instead of unordered_set was
 * borrowed from Wei Dong's kgraph: https://github.com/aaalgo/kgraph
 *
 * This trick works really well even in a multi-threaded mode. Indeed, the amount
 * of allocated memory is small. For example, if one has 8M entries, the size of
 * the bitmap is merely 1 MB. Furthermore, setting 1MB of entries to zero via memset would take only
 * a fraction of millisecond.
 */
            vector<bool>                        visitedBitset(ElList_.size());

            for (size_t i=0; i < initSearchAttempts_; i++) {
                /**
                 * Search of most k-closest elements to the query.
                 */
#ifdef START_WITH_E0_AT_QUERY_TIME
                kNNGNode* provider = i ? getRandomEntryPoint(): ElList_[0];
#else
                RNGNode* provider = getInitialVertice();
#endif
                //MSWNode* provider = getRandomEntryPoint();

                priority_queue <dist_t>                          closestDistQueue; //The set of all elements which distance was calculated
                //priority_queue<EvaluatedRNGNode<dist_t> , vector<EvaluatedRNGNode<dist_t> >, less<EvaluatedRNGNode<dist_t> > candidateQueue;
                priority_queue<EvaluatedRNGNode<dist_t>, vector<EvaluatedRNGNode<dist_t>>, greater<EvaluatedRNGNode<dist_t>>> candidateQueue;
                //the set of elements which we can use to evaluate

                const Object* currObj = provider->getData();
                dist_t d = query->DistanceObjLeft(currObj);
                query->CheckAndAddToResult(d, currObj); // This should be done before the object goes to the queue: otherwise it will not be compared to the query at all!

                //EvaluatedkNNGNode<dist_t> ev(provider, d);
                candidateQueue.push(EvaluatedRNGNode<dist_t>(provider, d));
                closestDistQueue.emplace(d);

                size_t nodeId = provider->getId();
                // data_.size() is guaranteed to be equal to ElList_.size()
                if (nodeId >= data_.size()) {
                    stringstream err;
                    err << "Bug: nodeId > data_size()";
                    LOG(LIB_INFO) << err.str();
                    throw runtime_error(err.str());
                }
                visitedBitset[nodeId] = true;

                while(!candidateQueue.empty()){

                    auto iter = candidateQueue.top(); // This one was already compared to the query
                    const EvaluatedRNGNode<dist_t>& currEv = iter;

                    // Did we reach a local minimum?
                    if (currEv.getDistance() > closestDistQueue.top()) {
                        break;
                    }

                    for (RNGNode* neighbor : (currEv.getRNGNode())->getNeighbors()) {
                        _mm_prefetch(reinterpret_cast<const char*>(const_cast<const Object*>(neighbor->getData())), _MM_HINT_T0);
                    }
                    for (RNGNode* neighbor : (currEv.getRNGNode())->getNeighbors()) {
                        _mm_prefetch(const_cast<const char*>(neighbor->getData()->data()), _MM_HINT_T0);
                    }

                    const vector<RNGNode*>& neighbor = (currEv.getRNGNode())->getNeighbors();

                    // Can't access curEv anymore! The reference would become invalid
                    candidateQueue.pop();

                    //calculate distance to each neighbor
                    for (auto iter = neighbor.begin(); iter != neighbor.end(); ++iter){
                        nodeId = (*iter)->getId();
                        // data_.size() is guaranteed to be equal to ElList_.size()
                        if (nodeId >= data_.size()) {
                            stringstream err;
                            err << "Bug: nodeId > data_size()";
                            LOG(LIB_INFO) << err.str();
                            throw runtime_error(err.str());
                        }
                        if (!visitedBitset[nodeId]) {
                            currObj = (*iter)->getData();
                            d = query->DistanceObjLeft(currObj);
                            visitedBitset[nodeId] = true;

                            if (closestDistQueue.size() < efSearch_ || d < closestDistQueue.top()) {
                                closestDistQueue.emplace(d);
                                if (closestDistQueue.size() > efSearch_) {
                                    closestDistQueue.pop();
                                }

                                candidateQueue.emplace(*iter, d);
                            }

                            query->CheckAndAddToResult(d, currObj);
                        }
                    }
                }
            }
        }

    template<typename dist_t>
    void RNG<dist_t>::swSearchV1Merge(KNNQuery<dist_t> *query) const { //searchV1Merge (Small World Search)
        LOG(LIB_INFO) << "Small World Search:: efSearch" << efSearch_ ;
        if (ElList_.empty()) return;
        CHECK_MSG(efSearch_ > 0, "efSearch should be > 0");
        vector<bool> visitedBitset(ElList_.size());
        for (size_t i=0; i < initSearchAttempts_; i++) {
            /**
             * Search of most k-closest elements to the query.
             */
            RNGNode* currNode = getInitialVertice();
            SortArrBI<dist_t,RNGNode*> sortedArr(max<size_t>(efSearch_, query->GetK()));

            const Object* currObj = currNode->getData();
            dist_t d = query->DistanceObjLeft(currObj);
            sortedArr.push_unsorted_grow(d, currNode); // It won't grow

            size_t nodeId = currNode->getId();
            // data_.size() is guaranteed to be equal to ElList_.size()
            CHECK(nodeId < data_.size());

            visitedBitset[nodeId] = true;

            int_fast32_t  currElem = 0;

            typedef typename SortArrBI<dist_t,RNGNode*>::Item  QueueItem;

            vector<QueueItem>& queueData = sortedArr.get_data();
            vector<QueueItem>  itemBuff(8*currNode->getNeighbors().size());//k  do sw
            // efSearch_ is always <= # of elements in the queueData.size() (the size of the BUFFER), but it can be
            // larger than sortedArr.size(), which returns the number of actual elements in the buffer
            while(currElem < min(sortedArr.size(),efSearch_)){
                auto& e = queueData[currElem];
                CHECK(!e.used);
                e.used = true;
                currNode = e.data;
                ++currElem;

                for (RNGNode* neighbor : currNode->getNeighbors()) {
                    _mm_prefetch(reinterpret_cast<const char*>(const_cast<const Object*>(neighbor->getData())), _MM_HINT_T0);
                }
                for (RNGNode* neighbor : currNode->getNeighbors()) {
                    _mm_prefetch(const_cast<const char*>(neighbor->getData()->data()), _MM_HINT_T0);
                }

                if (currNode->getNeighbors().size() > itemBuff.size())
                    itemBuff.resize(currNode->getNeighbors().size());

                size_t itemQty = 0;

                dist_t topKey = sortedArr.top_key();
                //calculate distance to each neighbor
                for (RNGNode* neighbor : currNode->getNeighbors()) {
                    nodeId = neighbor->getId();
                    // data_.size() is guaranteed to be equal to ElList_.size()
                    CHECK(nodeId < data_.size());

                    if (!visitedBitset[nodeId]) {
                        currObj = neighbor->getData();
                        d = query->DistanceObjLeft(currObj);
                        visitedBitset[nodeId] = true;
                        if (sortedArr.size() < efSearch_ || d < topKey) {
                            itemBuff[itemQty++]=QueueItem(d, neighbor);
                        }
                    }
                }

                if (itemQty) {
                    _mm_prefetch(const_cast<const char*>(reinterpret_cast<char*>(&itemBuff[0])), _MM_HINT_T0);
                    std::sort(itemBuff.begin(), itemBuff.begin() + itemQty);

                    size_t insIndex=0;
                    if (itemQty > MERGE_BUFFER_ALGO_SWITCH_THRESHOLD) {
                        insIndex = sortedArr.merge_with_sorted_items(&itemBuff[0], itemQty);

                        if (insIndex < currElem) {
                            currElem = insIndex;
                        }
                    } else {
                        for (size_t ii = 0; ii < itemQty; ++ii) {
                            size_t insIndex = sortedArr.push_or_replace_non_empty_exp(itemBuff[ii].key, itemBuff[ii].data);

                            if (insIndex < currElem) {
                                currElem = insIndex;
                            }
                        }
                    }
                }

                // To ensure that we either reach the end of the unexplored queue or currElem points to the first unused element
                while (currElem < sortedArr.size() && queueData[currElem].used == true)
                    ++currElem;
            }

            for (int_fast32_t i = 0; i < query->GetK() && i < sortedArr.size(); ++i) {
                query->CheckAndAddToResult(queueData[i].key, queueData[i].data->getData());
            }
        }
    }

    template
    class RNG<float>;

    template
    class RNG<double>;

    template
    class RNG<int>;

}
