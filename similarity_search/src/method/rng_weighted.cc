//
// Created by Larissa Shimomura on 05/07/17.
//

#include <cmath>
#include <memory>
#include <iostream>
#include <mmintrin.h>
#include <immintrin.h>
#include <string>
//#include <smmintrin.h>
//#include <tmmintrin.h>

#include "space.h"
#include "knnquery.h"
#include "knnqueue.h"
#include "rangequery.h"
#include "ported_boost_progress.h"
#include "method/knng_weighted.h"
#include "sort_arr_bi.h"
#include "min_heap.h"
#include "max_heap.h"
#include "../min_heap.cc"
#include "../max_heap.cc"

#include <vector>

#include <set>
#include <sstream>
#include <typeinfo>
#include <method/rng_weighted.h>

#define USE_BITSET_FOR_INDEXING 1
#define ita 100 //according to experiments in paredes thesis
#define eps 10
#define MAX inft

namespace similarity{


    using namespace std;

    template<typename dist_t>
    struct IndexRNGWParams{
        //dados sobre o que colocar no grafo
        const Space<dist_t>&                        space_;
        RNGW<dist_t>&                             index_;
        const ObjectVector&                         data_;
        size_t                                      index_every_;
        size_t                                      out_of_;
        ProgressDisplay*                            progress_bar_;
        mutex&                                      display_mutex_;
        size_t                                      progress_update_qty_;

        IndexRNGWParams(
                const Space<dist_t>&             space,
                RNGW<dist_t>&                  index,
                const ObjectVector&              data,
                size_t                           index_every,
                size_t                           out_of,
                ProgressDisplay*                 progress_bar,
                mutex&                           display_mutex,
                size_t                           progress_update_qty
        ) :
                space_(space),
                index_(index),
                data_(data),
                index_every_(index_every),
                out_of_(out_of),
                progress_bar_(progress_bar),
                display_mutex_(display_mutex),
                progress_update_qty_(progress_update_qty)
        { }


    };

    template <typename dist_t>
    RNGW<dist_t>::RNGW(bool PrintProgress,const Space<dist_t>& space,const ObjectVector& data) :
            space_(space), data_(data), PrintProgress_(PrintProgress) {
        vector< vector<dist_t> > matrix(data_.size(), vector<dist_t>(data_.size(),0));
        DistanceMatrix = matrix;
    }


    template <typename dist_t>
    RNGWNode<dist_t>* RNGW<dist_t>::addCriticalSection(RNGWNode<dist_t>* element){
        unique_lock<mutex> lock(ElListGuard_);// arrumar para multi
        ElList_.push_back(element);
        return ElList_.back();
        //LOG(LIB_INFO) << "Critical Section::: " << element->getId();
    }


    template <typename dist_t>
    struct IndexRNGW {
        void operator()(IndexRNGWParams<dist_t>& prm) {
            ProgressDisplay*  progress_bar = prm.progress_bar_;
            mutex&            display_mutex(prm.display_mutex_);
            //primeiro elemento já adicionado
            size_t nextQty = prm.progress_update_qty_;
            for (size_t id = 0; id < prm.data_.size(); ++id) {//prm.data_.size()
                if (prm.index_every_ == id % prm.out_of_) {
                    RNGWNode<dist_t>* node = new RNGWNode<dist_t>(prm.data_[id], id);
                    //RNGWNode<dist_t>* newElement = addCriticalSection(node);
                    //  LOG(LIB_INFO) << "Nó inserido atualmente:" << newElement->getId();
                    prm.index_.addIndexMatrix(node, id);
                    if ((id + 1 >= min(prm.data_.size(), nextQty)) && progress_bar) { //prm.data_.size()
                        unique_lock<mutex> lock(display_mutex);
                        (*progress_bar) += (nextQty - progress_bar->count());
                        nextQty += prm.progress_update_qty_;
                    }
                }
            }
            if (progress_bar) {
                unique_lock<mutex> lock(display_mutex);
                (*progress_bar) += (progress_bar->expected_count() - progress_bar->count());
            }
        }
    };

    template <typename dist_t>
    struct IndexThreadRNGW {
        void operator()(IndexRNGWParams<dist_t>& prm) {
            ProgressDisplay*  progress_bar = prm.progress_bar_;
            mutex&            display_mutex(prm.display_mutex_);
            // Skip the first element, it was added already
            size_t nextQty = prm.progress_update_qty_;
            for (size_t id = 0; id < prm.data_.size(); ++id) { //prm.data_.size()
                if (prm.index_every_ == id % prm.out_of_) {
                    RNGWNode<dist_t>* node = new RNGWNode<dist_t>(prm.data_[id], id);
                    //RNGWNode<dist_t>* newElement = addCriticalSection(node);
                    //  LOG(LIB_INFO) << "Nó inserido atualmente thread:" << newElement->getId();
                    prm.index_.addIndexMatrix(node, id);
                    if ((id + 1 >= min(prm.data_.size(), nextQty)) && progress_bar) {//prm.data_.size()
                        unique_lock<mutex> lock(display_mutex);
                        (*progress_bar) += (nextQty - progress_bar->count());
                        nextQty += prm.progress_update_qty_;
                    }
                }
            }
            if (progress_bar) {
                unique_lock<mutex> lock(display_mutex);
                (*progress_bar) += (progress_bar->expected_count() - progress_bar->count());
            }
        }
    };


    template<typename dist_t>
    void RNGW<dist_t>::IndexMatrix() {//n^2 calculos de distancia  --assumindo o triangulo isosceles
        vector<vector<dist_t> > matrix(data_.size(), vector<dist_t>(data_.size(), 0));
        DistanceMatrix = matrix;
        unique_ptr<ProgressDisplay> progress_bar(PrintProgress_ ? new ProgressDisplay(data_.size(), cerr) : NULL);
            // RNGNode *node = new RNGNode(data_[0], 0);
            //addIndexMatrix(node, 0);
            if (progress_bar) ++(*progress_bar);
            for (size_t id = 0; id < data_.size(); ++id) { //data_.size()
                RNGWNode<dist_t> *node = new RNGWNode<dist_t>(data_[id], id);
                addCriticalSection(node);
                //LOG(LIB_INFO) << "Número de elementos::" << ElList_.size();
            }
            for(size_t id=0; id < data_.size();id++){
                addIndexMatrix(ElList_[id], id);
            }
        //writeGephi("gephiIndexMatrix.gexf");
       // printGraph("/Users/larissashimomura/Documents/mestrado/SurveyGraph/results/rngw/graphRNG30.txt");
    }

    template <typename dist_t>
    void RNGW<dist_t>::addIndexMatrix(RNGWNode<dist_t>* newElement, size_t idAtual) {
        bool isEmpty = false;
        {
            unique_lock<mutex> lock(ElListGuard_);
            isEmpty = ElList_.empty();
        }
        if(isEmpty){
            // Before add() is called, the first node should be created!
            LOG(LIB_INFO) << "Bug: the list of nodes shouldn't be empty!";
            throw runtime_error("Bug: the list of nodes shouldn't be empty!");
        }
        for (size_t i = 0; i < data_.size(); i++) {//data_.size()
            if(i == idAtual) continue;
            else{
                //calcular distancia
                dist_t d;
                if(DistanceMatrix[idAtual][i] == 0 && DistanceMatrix[i][idAtual] == 0){
                    d = space_.IndexTimeDistance(newElement->getData(), ElList_[i]->getData());
                    DistanceMatrix[idAtual][i] = d;
                    DistanceMatrix[i][idAtual] = d;
                }else{
                    d = DistanceMatrix[idAtual][i];
                }
                EvaluatedRNGWNode<dist_t>* evNode = new EvaluatedRNGWNode<dist_t>(ElList_[i], d);
                bool connect = shouldConnectMatrix(evNode, newElement);
                //bool connect = shouldConnectMatrix(evNode, newElement);
                if(connect){
                   // LOG(LIB_INFO) << "Link::::" << evNode->getRNGWNode()->getId();
                    link(evNode->getRNGWNode(), newElement, evNode->getDistance());
                }
                delete evNode;
                //  LOG(LIB_INFO) << newElement->getNeighbors().size() << "Neighbors";
            }
        }
    }

    template <typename dist_t>
    bool RNGW<dist_t>::shouldConnectMatrix(EvaluatedRNGWNode<dist_t>* evNode, RNGWNode<dist_t>* newEl){
        if(newEl->getId() == evNode->getRNGWNode()->getId()) return false;
        for(size_t i = 0; i < data_.size(); i++){ //data_.size();
            dist_t dNewElement, dEvaluated;
            if(DistanceMatrix[newEl->getId()][i] == 0 && DistanceMatrix[i][newEl->getId()] == 0){
                dNewElement = space_.IndexTimeDistance(newEl->getData(), data_[i]);
                DistanceMatrix[newEl->getId()][i] = dNewElement;
                DistanceMatrix[i][newEl->getId()] = dNewElement;
            }else dNewElement = DistanceMatrix[newEl->getId()][i];
            if(DistanceMatrix[evNode->getRNGWNode()->getId()][i] == 0 && DistanceMatrix[i][evNode->getRNGWNode()->getId()] == 0){
                dEvaluated = space_.IndexTimeDistance(evNode->getRNGWNode()->getData(), data_[i]);
                DistanceMatrix[evNode->getRNGWNode()->getId()][i] = dEvaluated;
                DistanceMatrix[i][evNode->getRNGWNode()->getId()] = dEvaluated;
            }else dEvaluated = DistanceMatrix[evNode->getRNGWNode()->getId()][i];
            if(dNewElement < evNode->getDistance() && dEvaluated < evNode->getDistance()){
                return false;
                break;
            }
        }
        return true;
    }


    template<typename dist_t>
    void RNGW<dist_t>::CreateIndex(const AnyParams &IndexParams) {
        AnyParamManager pmgr(IndexParams);
        string tmp;
        pmgr.GetParamOptional("initIndexAttempts", initIndexAttempts_, 1);
        pmgr.GetParamOptional("indexThreadQty", indexThreadQty_, thread::hardware_concurrency());
        pmgr.GetParamOptional("algoType", tmp, "matrix");
        ToLower(tmp);
        if (tmp == "old") indexAlgoType_ = old;
        else if (tmp == "matrix") indexAlgoType_ = matrix;
        else {
            throw runtime_error("algoType should be one of the following: old, matrix");
        }
        LOG(LIB_INFO) << "initIndexAttempts   = " << initIndexAttempts_;
        LOG(LIB_INFO) << "indexThreadQty      = " << indexThreadQty_;
        LOG(LIB_INFO) << "algoType           =" << indexAlgoType_;

        pmgr.CheckUnused();

        SetQueryTimeParams(getEmptyParams());
        unique_ptr<ProgressDisplay> progress_bar(PrintProgress_ ?
                                                 new ProgressDisplay(data_.size(), cerr)
                                                                :NULL);

        if (data_.empty()) return;
        if(indexAlgoType_==matrix && indexThreadQty_==1) IndexMatrix();
        else{
            for (int i = 0; i < data_.size(); i++) {
                RNGWNode<dist_t> *node = new RNGWNode<dist_t>(data_[i], i);
                addCriticalSection(node);
            }
            vector<thread>   threads(indexThreadQty_);
            vector<shared_ptr<IndexRNGWParams<dist_t>>>   threadParams;
            mutex      progressBarMutex;

            for (size_t i = 0; i < indexThreadQty_; ++i) {
                threadParams.push_back(shared_ptr<IndexRNGWParams<dist_t>>(
                        new IndexRNGWParams<dist_t>(space_, *this, data_, i, indexThreadQty_,
                                                    progress_bar.get(), progressBarMutex, 200)));
            }
            for (size_t i = 0; i < indexThreadQty_; ++i) {
                threads[i] = thread(IndexThreadRNGW<dist_t>(), ref(*threadParams[i]));
            }
            for (size_t i = 0; i < indexThreadQty_; ++i) {
                threads[i].join();
            }
            if (ElList_.size() != data_.size()) {
                stringstream err;
                err << "Bug: ElList_.size() (" << ElList_.size() << ") isn't equal to data_.size() (" << data_.size() << ")";
                LOG(LIB_INFO) << err.str();
                throw runtime_error(err.str());
            }
            LOG(LIB_INFO) << indexThreadQty_ << " indexing threads have finished";
        }
        LOG(LIB_INFO) << "Numero de vertices" << ElList_.size();
    }

    template <typename dist_t>
    class EvaluatedComparator{
    public:
        bool operator() (EvaluatedRNGWNode<dist_t>* n1, EvaluatedRNGWNode<dist_t>* n2) {
            //comparison code here
            return n1->getDistance() > n2->getDistance();
        }
    };

    template <typename dist_t> //não direcionado
    void RNGW<dist_t>::link(RNGWNode<dist_t>* first, RNGWNode<dist_t>* second, dist_t distance){
        if(first->hasNeighbor(second->getId())) return;
        //EvaluatedRNGWNode<dist_t>* Fneighbor = new EvaluatedRNGWNode<dist_t>(second, distance);
        //first->addNeighbor(Fneighbor);
        first->addNeighbor(new EvaluatedRNGWNode<dist_t>(second, distance));
        //EvaluatedRNGWNode<dist_t>* Sneighbor = new EvaluatedRNGWNode<dist_t>(first, distance);
        //second->addNeighbor(Sneighbor);
        second->addNeighbor(new EvaluatedRNGWNode<dist_t>(first, distance));
    }

    template <typename dist_t>
    void RNGW<dist_t>::SaveIndex(const string &location) {
        ofstream outFile(location);
        CHECK_MSG(outFile, "Cannot open file '" + location + "' for writing");
        outFile.exceptions(std::ios::badbit);
        size_t lineNum = 0;

        WriteField(outFile, METHOD_DESC, StrDesc()); lineNum++;
//  WriteField(outFile, "NN", NN_); lineNum++;

        for (RNGWNode<dist_t>* pNode: ElList_) {
            size_t nodeID = pNode->getId();
            CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                      "Bug: unexpected node ID " + ConvertToString(nodeID) +
                      " for object ID " + ConvertToString(pNode->getData()->id()) +
                      "data_.size() = " + ConvertToString(data_.size()));
            outFile << nodeID << ":" << pNode->getData()->id() << ":";
            for (EvaluatedRNGWNode<dist_t>* pNodeFriend: pNode->getNeighbors()) {
                IdType nodeFriendID = pNodeFriend->getRNGWNode()->getId();
                CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                          "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                          " for object ID " + ConvertToString(pNodeFriend->getRNGWNode()->getData()->id()) +
                          "data_.size() = " + ConvertToString(data_.size()));
                outFile << ' ' << nodeFriendID;
            }
            outFile << endl; lineNum++;
        }
        outFile << endl; lineNum++; // The empty line indicates the end of data entries
        WriteField(outFile, LINE_QTY, lineNum + 1 /* including this line */);
        outFile.close();
    }

    template<typename dist_t>
    void RNGW<dist_t>::LoadIndex(const string &location) {
        vector<RNGWNode<dist_t> *> ptrMapper(data_.size());

        for (unsigned pass = 0; pass < 2; ++pass) {
            ifstream inFile(location);
            CHECK_MSG(inFile, "Cannot open file '" + location + "' for reading");
            inFile.exceptions(std::ios::badbit);

            size_t lineNum = 1;
            string methDesc;
            ReadField(inFile, METHOD_DESC, methDesc);
            lineNum++;
            CHECK_MSG(methDesc == StrDesc(),
                      "Looks like you try to use an index created by a different method: " + methDesc);

            string line;
            while (getline(inFile, line)) {
                if (line.empty()) {
                    lineNum++;
                    break;
                }
                stringstream str(line);
                str.exceptions(std::ios::badbit);
                char c1, c2;
                IdType nodeID, objID;
                CHECK_MSG((str >> nodeID) && (str >> c1) && (str >> objID) && (str >> c2),
                          "Bug or inconsitent data, wrong format, line: " + ConvertToString(lineNum)
                );
                CHECK_MSG(c1 == ':' && c2 == ':',
                          string("Bug or inconsitent data, wrong format, c1=") + c1 + ",c2=" + c2 +
                          " line: " + ConvertToString(lineNum)
                );
                CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                          DATA_MUTATION_ERROR_MSG + " (unexpected node ID " + ConvertToString(nodeID) +
                          " for object ID " + ConvertToString(objID) +
                          " data_.size() = " + ConvertToString(data_.size()) + ")");
                CHECK_MSG(data_[nodeID]->id() == objID,
                          DATA_MUTATION_ERROR_MSG + " (unexpected object ID " + ConvertToString(data_[nodeID]->id()) +
                          " for data element with ID " + ConvertToString(nodeID) +
                          " expected object ID: " + ConvertToString(objID) + ")"
                );
                if (pass == 0) {
                    unique_ptr<RNGWNode<dist_t>> node(new RNGWNode<dist_t>(data_[nodeID], nodeID));
                    ptrMapper[nodeID] = node.get();
                    ElList_.push_back(node.release());
                } else {
                    RNGWNode<dist_t> *pNode = ptrMapper[nodeID];
                    CHECK_MSG(pNode != NULL,
                              "Bug, got NULL pointer in the second pass for nodeID " + ConvertToString(nodeID));
                    IdType nodeFriendID;
                    while (str >> nodeFriendID) {
                        CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                                  "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                                  "data_.size() = " + ConvertToString(data_.size()));
                        RNGWNode<dist_t> *pFriendNode = ptrMapper[nodeFriendID];
                        CHECK_MSG(pFriendNode != NULL,
                                  "Bug, got NULL pointer in the second pass for nodeID " +
                                  ConvertToString(nodeFriendID));
                        dist_t friendDist = space_.IndexTimeDistance(pNode->getData(), pFriendNode->getData());
                        EvaluatedRNGWNode<dist_t>* friendEval;
                        friendEval->setNode(pFriendNode);
                        friendEval->setDistance(friendDist);
                        pNode->addNeighbor(friendEval);//, false /* don't check for duplicates */);
                    }
                    CHECK_MSG(str.eof(),
                              "It looks like there is some extract erroneous stuff in the end of the line " +
                              ConvertToString(lineNum));
                }
                ++lineNum;
            }

            size_t ExpLineNum;
            ReadField(inFile, LINE_QTY, ExpLineNum);
            CHECK_MSG(lineNum == ExpLineNum,
                      DATA_MUTATION_ERROR_MSG + " (expected number of lines " + ConvertToString(ExpLineNum) +
                      " read so far doesn't match the number of read lines: " + ConvertToString(lineNum) + ")");
            inFile.close();
        }
    }


    template <typename dist_t>
    RNGWNode<dist_t>* RNGW<dist_t>::getInitialVertice() const{
        size_t size = ElList_.size();
        if(!ElList_.size()) {
            return NULL;
        } else {
            size_t num = RandomInt()%size;
            return ElList_[num];
        }
    }


    template <typename dist_t>
    void RNGW<dist_t>::printGraph(string file) {
        ofstream outFile(file);
        CHECK_MSG(outFile, "Cannot open file");
        outFile.exceptions(std::ios::badbit);
        for(RNGWNode<dist_t>* node : ElList_){
            for(EvaluatedRNGWNode<dist_t>* neighbor : node->getNeighbors()){
                outFile << node->getId()+1 << "   ";
                outFile << neighbor->getRNGWNode()->getId()+1 << ", " << neighbor->getDistance() << endl;
            }
        }
        outFile.close();
    }


    template <typename dist_t>
    void printDg(string file, vector<dist_t> vet, string title) {
        ofstream outFile(file);
        CHECK_MSG(outFile, "Cannot open file");
        outFile.exceptions(std::ios::badbit);
        outFile << title << endl;
        for(int i=0; i < vet.size();i++){
            outFile << i << " : " << vet[i] << endl;
        }
        outFile.close();
    }


    template <typename dist_t>
    void RNGW<dist_t>::nearestNeighborGS(KNNQuery<dist_t>* query) const {
        if (ElList_.empty()) return ;
        //vector<bool>     visitedBitset(ElList_.size());
        RNGWNode<dist_t>* currNode = getInitialVertice();
        bool hasNearestNeighbor = true;
        dist_t d = query->DistanceObjLeft(currNode->getData());
        RNGWNode<dist_t>* nearestNeighbor;
        while(hasNearestNeighbor){
            //vector<RNGWNode*> neighbors = currNode.getNeighbors();
            //LOG(LIB_INFO) << "while";
            hasNearestNeighbor = false;
           // LOG(LIB_INFO) << currNode->getNeighbors().size();
            for(const EvaluatedRNGWNode<dist_t>* neighbor : currNode->getNeighbors()){
                size_t neighborId = neighbor->getRNGWNode()->getId();
                const Object* currObj = neighbor->getRNGWNode()->getData();
                dist_t dNeighbor = query->DistanceObjLeft(currObj);
                if(dNeighbor < d){
                   // LOG(LIB_INFO) << "if";
                    nearestNeighbor = neighbor->getRNGWNode();
                    d = dNeighbor;
                    hasNearestNeighbor = true;
                }
            }
            currNode = nearestNeighbor;
        }
        query->CheckAndAddToResult(d, currNode->getData());
    }

    template <typename dist_t>
    dist_t RNGW<dist_t>::CoveringRadius(RNGWNode<dist_t>* u) const{ //ok
        //u = ElList_[u->getId()];
        EvaluatedRNGWNode<dist_t> *neighbor1 = u->getNeighbors()[0];
        dist_t radius = neighbor1->getDistance();
        for(EvaluatedRNGWNode<dist_t>* neighbor : u->getNeighbors()){
            if(neighbor->getDistance() > radius){
                radius = neighbor->getDistance();
            }
        }
        return radius;
    }

    template <typename dist_t>
    class PairCompare{
    public:
        bool operator()(pair<int,dist_t> n1,pair<int,dist_t> n2) {
            return n1.second > n2.second;
        }
    };

    template <typename dist_t> //dijkstra com limite rexp
    vector<dist_t> RNGW<dist_t>::Dijkstra(EvaluatedRNGWNode<dist_t> *p, dist_t rexp) const {
        MinHeap<int, dist_t> minHeap;
        int startingIndex;
        vector<dist_t> dG(ElList_.size(), INFINITY); //manter ordem da ElList_
        //LOG(LIB_INFO) << ElList_.size();
        for(int i=0; i < ElList_.size(); i++){
            if(ElList_[i] == p->getRNGWNode()){
                heapMember<int, dist_t> el;
                dG[i] = 0;
                startingIndex = i;
                el.setNode(i);
                el.setValue(0);
                minHeap.insert(el, i);
                //pQ.push(make_pair(i,0));
            }
            else{
                heapMember<int, dist_t> el;
                el.setNode(i);
                el.setValue(INFINITY);
                minHeap.insert(el, i);
            }
        }//inicializacao (todos com infinito menos o ponto de inicio)
        //LOG(LIB_INFO) << "rexp:" << rexp;
        while(minHeap.getSize() > 0){
            /*LOG(LIB_INFO) << "minHeap";
            for(int i=0; i < minHeap.getSize(); i++){
                LOG(LIB_INFO) << "node:" << minHeap.getItem(i).getNode() << "value:" << minHeap.getItem(i).getValue()  << endl;
            }*/
            heapMember<int, dist_t> minHeapMember = minHeap.extractMin();//retira o primeiro, na primeira iteracao o ponto de inicio dg 0
            int idExtracted = minHeapMember.getNode();
            //LOG(LIB_INFO) << "idExtracted" << idExtracted << " dG:" << dG[idExtracted];
            for(EvaluatedRNGWNode<dist_t> *neighbor : ElList_[idExtracted]->getNeighbors()){ //iteracao sobre os vizinhos
                //LOG(LIB_INFO) << "vizinho de" << idExtracted << "::" << neighbor->getRNGWNode()->getId();
                if(minHeap.contain(neighbor->getRNGWNode()->getId()) && dG[idExtracted] != INFINITY &&
                   (neighbor->getDistance() + dG[idExtracted]) < dG[neighbor->getRNGWNode()->getId()]){
                    //LOG(LIB_INFO) << "idExtracted" << idExtracted << " dG:" << dG[idExtracted];
                    if(neighbor->getDistance()+dG[idExtracted] < rexp) { //verificar rexp
                        dG[neighbor->getRNGWNode()->getId()] = dG[idExtracted] + neighbor->getDistance();
                        //LOG(LIB_INFO) << "neighbor id"<< neighbor->getRNGWNode()->getId() << " peso" << dG[neighbor->getRNGWNode()->getId()];
                        //minHeap.decreaseKey(neighbor->getRNGWNode()->getId(), dG[neighbor->getRNGWNode()->getId()]);
                        minHeap.setNewValue(neighbor->getRNGWNode()->getId(), dG[neighbor->getRNGWNode()->getId()]);
                        //LOG(LIB_INFO) << "descreased:" << minHeap.getId(neighbor->getRNGWNode()->getId()).getValue();
                    }
                }
            }
        }
        return dG;
    }


    template <typename dist_t>
    RNGWNode<dist_t>* RNGW<dist_t>::nextToReview() const { //ok
        heapMember<RNGWNode<dist_t>*,dist_t> cand;
        heapMember<RNGWNode<dist_t>*,dist_t> max;
        while(radii.getSize() > 0){
            cand = radii.extractMin();
            //LOG(LIB_INFO) << "min node id:" << cand.getNode()->getId()+1 << " value:" << cand.getValue();
            if(C.contain(cand.getNode()->getId())){
                if(C.getId((int)cand.getNode()->getId()).getValue()==0){
                  //  LOG(LIB_INFO) << cand.getNode()->getId();
                    return cand.getNode();
                }
            }
        }
        max = C.extractMax();
        return max.getNode();
    }

    template <typename dist_t>
    void RNGW<dist_t>::extractNode(RNGWNode<dist_t>* v) const{ //ok
        //LOG(LIB_INFO) << "extract Node: " << v->getId();
        if(C.contain(v->getId())){
            //LOG(LIB_INFO) << "remove::" << v->getId();
            C.removeId(v->getId());
        }
        return;
    }

    template<typename dist_t>
    void RNGW<dist_t>::useContainerNNQ(EvaluatedRNGWNode<dist_t> *u, dist_t duq, dist_t radius) const {
        //u == p
        dist_t cru = CoveringRadius(u->getRNGWNode());
        //LOG(LIB_INFO) << "covering radius"<<cru << " id:" << u->getRNGWNode()->getId();
        if(container.getDistance() < (cru - duq)){
            //LOG(LIB_INFO) << "container muda" << cru-duq << " id:" << u->getRNGWNode()->getId();
            container.setNode(u->getRNGWNode());
            container.setDistance(cru - duq);
        }
        /*if(radius < container.getDistance()){
            LOG(LIB_INFO) << "container distance"<< container.getDistance();
            set<size_t> UNodeids;
            for(EvaluatedRNGWNode<dist_t> *neighbor : u->getRNGWNode()->getNeighbors()){
               // LOG(LIB_INFO) << neighbor->getRNGWNode()->getId();
                UNodeids.insert(neighbor->getRNGWNode()->getId());
                // UNodeids.push_back(neighbor.getRNGWNode()->getId());
            }
            for(int i=0; i < C.getSize(); i++){                //C = C intersect (p, crp)
                heapMember<RNGWNode<dist_t>*, dist_t> el = C.getItem(i);
                if(UNodeids.find(el.getNode()->getId()) == UNodeids.end()){
                   //LOG(LIB_INFO) << "extract container" << el.getNode()->getId();
                    extractNode(el.getNode()); //extrai todos que não fazem parte de vizinhos de u
                }
            }
        }*/ //parte que a geometria não dá certo para o RNG (apenas para knng)
        /*if(radius < container.getDistance()){
           vector<dist_t> dg = Dijkstra(&container, (1+eps)*container.getDistance());
            for(int i=0; i< C.getSize();i++){
                heapMember<RNGWNode<dist_t>*, dist_t> el = C.getItem(i);
                if((dg[el.getNode()->getId()] - radius) > container.getDistance()){
                    extractNode(el.getNode());
                }
            }
        }*/ //verificar geometria desta parte
    }

    template <typename dist_t>
    pair<size_t, dist_t> RNGW<dist_t>::traverse(KNNQuery<dist_t>* query,RNGWNode<dist_t> *p, dist_t dpq) const{
        //LOG(LIB_INFO) << "id:" << p->getId() << " distance" << dpq;
        EvaluatedRNGWNode<dist_t> *f = nullptr;
        pair<size_t, dist_t> farthest; //id e distance
        farthest.first = p->getId();
        farthest.second = dpq;
        pair<int , dist_t> nearest;
        int u = -1;
        nearest.first = p->getId();
        nearest.second = dpq;
        //LOG(LIB_INFO) << "nnc top Distance::" << nnc.top().getDistance();
        nnc.pop(); //extract max
        EvaluatedRNGWNode<dist_t> n;
        n.setDistance(dpq);
        n.setNode(p);
        nnc.push(n);//insert(p,dpq)
        query->CheckAndAddToResult(p->getData());//insert p dpq na resposta
        RNGWNode<dist_t>* uNode;
        while(nearest.first != -1){// && nearest.first != u){ //seguindo o algoritmo da tese
            u = nearest.first;
            uNode = ElList_[u];
            nearest.first = -1;
           // LOG(LIB_INFO) << "u::" << u << " nearest" << nearest.first;
            for(EvaluatedRNGWNode<dist_t>* v : uNode->getNeighbors()){ //v ∈ NN k (u) d ∩ C
                if(C.contain(v->getRNGWNode()->getId())){
                    dist_t dvq = query->DistanceObjLeft(v->getRNGWNode()->getData());
                    extractNode(v->getRNGWNode()); //extrais o nó v
                    dist_t coveringV = CoveringRadius(v->getRNGWNode());
                    //LOG(LIB_INFO) << "coveringV" << coveringV;
                    //LOG(LIB_INFO) << "neighbor::" << v->getRNGWNode()->getId() << " distancia:" << dvq;
                    if(dvq <= nnc.top().getDistance()){
                        //LOG(LIB_INFO) << "top Distance" << nnc.top().getDistance();
                        nnc.pop();
                        EvaluatedRNGWNode<dist_t> vNearest;
                        vNearest.setNode(v->getRNGWNode());
                        vNearest.setDistance(dvq);
                        nnc.push(vNearest);
                        query->CheckAndAddToResult(vNearest.getRNGWNode()->getData());
                    }
                    if(dvq <= nearest.second){
                        //LOG(LIB_INFO) << "nearestSecond" << nearest.second;
                        nearest.first = (int) v->getRNGWNode()->getId();
                        nearest.second = dvq;
                    }
                    if(dvq >= farthest.second){
                       // LOG(LIB_INFO) << "farthest id:" << farthest.first << " farthest second" << farthest.second;
                        farthest.first = v->getRNGWNode()->getId();
                        farthest.second = dvq;
                    }
                    if(container.getDistance() <= (coveringV - dvq)){
                       // LOG(LIB_INFO) << "container distance" << container.getDistance();
                        container.setNode(v->getRNGWNode());
                        container.setDistance(coveringV - dvq);
                              }
                }
            }
        }
        //LOG(LIB_INFO) << "farthest" << ElList_[farthest.first]->getId();
        //f->setNode(ElList_[farthest.first]);
        //f->setDistance(farthest.second);
        return farthest;
    }


    template <typename dist_t>
    void RNGW<dist_t>::Search(KNNQuery<dist_t>* query, IdType) const{
       // LOG(LIB_INFO) << "Query:::" ;
        query->PrintId();
        container.setNode(nullptr);
        container.setDistance(-INFINITY);
        EvaluatedRNGWNode<dist_t> ini;
        ini.setNode(nullptr);
        ini.setDistance(INFINITY);
        vector<EvaluatedRNGWNode<dist_t>> iniVec(query->GetK(), ini);
        for(EvaluatedRNGWNode<dist_t> ii : iniVec){
            nnc.push(ii);
        }
        //nnc(iniVec);
        //inicializar priority queue nnc NULL INFINITY query->getK()
        vector<dist_t> LB(ElList_.size(), 0);
        dist_t rexp;
        int it = 0;
        for(RNGWNode<dist_t>* u: ElList_){ //inserir todos elementos como candidatos iniciais
            heapMember<RNGWNode<dist_t>*, dist_t> el;
            el.setNode(u);
            el.setValue(0);
            C.insert(el, u->getId());
            el.setValue(CoveringRadius(u));
            radii.insert(el, u->getId());
        }
        while(C.getSize() > 0){
           // LOG(LIB_INFO) << "Candidate size" << C.getSize();
            RNGWNode<dist_t>* pNode = nextToReview();
           // LOG(LIB_INFO) << "Next to review" << pNode->getId();
            dist_t dpq = query->DistanceObjLeft(pNode->getData());
            //LOG(LIB_INFO) << "distance" << dpq;
            EvaluatedRNGWNode<dist_t> pEval(pNode, dpq);
            EvaluatedRNGWNode<dist_t> *p = &pEval;
            //p->setNode(pNode);
            //p->setDistance(dpq);
            //LOG(LIB_INFO) << "p" << p->getRNGWNode()->getId();
            extractNode(p->getRNGWNode());
            //LOG(LIB_INFO) << "Candidate size next" << C.getSize();
            //LOG(LIB_INFO) << "Candidate size" << C.getSize();
            it++; //incrementa it
            if(dpq < nnc.top().getDistance()){
            //if(dpq < query->Radius()){
                //LOG(LIB_INFO) << "query Radius"<< query->Radius();
                //LOG(LIB_INFO) << "nnc distance"<< nnc.top().getDistance();
                pair<size_t , dist_t> farthest = traverse(query, p->getRNGWNode(), p->getDistance());
                p->setNode(ElList_[farthest.first]);
                p->setDistance(farthest.second);
//                LOG(LIB_INFO) << p->getRNGWNode()->getId();
            }
           // LOG(LIB_INFO) << "farthest" << p->getRNGWNode()->getId();
            useContainerNNQ(p, p->getDistance(), nnc.top().getDistance()); //crq = nnc.top.distance raio máximo do elemento de query
            for(EvaluatedRNGWNode<dist_t>* u : p->getRNGWNode()->getNeighbors()){
                if(C.contain(u->getRNGWNode()->getId())){
                   // LOG(LIB_INFO) << "for u id:" << u->getRNGWNode()->getId();
                    if((u->getDistance() - p->getDistance()) > LB[u->getRNGWNode()->getId()]){ //dpu < dpq
                        LB[u->getRNGWNode()->getId()] = u->getDistance() - p->getDistance();
                      //  LOG(LIB_INFO) << "LB id:::" << u->getRNGWNode()->getId() << " LB" << LB[u->getRNGWNode()->getId()];
                    }
                }
            }
            if(radii.getSize() == 0 && it > it_){
                rexp = (1 + eps)*p->getDistance();
               // LOG(LIB_INFO) << "rexp" << rexp;
            }else{
                rexp = INFINITY;
            }
            //LOG(LIB_INFO) << "rexp" << rexp;
            vector<dist_t> dG = Dijkstra(p, rexp);
            //LOG(LIB_INFO) << "dG size" << dG.size();
            string s;
            s.append("/Users/larissashimomura/Documents/mestrado/SurveyGraph/results/rngw/dg_30_");
            s.append(to_string(C.getSize()));
            s.append(".txt");
           // printDg(s, dG, "dG");
            int i =0;
            for(int i=0; i < C.getSize(); i++){
                //while(i <= C.getSize()){
                //LOG(LIB_INFO) << "C size" << C.getSize();
                heapMember<RNGWNode<dist_t>*, dist_t> u = C.getItem(i);
                //LOG(LIB_INFO) << "Valor de LB:" << u.getNode()->getId() << " lb:" << LB[u.getNode()->getId()];
                if((p->getDistance() - dG[u.getNode()->getId()]) > LB[u.getNode()->getId()]){
                    LB[u.getNode()->getId()] = p->getDistance() - dG[u.getNode()->getId()];
                    //LOG(LIB_INFO) << "Atualização de LB" << u.getNode()->getId() << " LB:" << LB[u.getNode()->getId()];
                    if(LB[u.getNode()->getId()] > nnc.top().getDistance()){
                        extractNode(u.getNode());
                        //LOG(LIB_INFO) << "extractNode" << u.getNode()->getId();
                    }else if(rexp == INFINITY && dG[u.getNode()->getId()] < INFINITY){
                        //LOG(LIB_INFO) << "C contents";
                        //for(int j =0; j < C.getSize(); j++){
                        //    heapMember<RNGWNode<dist_t>*, dist_t> u = C.getItem(j);
                        //    LOG(LIB_INFO) << "Pos" << j << " Id" << u.getNode()->getId();
                        //}
                        heapMember<RNGWNode<dist_t>*, dist_t> d = C.getId(u.getNode()->getId());
                        //LOG(LIB_INFO) << "id" << u.getNode()->getId() << " c value" << d.getValue();
                        C.increaseKey(u.getNode()->getId(), dG[u.getNode()->getId()]);
                        d = C.getId(u.getNode()->getId());
                        //LOG(LIB_INFO) << "id" << u.getNode()->getId() << " c value after:" << d.getValue();
                    }
                }
                //LOG(LIB_INFO) << "end while";
                //  i++;
            }
            string g;
            g.append("/Users/larissashimomura/Documents/mestrado/SurveyGraph/results/rngw/lb_");
            g.append(to_string(C.getSize()));
            g.append(".txt");
           // printDg(g, LB, "LB");
        }
        /*LOG(LIB_INFO) << "fora while";
         for(int i=0; i < query->GetK(); i++){ //adicionar os k vizinhos em query (biblioteca)
             query->CheckAndAddToResult(nnc.top().getRNGWNode()->getData());
             nnc.pop();
         }*/
    }


    template <typename dist_t>
    void RNGW<dist_t>::writeGephi(string file){ //arrumar
        ofstream outFile(file);
        CHECK_MSG(outFile, "Cannot open file '" + file + "' for writing");
        outFile.exceptions(std::ios::badbit);
        size_t lineNum = 0;

        //WriteField(outFile, METHOD_DESC, StrDesc()); lineNum++;
//  WriteField(outFile, "NN", NN_); lineNum++;
        outFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                << "<gexf xmlns=\"http://www.gexf.net/1.2draft\" version=\"1.2\">\n"
                << "<meta>\n"
                << "<creator>info.debatty.java.graphs.Graph</creator>\n"
                << "<description></description>\n"
                << "</meta>\n"
                << "<graph mode=\"static\" defaultedgetype=\"directed\">\n";
        outFile << "<nodes>\n";
        for (RNGWNode<dist_t>* pNode: ElList_) {
            size_t nodeID = pNode->getId();
            CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                      "Bug: unexpected node ID " + ConvertToString(nodeID) +
                      " for object ID " + ConvertToString(pNode->getData()->id()) +
                      "data_.size() = " + ConvertToString(data_.size()));
            //outFile << nodeID << ":" << pNode->getData()->id() << ":";
            outFile <<"<node id=\"" << nodeID
                    << "\" label=\"" << pNode->getData()->id() << "\" />\n";
        }
        outFile <<"</nodes>\n";
        outFile <<"<edges>\n";
        int i =0;
        for(RNGWNode<dist_t>* pNode: ElList_){
            for (EvaluatedRNGWNode<dist_t>* p: pNode->getNeighbors()) {
                RNGWNode<dist_t>* pNodeFriend = p->getRNGWNode();
                IdType nodeFriendID = pNodeFriend->getId();
                CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                          "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                          " for object ID " + ConvertToString(pNodeFriend->getData()->id()) +
                          "data_.size() = " + ConvertToString(data_.size()));
                outFile << "<edge id=\"" << i++ << "\" source=\"" << pNode->getData()->id() << "\" "
                        << "target=\"" << pNodeFriend->getData()->id() << "\" />\n";
            }
        }
        outFile << "</edges>\n";
        outFile << "</graph>\n";
        outFile   << "</gexf>";
        outFile.close();
    }


    template <typename dist_t>
    void RNGW<dist_t>::SetQueryTimeParams(const AnyParams& QueryTimeParams) {
        AnyParamManager pmgr(QueryTimeParams);
        pmgr.GetParamOptional("initSearchAttempts", initSearchAttempts_,  1);
        pmgr.CheckUnused();
        LOG(LIB_INFO) << "Set RNG query-time parameters:";
        LOG(LIB_INFO) << "initSearchAttempts =" << initSearchAttempts_;
    }

    template class RNGW<float>;
    template class RNGW<double>;
    template class RNGW<int>;
}
