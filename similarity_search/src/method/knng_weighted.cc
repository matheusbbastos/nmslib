//
// Created by Larissa Shimomura on 05/07/17.
//

#include <cmath>
#include <memory>
#include <iostream>
#include <mmintrin.h>
#include <immintrin.h>
//#include <smmintrin.h>
//#include <tmmintrin.h>

#include "space.h"
#include "knnquery.h"
#include "knnqueue.h"
#include "rangequery.h"
#include "ported_boost_progress.h"
#include "method/knng_weighted.h"
#include "sort_arr_bi.h"
#include "min_heap.h"
#include "max_heap.h"
#include "../min_heap.cc"
#include "../max_heap.cc"

#include <vector>

#include <set>
#include <sstream>
#include <typeinfo>
#include <method/knng_weighted.h>

#define USE_BITSET_FOR_INDEXING 1
//#define ita 100 //according to experiments in paredes thesis
#define eps 10
#define MAX inft

namespace similarity{


    using namespace std;

    template<typename dist_t>
    struct IndexkNNGWParams{
        //dados sobre o que colocar no grafo
        const Space<dist_t>&                        space_;
        kNNGW<dist_t>&                             index_;
        const ObjectVector&                         data_;
        size_t                                      index_every_;
        size_t                                      out_of_;
        ProgressDisplay*                            progress_bar_;
        mutex&                                      display_mutex_;
        size_t                                      progress_update_qty_;

        IndexkNNGWParams(
                const Space<dist_t>&             space,
                kNNGW<dist_t>&                  index,
                const ObjectVector&              data,
                size_t                           index_every,
                size_t                           out_of,
                ProgressDisplay*                 progress_bar,
                mutex&                           display_mutex,
                size_t                           progress_update_qty
        ) :
                space_(space),
                index_(index),
                data_(data),
                index_every_(index_every),
                out_of_(out_of),
                progress_bar_(progress_bar),
                display_mutex_(display_mutex),
                progress_update_qty_(progress_update_qty)
        { }


    };

    template <typename dist_t>
    kNNGW<dist_t>::kNNGW(bool PrintProgress,const Space<dist_t>& space,const ObjectVector& data) :
            space_(space), data_(data), PrintProgress_(PrintProgress) {
    }


    template <typename dist_t>
    kNNGWNode<dist_t>* kNNGW<dist_t>::addCriticalSection(kNNGWNode<dist_t>* element){
        //unique_lock<mutex> lock(ElListGuard_); arrumar para multi
        ElList_.push_back(element);
        return ElList_.back();
        //LOG(LIB_INFO) << "Critical Section::: " << element->getId();
    }


    template <typename dist_t>
    struct IndexkNNGW {
        void operator()(IndexkNNGWParams<dist_t>& prm) {
            ProgressDisplay*  progress_bar = prm.progress_bar_;
            mutex&            display_mutex(prm.display_mutex_);
            //primeiro elemento já adicionado
            size_t nextQty = prm.progress_update_qty_;
            for (size_t id = 0; id < prm.data_.size(); ++id) {//prm.data_.size()
                if (prm.index_every_ == id % prm.out_of_) {
                    kNNGWNode<dist_t>* node = new kNNGWNode<dist_t>(prm.data_[id], id);
                    //kNNGWNode<dist_t>* newElement = addCriticalSection(node);
                    //  LOG(LIB_INFO) << "Nó inserido atualmente:" << newElement->getId();
                    prm.index_.add(node, id);
                    if ((id + 1 >= min(prm.data_.size(), nextQty)) && progress_bar) { //prm.data_.size()
                        unique_lock<mutex> lock(display_mutex);
                        (*progress_bar) += (nextQty - progress_bar->count());
                        nextQty += prm.progress_update_qty_;
                    }
                }
            }
            if (progress_bar) {
                unique_lock<mutex> lock(display_mutex);
                (*progress_bar) += (progress_bar->expected_count() - progress_bar->count());
            }
        }
    };

    template <typename dist_t>
    struct IndexThreadkNNGW {
        void operator()(IndexkNNGWParams<dist_t>& prm) {
            ProgressDisplay*  progress_bar = prm.progress_bar_;
            mutex&            display_mutex(prm.display_mutex_);
            // Skip the first element, it was added already
            size_t nextQty = prm.progress_update_qty_;
            for (size_t id = 0; id < prm.data_.size(); ++id) { //prm.data_.size()
                if (prm.index_every_ == id % prm.out_of_) {
                    kNNGWNode<dist_t>* node = new kNNGWNode<dist_t>(prm.data_[id], id);
                    //kNNGWNode<dist_t>* newElement = addCriticalSection(node);
                    //  LOG(LIB_INFO) << "Nó inserido atualmente thread:" << newElement->getId();
                    prm.index_.add(node, id);
                    if ((id + 1 >= min(prm.data_.size(), nextQty)) && progress_bar) {//prm.data_.size()
                        unique_lock<mutex> lock(display_mutex);
                        (*progress_bar) += (nextQty - progress_bar->count());
                        nextQty += prm.progress_update_qty_;
                    }
                }
            }
            if (progress_bar) {
                unique_lock<mutex> lock(display_mutex);
                (*progress_bar) += (progress_bar->expected_count() - progress_bar->count());
            }
        }
    };


    template <typename dist_t>
    void kNNGW<dist_t>::CreateIndex(const AnyParams& IndexParams){
        //construir um kNNG força bruta com peso
        AnyParamManager pmgr(IndexParams);
        string tmp;
        pmgr.GetParamOptional("initIndexAttempts",  initIndexAttempts_,   1);
        pmgr.GetParamOptional("indexThreadQty",     indexThreadQty_,      thread::hardware_concurrency());
        pmgr.GetParamOptional("NN", k_ , 5); //valor de k, default 5
        pmgr.GetParamOptional("it", it_, 100);
        if(k_ > data_.size()){
            LOG(LIB_INFO) << "Invalid k value!";
            throw runtime_error("Invalid k value!");
        }
        LOG(LIB_INFO) << "initIndexAttempts   = " << initIndexAttempts_;
        LOG(LIB_INFO) << "indexThreadQty      = " << indexThreadQty_;
        LOG(LIB_INFO) << "kNN value          =" << k_;
        LOG(LIB_INFO) << "it* number         =" << it_;

        pmgr.CheckUnused();

        SetQueryTimeParams(getEmptyParams());
        unique_ptr<ProgressDisplay> progress_bar(PrintProgress_ ?
                                                 new ProgressDisplay(data_.size(), cerr)
                                                                :NULL);
        if (data_.empty()) return;
        /*kNNG força bruta */
        if(indexThreadQty_ <= 1){
            for (size_t i = 0; i < data_.size(); i++) { //data_.size()
                kNNGWNode<dist_t> *node = new kNNGWNode<dist_t>(data_[i], i);
                addCriticalSection(node);
                //add(node, i);
            }
            addEdges();
        }else{
            size_t id = 0;
            kNNGWNode<dist_t>* node = new kNNGWNode<dist_t>(data_[id], id);
            //RNGNode* newElement = addCriticalSection(node);
            //  LOG(LIB_INFO) << "Nó inserido atualmente:" << newElement->getId();
            //  add(newElement, id); //verificar e inserir os vizinhos
            //add(node, id);
            vector<thread>   threads(indexThreadQty_);
            vector<shared_ptr<IndexkNNGWParams<dist_t>>>   threadParams;
            mutex      progressBarMutex;

            for (size_t i = 0; i < indexThreadQty_; ++i) {
                threadParams.push_back(shared_ptr<IndexkNNGWParams<dist_t>>(
                        new IndexkNNGWParams<dist_t>(space_, *this, data_, i, indexThreadQty_,
                                                    progress_bar.get(), progressBarMutex, 200)));
            }
            for (size_t i = 0; i < indexThreadQty_; ++i) {
                threads[i] = thread(IndexThreadkNNGW<dist_t>(), ref(*threadParams[i]));
            }
            for (size_t i = 0; i < indexThreadQty_; ++i) {
                threads[i].join();
            }
            if (ElList_.size() != data_.size()) {
                stringstream err;
                err << "Bug: ElList_.size() (" << ElList_.size() << ") isn't equal to data_.size() (" << data_.size() << ")";
                LOG(LIB_INFO) << err.str();
                throw runtime_error(err.str());
            }
            LOG(LIB_INFO) << indexThreadQty_ << " indexing threads have finished";
        }
        //writeGephi("knngwData");
       //printGraph("/home/larissa/Documents/survey/rngw/2nngwData.txt");
      //  printGraph("/Users/larissashimomura/Documents/mestrado/SurveyGraph/results/rngw/knng10.txt");
    }

    template<typename dist_t>
    class EvaluatedComparatorG {
    public:
        bool operator()(EvaluatedkNNGWNode<dist_t> *n1, EvaluatedkNNGWNode<dist_t> *n2) {
            //comparison code here
            return n1->getDistance() < n2->getDistance(); //maior no topo
        }
    };


    template<typename dist_t>
    class EvaluatedPair{
    public:
        bool operator()(pair<int, dist_t> n1, pair<int, dist_t> n2){
            return n1.second < n2.second;
        }
    };



    template<typename dist_t>
    void kNNGW<dist_t>::addEdges() {
        bool isEmpty = false;
        {
            unique_lock<mutex> lock(ElListGuard_);
            isEmpty = ElList_.empty();
        }
        if (isEmpty) {
            // Before add() is called, the first node should be created!
            LOG(LIB_INFO) << "Bug: the list of nodes shouldn't be empty!";
            throw runtime_error("Bug: the list of nodes shouldn't be empty!");
        }
        priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPair<dist_t> > resultSet;
        for(size_t idAtual=0; idAtual < data_.size(); idAtual++){
            //kNNGNode* newElement = ElList_[idAtual];
            //EvaluatedkNNGNode<dist_t> *evNode;
            for (size_t i = 0; i < data_.size(); i++) {//data_.size()
                if (i != idAtual) {
                    //LOG(LIB_INFO) << "id::" << i;
                    //kNNGNode *evaluated = ElList_[i];
                    //calcular distancia
                    dist_t d = space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData());
                    //EvaluatedkNNGNode<dist_t> *evNode = new EvaluatedkNNGNode<dist_t>(ElList_[i], d);
                    //  LOG(LIB_INFO) << newElement->getNeighbors().size() << "Neighbors";
                    //if(resultSet.size() < k_) resultSet.push(EvaluatedkNNGNode<dist_t>(ElList_[i], d));
                    if(resultSet.size() < k_) resultSet.push(make_pair((int)i, d));
                    else{
                        //if(resultSet.top().getDistance() > evNode->getDistance()){
                        //if(resultSet.top().getDistance() > d){
                        if(resultSet.top().second > d){
                            resultSet.pop();
                            //resultSet.push(evNode);
                            //resultSet.push(EvaluatedkNNGNode<dist_t>(ElList_[i], d));
                            resultSet.push(make_pair((int)i, d));
                        }//else delete evNode;
                    }
                }
            }
            for (size_t i = 0; i < k_; i++) {
                //EvaluatedkNNGNode<dist_t> resultNode = resultSet.top();
                pair<int, dist_t> resultNode = resultSet.top();
                resultSet.pop();
                //link(ElList_[idAtual], ElList_[resultNode.getkNNGNode()->getId()], dir);
                //EvaluatedkNNGWNode<dist_t> *neighbor = new EvaluatedkNNGWNode<dist_t>(ElList_[resultNode.first], resultNode.second);
                //link(ElList_[idAtual], neighbor);
                link(ElList_[idAtual], new EvaluatedkNNGWNode<dist_t>(ElList_[resultNode.first], resultNode.second));
                //delete resultNode;
            }
            if(!resultSet.empty()) LOG(LIB_INFO) << "NOT EMPTY";
            //resultSet = priority_queue<EvaluatedkNNGNode<dist_t> , vector<EvaluatedkNNGNode<dist_t> >, EvaluatedComparatorConst<dist_t> >();
            resultSet = priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPair<dist_t> >();
            //clear resultSet ///destroy it
        }
    }


    template <typename dist_t>
    class EvaluatedComparator{
    public:
        bool operator() (EvaluatedkNNGWNode<dist_t>* n1, EvaluatedkNNGWNode<dist_t>* n2) {
            //comparison code here
            return n1->getDistance() > n2->getDistance();
        }
    };

   template <typename dist_t>
    void kNNGW<dist_t>::add(kNNGWNode<dist_t>* newElement, size_t idAtual){
//  newElement->removeAllNeighbors();
        newElement = addCriticalSection(newElement);
        bool isEmpty = false;
        {
            unique_lock<mutex> lock(ElListGuard_);
            isEmpty = ElList_.empty();
        }
        if(isEmpty){
            // Before add() is called, the first node should be created!
            LOG(LIB_INFO) << "Bug: the list of nodes shouldn't be empty!";
            throw runtime_error("Bug: the list of nodes shouldn't be empty!");
        }
        priority_queue<EvaluatedkNNGWNode<dist_t>*, vector<EvaluatedkNNGWNode<dist_t>*>, EvaluatedComparator<dist_t>> resultSet;
        for (size_t i = 0; i < data_.size(); i++) {//data_.size()
            if(i != idAtual){
                //LOG(LIB_INFO) << "id::" << i;
                kNNGWNode<dist_t>* evaluated = new kNNGWNode<dist_t>(data_[i], i);
                //calcular distancia
                dist_t d = space_.IndexTimeDistance(newElement->getData(), evaluated->getData());
                EvaluatedkNNGWNode<dist_t>* evNode = new EvaluatedkNNGWNode<dist_t>(evaluated, d);
                //  LOG(LIB_INFO) << newElement->getNeighbors().size() << "Neighbors";
                resultSet.push(evNode);
            }
        }
        for(size_t i = 0; i < k_ ; i++){
            EvaluatedkNNGWNode<dist_t>* resultNode = resultSet.top();
            resultSet.pop();
            link(newElement, resultNode);
        }
        //  addCriticalSection(newElement);
    }

    template <typename dist_t>
    void kNNGW<dist_t>::link(kNNGWNode<dist_t>* first, EvaluatedkNNGWNode<dist_t>* second){
       // LOG(LIB_INFO) << "peso id:" << second->getDistance() << "id" << second->getkNNGWNode()->getId();
        first->addNeighbor(second); //grafo direcionado
    }

    template <typename dist_t>
    void kNNGW<dist_t>::SaveIndex(const string &location) {
        ofstream outFile(location);
        CHECK_MSG(outFile, "Cannot open file '" + location + "' for writing");
        outFile.exceptions(std::ios::badbit);
        size_t lineNum = 0;

        WriteField(outFile, METHOD_DESC, StrDesc()); lineNum++;
        WriteField(outFile, "NN", k_); lineNum++;

        for (kNNGWNode<dist_t>* pNode: ElList_) {
            size_t nodeID = pNode->getId();
            CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                      "Bug: unexpected node ID " + ConvertToString(nodeID) +
                      " for object ID " + ConvertToString(pNode->getData()->id()) +
                      "data_.size() = " + ConvertToString(data_.size()));
            outFile << nodeID << ":" << pNode->getData()->id() << ":";
            for (EvaluatedkNNGWNode<dist_t>* pNodeFriend: pNode->getNeighbors()) {
                IdType nodeFriendID = pNodeFriend->getkNNGWNode()->getId();
                CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                          "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                          " for object ID " + ConvertToString(pNodeFriend->getkNNGWNode()->getData()->id()) +
                          "data_.size() = " + ConvertToString(data_.size()));
                outFile << ' ' << nodeFriendID ;
            }
            outFile << endl; lineNum++;
        }
        outFile << endl; lineNum++; // The empty line indicates the end of data entries
        WriteField(outFile, LINE_QTY, lineNum + 1 /* including this line */);
        outFile.close();
    }

    template<typename dist_t>
    void kNNGW<dist_t>::LoadIndex(const string &location) {
        vector<kNNGWNode<dist_t> *> ptrMapper(data_.size());

        for (unsigned pass = 0; pass < 2; ++pass) {
            ifstream inFile(location);
            CHECK_MSG(inFile, "Cannot open file '" + location + "' for reading");
            inFile.exceptions(std::ios::badbit);

            size_t lineNum = 1;
            string methDesc;
            ReadField(inFile, METHOD_DESC, methDesc);
            lineNum++;
            CHECK_MSG(methDesc == StrDesc(),
                      "Looks like you try to use an index created by a different method: " + methDesc);
            ReadField(inFile, "NN", k_);
            lineNum++;

            string line;
            while (getline(inFile, line)) {
                if (line.empty()) {
                    lineNum++;
                    break;
                }
                stringstream str(line);
                str.exceptions(std::ios::badbit);
                char c1, c2;
                IdType nodeID, objID;
                CHECK_MSG((str >> nodeID) && (str >> c1) && (str >> objID) && (str >> c2),
                          "Bug or inconsitent data, wrong format, line: " + ConvertToString(lineNum)
                );
                CHECK_MSG(c1 == ':' && c2 == ':',
                          string("Bug or inconsitent data, wrong format, c1=") + c1 + ",c2=" + c2 +
                          " line: " + ConvertToString(lineNum)
                );
                CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                          DATA_MUTATION_ERROR_MSG + " (unexpected node ID " + ConvertToString(nodeID) +
                          " for object ID " + ConvertToString(objID) +
                          " data_.size() = " + ConvertToString(data_.size()) + ")");
                CHECK_MSG(data_[nodeID]->id() == objID,
                          DATA_MUTATION_ERROR_MSG + " (unexpected object ID " + ConvertToString(data_[nodeID]->id()) +
                          " for data element with ID " + ConvertToString(nodeID) +
                          " expected object ID: " + ConvertToString(objID) + ")"
                );
                if (pass == 0) {
                    unique_ptr<kNNGWNode<dist_t>> node(new kNNGWNode<dist_t>(data_[nodeID], nodeID));
                    ptrMapper[nodeID] = node.get();
                    ElList_.push_back(node.release());
                } else {
                    kNNGWNode<dist_t> *pNode = ptrMapper[nodeID];
                    CHECK_MSG(pNode != NULL,
                              "Bug, got NULL pointer in the second pass for nodeID " + ConvertToString(nodeID));
                    IdType nodeFriendID;
                    while (str >> nodeFriendID) {
                        CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                                  "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                                  "data_.size() = " + ConvertToString(data_.size()));
                        kNNGWNode<dist_t> *pFriendNode = ptrMapper[nodeFriendID];
                        CHECK_MSG(pFriendNode != NULL,
                                  "Bug, got NULL pointer in the second pass for nodeID " +
                                  ConvertToString(nodeFriendID));
                        dist_t friendDist = space_.IndexTimeDistance(pNode->getData(), pFriendNode->getData());
                        EvaluatedkNNGWNode<dist_t>* friendEval;
                        friendEval->setNode(pFriendNode);
                        friendEval->setDistance(friendDist);
                        pNode->addNeighbor(friendEval);//, false /* don't check for duplicates */);
                    }
                    CHECK_MSG(str.eof(),
                              "It looks like there is some extract erroneous stuff in the end of the line " +
                              ConvertToString(lineNum));
                }
                ++lineNum;
            }

            size_t ExpLineNum;
            ReadField(inFile, LINE_QTY, ExpLineNum);
            CHECK_MSG(lineNum == ExpLineNum,
                      DATA_MUTATION_ERROR_MSG + " (expected number of lines " + ConvertToString(ExpLineNum) +
                      " read so far doesn't match the number of read lines: " + ConvertToString(lineNum) + ")");
            inFile.close();
        }
    }


    template <typename dist_t>
    kNNGWNode<dist_t>* kNNGW<dist_t>::getInitialVertice() const{
        size_t size = ElList_.size();
        if(!ElList_.size()) {
            return NULL;
        } else {
            size_t num = RandomInt()%size;
            return ElList_[num];
        }
    }

    template <typename dist_t>
    void kNNGW<dist_t>::printGraph(string file) {
        ofstream outFile(file);
        CHECK_MSG(outFile, "Cannot open file");
        outFile.exceptions(std::ios::badbit);
        for(kNNGWNode<dist_t>* node : ElList_){
            for(EvaluatedkNNGWNode<dist_t>* neighbor : node->getNeighbors()){
                //LOG(LIB_INFO) << node->getId() << "   ";
                outFile << node->getId() << "   ";
                //LOG(LIB_INFO) <<  neighbor->getkNNGWNode()->getId() << ", " << neighbor->getDistance();
                outFile << neighbor->getkNNGWNode()->getId() << ", " << neighbor->getDistance() << endl;
            }
        }
        outFile.close();
    }


    template <typename dist_t>
    void kNNGW<dist_t>::nearestNeighborGS(KNNQuery<dist_t>* query) const {
        if (ElList_.empty()) return ;
        //vector<bool>     visitedBitset(ElList_.size());
        kNNGWNode<dist_t>* currNode = getInitialVertice();
        bool hasNearestNeighbor = true;
        dist_t d = query->DistanceObjLeft(currNode->getData());
        kNNGWNode<dist_t>* nearestNeighbor;
        while(hasNearestNeighbor){
            //vector<kNNGWNode*> neighbors = currNode.getNeighbors();
            //LOG(LIB_INFO) << "while";
            hasNearestNeighbor = false;
            //LOG(LIB_INFO) << currNode->getNeighbors().size();
            for(const EvaluatedkNNGWNode<dist_t>* neighbor : currNode->getNeighbors()){
                size_t neighborId = neighbor->getkNNGWNode()->getId();
                const Object* currObj = neighbor->getkNNGWNode()->getData();
                dist_t dNeighbor = query->DistanceObjLeft(currObj);
                if(dNeighbor < d){
                    //LOG(LIB_INFO) << "if";
                    nearestNeighbor = neighbor->getkNNGWNode();
                    d = dNeighbor;
                    hasNearestNeighbor = true;
                }
            }
            currNode = nearestNeighbor;
        }
        query->CheckAndAddToResult(d, currNode->getData());
    }

    //seguir heuristica rq 2

    //verificar no metodo de construcao de o ultimo inserido sempre é o mais distante (k-esimo)
    template <typename dist_t>
    dist_t kNNGW<dist_t>::CoveringRadius(kNNGWNode<dist_t>* u) const{//ok
        u = ElList_[u->getId()];
        EvaluatedkNNGWNode<dist_t> *neighbor1 = u->getNeighbors()[0];
        dist_t radius = neighbor1->getDistance();
        for(EvaluatedkNNGWNode<dist_t>* neighbor : u->getNeighbors()){
            if(neighbor->getDistance() > radius){
                radius = neighbor->getDistance();
            }
        }
        return radius;
    }

    template <typename dist_t>
    class PairCompare{
    public:
        bool operator()(pair<int,dist_t> n1,pair<int,dist_t> n2) {
            return n1.second > n2.second;
        }
    };

    template <typename dist_t> //dijkstra com limite rexp
    vector<dist_t> kNNGW<dist_t>::Dijkstra(EvaluatedkNNGWNode<dist_t> *p, dist_t rexp) const {
        MinHeap<int, dist_t> minHeap;
        int startingIndex;
        vector<dist_t> dG(ElList_.size(), INFINITY); //manter ordem da ElList_
        for(int i=0; i < ElList_.size(); i++){
            if(ElList_[i] == p->getkNNGWNode()){
                heapMember<int, dist_t> el;
                dG[i] = 0;
                startingIndex = i;
                el.setNode(i);
                el.setValue(0);
                minHeap.insert(el, i);
                //pQ.push(make_pair(i,0));
            }
            else{
                heapMember<int, dist_t> el;
                el.setNode(i);
                el.setValue(INFINITY);
                minHeap.insert(el, i);
            }
        }//inicializacao
        while(minHeap.getSize() > 0){
            heapMember<int, dist_t> minHeapMember = minHeap.extractMin();
            int idExtracted = minHeapMember.getNode();
            for(EvaluatedkNNGWNode<dist_t> *neighbor : ElList_[idExtracted]->getNeighbors()){
                if(minHeap.contain(neighbor->getkNNGWNode()->getId()) && dG[idExtracted] != INFINITY &&
                        (neighbor->getDistance() + dG[idExtracted]) < dG[neighbor->getkNNGWNode()->getId()]){
                    if(neighbor->getDistance()+dG[idExtracted] <= rexp) { //verificar rexp
                        dG[neighbor->getkNNGWNode()->getId()] = dG[idExtracted] + neighbor->getDistance();
                        //minHeap.decreaseKey(neighbor->getkNNGWNode()->getId(), dG[neighbor->getkNNGWNode()->getId()]);
                        minHeap.setNewValue(neighbor->getkNNGWNode()->getId(), dG[neighbor->getkNNGWNode()->getId()]);
                    }
                }
            }
        }
        return dG;
    }


    template <typename dist_t>
    kNNGWNode<dist_t>* kNNGW<dist_t>::nextToReview() const { //ok
        heapMember<kNNGWNode<dist_t>*,dist_t> cand;
        heapMember<kNNGWNode<dist_t>*,dist_t> max;
        while(radii.getSize() > 0){
            cand = radii.extractMin();
            if(C.contain(cand.getNode()->getId())){
                if(C.getId((int)cand.getNode()->getId()).getValue()==0){
                    return cand.getNode();
                }
            }
        }
        //LOG(LIB_INFO) << "extract antwes";
        max = C.extractMax();
        //LOG(LIB_INFO) << "extract max" ;
        return max.getNode();
    }

    template <typename dist_t>
    void kNNGW<dist_t>::extractNode(kNNGWNode<dist_t>* v) const{ //ok
        //LOG(LIB_INFO) << "extract Node: " << v->getId();
        if(C.contain(v->getId())){
            //LOG(LIB_INFO) << "contain";
            //LOG(LIB_INFO) << C.getSize();
            C.removeId(v->getId());
        }
        //LOG(LIB_INFO) << "extract node" ;
        return;
    }

    template<typename dist_t>
    void kNNGW<dist_t>::useContainerNNQ(EvaluatedkNNGWNode<dist_t> *u, dist_t duq, dist_t radius) const {
        //u == p
        dist_t cru = CoveringRadius(u->getkNNGWNode());
        if(container.getDistance() < (cru - duq)){
            container.setNode(u->getkNNGWNode());
            container.setDistance(cru - duq);
        }
        //LOG(LIB_INFO) << "container nnq";
        if(radius < container.getDistance()){
            //LOG(LIB_INFO) << "raio";
            set<size_t> UNodeids;
            for(EvaluatedkNNGWNode<dist_t> *neighbor : u->getkNNGWNode()->getNeighbors()){
                //LOG(LIB_INFO) << neighbor->getkNNGWNode()->getId();
                UNodeids.insert(neighbor->getkNNGWNode()->getId());
                // UNodeids.push_back(neighbor.getkNNGWNode()->getId());
            }
            for(int i=0; i < C.getSize(); i++){                //C = C intersect (p, crp)
                heapMember<kNNGWNode<dist_t>*, dist_t> el = C.getItem(i);
                if(UNodeids.find(el.getNode()->getId()) == UNodeids.end()){
                   // LOG(LIB_INFO) << "Extract::" << el.getNode()->getId();
                    extractNode(el.getNode()); //extrai todos que não fazem parte de vizinhos de u
                }
            }
        }
    }

    template <typename dist_t>
    pair<size_t, dist_t> kNNGW<dist_t>::traverse(KNNQuery<dist_t>* query,kNNGWNode<dist_t> *p, dist_t dpq) const{
       // LOG(LIB_INFO) << "traverse";
        EvaluatedkNNGWNode<dist_t> *f = nullptr;
        pair<size_t, dist_t> farthest; //id e distance
        farthest.first = p->getId();
        farthest.second = dpq;
        pair<int , dist_t> nearest;
        int u = -1;
        nearest.first = p->getId();
        nearest.second = dpq;
        nnc.pop(); //extract max
        EvaluatedkNNGWNode<dist_t> n;
        n.setDistance(dpq);
        n.setNode(p);
        nnc.push(n);//insert(p,dpq)
        query->CheckAndAddToResult(p->getData());//insert p dpq na resposta
        kNNGWNode<dist_t>* uNode;
        while(nearest.first != -1){// && nearest.first != u){ //seguindo o algoritmo da tese
            u = nearest.first;
            uNode = ElList_[(size_t )u];
            nearest.first = -1;
            for(EvaluatedkNNGWNode<dist_t>* v : uNode->getNeighbors()){ //v ∈ NN k (u) d ∩ C
                if(C.contain(v->getkNNGWNode()->getId())){
                    dist_t dvq = query->DistanceObjLeft(v->getkNNGWNode()->getData());
                    extractNode(v->getkNNGWNode()); //extrais o nó v
                    //LOG(LIB_INFO) << v->getkNNGWNode()->getId();
                    dist_t coveringV = CoveringRadius(v->getkNNGWNode());
                    //LOG(LIB_INFO) << "coveringV" << coveringV;
                    //LOG(LIB_INFO) << "neighbor::" << v->getkNNGWNode()->getId() << " distancia:" << dvq;
                    if(dvq <= nnc.top().getDistance()){
                        //LOG(LIB_INFO) << "top Distance" << nnc.top().getDistance();
                        nnc.pop();
                        EvaluatedkNNGWNode<dist_t> vNearest;
                        vNearest.setNode(v->getkNNGWNode());
                        vNearest.setDistance(dvq);
                        nnc.push(vNearest);
                        query->CheckAndAddToResult(vNearest.getkNNGWNode()->getData());
                    }
                    if(dvq <= nearest.second){
                       // LOG(LIB_INFO) << "nearestSecond" << nearest.second;
                        nearest.first = (int) v->getkNNGWNode()->getId();
                        nearest.second = dvq;
                    }
                    if(dvq >= farthest.second){
                       // LOG(LIB_INFO) << "farthest id:" << farthest.first << " farthest second" << farthest.second;
                        farthest.first = v->getkNNGWNode()->getId();
                        farthest.second = dvq;
                    }
                    if(container.getDistance() <= (coveringV - dvq)){
                       // LOG(LIB_INFO) << "container distance" << container.getDistance();
                        container.setNode(v->getkNNGWNode());
                        container.setDistance(coveringV - dvq);
                       // LOG(LIB_INFO) << "container distance after" << container.getDistance();
                    }
                }
            }
        }
        //LOG(LIB_INFO) << "farthest" << ElList_[farthest.first]->getId();
        //f->setNode(ElList_[farthest.first]);
        //f->setDistance(farthest.second);
        return farthest;
    }

    template<typename dist_t>
    vector<dist_t> kNNGW<dist_t>::LimDijkstra(EvaluatedkNNGWNode<dist_t> *p, vector<int> visitedBitset, dist_t rexp) const {
        MinHeap<int, dist_t> minHeap;
        int startingIndex;
        vector<dist_t> dG(ElList_.size(), INFINITY); //manter ordem da ElList_
        for(int i=0; i < ElList_.size(); i++){
            if(ElList_[i] == p->getkNNGWNode()){
                heapMember<int, dist_t> el;
                dG[i] = 0;
                startingIndex = i;
                el.setNode(i);
                el.setValue(0);
                minHeap.insert(el, i);
                //pQ.push(make_pair(i,0));
            }
            else{
                heapMember<int, dist_t> el;
                el.setNode(i);
                el.setValue(INFINITY);
                minHeap.insert(el, i);
            }
        }//inicializacao
        while(minHeap.getSize() > 0) {
            heapMember<int, dist_t> minHeapMember = minHeap.extractMin();
            int idExtracted = minHeapMember.getNode();
            if (visitedBitset[idExtracted] == 1) { //limitar a explorar apenas a região que já visitou?
                for (EvaluatedkNNGWNode<dist_t> *neighbor : ElList_[idExtracted]->getNeighbors()) {
                    if (minHeap.contain(neighbor->getkNNGWNode()->getId()) && dG[idExtracted] != INFINITY &&
                        (neighbor->getDistance() + dG[idExtracted]) < dG[neighbor->getkNNGWNode()->getId()]) {
                        if (neighbor->getDistance() + dG[idExtracted] <= rexp) { //verificar rexp
                            dG[neighbor->getkNNGWNode()->getId()] = dG[idExtracted] + neighbor->getDistance();
                            //minHeap.decreaseKey(neighbor->getkNNGWNode()->getId(), dG[neighbor->getkNNGWNode()->getId()]);
                            minHeap.setNewValue(neighbor->getkNNGWNode()->getId(),
                                                dG[neighbor->getkNNGWNode()->getId()]);
                        }
                    }
                }
            }
        }
        return dG;
    }


   template <typename dist_t>
    void kNNGW<dist_t>::Search(KNNQuery<dist_t>* query, IdType) const{
        if(chavez_){
            LOG(LIB_INFO) << "Exact Search Algorithm";
            SearchChavez(query);
        }else if(approx_){
            LOG(LIB_INFO) << "Approx Search Algorithm";
            SearchApproximate(query);
        }
    }

    template <typename dist_t>
    void kNNGW<dist_t>::SearchApproximate(KNNQuery<dist_t> *query) const {
        LOG(LIB_INFO) << "search approx";
        vector<int> visitedBitset(ElList_.size(), 0);
        for(int i=0; i < initSearchAttempts_ ; i++) {
            vector<dist_t> LB(ElList_.size(), 0);//vetor de lowerbounds
            MinHeap<kNNGWNode<dist_t> *, dist_t> candidates;//min heap para candidates
            size_t num = RandomInt() % ElList_.size();
            kNNGWNode<dist_t> *pNode = ElList_[num];//start == random
            dist_t dpq = query->DistanceObjLeft(pNode->getData());
            heapMember<kNNGWNode<dist_t> *, dist_t> el;
            el.setValue(0);//start node
            el.setNode(pNode);
            candidates.insert(el, pNode->getId());
            do {
                dist_t dpq = query->DistanceObjLeft(pNode->getData());
                EvaluatedkNNGWNode<dist_t> pEval(pNode, dpq);
                EvaluatedkNNGWNode<dist_t> *p = &pEval;
                query->CheckAndAddToResult(dpq, pNode->getData()); //adiciona na lista de respostas
                visitedBitset[pNode->getId()] = 1;
                for (EvaluatedkNNGWNode<dist_t> *u : pNode->getNeighbors()) {
                    if (!visitedBitset[u->getkNNGWNode()->getId()]) {
                        heapMember<kNNGWNode<dist_t> *, dist_t> el;
                        el.setNode(u->getkNNGWNode());
                        el.setValue(u->getDistance());
                        candidates.insert(el, u->getkNNGWNode()->getId());
                    }
                }
                heapMember<kNNGWNode<dist_t> *, dist_t> minDistCand = candidates.getRoot();
                kNNGWNode<dist_t> *uNode = minDistCand.getNode();
                dist_t dpu = minDistCand.getValue();
                if (dpu - dpq > LB[uNode->getId()]) {
                    LB[uNode->getId()] = dpu - dpq;
                }
                //chamar dijkstra
                //vector<dist_t> dG = Dijkstra(p, (1 + eps)*p->getDistance());
                vector<dist_t> dG = LimDijkstra(p, visitedBitset, (1 + eps) * p->getDistance());
                //limitar dijkstra para calcular local
                for (int i = 0; i < candidates.getSize(); i++) {
                    //while(i <= C.getSize()){
                    //LOG(LIB_INFO) << "C size" << C.getSize();
                    heapMember<kNNGWNode<dist_t> *, dist_t> u = candidates.getItem(i);
                    if ((p->getDistance() - dG[u.getNode()->getId()]) > LB[u.getNode()->getId()]) {
                        //LOG(LIB_INFO) << "dG" << dG[u.getNode()->getId()];
                        LB[u.getNode()->getId()] = p->getDistance() - dG[u.getNode()->getId()];
                        if (LB[u.getNode()->getId()] > query->Radius()) { //ok
                            if (candidates.contain(u.getNode()->getId())) {
                                candidates.removeId(u.getNode()->getId());
                            }
                        }
                    }
                }
                candidates.removeId(pNode->getId());
               // LOG(LIB_INFO) << pNode->getId();
                if (candidates.getSize() > 0) {
                    pNode = candidates.getRoot().getNode(); //proximo a ser avaliado
                }
                //reduzir algoritmo do chavez para uma busca local --> aproximado mas com menos comp. distancias
               // LOG(LIB_INFO) << "pNode" << pNode->getId();
            } while (candidates.getSize() != 0);
        }
    }


    template <typename dist_t>
    void kNNGW<dist_t>::SearchChavez(KNNQuery<dist_t>* query) const{
        container.setNode(nullptr);
        container.setDistance(-1);
        EvaluatedkNNGWNode<dist_t> ini;
        ini.setNode(nullptr);
        ini.setDistance(INFINITY);
        vector<EvaluatedkNNGWNode<dist_t>> iniVec(query->GetK(), ini);
        for(EvaluatedkNNGWNode<dist_t> ii : iniVec){
            nnc.push(ii);
        }
        //nnc(iniVec);
        //inicializar priority queue nnc NULL INFINITY query->getK()
        vector<dist_t> LB(ElList_.size(), 0);
        dist_t rexp;
        int it = 0;
        for(kNNGWNode<dist_t>* u: ElList_){ //inserir todos elementos como candidatos iniciais
            heapMember<kNNGWNode<dist_t>*, dist_t> el;
            el.setNode(u);
            el.setValue(0);
            C.insert(el, u->getId());
            el.setValue(CoveringRadius(u));
            radii.insert(el, u->getId());
        }
        while(C.getSize() > 0){
            //LOG(LIB_INFO) << "Candidate size" << C.getSize();
            kNNGWNode<dist_t>* pNode = nextToReview();
            LOG(LIB_INFO) << "Next to review" << pNode->getId();
            dist_t dpq = query->DistanceObjLeft(pNode->getData());
            LOG(LIB_INFO) << "distance" << dpq;
            EvaluatedkNNGWNode<dist_t> pEval(pNode, dpq);
            EvaluatedkNNGWNode<dist_t> *p = &pEval;
            //p->setNode(pNode);
            //p->setDistance(dpq);
            //LOG(LIB_INFO) << "p" << p->getkNNGWNode()->getId();
            extractNode(p->getkNNGWNode());
            //LOG(LIB_INFO) << "Candidate size next" << C.getSize();
            //LOG(LIB_INFO) << "Candidate size" << C.getSize();
            it++; //incrementa it
            //if(dpq < nnc.top().getDistance()){
            if(dpq < nnc.top().getDistance()){
                //LOG(LIB_INFO) << "aqui";
                pair<size_t , dist_t> farthest = traverse(query, p->getkNNGWNode(), p->getDistance());
                p->setNode(ElList_[farthest.first]);
                p->setDistance(farthest.second);
//                LOG(LIB_INFO) << p->getkNNGWNode()->getId();
            }
            LOG(LIB_INFO) << "farthest" << p->getkNNGWNode()->getId();
            useContainerNNQ(p, p->getDistance(), nnc.top().getDistance()); //crq = nnc.top.distance raio máximo do elemento de query
            for(EvaluatedkNNGWNode<dist_t>* u : p->getkNNGWNode()->getNeighbors()){
                if(C.contain(u->getkNNGWNode()->getId())){
                    if((u->getDistance() - p->getDistance()) > LB[u->getkNNGWNode()->getId()]){ //dpu < dpq
                        LB[u->getkNNGWNode()->getId()] = u->getDistance() - p->getDistance();
                    }
                }
            }
            if(radii.getSize() == 0 && it > it_){
                rexp = (1 + eps)*p->getDistance();
            }else{
                rexp = INFINITY;
            }
            vector<dist_t> dG = Dijkstra(p, rexp);
            //LOG(LIB_INFO) << "dG size" << dG.size();
            int i =0;
            for(int i=0; i < C.getSize(); i++){
                //while(i <= C.getSize()){
                //LOG(LIB_INFO) << "C size" << C.getSize();
                heapMember<kNNGWNode<dist_t>*, dist_t> u = C.getItem(i);
                if((p->getDistance() - dG[u.getNode()->getId()]) > LB[u.getNode()->getId()]){
                    LB[u.getNode()->getId()] = p->getDistance() - dG[u.getNode()->getId()];
                    if(LB[u.getNode()->getId()] > nnc.top().getDistance()){
                        extractNode(u.getNode());
                    }else if(rexp == INFINITY && dG[u.getNode()->getId()] < INFINITY){
                        //LOG(LIB_INFO) << "C contents";
                        //for(int j =0; j < C.getSize(); j++){
                        //    heapMember<kNNGWNode<dist_t>*, dist_t> u = C.getItem(j);
                        //    LOG(LIB_INFO) << "Pos" << j << " Id" << u.getNode()->getId();
                        //}
                        C.increaseKey(u.getNode()->getId(), dG[u.getNode()->getId()]);
                    }
                }
                // LOG(LIB_INFO) << "end while";
                //  i++;
            }
        }
        /*LOG(LIB_INFO) << "fora while";
         for(int i=0; i < query->GetK(); i++){ //adicionar os k vizinhos em query (biblioteca)
             query->CheckAndAddToResult(nnc.top().getkNNGWNode()->getData());
             nnc.pop();
         }*/
    }





/*template <typename dist_t>
void kNNGW<dist_t>::SSaveIndex(const string &location){

}*/
    template <typename dist_t>
    void kNNGW<dist_t>::writeGephi(string file){ //arrumar
        ofstream outFile(file);
        CHECK_MSG(outFile, "Cannot open file '" + file + "' for writing");
        outFile.exceptions(std::ios::badbit);
        size_t lineNum = 0;

        //WriteField(outFile, METHOD_DESC, StrDesc()); lineNum++;
//  WriteField(outFile, "NN", NN_); lineNum++;
        outFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                << "<gexf xmlns=\"http://www.gexf.net/1.2draft\" version=\"1.2\">\n"
                << "<meta>\n"
                << "<creator>info.debatty.java.graphs.Graph</creator>\n"
                << "<description></description>\n"
                << "</meta>\n"
                << "<graph mode=\"static\" defaultedgetype=\"directed\">\n";
        outFile << "<nodes>\n";
        for (kNNGWNode<dist_t>* pNode: ElList_) {
            size_t nodeID = pNode->getId();
            CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                      "Bug: unexpected node ID " + ConvertToString(nodeID) +
                      " for object ID " + ConvertToString(pNode->getData()->id()) +
                      "data_.size() = " + ConvertToString(data_.size()));
            //outFile << nodeID << ":" << pNode->getData()->id() << ":";
            outFile <<"<node id=\"" << nodeID
                    << "\" label=\"" << pNode->getData()->id() << "\" />\n";
        }
        outFile <<"</nodes>\n";
        outFile <<"<edges>\n";
        int i =0;
        for(kNNGWNode<dist_t>* pNode: ElList_){
            for (EvaluatedkNNGWNode<dist_t>* p: pNode->getNeighbors()) {
                kNNGWNode<dist_t>* pNodeFriend = p->getkNNGWNode();
                IdType nodeFriendID = pNodeFriend->getId();
                CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                          "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                          " for object ID " + ConvertToString(pNodeFriend->getData()->id()) +
                          "data_.size() = " + ConvertToString(data_.size()));
                outFile << "<edge id=\"" << i++ << "\" source=\"" << pNode->getData()->id() << "\" "
                        << "target=\"" << pNodeFriend->getData()->id() << "\" />\n";
            }
        }
        outFile << "</edges>\n";
        outFile << "</graph>\n";
        outFile   << "</gexf>";
        outFile.close();
    }


    template <typename dist_t>
    void kNNGW<dist_t>::SetQueryTimeParams(const AnyParams& QueryTimeParams) {
        AnyParamManager pmgr(QueryTimeParams);
        pmgr.GetParamOptional("initSearchAttempts", initSearchAttempts_,  1);
        pmgr.GetParamOptional("searchChavez", chavez_, 1);
        pmgr.GetParamOptional("searchApprox", approx_, 0);
        pmgr.CheckUnused();
        LOG(LIB_INFO) << "Set kNNG query-time parameters:";
        LOG(LIB_INFO) << "initSearchAttempts =" << initSearchAttempts_;
    }


    template class kNNGW<float>;
    template class kNNGW<double>;
    template class kNNGW<int>;
}
