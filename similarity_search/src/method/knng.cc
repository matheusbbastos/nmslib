#include <cmath>
#include <memory>
#include <iostream>
#include <mmintrin.h>
#include <immintrin.h>
//#include <smmintrin.h>
//#include <tmmintrin.h>

#include "space.h"
#include "knnquery.h"
#include "knnqueue.h"
#include "rangequery.h"
#include "ported_boost_progress.h"
#include "method/knng.h"
#include "method/nndes.h"
#include "sort_arr_bi.h"

#include <vector>

#include <set>
#include <map>
#include <sstream>
#include <typeinfo>


#define MERGE_BUFFER_ALGO_SWITCH_THRESHOLD 100

#define USE_BITSET_FOR_INDEXING 1
//#define USE_ALTERNATIVE_FOR_INDEXING

namespace similarity {
    using namespace std;

    template<typename dist_t>
    struct IndexkNNGParams {
        //dados sobre o que colocar no grafo
        const Space<dist_t> &space_;
        kNNG<dist_t> &index_;
        const ObjectVector &data_;
        size_t index_every_;
        size_t out_of_;
        ProgressDisplay *progress_bar_;
        mutex &display_mutex_;
        size_t progress_update_qty_;

        IndexkNNGParams(
                const Space<dist_t> &space,
                kNNG<dist_t> &index,
                const ObjectVector &data,
                size_t index_every,
                size_t out_of,
                ProgressDisplay *progress_bar,
                mutex &display_mutex,
                size_t progress_update_qty
        ) :
                space_(space),
                index_(index),
                data_(data),
                index_every_(index_every),
                out_of_(out_of),
                progress_bar_(progress_bar),
                display_mutex_(display_mutex),
                progress_update_qty_(progress_update_qty) {}


    };

    template<typename dist_t>
    kNNG<dist_t>::kNNG(bool PrintProgress, const Space<dist_t> &space, const ObjectVector &data) :
            space_(space), data_(data), PrintProgress_(PrintProgress) {
    }


    template<typename dist_t>
    kNNGNode *kNNG<dist_t>::addCriticalSection(kNNGNode *element) {
        unique_lock<mutex> lock(ElListGuard_);
        //element->setId(ElList_.size());
        ElList_.push_back(element);
        return ElList_.back();
        //LOG(LIB_INFO) << "Critical Section::: " << element->getId();
    }

    template<typename dist_t>
    class EvaluatedComparator {
    public:
        bool operator()(EvaluatedkNNGNode<dist_t> *n1, EvaluatedkNNGNode<dist_t> *n2) {
            //comparison code here
            return n1->getDistance() < n2->getDistance(); //maior no topo
        }
    };

    template<typename dist_t>
    class EvaluatedComparatorConst {
    public:
        bool operator()(EvaluatedkNNGNode<dist_t> n1, EvaluatedkNNGNode<dist_t> n2) {
            //comparison code here
            return n1.getDistance() < n2.getDistance(); //maior no topo
        }
    };

    template<typename dist_t>
    class EvaluatedPair{
    public:
        bool operator()(pair<int, dist_t> n1, pair<int, dist_t> n2){
            return n1.second < n2.second;
        }
    };

    template<typename dist_t>
    class EvaluatedComparatorReverse {
    public:
        bool operator()(EvaluatedkNNGNode<dist_t> n1, EvaluatedkNNGNode<dist_t> n2) {
            //comparison code here
            return n1.getDistance() > n2.getDistance(); //menor no topo
        }
    };


    template<typename dist_t>
    void kNNG<dist_t>::ThreadedAdd(kNNGNode *newElement) {
        newElement->setId(ElList_.size());
        ElList_.push_back(newElement);
    }

    template<typename dist_t>
    void kNNG<dist_t>::ThreadedConnect(size_t id) {
        kNNGNode *currNode = ElList_[id];
        priority_queue<EvaluatedkNNGNode<dist_t> *, vector<EvaluatedkNNGNode<dist_t> *>, EvaluatedComparator<dist_t> > resultSet;
        for (size_t i = 0; i < ElList_.size(); i++) {//data_.size()
            if (i != id) {
                //LOG(LIB_INFO) << "id::" << i;
                kNNGNode *evaluated = ElList_[i];
                //calcular distancia
                dist_t d = space_.IndexTimeDistance(currNode->getData(), evaluated->getData());
                EvaluatedkNNGNode<dist_t> *evNode = new EvaluatedkNNGNode<dist_t>(evaluated, d);
                //  LOG(LIB_INFO) << newElement->getNeighbors().size() << "Neighbors";
                if(resultSet.size() < k_) resultSet.push(evNode);
                else{
                    if(resultSet.top()->getDistance() > evNode->getDistance()){
                        resultSet.pop();
                        resultSet.push(evNode);
                    }
                }
            }
        }
        for (size_t i = 0; i < k_; i++) {
            EvaluatedkNNGNode<dist_t> *resultNode = resultSet.top();
            resultSet.pop();
            link(currNode, resultNode->getkNNGNode(), dir);
        }
    }

    template<typename dist_t>
    struct IndexkNNG {
        void operator()(IndexkNNGParams<dist_t> &prm) {
            ProgressDisplay *progress_bar = prm.progress_bar_;
            mutex &display_mutex(prm.display_mutex_);
            //primeiro elemento já adicionado
            size_t nextQty = prm.progress_update_qty_;
            for (size_t id = 0; id < prm.data_.size(); ++id) {//prm.data_.size()
                if (prm.index_every_ == id % prm.out_of_) {
                    // kNNGNode* node = new kNNGNode(prm.data_[id], id);
                    //kNNGNode* newElement = addCriticalSection(node);
                    //  LOG(LIB_INFO) << "Nó inserido atualmente:" << newElement->getId();
                    prm.index_.ThreadedConnect(id);
                    if ((id + 1 >= min(prm.data_.size(), nextQty)) && progress_bar) { //prm.data_.size()
                        unique_lock<mutex> lock(display_mutex);
                        (*progress_bar) += (nextQty - progress_bar->count());
                        nextQty += prm.progress_update_qty_;
                    }
                }
            }
            if (progress_bar) {
                unique_lock<mutex> lock(display_mutex);
                (*progress_bar) += (progress_bar->expected_count() - progress_bar->count());
            }
        }
    };

    template<typename dist_t>
    struct IndexThreadkNNG {
        void operator()(IndexkNNGParams<dist_t> &prm) {
            ProgressDisplay *progress_bar = prm.progress_bar_;
            mutex &display_mutex(prm.display_mutex_);
            // Skip the first element, it was added already
            size_t nextQty = prm.progress_update_qty_;
            for (size_t id = 1; id < prm.data_.size(); ++id) { //prm.data_.size()
                if (prm.index_every_ == id % prm.out_of_) {
                    // kNNGNode* node = new kNNGNode(prm.data_[id], id);
                    //kNNGNode* newElement = addCriticalSection(node);
                    //  LOG(LIB_INFO) << "Nó inserido atualmente thread:" << newElement->getId();
                    // prm.index_.add(node, id);
                    prm.index_.ThreadedConnect(id);
                    if ((id + 1 >= min(prm.data_.size(), nextQty)) && progress_bar) {//prm.data_.size()
                        unique_lock<mutex> lock(display_mutex);
                        (*progress_bar) += (nextQty - progress_bar->count());
                        nextQty += prm.progress_update_qty_;
                    }
                }
            }
            if (progress_bar) {
                unique_lock<mutex> lock(display_mutex);
                (*progress_bar) += (progress_bar->expected_count() - progress_bar->count());
            }
        }
    };


    template<typename dist_t>
    void kNNG<dist_t>::CreateIndex(const AnyParams &IndexParams) {
        //construir um kNNG
        AnyParamManager pmgr(IndexParams);
        string tmp;
        pmgr.GetParamOptional("initIndexAttempts", initIndexAttempts_, 1);
        pmgr.GetParamOptional("indexThreadQty", indexThreadQty_, thread::hardware_concurrency());
        pmgr.GetParamOptional("NN", k_, 5); //valor de k, default 5
        pmgr.GetParamOptional("dir", dir, 1); //1 para direcionado, 0 para não direcionado
        pmgr.GetParamOptional("algoType", tmp, "old");
        ToLower(tmp);
        if (tmp == "old") indexAlgoType_ = old;
        else if (tmp == "matrix") indexAlgoType_ = matrix;
        else {
            throw runtime_error("algoType should be one of the following: old, matrix");
        }
        if (k_ > data_.size()) {
            LOG(LIB_INFO) << "Invalid k value!";
            throw runtime_error("Invalid k value!");
        }
        LOG(LIB_INFO) << "initIndexAttempts   = " << initIndexAttempts_;
        LOG(LIB_INFO) << "indexThreadQty      = " << indexThreadQty_;
        pmgr.CheckUnused();

        SetQueryTimeParams(getEmptyParams());


        if (data_.empty()) return;
        unique_ptr<ProgressDisplay> progress_bar(PrintProgress_ ?
                                                 new ProgressDisplay(data_.size(), cerr)
                                                                : NULL);
        /*kNNG força bruta */
        if (indexThreadQty_ <= 1) {
            for (size_t i = 0; i < data_.size(); i++) { //data_.size()
                kNNGNode *node = new kNNGNode(data_[i], i);
                addCriticalSection(node);
                //add(node, i);
            }
            //if (indexAlgoType_ == old) {
                addEdges();
                //}else if(indexAlgoType_ == matrix){
                //  addEdgesMatrix();
                //}
            } else {
                //size_t id = 0;
                //kNNGNode* node = new kNNGNode(data_[id], id);
                //RNGNode* newElement = addCriticalSection(node);
                //  LOG(LIB_INFO) << "Nó inserido atualmente:" << newElement->getId();
                //  add(newElement, id); //verificar e inserir os vizinhos
                // add(node, id);
                for (size_t i = 0; i < data_.size(); i++) { //insere todos os nós no grafo primeiro
                    kNNGNode *node = new kNNGNode(data_[i], i);
                    addCriticalSection(node);
                }
                vector<thread> threads(indexThreadQty_);
                vector<shared_ptr<IndexkNNGParams<dist_t>>> threadParams;
                mutex progressBarMutex;

                for (size_t i = 0; i < indexThreadQty_; ++i) {
                    threadParams.push_back(shared_ptr<IndexkNNGParams<dist_t>>(
                            new IndexkNNGParams<dist_t>(space_, *this, data_, i, indexThreadQty_,
                                                        progress_bar.get(), progressBarMutex, 200)));
                }
                for (size_t i = 0; i < indexThreadQty_; ++i) {
                    threads[i] = thread(IndexThreadkNNG<dist_t>(), ref(*threadParams[i]));
                }
                for (size_t i = 0; i < indexThreadQty_; ++i) {
                    threads[i].join();
                }
                if (ElList_.size() != data_.size()) {
                    stringstream err;
                    err << "Bug: ElList_.size() (" << ElList_.size() << ") isn't equal to data_.size() ("
                        << data_.size()
                        << ")";
                    LOG(LIB_INFO) << err.str();
                    throw runtime_error(err.str());
                }
                LOG(LIB_INFO) << indexThreadQty_ << " indexing threads have finished";
            }
           // printGraph("/home/larissa/Documents/NonMetricSpaceLib/hgraph/knng.txt");
        }
   // }

   /* template<typename dist_t>
    void kNNG<dist_t>::addEdgesMatrix(){
        vector<vector<dist_t> > matrix(data_.size(), vector<dist_t>(data_.size(), 0));
        DistanceMatrix = matrix;
        LOG(LIB_INFO) << "thread 1 -- add Edges Matrix";
        bool isEmpty = false;
        {
            unique_lock<mutex> lock(ElListGuard_);
            isEmpty = ElList_.empty();
        }
        if (isEmpty) {
            // Before add() is called, the first node should be created!
            LOG(LIB_INFO) << "Bug: the list of nodes shouldn't be empty!";
            throw runtime_error("Bug: the list of nodes shouldn't be empty!");
        }
        for(size_t idAtual=0; idAtual < data_.size(); idAtual++){

        }
    }*/


    template<typename dist_t>
    void kNNG<dist_t>::addEdges() {
        LOG(LIB_INFO) << "thread 1 -- add Edges";
        bool isEmpty = false;
        {
            unique_lock<mutex> lock(ElListGuard_);
            isEmpty = ElList_.empty();
        }
        if (isEmpty) {
            // Before add() is called, the first node should be created!
            LOG(LIB_INFO) << "Bug: the list of nodes shouldn't be empty!";
            throw runtime_error("Bug: the list of nodes shouldn't be empty!");
        }
        //priority_queue<EvaluatedkNNGNode<dist_t> , vector<EvaluatedkNNGNode<dist_t> >, EvaluatedComparatorConst<dist_t> > resultSet;
       priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPair<dist_t> > resultSet;
       for(size_t idAtual=0; idAtual < data_.size(); idAtual++){
            //kNNGNode* newElement = ElList_[idAtual];
            //EvaluatedkNNGNode<dist_t> *evNode;
            for (size_t i = 0; i < data_.size(); i++) {//data_.size()
                if (i != idAtual) {
                    //LOG(LIB_INFO) << "id::" << i;
                    //kNNGNode *evaluated = ElList_[i];
                    //calcular distancia
                    dist_t d = space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData());
                    //EvaluatedkNNGNode<dist_t> *evNode = new EvaluatedkNNGNode<dist_t>(ElList_[i], d);
                    //  LOG(LIB_INFO) << newElement->getNeighbors().size() << "Neighbors";
                    //if(resultSet.size() < k_) resultSet.push(EvaluatedkNNGNode<dist_t>(ElList_[i], d));
                    if(resultSet.size() < k_) resultSet.push(make_pair((int)i, d));
                    else{
                        //if(resultSet.top().getDistance() > evNode->getDistance()){
                         //if(resultSet.top().getDistance() > d){
                        if(resultSet.top().second > d){
                            resultSet.pop();
                            //resultSet.push(evNode);
                            //resultSet.push(EvaluatedkNNGNode<dist_t>(ElList_[i], d));
                            resultSet.push(make_pair((int)i, d));
                        }//else delete evNode;
                    }
                }
            }
            for (size_t i = 0; i < k_; i++) {
                //EvaluatedkNNGNode<dist_t> resultNode = resultSet.top();
                pair<int, dist_t> resultNode = resultSet.top();
                resultSet.pop();
               //LOG(LIB_INFO) << "ResultNode:" << resultNode.first << " Distance:" << resultNode.second << "Link:" << idAtual;
                //link(ElList_[idAtual], ElList_[resultNode.getkNNGNode()->getId()], dir);
                link(ElList_[idAtual], ElList_[resultNode.first], dir);
                //delete resultNode;
            }
        //if(!resultSet.empty()) LOG(LIB_INFO) << "NOT EMPTY";
        //resultSet = priority_queue<EvaluatedkNNGNode<dist_t> , vector<EvaluatedkNNGNode<dist_t> >, EvaluatedComparatorConst<dist_t> >();
        resultSet = priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPair<dist_t> >();
           //clear resultSet ///destroy it
        }
    }

    template<typename dist_t>
    void kNNG<dist_t>::add(kNNGNode *newElement, size_t idAtual) {
//  newElement->removeAllNeighbors();
        newElement = addCriticalSection(newElement);
        bool isEmpty = false;
        {
            unique_lock<mutex> lock(ElListGuard_);
            isEmpty = ElList_.empty();
        }
        if (isEmpty) {
            // Before add() is called, the first node should be created!
            LOG(LIB_INFO) << "Bug: the list of nodes shouldn't be empty!";
            throw runtime_error("Bug: the list of nodes shouldn't be empty!");
        }
        priority_queue<EvaluatedkNNGNode<dist_t> *, vector<EvaluatedkNNGNode<dist_t> *>, EvaluatedComparator<dist_t> > resultSet;
        for (size_t i = 0; i < data_.size(); i++) {//data_.size()
            if (i != idAtual) {
                //LOG(LIB_INFO) << "id::" << i;
                kNNGNode *evaluated = new kNNGNode(data_[i], i);
                //calcular distancia
                dist_t d = space_.IndexTimeDistance(newElement->getData(), evaluated->getData());
                EvaluatedkNNGNode<dist_t> *evNode = new EvaluatedkNNGNode<dist_t>(evaluated, d);
                //  LOG(LIB_INFO) << newElement->getNeighbors().size() << "Neighbors";
                if(resultSet.size() < k_) resultSet.push(evNode);
                else{
                    if(resultSet.top()->getDistance() > evNode->getDistance()){
                        resultSet.pop();
                        resultSet.push(evNode);
                    }
                }
            }
        }
        for (size_t i = 0; i < k_; i++) {
            EvaluatedkNNGNode<dist_t> *resultNode = resultSet.top();
            //LOG(LIB_INFO) << "Distance:" << resultNode->getDistance() << " Id:" << resultNode->getkNNGNode()->getId();
            resultSet.pop();
            link(newElement, resultNode->getkNNGNode(), dir);
        }
        //  addCriticalSection(newElement);
    }

    template<typename dist_t>
    void kNNG<dist_t>::link(kNNGNode *first, kNNGNode *second, bool dir) {
        if(dir){
            first->addNeighbor(second); //grafo direcionado
        }else{ //hasNeighbor --> check for duplicates
            if(!first->hasNeighbor(second->getId()))
            first->addNeighbor(second); //grafo não direcionado
            if(!second->hasNeighbor(first->getId()))
            second->addNeighbor(first);
        }
    }

    template<typename dist_t>
    void kNNG<dist_t>::printGraph(string file) {
        ofstream outFile(file);
        CHECK_MSG(outFile, "Cannot open file");
        outFile.exceptions(std::ios::badbit);
        for (kNNGNode *node : ElList_) {
            for (kNNGNode *neighbor : node->getNeighbors()) {
                outFile << node->getId() << ", ";
                outFile << neighbor->getId() << endl;
            }
        }
        outFile.close();
    }


    template<typename dist_t>
    void kNNG<dist_t>::SaveIndex(const string &location) {
        ofstream outFile(location);
        CHECK_MSG(outFile, "Cannot open file '" + location + "' for writing");
        outFile.exceptions(std::ios::badbit);
        size_t lineNum = 0;

        WriteField(outFile, METHOD_DESC, StrDesc());
        lineNum++;
        WriteField(outFile, "NN", k_);
        lineNum++;

        for (kNNGNode *pNode: ElList_) {
            size_t nodeID = pNode->getId();
            CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                      "Bug: unexpected node ID " + ConvertToString(nodeID) +
                      " for object ID " + ConvertToString(pNode->getData()->id()) +
                      "data_.size() = " + ConvertToString(data_.size()));
            outFile << nodeID << ":" << pNode->getData()->id() << ":";
            for (kNNGNode *pNodeFriend: pNode->getNeighbors()) {
                IdType nodeFriendID = pNodeFriend->getId();
                CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                          "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                          " for object ID " + ConvertToString(pNodeFriend->getData()->id()) +
                          "data_.size() = " + ConvertToString(data_.size()));
                outFile << ' ' << nodeFriendID;
            }
            outFile << endl;
            lineNum++;
        }
        outFile << endl;
        lineNum++; // The empty line indicates the end of data entries
        WriteField(outFile, LINE_QTY, lineNum + 1 /* including this line */);
        outFile.close();
    }

    template<typename dist_t>
    kNNGNode *kNNG<dist_t>::getInitialVertice() const {
        size_t size = ElList_.size();
        if (!ElList_.size()) {
            return NULL;
        } else {
            size_t num = RandomInt() % size;
            return ElList_[num];
        }
    }

    template<typename dist_t>
    void kNNG<dist_t>::nearestNeighborGS(KNNQuery<dist_t> *query) const {
        if (ElList_.empty()) return;
        //vector<bool>     visitedBitset(ElList_.size());
        kNNGNode *currNode = getInitialVertice();
        bool hasNearestNeighbor = true;
        dist_t d = query->DistanceObjLeft(currNode->getData());
        //kNNGNode *nearestNeighbor;
        size_t nearestId = currNode->getId();
        while (hasNearestNeighbor) {
            //vector<kNNGNode*> neighbors = currNode.getNeighbors();
            //LOG(LIB_INFO) << "while";
            hasNearestNeighbor = false;
            for (kNNGNode *neighbor : currNode->getNeighbors()) {
                size_t neighborId = neighbor->getId();
                const Object *currObj = neighbor->getData();
                dist_t dNeighbor = query->DistanceObjLeft(currObj);
                if (dNeighbor < d) {
                    LOG(LIB_INFO) << "if";
                    //nearestNeighbor = neighbor;
                    nearestId = neighborId;
                    d = dNeighbor;
                    hasNearestNeighbor = true;
                }
            }
            //currNode = nearestNeighbor;
            currNode = ElList_[nearestId];
            if (currNode->getId() == nearestId) LOG(LIB_INFO) << "currNode" << currNode->getId();
            else
                LOG(LIB_INFO) << "DIFF";
        }
        query->CheckAndAddToResult(d, currNode->getData());
    }


    template<typename dist_t>
    void kNNG<dist_t>::GNNS(KNNQuery<dist_t> *query) const{
        for (size_t i = 0; i < initSearchAttempts_; i++) {
            size_t curr = RandomInt() % ElList_.size();
            dist_t currDist = query->DistanceObjLeft(ElList_[curr]->getData());
            query->CheckAndAddToResult(currDist, ElList_[curr]->getData());
            size_t currOld;
            do {
                currOld = curr;
                int i=0;
                for (kNNGNode *e : ElList_[currOld]->getNeighbors()) {
                    i++;
                    size_t currNew = e->getId();
                    //if(currNew != bad){
                    dist_t currDistNew = query->DistanceObjLeft(ElList_[currNew]->getData());
                    query->CheckAndAddToResult(currDistNew, ElList_[currNew]->getData());
                    if (currDistNew < currDist) {
                        curr = currNew;
                        currDist = currDistNew;
                    }
                    //}
                }
                LOG(LIB_INFO) << "NN:::" << i;
            } while (currOld != curr);
        }
    }

    template <typename dist_t>
    kNNGNode* kNNG<dist_t>::getRandomEntryPoint() const {
        size_t size = ElList_.size();
        if (!ElList_.size()) {
            return NULL;
        } else {
            size_t num = RandomInt() % size;
            return ElList_[num];
        }
    }

    template<typename dist_t>
    void kNNG<dist_t>::swSearch(KNNQuery<dist_t> *query) const {
        if (ElList_.empty()) return;
        CHECK_MSG(efSearch_ > 0, "efSearch should be > 0");
/*
 * The trick of using large dense bitsets instead of unordered_set was
 * borrowed from Wei Dong's kgraph: https://github.com/aaalgo/kgraph
 *
 * This trick works really well even in a multi-threaded mode. Indeed, the amount
 * of allocated memory is small. For example, if one has 8M entries, the size of
 * the bitmap is merely 1 MB. Furthermore, setting 1MB of entries to zero via memset would take only
 * a fraction of millisecond.
 */
        vector<bool>                        visitedBitset(ElList_.size());

        for (size_t i=0; i < initSearchAttempts_; i++) {
            /**
             * Search of most k-closest elements to the query.
             */
#ifdef START_WITH_E0_AT_QUERY_TIME
            kNNGNode* provider = i ? getRandomEntryPoint(): ElList_[0];
#else
            kNNGNode* provider = getRandomEntryPoint();
#endif
            //MSWNode* provider = getRandomEntryPoint();

            priority_queue <dist_t>                          closestDistQueue; //The set of all elements which distance was calculated
            priority_queue<EvaluatedkNNGNode<dist_t> , vector<EvaluatedkNNGNode<dist_t> >, EvaluatedComparatorReverse<dist_t> > candidateQueue;
            //the set of elements which we can use to evaluate

            const Object* currObj = provider->getData();
            dist_t d = query->DistanceObjLeft(currObj);
            query->CheckAndAddToResult(d, currObj); // This should be done before the object goes to the queue: otherwise it will not be compared to the query at all!

            //EvaluatedkNNGNode<dist_t> ev(provider, d);
            candidateQueue.push(EvaluatedkNNGNode<dist_t>(provider, d));
            closestDistQueue.emplace(d);

            size_t nodeId = provider->getId();
            // data_.size() is guaranteed to be equal to ElList_.size()
            if (nodeId >= data_.size()) {
                stringstream err;
                err << "Bug: nodeId > data_size()";
                LOG(LIB_INFO) << err.str();
                throw runtime_error(err.str());
            }
            visitedBitset[nodeId] = true;

            while(!candidateQueue.empty()){

                auto iter = candidateQueue.top(); // This one was already compared to the query
                const EvaluatedkNNGNode<dist_t>& currEv = iter;

                // Did we reach a local minimum?
                if (currEv.getDistance() > closestDistQueue.top()) {
                    break;
                }

                for (kNNGNode* neighbor : (currEv.getkNNGNode())->getNeighbors()) {
                    _mm_prefetch(reinterpret_cast<const char*>(const_cast<const Object*>(neighbor->getData())), _MM_HINT_T0);
                }
                for (kNNGNode* neighbor : (currEv.getkNNGNode())->getNeighbors()) {
                    _mm_prefetch(const_cast<const char*>(neighbor->getData()->data()), _MM_HINT_T0);
                }

                const vector<kNNGNode*>& neighbor = (currEv.getkNNGNode())->getNeighbors();

                // Can't access curEv anymore! The reference would become invalid
                candidateQueue.pop();

                //calculate distance to each neighbor
                for (auto iter = neighbor.begin(); iter != neighbor.end(); ++iter){
                    nodeId = (*iter)->getId();
                    // data_.size() is guaranteed to be equal to ElList_.size()
                    if (nodeId >= data_.size()) {
                        stringstream err;
                        err << "Bug: nodeId > data_size()";
                        LOG(LIB_INFO) << err.str();
                        throw runtime_error(err.str());
                    }
                    if (!visitedBitset[nodeId]) {
                        currObj = (*iter)->getData();
                        d = query->DistanceObjLeft(currObj);
                        visitedBitset[nodeId] = true;

                        if (closestDistQueue.size() < efSearch_ || d < closestDistQueue.top()) {
                            closestDistQueue.emplace(d);
                            if (closestDistQueue.size() > efSearch_) {
                                closestDistQueue.pop();
                            }

                            candidateQueue.emplace(*iter, d);
                        }

                        query->CheckAndAddToResult(d, currObj);
                    }
                }
            }
        }
    }



    template<typename dist_t>
    void kNNG<dist_t>::swSearchV1Merge(KNNQuery<dist_t> *query) const { //searchV1Merge (Small World Search)
        LOG(LIB_INFO) << "Small World Search:: efSearch" << efSearch_ ;
        if (ElList_.empty()) return;
        CHECK_MSG(efSearch_ > 0, "efSearch should be > 0");
        vector<bool> visitedBitset(ElList_.size());
        for (size_t i=0; i < initSearchAttempts_; i++) {
            /**
             * Search of most k-closest elements to the query.
             */
            kNNGNode* currNode = getRandomEntryPoint();
            SortArrBI<dist_t,kNNGNode*> sortedArr(max<size_t>(efSearch_, query->GetK()));

            const Object* currObj = currNode->getData();
            dist_t d = query->DistanceObjLeft(currObj);
            sortedArr.push_unsorted_grow(d, currNode); // It won't grow

            size_t nodeId = currNode->getId();
            // data_.size() is guaranteed to be equal to ElList_.size()
            CHECK(nodeId < data_.size());

            visitedBitset[nodeId] = true;

            int_fast32_t  currElem = 0;

            typedef typename SortArrBI<dist_t,kNNGNode*>::Item  QueueItem;

            vector<QueueItem>& queueData = sortedArr.get_data();
            vector<QueueItem>  itemBuff(8*k_);
            // efSearch_ is always <= # of elements in the queueData.size() (the size of the BUFFER), but it can be
            // larger than sortedArr.size(), which returns the number of actual elements in the buffer
            while(currElem < min(sortedArr.size(),efSearch_)){
                auto& e = queueData[currElem];
                CHECK(!e.used);
                e.used = true;
                currNode = e.data;
                ++currElem;

                for (kNNGNode* neighbor : currNode->getNeighbors()) {
                    _mm_prefetch(reinterpret_cast<const char*>(const_cast<const Object*>(neighbor->getData())), _MM_HINT_T0);
                }
                for (kNNGNode* neighbor : currNode->getNeighbors()) {
                    _mm_prefetch(const_cast<const char*>(neighbor->getData()->data()), _MM_HINT_T0);
                }

                if (currNode->getNeighbors().size() > itemBuff.size())
                    itemBuff.resize(currNode->getNeighbors().size());

                size_t itemQty = 0;

                dist_t topKey = sortedArr.top_key();
                //calculate distance to each neighbor
                for (kNNGNode* neighbor : currNode->getNeighbors()) {
                    nodeId = neighbor->getId();
                    // data_.size() is guaranteed to be equal to ElList_.size()
                    CHECK(nodeId < data_.size());

                    if (!visitedBitset[nodeId]) {
                        currObj = neighbor->getData();
                        d = query->DistanceObjLeft(currObj);
                        visitedBitset[nodeId] = true;
                        if (sortedArr.size() < efSearch_ || d < topKey) {
                            itemBuff[itemQty++]=QueueItem(d, neighbor);
                        }
                    }
                }

                if (itemQty) {
                    _mm_prefetch(const_cast<const char*>(reinterpret_cast<char*>(&itemBuff[0])), _MM_HINT_T0);
                    std::sort(itemBuff.begin(), itemBuff.begin() + itemQty);

                    size_t insIndex=0;
                    if (itemQty > MERGE_BUFFER_ALGO_SWITCH_THRESHOLD) {
                        insIndex = sortedArr.merge_with_sorted_items(&itemBuff[0], itemQty);

                        if (insIndex < currElem) {
                            currElem = insIndex;
                        }
                    } else {
                        for (size_t ii = 0; ii < itemQty; ++ii) {
                            size_t insIndex = sortedArr.push_or_replace_non_empty_exp(itemBuff[ii].key, itemBuff[ii].data);

                            if (insIndex < currElem) {
                                currElem = insIndex;
                            }
                        }
                    }
                }

                // To ensure that we either reach the end of the unexplored queue or currElem points to the first unused element
                while (currElem < sortedArr.size() && queueData[currElem].used == true)
                    ++currElem;
            }

            for (int_fast32_t i = 0; i < query->GetK() && i < sortedArr.size(); ++i) {
                query->CheckAndAddToResult(queueData[i].key, queueData[i].data->getData());
            }
        }
    }

    template<typename dist_t>
    void kNNG<dist_t>::Search(KNNQuery<dist_t> *query, IdType) const {//1 nn aproximacao espacial
        if(gnns){
            GNNS(query);
        }else if(swsearch){
            swSearch(query);
        }else if(query->GetK()==1){
            nearestNeighborGS(query);
        }else __throw_runtime_error("No algorithm for search available for these parameters");
    }

/*template <typename dist_t>
void kNNG<dist_t>::Search(RangeQuery<dist_t>* query, IdType) const { //recursivo
  if (ElList_.empty()) return ;
  //vector<bool>     visitedBitset(ElList_.size());
  kNNGNode* initialVertice = getInitialVertice();
  set<kNNGNode*> explored;
  dist_t mindist = query->DistanceObjLeft(initialVertice->getData());
  return RecursiveRange(initialVertice, query, mindist, explored);
  //range query do ocsa
}*/

    template<typename dist_t>
    void kNNG<dist_t>::writeGephi(string file) { //arrumar
        ofstream outFile(file);
        CHECK_MSG(outFile, "Cannot open file '" + file + "' for writing");
        outFile.exceptions(std::ios::badbit);
        //size_t lineNum = 0;

        //WriteField(outFile, METHOD_DESC, StrDesc()); lineNum++;
//  WriteField(outFile, "NN", NN_); lineNum++;
        outFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                << "<gexf xmlns=\"http://www.gexf.net/1.2draft\" version=\"1.2\">\n"
                << "<meta>\n"
                << "<creator>info.debatty.java.graphs.Graph</creator>\n"
                << "<description></description>\n"
                << "</meta>\n"
                << "<graph mode=\"static\" defaultedgetype=\"directed\">\n";
        outFile << "<nodes>\n";
        for (kNNGNode *pNode: ElList_) {
            size_t nodeID = pNode->getId();
            CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                      "Bug: unexpected node ID " + ConvertToString(nodeID) +
                      " for object ID " + ConvertToString(pNode->getData()->id()) +
                      "data_.size() = " + ConvertToString(data_.size()));
            //outFile << nodeID << ":" << pNode->getData()->id() << ":";
            outFile << "<node id=\"" << nodeID
                    << "\" label=\"" << pNode->getData()->id() << "\" />\n";
        }
        outFile << "</nodes>\n";
        outFile << "<edges>\n";
        int i = 0;
        for (kNNGNode *pNode: ElList_) {
            for (kNNGNode *pNodeFriend: pNode->getNeighbors()) {
                IdType nodeFriendID = pNodeFriend->getId();
                CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                          "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                          " for object ID " + ConvertToString(pNodeFriend->getData()->id()) +
                          "data_.size() = " + ConvertToString(data_.size()));
                outFile << "<edge id=\"" << i++ << "\" source=\"" << pNode->getData()->id() << "\" "
                        << "target=\"" << pNodeFriend->getData()->id() << "\" />\n";
            }
        }
        outFile << "</edges>\n";
        outFile << "</graph>\n";
        outFile << "</gexf>";
        outFile.close();
    }


    template<typename dist_t>
    void kNNG<dist_t>::SetQueryTimeParams(const AnyParams &QueryTimeParams) {
        AnyParamManager pmgr(QueryTimeParams);
        pmgr.GetParamOptional("initSearchAttempts", initSearchAttempts_, 1);
        pmgr.GetParamOptional("gnns", gnns, 0);
        pmgr.GetParamOptional("sw", swsearch, 0);
        pmgr.GetParamOptional("efSearch", efSearch_, 5);
        pmgr.CheckUnused();
        LOG(LIB_INFO) << "Set kNNG query-time parameters:";
        LOG(LIB_INFO) << "initSearchAttempts(for GNNS) =" << initSearchAttempts_;
        LOG(LIB_INFO) << "efSearch (for SWSearch) =" << efSearch_;
    } //efSearch parameter


    template<typename dist_t>
    void kNNG<dist_t>::LoadIndex(const string &location) {
        vector<kNNGNode *> ptrMapper(data_.size());

        for (unsigned pass = 0; pass < 2; ++pass) {
            ifstream inFile(location);
            CHECK_MSG(inFile, "Cannot open file '" + location + "' for reading");
            inFile.exceptions(std::ios::badbit);

            size_t lineNum = 1;
            string methDesc;
            ReadField(inFile, METHOD_DESC, methDesc);
            lineNum++;
            CHECK_MSG(methDesc == StrDesc(),
                      "Looks like you try to use an index created by a different method: " + methDesc);
            ReadField(inFile, "NN", k_);
            lineNum++;

            string line;
            while (getline(inFile, line)) {
                if (line.empty()) {
                    lineNum++;
                    break;
                }
                stringstream str(line);
                str.exceptions(std::ios::badbit);
                char c1, c2;
                IdType nodeID, objID;
                CHECK_MSG((str >> nodeID) && (str >> c1) && (str >> objID) && (str >> c2),
                          "Bug or inconsitent data, wrong format, line: " + ConvertToString(lineNum)
                );
                CHECK_MSG(c1 == ':' && c2 == ':',
                          string("Bug or inconsitent data, wrong format, c1=") + c1 + ",c2=" + c2 +
                          " line: " + ConvertToString(lineNum)
                );
                CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                          DATA_MUTATION_ERROR_MSG + " (unexpected node ID " + ConvertToString(nodeID) +
                          " for object ID " + ConvertToString(objID) +
                          " data_.size() = " + ConvertToString(data_.size()) + ")");
                CHECK_MSG(data_[nodeID]->id() == objID,
                          DATA_MUTATION_ERROR_MSG + " (unexpected object ID " + ConvertToString(data_[nodeID]->id()) +
                          " for data element with ID " + ConvertToString(nodeID) +
                          " expected object ID: " + ConvertToString(objID) + ")"
                );
                if (pass == 0) {
                    unique_ptr<kNNGNode> node(new kNNGNode(data_[nodeID], nodeID));
                    ptrMapper[nodeID] = node.get();
                    ElList_.push_back(node.release());
                } else {
                    kNNGNode *pNode = ptrMapper[nodeID];
                    CHECK_MSG(pNode != NULL,
                              "Bug, got NULL pointer in the second pass for nodeID " + ConvertToString(nodeID));
                    IdType nodeFriendID;
                    while (str >> nodeFriendID) {
                        CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                                  "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                                  "data_.size() = " + ConvertToString(data_.size()));
                        kNNGNode *pFriendNode = ptrMapper[nodeFriendID];
                        CHECK_MSG(pFriendNode != NULL,
                                  "Bug, got NULL pointer in the second pass for nodeID " +
                                  ConvertToString(nodeFriendID));
                        pNode->addNeighbor(pFriendNode);//, false /* don't check for duplicates */);
                    }
                    CHECK_MSG(str.eof(),
                              "It looks like there is some extract erroneous stuff in the end of the line " +
                              ConvertToString(lineNum));
                }
                ++lineNum;
            }

            size_t ExpLineNum;
            ReadField(inFile, LINE_QTY, ExpLineNum);
            CHECK_MSG(lineNum == ExpLineNum,
                      DATA_MUTATION_ERROR_MSG + " (expected number of lines " + ConvertToString(ExpLineNum) +
                      " read so far doesn't match the number of read lines: " + ConvertToString(lineNum) + ")");
            inFile.close();
        }
    }

    template
    class kNNG<double>;

    template
    class kNNG<int>;

    template
    class kNNG<float>;

}
