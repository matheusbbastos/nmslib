//
// Created by larissa on 1/10/19.
//
#include "space.h"
#include "knnquery.h"
#include "knnqueue.h"
#include "rangequery.h"
#include "ported_boost_progress.h"
#include "method/hgraphSet.h"
//#include "sort_arr_bi.h"

#include <vector>

#include <set>
#include <map>
#include <random>
#include <iterator>
#include <sstream>
#include <typeinfo>
#include <method/knng.h>
#include <method/rng.h>
#include <method/nndes.h>

#define MERGE_BUFFER_ALGO_SWITCH_THRESHOLD 100

#define USE_BITSET_FOR_INDEXING 1

//#define NOT_MAC 0

#define RATE_OVERLAP 0.02

#define NOT_EXACT_BASE_GRAPH 1

#define DEFINED_LCOUNT_GRAPH 4
#define START_PIVOTS 0

#define WITH_LONG_EDGES 1

namespace similarity {

    template<typename dist_t>
    void HGraphSet<dist_t>::addNodes(vector<int> &pIndices){
        for(int i=0; i < data_.size();i++){
            addCriticalSection(new HNodeSet<dist_t>(data_[i], i));
            pIndices.push_back(i);
        }
    }

    template<typename dist_t>
    void HGraphSet<dist_t>::addCriticalSection(HNodeSet<dist_t> *node){
        ElList_.push_back(node);
    }

    template<typename dist_t>
    void HGraphSet<dist_t>::printSeeds(vector<int> seeds){
        for(int i=0; i < seeds.size();i++){
            LOG(LIB_INFO) << "i:" << i << " seeds:" << seeds[i] << "id:" << ElList_[seeds[i]]->getData()->id();
        }
    }

    template<typename dist_t>
    void HGraphSet<dist_t>::CreateIndex(const AnyParams &IndexParams)  {
        AnyParamManager  pmgr(IndexParams);
        pmgr.GetParamOptional("levels", L_, 2);
        pmgr.GetParamOptional("seeds", S_, 2);
        pmgr.GetParamOptional("gtype1",indexGraphType_,"knng");//mesmo nome que o método
        pmgr.GetParamOptional("overlap", overRate_,0.1);
        pmgr.GetParamOptional("eps", eps_, 0);
        pmgr.GetParamOptional("NN", NN_, 5);
        pmgr.GetParamOptional("indexThreadQty", indexThreadQty_, 0);
        pmgr.GetParamOptional("gtype2", gtype_, "knng");
        pmgr.GetParamOptional("ptype", ptype_, "random");
        pmgr.GetParamOptional("nrandom", nparam_, 3);
        pmgr.GetParamOptional("efConstruction", efConstruction_, 5);
        pmgr.CheckUnused();
        this->ResetQueryTimeParams();
        stopConstruction = false;
        //vector<vector<int>> v(L_, vector<int>(1));
        //vector<set<int>> v(L_);
        //levelSeeds_ = v;
        vector<int> pIndices;
        addNodes(pIndices);//adiciona todos os nós no grafo
        if(indexThreadQty_ == 0){
            int lcount = 0;
            //inicializar Set X_
            // notSelected.insert(pIndices.begin(), pIndices.end());
            LOG(LIB_INFO) << "Top Down Construction";
            if(indexGraphType_=="knng" && data_.size()/S_ < NN_){
                LOG(LIB_WARNING) << "This choice of parameters may not give you the approximated graph wanted";
            }
            if(L_ == 1) createGraph(pIndices, 1);
            else{
                createIndexTD(lcount, pIndices, S_);
            }
        }/*else{
            int lcount=0;
            createIndexTDParallel(lcount, pIndices, S_);
            //  createIndexTd --> cada recursão um processo em paralelo --> numeros de threads -> numero de seeds
        }*/
#if WITH_LONG_EDGES
        if(L_ != 1){ //se 1 --> knng ou grqaph normal
            //for(int i=0; i < L_; i++){//ultimo não precisa de refine
            //  printSeeds(levelSeeds_[L_-1]);
            //  refineGraph(levelSeeds_[i], i);
            // refineGraph(levelSeeds_[L_-1], L_-1, gtype_);
            //refineGraphSet(levelSeeds_, L_ - 1, gtype_);
            refineGraphSet();
            // vector<int> seeds(levelSeeds_.begin(), levelSeeds_.end());
            // refineGraph(seeds, L_-1, gtype_);
            //}
        } //aqui refine graph*
#endif
        /*for(int i =0; i < ElList_.size(); i++){
            if(ElList_[i]->getNeighbors().size()==0){
                LOG(LIB_INFO) << "Ellist 0 id:" << i;
            }
        }*/
        //LOG(LIB_INFO) << "prune Edges";
        //pruneEdgesSet();
        //printGraph("/home/larissa/Documents/NonMetricSpaceLib/hgraph/testswg.txt");
       // writeGephi("/home/larissa/Documents/NonMetricSpaceLib/hgraph/swgHgraphLong.gexf");
       //writeGephi("/home/larissa/Documents/NonMetricSpaceLib/hgraph/knngHgraph.gexf");
        // LOG(LIB_INFO) << "count Refine::::" << countRefine;
       /* LOG(LIB_INFO) << "number of neighbors:";
        for(HNodeSet<dist_t>* idAtual : ElList_){
            LOG(LIB_INFO) << "id::" << idAtual->getId() << " neighbors::" << idAtual->getNeighbors().size();
        }*/
    }


    template <typename dist_t>
    void HGraphSet<dist_t>::createIndexTD(int lcount, vector<int> &X, int nP){
        //bool base = true;
        lcount++;
        LOG(LIB_INFO) << "lcount" << lcount << " tam:" << X.size();
#if NOT_EXACT_BASE_GRAPH
        if (X.size() <= L_) {
            LOG(LIB_INFO) << "create graph";
            createGraph(X, lcount);
            LOG(LIB_INFO) << "create graph ok!";
            return;
        }
#else
        if(lcount == DEFINED_LCOUNT_GRAPH){
            if(stopConstruction){
                LOG(LIB_INFO) << "OK stopConstruction";
                return;
            }else{
                LOG(LIB_INFO) << "create graph EXACT";
                LOG(LIB_INFO) << "X:::" << X.size();
                createGraph(X, lcount);
                if(overRate_==1 && X.size() == data_.size()) stopConstruction = true;
                LOG(LIB_INFO) << "create graph ok!";
                return;
            }
        }
#endif
        else {
            // base = false;
            vector<int> levelSeeds = chooseSeed(X, ptype_, nP);//funcao para escolha de seeds
            printSeeds(levelSeeds);
            insertSeeds(levelSeeds, lcount);//estrutura que guarda as seeds de cada nivel --> refine depois
            LOG(LIB_INFO) << levelSeeds_.size();
            //vector<vector<int>> pivotsTable = partitionSpaceOver(levelSeeds, X);
            //float over = adjustOverlap(overRate, lcount, X.size());
            if (nP == 1) { //se partição vier apenas com um pivot --> colocar no minimo dois (forçar particionar)
                int proporcao = round((float) data_.size() / (pow(S_, lcount)));
                int numberPivots;
                if (proporcao == 0) numberPivots = 2;
                else numberPivots = round((float) X.size() / proporcao);
                if (numberPivots <= 0) numberPivots = 1;
                //LOG(LIB_INFO) << "number of pivots::: " << numberPivots;
                return createIndexTD(lcount, X, numberPivots);
            }//nao precisa passar por todos os processos --> apenas uma partição
            LOG(LIB_INFO) << "pivotsTable";
            vector<vector<int>> pivotsTable(levelSeeds.size(), vector<int>(1));
            if(overRate_ == 1) partitionExact(levelSeeds, X, overRate_, pivotsTable);
            else partitionSpaceOverDist(levelSeeds, X, overRate_, pivotsTable);
            //vector<vector<int>> pivotsTable = partitionSpaceOverDist(levelSeeds, X, overRate_);
            //createGraph(levelSeeds, lcount);
            LOG(LIB_INFO) << "pivotsTable ok";
#if WITH_LONG_EDGES
            if (levelSeeds.size() > NN_) knngConstruction(levelSeeds, 2 * NN_, false, true);
            else knngConstruction(levelSeeds, NN_, false, true);//aqui knng construction nas seeds*/
#endif
            //completeGraph(levelSeeds);
            //printSeeds(levelSeeds);
            // refine(); -->refinar as seeds com outras partições
            /*estimar numero de pivots para cada partição*/
            //------
            //proporção para X.size()
            int proporcao = round((float) data_.size() / (pow(S_, lcount)));
            //LOG(LIB_INFO) << "proporcao:::" << proporcao;
            vector<int> nPivots(levelSeeds.size());
            for (int i = 0; i < levelSeeds.size(); i++) {
                int numberPivots;
                if (proporcao == 0) numberPivots = 2;
                else numberPivots = round((float) pivotsTable[i].size() / proporcao);
                if (numberPivots == 0) numberPivots = 1;
                nPivots[i] = numberPivots;
            }
            for (int i = 0; i < levelSeeds.size(); i++) {
                //createGraph(pivotsTable[i], lcount);
                //LOG(LIB_INFO) << "size:" << pivotsTable[i].size() << " npivots" << nPivots[i] << " lcount:" << lcount;
                createIndexTD(lcount, pivotsTable[i], nPivots[i]);//recursivo
            }
        }
        /*if(!base){
            LOG(LIB_INFO) << "Base ok";
            for(int i=0; i < L_; i++){//ultimo não precisa de refine
                refineGraph(levelSeeds_[i], i);
            }
        }*/
    }

    int select_randomly(vector<int> X) {
        static std::random_device rd;
        static std::mt19937 gen(rd());
        std::uniform_int_distribution<> dis(0, X.size()-1);
        return dis(gen);
    } //https://gist.github.com/cbsmith/5538174


    template<typename dist_t>
    vector<int> HGraphSet<dist_t>::chooseSeed(vector<int> &X, string ptype, int S) {
       LOG(LIB_INFO) << "ptype <<" << ptype;
        vector<int> seeds;
        set<int> randomNum;
        srand(time(NULL));
        int i = 0;
        if(X.size() == S ) return X;
        /* if(useRandom) {
             while (i < S) {
                 int id = rand() % X.size();
                 if (X[id] != X[0]) {
                     seeds.push_back(X[id]);
                     X.erase(X.begin() + id);
                     i++;
                 }
             }*/
        if(ptype == "random"){
           // if(X.size() != data_.size()){
                seeds.push_back(X[0]);//garantir a primeira seed de antes também
                S = S -1;
           // }
            while(randomNum.size() < S){
               //int id = rand() % X.size();
               int id = select_randomly(X);
               if(id != 0) randomNum.insert(id);
            }
               set<int>::iterator it = randomNum.begin();
               while(it != randomNum.end()){
                //   LOG(LIB_INFO) << "randomInt:" << *it << "id de X::" << ElList_[X[*it]]->getId();
                   seeds.push_back(X[*it]);
                   it++;
               }
        }else if(ptype=="fft") seeds = FFT(X, S);
        else if(ptype=="nrandom") seeds = NRandom(X, S, (int)nparam_);
        else if(ptype == "pam" ) seeds = PAM(X, S);
        else if(ptype=="nfft"){
            seeds = chooseSeed(X, "random", ceil(nparam_*X.size()));
            seeds = FFT(seeds, S);
        }else if(ptype=="npam"){
            seeds = chooseSeed(X, "random", ceil(nparam_*X.size()));
            seeds = PAM(seeds, S);
        }else{
            LOG(LIB_INFO) << "ptype does not exist -> process is going to end";
        }
        return seeds; //retorna o id das seeds na ElList
    }

    template<typename dist_t>
    vector<int> HGraphSet<dist_t>::FFT(vector<int> X, int S){
       vector<int> seeds;
       set<int> seedsFind;
        if(X.size() <= S) return X;
        seeds.push_back(X[0]);//garantir a primeira seed de antes também
        seedsFind.insert(X[0]);
        S = S -1;
           int maior=0;
           dist_t maxDist = -INFINITY;
           for(int x : X){
               dist_t calc = space_.IndexTimeDistance(ElList_[X[0]]->getData(), ElList_[x]->getData());
               if(calc > maxDist){
                   maxDist = calc;
                   maior = x;
               } //primeiro maior
           }
           if(seedsFind.find(maior) == seedsFind.end()){
               seeds.push_back(maior);
               seedsFind.insert(maior);
           }
           dist_t edge = maior;
           dist_t min;
           int nextSeed = seeds[0];
           //printSeeds(seeds);
        bool equal;
        while(seeds.size() < S){
            //LOG(LIB_INFO) << "loop::" << seeds.size() << " S:" << S << " X::" << X.size();
            min = INFINITY;
            for(int x : X){
                dist_t sum = 0;
                equal = false;
              //  LOG(LIB_INFO) << "seeds tam:" << seeds.size();
              if(seedsFind.find(x) == seedsFind.end()){
                 // LOG(LIB_INFO) << "dentro for 1";
                  for(int seed : seeds){
                      if(x == seed){
                          equal = true;
                          continue;
                      }
                      dist_t distanceFg = space_.IndexTimeDistance(ElList_[seed]->getData(), ElList_[x]->getData());
                      sum = sum + abs(edge - distanceFg);
                  }
                  if(sum <= min && !equal){
                    //  LOG(LIB_INFO) << "entrou!!!";
                      min = sum;
                      nextSeed = x;
                  }
              }
            }
            if(seedsFind.find(nextSeed) == seedsFind.end()){
                seeds.push_back(nextSeed);
                seedsFind.insert(nextSeed);
            }
       }
       return seeds;
    }

    template<typename dist_t>
    vector<int> HGraphSet<dist_t>::NRandom(vector<int> X, int S, int n){
        vector<vector<int>> pivotsTable(n, vector<int>(1));
        float mean = -INFINITY;
        int seeds = 0;
        for(int i=0; i < n; i++){
            pivotsTable[i] = chooseSeed(X, "random", S); //aqui escolhe os mesmos
            //LOG(LIB_INFO) << "ok:::" << pivotsTable[i].size() << "ok!";
            dist_t sDistMean=0, sumDist=0;
            for(int idAtual : pivotsTable[i]){
                for(int id : pivotsTable[i]){
                    if(id == idAtual) continue;
                 //   LOG(LIB_INFO) << "before sumDist idAtual:" << ElList_[id]->getId();
                    sumDist += space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[id]->getData());
                 //   LOG(LIB_INFO) << "after sumDist";
                }
            }
            sDistMean = sumDist/(dist_t)S;
            if(sDistMean > mean){
                mean = sDistMean;
                seeds = i;
            }
        }
        return pivotsTable[seeds];
    }



    template<typename dist_t>
    vector<dist_t> totalCost(vector<vector<pair<int,dist_t>>> pivotsTable){
        vector<dist_t> cost;
        for(vector<pair<int,dist_t>> v : pivotsTable){
            dist_t sum = 0;
            for(pair<int, dist_t> p : v){
                sum += p.second;
            }
            cost.push_back(sum);
        }
        return cost;
    }

    template<typename dist_t>
    vector<int> newSeeds(vector<vector<pair<int,dist_t>>> pivotsTable, vector<int> medoids){
        srand(time(NULL));
        int id = rand() % medoids.size();
        int size = pivotsTable[id].size();
        int id2 = rand() % size;
        medoids[id] = pivotsTable[id][id2].first;
        return medoids;
    }

    bool inPivots(int menorPivots, vector<int> &pivots){
        for(int i : pivots){
            if (i == menorPivots) return true;
        }
        return false;
    }

    template<typename dist_t>
    vector<int> HGraphSet<dist_t>::PAM(vector<int> X, int nP){
        LOG(LIB_INFO) << "chooseSeeds PAM::" << nP;
        LOG(LIB_INFO) << "X size::" << X.size();
        vector<int> pivots;
        if(X.size() <= nP){
            for(int i : X) pivots.push_back(i);
            return pivots;
        }
        pivots = chooseSeed(X, "random", nP);
      /*  LOG(LIB_INFO) << "random::";
        printSeeds(pivots);
        LOG(LIB_INFO) << "-random-";*/
        vector<vector<pair<int,dist_t>>> pivotsTable(pivots.size());
        //inicializar
        bool swap = true;
        int pivotIndex = 0;
        dist_t menor = INFINITY;
        for(int i=0; i < pivots.size();i++){
            pivotsTable[i].push_back(pair<int,dist_t>(pivots[i], 0)); //inicializar o primeiro elemento como o pivô da subdivisão
        }
        while(swap){
            swap = false;
            for(int i=0; i < X.size(); i++){
                for(int j=0; j < pivots.size(); j++){
                    if(X[i] == pivots[j]) continue;
                    dist_t d = space_.IndexTimeDistance(ElList_[i]->getData(), ElList_[j]->getData());
                    if(d < menor){
                        menor = d;
                        pivotIndex = j;
                    }
                }
                pair<int, dist_t> pair(X[i], menor);
                pivotsTable[pivotIndex].push_back(pair);
            }
            for(int j=0; j < pivots.size(); j++){
                dist_t menorAvg = INFINITY;
                int menorPivot;
                for(pair<int,dist_t> p1 : pivotsTable[j]){
                    dist_t sum =0;
                    for(pair<int,dist_t> p2 : pivotsTable[j]){
                        sum += space_.IndexTimeDistance(ElList_[p1.first]->getData(), ElList_[p2.first]->getData());
                    }
                    dist_t CurrAvg = sum/(dist_t) pivotsTable[j].size();
                    if(CurrAvg < menorAvg){
                        menorAvg = CurrAvg;
                        menorPivot = p1.first;
                    }
                }
                if(menorPivot != pivots[j] && !inPivots(menorPivot,pivots)){
                    swap = true;
                    pivots[j] = menorPivot;
                }
            }
        }
        return pivots;
    }

 /*   template<typename dist_t>
    vector<int> HGraphSet<dist_t>::kmedoids(std::vector<int> X, int iterations) {
        vector<int> medoids;
        vector<vector<pair<int,dist_t>>> pivotsTable;
        vector<vector<pair<int,dist_t>>> bestPivotsTable;
        bool initialMedoids = true;
        bool change = true;
        dist_t pastCost = 0;
        int it = 0;
        vector<int> bestMedoids;
        while(it < iterations){
            //   LOG(LIB_INFO) << "aqui";
            if(initialMedoids){
                medoids = chooseSeed(X, "random", (int)S_);
                bestMedoids = medoids;
            }else{
                //change one medoid
                medoids = newSeeds(bestPivotsTable, bestMedoids);
            }
            //associate medoids with points
            //    LOG(LIB_INFO) << "aqui 2";
            //      LOG(LIB_INFO) << medoids.size();
            pivotsTable = partitionSpaceMedoids(medoids, X);
            if(initialMedoids) bestPivotsTable = pivotsTable;
//            LOG(LIB_INFO) << "aqui 3";
            vector<dist_t> cost = totalCost(pivotsTable);
            dist_t totalSum  = 0;
            for(int i=0; i < cost.size(); i++){
                totalSum += cost[i];
            }
            if(totalSum - pastCost < 0){
                bestMedoids = medoids;
                bestPivotsTable = pivotsTable;
            }
            initialMedoids = false;
            pastCost = totalSum;
            it++;
        }
        return bestMedoids;
    }*/

    template <typename dist_t>
    vector<vector<pair<int,dist_t>>> HGraphSet<dist_t>::partitionSpaceMedoids(vector<int> seeds, vector<int> X){
        vector<vector<pair<int,dist_t>>> pivotsTable(seeds.size(), vector<pair<int, dist_t>>(1, make_pair(0,0)));
//        LOG(LIB_INFO) << seeds.size();
        for(int i=0; i < seeds.size();i++){
            //  LOG(LIB_INFO) << i;
            pivotsTable[i][0] = make_pair(seeds[i],0);
        }
        for(int x : X){
            dist_t menor = INFINITY;
            int pos = 0;
            bool equal = false; //para nao inserir igual a seed
            for(int j=0; !equal && j < seeds.size();j++){
                if(x == seeds[j]){
                    equal = true;
                }
                dist_t pj = space_.IndexTimeDistance(ElList_[seeds[j]]->getData(), ElList_[x]->getData());
                if(pj < menor){
                    menor = pj;
                    pos = j;
                }
            }
            if(!equal){
                //LOG(LIB_INFO) << "ppos" << pivotsTable.size();
                pivotsTable[pos].push_back(std::make_pair(x,menor));
                //LOG(LIB_INFO) << "pairs 2";
            }
        }
        return pivotsTable;
    }

    template<typename dist_t>
    vector<int> HGraphSet<dist_t>::BPivotPosition(std::vector<int> X) {
        int n_l;
        if(X.size() < S_*NN_){
            n_l = X.size();
        }else n_l = S_*NN_;
        int i=0;
        vector<int> seeds;
        seeds.push_back(X[0]);//garantir a primeira seed de antes tambéms
        while (i < n_l - 1) {
            int id = rand() % X.size();
            if (X[id] != X[0]) {
                seeds.push_back(X[id]);
                X.erase(X.begin() + id);
                i++;
            }
        }
        while(seeds.size() > S_){
            // LOG(LIB_INFO) << "dentro while" << seeds.size();
            vector<dist_t> sum(seeds.size(), 0);
            vector<vector<dist_t>> distMat(seeds.size(), vector<dist_t>(X.size()));
            for(int i=0; i < X.size(); i++){
                for(int j=0; j < seeds.size(); j++){
                    distMat[j][i] = space_.IndexTimeDistance(ElList_[i]->getData(), ElList_[seeds[j]]->getData());
                    sum[j] += distMat[j][i];
                }
            }
            //calcular avg standar deviation
            dist_t menor = INFINITY;
            int p = 0;
            for(int j=0; j < seeds.size();j++){
                dist_t avg = sum[j]/X.size();
                dist_t std=0;
                for(int i=0; i < X.size(); i++){
                    std += pow(distMat[j][i] - avg, 2);
                }
                std = std/(X.size()-1);
                std = sqrt(std);
                if(std < menor){
                    p = j;
                }
            }
            //LOG(LIB_INFO) << "p::" << p << " seeds::" << seeds.size();
            seeds.erase(seeds.begin() + p);
            sum.clear();
            distMat.clear();
        }
        return seeds;
    }


    template <typename dist_t>
    vector<int> HGraphSet<dist_t>::getPivots(vector<vector<int>> pivotsTable){
        vector<int> pivots;
        for(int i=0; i < S_;i++){
            pivots.push_back(pivotsTable[i][0]);
        }
        //LOG(LIB_INFO) << "size::" << pivotsTable.size();
        return pivots;
    }

    template<typename dist_t>
    vector<int> HGraphSet<dist_t>::appendPivots(std::vector<int> Xs, std::vector<std::vector<int>> pivotsTable) {
        for(vector<int> p: pivotsTable){
            Xs.push_back(p[0]);
        }
        return Xs;
    }

    template<typename dist_t>
    vector<int> HGraphSet<dist_t>::appendOverlap(vector<int> Xs, float overlapRate, vector<vector<int>> pivotsTable) {
        for(vector<int> p : pivotsTable){
            Xs.push_back(p[0]);
            int numberOverlap = ceil(overlapRate*p.size());
            int i=0;
            p.erase(p.begin()); //erase local
            if(p.size() >= 1){
                vector<int> overlap = sample(p, numberOverlap);
                Xs.insert(Xs.end(), overlap.begin(),overlap.end());
            }
        }
        return Xs;
    }

    template<typename dist_t>
    vector<int> HGraphSet<dist_t>::sample(vector<int> p, int number) {
        vector<int> overlap;
        srand(time(NULL));
        for(int i=0; i < number;i++){
            int id = rand()%p.size();
            overlap.push_back(p[id]);
            p.erase(p.begin()+id);
        }
        return overlap;
    }

    template <typename dist_t>
    vector<vector<int>> HGraphSet<dist_t>::partitionSpace(vector<int> seeds, vector<int> X){
        vector<vector<int>> pivotsTable(seeds.size(), vector<int>(1));
        for(int i=0; i < seeds.size();i++){
            pivotsTable[i][0] = seeds[i];
        }
        for(int x : X){
            dist_t menor = INFINITY;
            int pos = 0;
            bool equal = false; //para nao inserir igual a seed
            for(int j=0; !equal && j < seeds.size();j++){
                if(x == seeds[j]){
                    equal = true;
                }
                dist_t pj = space_.IndexTimeDistance(ElList_[seeds[j]]->getData(), ElList_[x]->getData());
                if(pj < menor){
                    menor = pj;
                    pos = j;
                }
            }
            if(!equal) pivotsTable[pos].push_back(x);
        }
        return pivotsTable;
    }

    template<typename dist_t>
    void HGraphSet<dist_t>::partitionExact(vector<int> seeds, vector<int> X, float over, vector<vector<int>> &pivotsTable){
        dist_t pivotsDist[seeds.size()][seeds.size()];
        //int avgSizeOverlap = (int) ceil(((float)X.size()/seeds.size())*overRate_);
        //if(avgSizeOverlap == 0) avgSizeOverlap = 1;
        int avgSize = ceil(X.size()*over);
        LOG(LIB_INFO) << "average Size:::" << avgSize;
        if(avgSize <= 0) avgSize = 1;
        //vector<int> overLapSize(seeds.size(), 0);
        for(int i=0; i < seeds.size();i++){
            pivotsTable[i][0] = seeds[i]; //inicializar o primeiro elemento como o pivô da subdivisão
        }
        vector<priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPairSet<dist_t> >> resultSet(seeds.size());
        vector<pair<int,dist_t>> PivotsX(X.size());
        //matriz de pares pivo e distancia seguindo a ordem de x
        int t=0;
        for(int x : X){
            vector<dist_t> xSeeds(seeds.size(), 0);
            //dist_t xSeeds[seeds.size()];
            dist_t menor = INFINITY;
            int pos = 0;
            // bool equal = false;
            for(int j=0; j < seeds.size();j++){
                //  equal = false;
                if(x == seeds[j]){
                    // equal = true;
                    xSeeds[j] = 0;
                    //distTableX[t][j] = -1; //senão distancia dele com ele mesmo entra como overlap
                    //-1 para nao verificar
                    //distTableX[j][t] = 0;
                }else{
                    dist_t pj = space_.IndexTimeDistance(ElList_[seeds[j]]->getData(), ElList_[x]->getData());
                    xSeeds[j] = pj;
                    //distTableX[t][j] = pj;
                    //distTableX[j][t] = pj;
                    if(pj <= menor){
                        menor = pj;
                        pos = j;
                    }
                }
            }//calcula qual o pivo mais proximo, salva a distancia para todos os pivos para xSeeds
            PivotsX.push_back(make_pair(pos, menor));//par com o pivo e a distancia para o pivo
            //if(!equal){
            if(x != seeds[pos]){ //igual a seed ja foi adicionado
                pivotsTable[pos].push_back(x); //adiciona no seu hiperplano -> pos
                //distTableX[t][pos] = -1; //ja adicionou, nao precisa verificar de novo
                //distTableX[pos][t] = -1;
            }
            for(int i=0; i < xSeeds.size(); i++){
                if(xSeeds[i] != 0 && i != pos){
                    resultSet[i].push(make_pair(x, menor - xSeeds[i])); //constroi priority queue com a diferenca das distancia
                }
            }
            t++;
        }
        for(int i =0; i < seeds.size();i++){
            //LOG(LIB_INFO) << "seeds----:" << seeds[i-1] ;
            //for(int j=0; j < numberElements[i]; j++){
            int j=0;
            while(resultSet[i].size() > 0){
                    pair<int, dist_t> value = resultSet[i].top();
                    // LOG(LIB_INFO) << "aqui2";
                    resultSet[i].pop();
                    //  LOG(LIB_INFO) << "i::" << i << " value::" << value.first << " dist::" << value.second;
                    pivotsTable[i].push_back(value.first);
            }
            //LOG(LIB_INFO) << "ok 1";
            vector<int>::iterator ip = std::unique(pivotsTable[i].begin(), pivotsTable[i].end());
            pivotsTable[i].resize(std::distance(pivotsTable[i].begin(), ip));
            //LOG(LIB_INFO) << "ok 2:::" << i << ":::seeds::" << seeds[i];
        }
        resultSet = vector<priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPairSet<dist_t> >>();
    }

    template <typename dist_t>
    //vector<vector<int>> HGraphSet<dist_t>:: partitionSpaceOverDist(vector<int> seeds, vector<int> X, float over, vector<vector<int>> &pivotsTable){
    void HGraphSet<dist_t>:: partitionSpaceOverDist(vector<int> seeds, vector<int> X, float over, vector<vector<int>> &pivotsTable){
            LOG(LIB_INFO) << "seeds:::" << seeds.size();
        LOG(LIB_INFO) << "X:::" << X.size();
        //vector<vector<int>> pivotsTable(seeds.size(), vector<int>(1));//, vector<int>(1));
        //calcular dist pairwise pivots
        //seeds not repeated -- ok!
        dist_t pivotsDist[seeds.size()][seeds.size()];
        //int avgSizeOverlap = (int) ceil(((float)X.size()/seeds.size())*overRate_);
        //if(avgSizeOverlap == 0) avgSizeOverlap = 1;
        int avgSize = ceil(X.size()*over);
        if(avgSize <= 0) avgSize = 1;
        //vector<int> overLapSize(seeds.size(), 0);
        for(int i=0; i < seeds.size();i++){
            pivotsTable[i][0] = seeds[i]; //inicializar o primeiro elemento como o pivô da subdivisão
            //  LOG(LIB_INFO) << "seeds i:" << i << " value:" << seeds[i];
            /*for(int j=0; j < i; j++){
                //if(pivotsDist[i][j] >= 0) continue;
                if(i==j){
                    pivotsDist[i][j] = 0;
                    pivotsDist[j][i] = 0;
                }else{
                    dist_t d = space_.IndexTimeDistance(ElList_[seeds[i]]->getData(), ElList_[seeds[j]]->getData());
                    pivotsDist[i][j] = d;
                    pivotsDist[j][i] = d;
                    //LOG(LIB_INFO) << "i:" << i << " j:" << j << "dist" << pivotsDist[j][i];
                }
            }*/
        }//matriz de distancias entre pivos

        /*for(int i=0; i < seeds.size(); i++){
            for(int j=0; j < seeds.size(); j++){
                LOG(LIB_INFO) << "i:" << seeds[i] << " j:" << seeds[j] << "dist" << pivotsDist[i][j];
            }
        }*/
        //vector<priority_queue<pair<int,dist_t>,vector<pair<int,dist_t>>, EvaluatedPair<dist_t>> > overlapHeaps(seeds.size());
        /*for(int i=0; i < seeds.size(); i++){
            LOG(LIB_INFO) << "Size pivotsTable i" << i << ":::" << pivotsTable[i].size();
        }*/
        //ate aqui ok!!
        vector<priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPairSet<dist_t> >> resultSet(seeds.size());
        //vector<priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPairMin<dist_t> >> resultSet(seeds.size());
        //matriz de priority queue --> ordenar por diferenca de distancia
        //vector<vector<dist_t>> distTableX(X.size(), vector<dist_t>(seeds.size()));
        //matriz de distancia x
        vector<pair<int,dist_t>> PivotsX(X.size());
        //matriz de pares pivo e distancia seguindo a ordem de x
        int t=0;
        /*LOG(LIB_INFO) << "inicializacao ok!";
        printSeeds(seeds);
        LOG(LIB_INFO) << "inicializacao ok 2!";*/
        for(int x : X){
            vector<dist_t> xSeeds(seeds.size(), 0);
            //dist_t xSeeds[seeds.size()];
            dist_t menor = INFINITY;
            int pos = 0;
            // bool equal = false;
            for(int j=0; j < seeds.size();j++){
                //  equal = false;
                if(x == seeds[j]){
                    // equal = true;
                    xSeeds[j] = 0;
                    //distTableX[t][j] = -1; //senão distancia dele com ele mesmo entra como overlap
                    //-1 para nao verificar
                    //distTableX[j][t] = 0;
                }else{
                    dist_t pj = space_.IndexTimeDistance(ElList_[seeds[j]]->getData(), ElList_[x]->getData());
                    xSeeds[j] = pj;
                    //distTableX[t][j] = pj;
                    //distTableX[j][t] = pj;
                    if(pj <= menor){
                        menor = pj;
                        pos = j;
                    }
                }
            }//calcula qual o pivo mais proximo, salva a distancia para todos os pivos para xSeeds
            PivotsX.push_back(make_pair(pos, menor));//par com o pivo e a distancia para o pivo
            //if(!equal){
            if(x != seeds[pos]){ //igual a seed ja foi adicionado
                pivotsTable[pos].push_back(x); //adiciona no seu hiperplano -> pos
                //distTableX[t][pos] = -1; //ja adicionou, nao precisa verificar de novo
                //distTableX[pos][t] = -1;
            }
            for(int i=0; i < xSeeds.size(); i++){
                if(xSeeds[i] != 0 && i != pos){

                    resultSet[i].push(make_pair(x, menor - xSeeds[i])); //constroi priority queue com a diferenca das distancia
                }
            }
            t++;
        }//particionado + calculado a diferencia das distancias
        //LOG(LIB_INFO) << "passou pelo for";
        /*LOG(LIB_INFO) << distTableX.size();
        for(int i=0; i < X.size(); i++){
            for(int j=0; j < seeds.size(); j++){
                LOG(LIB_INFO) << "i::" << i << " j::" << j << " dist::" << distTableX[i][j];
            }
        }*/
        //até aqui ok!!!! -- overlap também funcionando
        //  LOG(LIB_INFO) << "ok1";
         /*for(int i=0; i < seeds.size(); i++){
             LOG(LIB_INFO) << "i:::::::" << i ;
             printSeeds(pivotsTable[i]);
         }*/
        //:::::como calcular o overlap::::
        /*vector<int> numberElements(seeds.size());
        for(int i=0; i < seeds.size(); i++){
            int n = ceil(pivotsTable[i].size()*over);
            if(n > 1) numberElements[i] = n;
            else numberElements[i] = 1;
        }*/
        for(int i =0; i < seeds.size();i++){
            //LOG(LIB_INFO) << "seeds----:" << seeds[i-1] ;
            //for(int j=0; j < numberElements[i]; j++){
            int j=0;
            for(int j=0; j < avgSize ; j++){
                if(j >= resultSet[i].size()) break;
              //  LOG(LIB_INFO) << "aqui" << resultSet[i].size();
                pair<int, dist_t> value = resultSet[i].top();
               // LOG(LIB_INFO) << "aqui2";
                resultSet[i].pop();
              //  LOG(LIB_INFO) << "i::" << i << " value::" << value.first << " dist::" << value.second;
                pivotsTable[i].push_back(value.first);
              //  LOG(LIB_INFO) << "ok!";
            }
            //LOG(LIB_INFO) << "ok 1";
            vector<int>::iterator ip = std::unique(pivotsTable[i].begin(), pivotsTable[i].end());
            pivotsTable[i].resize(std::distance(pivotsTable[i].begin(), ip));
            //LOG(LIB_INFO) << "ok 2:::" << i << ":::seeds::" << seeds[i];
        }
       // LOG(LIB_INFO) << "ultimo for";
        resultSet = vector<priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPairSet<dist_t> >>();
//        return pivotsTable;
    }

    template <typename dist_t>
    vector<vector<int>> HGraphSet<dist_t>::partitionSpaceOver(vector<int> seeds, vector<int> X){
        vector<vector<int>> pivotsTable(seeds.size(), vector<int>(1));
        for(int i=0; i < seeds.size();i++){
            pivotsTable[i][0] = seeds[i];
        }
        int avgSizeOverlap = ceil(((float)X.size()/seeds.size())*overRate_);
        vector<priority_queue<pair<int,dist_t>,vector<pair<int,dist_t>>, EvaluatedPairSet<dist_t>> > overlapHeaps(seeds.size());
        for(int x : X){
            dist_t menor = INFINITY;
            int pos = 0;
            int pos2 = 0; //segunda seed mais perto
            int seeds2 = seeds[0];
            dist_t menor2 = INFINITY;
            bool equal = false; //para nao inserir igual a seed
            for(int j=0; !equal && j < seeds.size();j++){
                if(x == seeds[j]){
                    equal = true;
                }
                dist_t pj = space_.IndexTimeDistance(ElList_[seeds[j]]->getData(), ElList_[x]->getData());
                if(pj < menor){
                    //LOG(LIB_INFO) << "pos" << pos << " pos2" << pos2;
                    menor2 = menor;
                    seeds2 = seeds[j];
                    pos2 = pos;//atualiza o segundo menor para overlap
                    menor = pj;
                    pos = j;
                }
            }
            if(!equal){
                pivotsTable[pos].push_back(x);
                if(avgSizeOverlap > 0 && pos2 != pos){ //cria uma overlapHeap de size para cada pos
                    if(overlapHeaps[pos2].size() < avgSizeOverlap){
                        overlapHeaps[pos2].push(pair<int,dist_t>(x,menor2));
                    }else if(overlapHeaps[pos2].top().second >= menor2){
                        overlapHeaps[pos2].pop();
                        overlapHeaps[pos2].push(pair<int,dist_t>(x,menor2));
                    }
                }
                //LOG(LIB_INFO) << "pivots Table: pos"<<pos;
                //printSeeds(pivotsTable[pos]);
            }
        }
        for(int i=0; i < seeds.size();i++){
            // printSeeds(pivotsTable[i]);
            while(overlapHeaps[i].size() > 0){ //adiciona no subset
                pivotsTable[i].push_back(overlapHeaps[i].top().first);
                overlapHeaps[i].pop();
            }
            //pivotsTable[i].insert(pivotsTable[i].end(), overlapHeaps[i].begin().first, overlapHeaps[i].end().first);
        }
        return pivotsTable;
    }


    template<typename dist_t>
    //void HGraphSet<dist_t>::refineGraphSet(set<int> levelSeeds, int lcount, string gtype) {
    void HGraphSet<dist_t>::refineGraphSet() {
        //LOG(LIB_INFO) << "GraphSeeds:::" << levelSeeds.size() << " Level:" << lcount;
        //SeedsRNG(levelSeeds); //occlusion rule in fanng como no rng (rng entre os nao conectados)
        LOG(LIB_INFO) << "::::::SEEDS::::::" << levelSeeds_.size();
        //printSeeds(levelSeeds);
        LOG(LIB_INFO) << ":::::REFINE GRAPH:::::";
        int lcount;
        if ((L_ -1) < 1) lcount = 1;
        if(gtype_ == "knng"){
            //int upperK = NN_;//((data_.size()/S_)/lcount);
            int upperK = 2*NN_;
            knngConstructionSet(levelSeeds_, upperK);
        }else if(gtype_ == "rng"){
            SeedsRNGSet(levelSeeds_, true);
        }
        //krngConstruction(levelSeeds, NN_);
        //completeGraph(levelSeeds);
    }

    template<typename dist_t>
    void HGraphSet<dist_t>::knngConstructionSet(set<int> levelSeeds, int nn_) {
        countRefine = 0;
        priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPairSet<dist_t> > resultSet;
        int nNeighbors = nn_;
        /*if(levelSeeds_.size() <= NN_){

        }*/
        for (auto idAtual : levelSeeds_) {
            for (auto i : levelSeeds_) {
                if (i == idAtual) continue;//&& !ElList_[idAtual]->isNeighbor(ElList_[i])
                dist_t d = space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData());
                if (resultSet.size() < nNeighbors) {
                    // LOG(LIB_INFO) << "add n neighbors";
                    resultSet.push(make_pair(i, d));
                } else {
                    if (resultSet.top().second > d) {
                        resultSet.pop();
                        resultSet.push(make_pair(i, d));
                    }//else delete evNode;
                }
            }
            for (int i = 0; i < nNeighbors; i++) {
                pair<int, dist_t> resultNode = resultSet.top();
                resultSet.pop();
                //link(ElList_[idAtual], ElList_[resultNode.first], false);
                link(ElList_[idAtual], ElList_[resultNode.first], false, resultNode.second,true, gtype_);//nao direcionado para a seed
                countRefine++;
            }
            // LOG(LIB_INFO) << "idAtual 2::" << idAtual;
            resultSet = priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPairSet<dist_t> >();
        }
    }


    template <typename dist_t>
    void HGraphSet<dist_t>::pruneEdgesSet() {
        //eliminar edges --> se 10NN elimina os outros
        // HNodeSetEvalCompare<dist_t> compareFunction;
        for(HNodeSet<dist_t>* v : ElList_){
            LOG(LIB_INFO) << "prune edges of v::" << v->getId();
            if(levelSeeds_.find(v->getId()) != levelSeeds_.end()){
                continue;
            }
            //para cada vértice: ordenar as arestas pela distancia e manter apenas as NN_ mais proximas
            //vector<HNodeSet_eval<dist_t>*> neighbors = v->getNeighbors();
            //sort(neighbors.begin(), neighbors.end(), HNodeSetEvalCompare<dist_t>());
            //LOG(LIB_INFO) << "ok!!!";
            //apenas para teste
            //neighbors.erase(neighbors.begin() + (int)NN_, neighbors.end());
            //LOG(LIB_INFO) << "neighbors size " << neighbors.size();
            //v->setNeighbors(neighbors);
            //v->eraseNeighbors((int)NN_);
            int i=0;
            for(HNodeSet_eval<dist_t>* n : v->getNeighbors()){
                LOG(LIB_INFO) << "Neighbors:: " << n->getHNodeSet()->getId() << " distance:" << n->getDistance() ;
                i++;
            }
        }
        return;
    }


    template<typename dist_t>
    void HGraphSet<dist_t>::SeedsRNG(vector<int> seeds, bool isPivots){
        for(int idAtual : seeds){
            for(int i: seeds){
                if(i != idAtual){
                    if(!ElList_[idAtual]->isNeighbor(ElList_[i])){ //caso não tiver nenhuma conexao verifica
                        dist_t d = space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData());
                        pair<int, dist_t> evNode = make_pair(i, d);
                        bool connect = shouldConnect(evNode, idAtual, seeds);
                        if(connect){
                            link(ElList_[evNode.first], ElList_[idAtual], false, d, isPivots, gtype_);
                        }
                    }
                }
            }
        }
    }


    template<typename dist_t>
    void HGraphSet<dist_t>::SeedsRNGSet(std::set<int> seeds, bool isPivots) {
        for(int idAtual : seeds){
            for(int i: seeds){
                if(i != idAtual){
                    if(!ElList_[idAtual]->isNeighbor(ElList_[i])){ //caso não tiver nenhuma conexao verifica
                        dist_t d = space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData());
                        pair<int, dist_t> evNode = make_pair(i, d);
                        bool connect = shouldConnectSet(evNode, idAtual, seeds);
                        if(connect){
                            link(ElList_[evNode.first], ElList_[idAtual], false, d, isPivots, gtype_);
                        }
                    }
                }
            }
        }
    }

    template<typename dist_t>
    bool HGraphSet<dist_t>::shouldConnectSet(pair<int, dist_t> evNode, int idAtual, set<int> Xs){
        if(idAtual != evNode.first){
            for(int i : Xs){
                dist_t dNewElement = space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData());
                dist_t dEvaluated = space_.IndexTimeDistance(ElList_[evNode.first]->getData(), ElList_[i]->getData());
                if(dNewElement < evNode.second && dEvaluated < evNode.second){
                    return false;
                    break;
                }
            }
            return true;
        }
        return false;
    }



    template<typename dist_t>
    void HGraphSet<dist_t>::insertSeeds(std::vector<int> &seeds, int lcount) {
        for(int s : seeds){
            levelSeeds_.insert(s);
            //levelSeeds_[lcount].push_back(s);
        }
    }


    template<typename dist_t>
    vector<int> HGraphSet<dist_t>::divideIndices(vector<int> X, int seedId, int size) {
        vector<int> Xs;
        while(Xs.size() <= size){
            int randId = RandomInt()%X.size();
            Xs.push_back(X[randId]);
            notSelected.erase(X[randId]);
            selected.insert(X[randId]);
            X.erase(X.begin()+randId); //erase aqui permite overlap para outras regiões --tratamento de bordas?
        }
        return Xs;
    }


    /*template <typename dist_t>
    vector<int> HGraphSet<dist_t>::divideIndicesSeeds(vector<int> X, int seedId, int size) {
        vector<int> Xs;
        priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPair<dist_t> > resultSet;

        //ideias com random escolhe 4 seeds, um for para calcular a dist de cada elemento para cada seed
        //particionar pelo os que estao mais perto de cada seed
        //kmeans?
        return Xs;
    }*/


    //pega uma seed e particiona por treshold (maior dist /numero de seeds) --> problema sizes diferentes em particoes

    template<typename dist_t>
    void HGraphSet<dist_t>::createGraph(vector<int> Xs, int lcount) {
        //um caso para cada tipo de grafo
        LOG(LIB_INFO) << "tamanho:::" << Xs.size();
        if(Xs.size() <= 1) return;
        //LOG(LIB_INFO) << "LCOUNT:" << lcount << " Xs Size:" << Xs.size();
        if(indexGraphType_=="knng"){
            //   LOG(LIB_INFO) << "knng";
            knngConstruction(Xs, NN_, true, false);
            //knngConstruction(Xs, NN_, false);
            //LOG(LIB_INFO) << "knng done";
            //funcao para construir knng local
        }else if(indexGraphType_ == "rng"){
            //funcao para construir rng
            rngConstruction(Xs, false);
        }else if(indexGraphType_ == "krng"){
            krngConstruction(Xs, NN_, false);
        }else if(indexGraphType_ == "swg"){
            swgConstruction(Xs, NN_, false);
        }
    }

    template<typename dist_t>
    void HGraphSet<dist_t>::link(HNodeSet<dist_t>* node1, HNodeSet<dist_t>* node2, bool dir, dist_t distance, bool isPivot, string gtype) {
        if(isPivot){
            if(dir){
                node1->addNeighbor(node2, true, distance); //direcionado
            }else{
                node1->addNeighbor(node2, true, distance); //direcionado
                node2->addNeighbor(node1, true, distance); //direcionado
            }
        }else{
            if(gtype=="knng"){
                node1->addNeighborCheckNN(node2, true, distance, NN_);
            }else if((gtype == "rng") | (gtype == "krng")){ //todo:verificar se é necessário
                node1->addNeighbor(node2, true, distance); //direcionado
                node2->addNeighbor(node1, true, distance); //direcionado
                /*typename set<HNodeSet_eval<dist_t>*,HNodeSetEvalCompare<dist_t>>::iterator it = node1->getNeighbors().begin();
                while(it != node1->getNeighbors().end()){
                    if(distance < (*it)->getDistance() && space_.IndexTimeDistance(node2->getData(),(*it)->getHNodeSet()->getData()) < (*it)->getDistance()){
                        it = node1->getNeighbors().erase(it);
                    }else it++;
                }*/   //demora muito --> tempo aumento a cada partição que é construída
            }else if(gtype == "swg"){ //funcionando --> teste de mesa
                node1->addNeighborCheckNN(node2, true, distance, 2*NN_);
                node2->addNeighborCheckNN(node1, true, distance, 2*NN_);
                //node1->addNeighbor(node2, true, distance); //direcionado
                //node2->addNeighbor(node1, true, distance); //direcionado
            }else if(gtype == "complete"){
                node1->addNeighbor(node2, true, distance); //direcionado
                node2->addNeighbor(node1, true, distance); //direcionado
            }

        }
    }

    template<typename dist_t> //min size tem que ser NN
    void HGraphSet<dist_t>::knngConstruction(vector<int> &Xs, size_t k, bool dir, bool isPivot) {
        priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPairSet<dist_t> > resultSet;
        int nNeighbors = k;
        //LOG(LIB_INFO) << "Number of elements:" << Xs.size();
        if(Xs.size()-1 == k){
            completeGraph(Xs);
            return;
        }
        if(Xs.size() <= k){
            //nNeighbors = Xs.size()-1;
            completeGraph(Xs);
            return;
        }
        //LOG(LIB_INFO) << "xxxx";
        for(int idAtual : Xs){
            for (int i : Xs) {
                // LOG(LIB_INFO) << "no loop::" << i;
                // if(i==idAtual) printSeeds(Xs);
                if (i == idAtual) continue;//&& !ElList_[idAtual]->isNeighbor(ElList_[i])
                dist_t d = space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData());
                if(resultSet.size() < nNeighbors){
                    // LOG(LIB_INFO) << "add n neighbors";
                    resultSet.push(make_pair(i, d));
                } else {
                    if (resultSet.top().second > d) {
                        resultSet.pop();
                        resultSet.push(make_pair(i, d));
                    }//else delete evNode;
                }
            }
            //LOG(LIB_INFO) << "idAtual::" << idAtual;
            //LOG(LIB_INFO) << "nNeighbors" << resultSet.size();
            for (int i = 0; i < nNeighbors; i++) {
                pair<int, dist_t> resultNode = resultSet.top();
                resultSet.pop();
                //link(ElList_[idAtual], ElList_[resultNode.first], false);
                if(idAtual==Xs[0]){
                    link(ElList_[idAtual], ElList_[resultNode.first], false, resultNode.second, isPivot, indexGraphType_);//nao direcionado para a seed
                }else{
                    link(ElList_[idAtual], ElList_[resultNode.first], dir, resultNode.second, isPivot, indexGraphType_);//de acordo com dir para outro
                }
            }
            // LOG(LIB_INFO) << "idAtual 2::" << idAtual;
            resultSet = priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPairSet<dist_t> >();
        }
    }

    template<typename dist_t>
    void HGraphSet<dist_t>::completeGraph(vector<int> &Xs){
        for(int idAtual : Xs){
            for(int i: Xs){
                if(i != idAtual){
                    link(ElList_[idAtual], ElList_[i], true, space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData()), false, "complete");
                }
            }
        }
    }

    template<typename dist_t>
    void HGraphSet<dist_t>::rngConstruction(vector<int> &Xs, bool isPivot) {
        for(int idAtual : Xs){
            for(int i: Xs){
                if(i != idAtual){
                    HNodeSet<dist_t> *evaluated = ElList_[i];
                    dist_t d = space_.IndexTimeDistance(ElList_[idAtual]->getData(), evaluated->getData());
                    pair<int, dist_t> evNode = make_pair(i, d);
                    bool connect = shouldConnect(evNode, idAtual, Xs);
                    if(connect){
                        link(ElList_[evNode.first], ElList_[idAtual], false, evNode.second,isPivot, indexGraphType_);
                    }
                }
            }
        }
    }

    template<typename dist_t>
    void HGraphSet<dist_t>::krngConstruction(std::vector<int> &Xs, size_t k, bool isPivot) {
        LOG(LIB_INFO) << Xs.size();
        for(int idAtual : Xs){
            for(int i: Xs){
                if(i != idAtual && !ElList_[idAtual]->isNeighbor(ElList_[i])){
                    HNodeSet<dist_t> *evaluated = ElList_[i];
                    dist_t d = space_.IndexTimeDistance(ElList_[idAtual]->getData(), evaluated->getData());
                    pair<int, dist_t> evNode = make_pair(i, d);
                    bool connect = shouldConnectK(evNode, idAtual, Xs, k);
                    if(connect){
                        // LOG(LIB_INFO) << "Link krng:" << evNode.first << "--" << idAtual;
                        link(ElList_[evNode.first], ElList_[idAtual], false, evNode.second, isPivot, indexGraphType_);
                    }
                }
            }
        }
    }

    template <typename dist_t>
    void HGraphSet<dist_t>::swgConstruction(vector<int> &Xs, size_t k, bool isPivot) {
        set<int> tempNodesSet;
        vector<int> tempNodes;
    for(int x : Xs){
        priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPairSet<dist_t> > resultSet;
        swgIndexSearch(ElList_[x]->getData(), Xs, resultSet, tempNodesSet, tempNodes);
        //LOG(LIB_INFO) << "size after:::" << resultSet.size();
        while (!resultSet.empty()) {
            pair<int, dist_t> top = resultSet.top();
            //LOG(LIB_INFO) << "resultSet:::" << top.first;
            link(ElList_[x], ElList_[top.first], false, top.second, isPivot, "swg");
            resultSet.pop();
        }
        tempNodesSet.insert(x);
        tempNodes.push_back(x);
        //LOG(LIB_INFO) << "tempSet after::" << tempNodesSet.size();
        //LOG(LIB_INFO) << "tempNodes::" << tempNodes.size();
    }

    }

    template<typename dist_t>
    void HGraphSet<dist_t>::swgIndexSearch(const Object *queryObj, vector<int> X, priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPairSet<dist_t> > &resultSet, set<int> &tempNodesSet, vector<int> &tempNodes) const {
        //TODO: search for indexing
        vector<bool> visitedBitset(data_.size());

        vector<HNodeSet<dist_t> *> neighborCopy;
        set<int> alreadyInserted;
        for (size_t i = 0; i < initSearchAttempts_; i++) {

            int entryPoint; //tempNodes simular um grafo vazio para construcao do swg
            if(!tempNodes.size()){
                entryPoint = NULL;
                return;
            }else{
                entryPoint = RandomInt()%tempNodes.size();
            }
            //int entryPoint = RandomInt() % X.size();
            int dataId = X[entryPoint];

            priority_queue<dist_t> closestDistQueue;
            priority_queue<pair<int, dist_t>, vector<pair<int, dist_t>>, EvaluatedPairMinSet<dist_t>> candidateSet;

            dist_t d = space_.IndexTimeDistance(queryObj, ElList_[dataId]->getData());

            pair<int, dist_t> ev;
            ev = make_pair(dataId, d);

            candidateSet.push(ev);
            closestDistQueue.push(d);

            if (closestDistQueue.size() > efConstruction_) {
                closestDistQueue.pop();
            }


            if (dataId >= data_.size()) {
                stringstream err;
                err << "Bug: nodeId > data_size()";
                LOG(LIB_INFO) << err.str();
                throw runtime_error(err.str());
            }
            visitedBitset[dataId] = true;

            if(alreadyInserted.find(dataId) == alreadyInserted.end()){
                alreadyInserted.insert(dataId);
                resultSet.emplace(dataId, d);
            }


            if (resultSet.size() > NN_) { // TODO check somewhere that NN > 0
                resultSet.pop();
            }

            while (!candidateSet.empty()) {
                const pair<int, dist_t> currEv = candidateSet.top();
                dist_t lowerBound = closestDistQueue.top();

                /*
                 * Check if we reached a local minimum.
                 */
                if (currEv.second > lowerBound) {
                    break;
                }

                /*
                 * This lock protects currNode from being modified
                 * while we are accessing elements of currNode.
                 */
                /*size_t neighborQty = 0;
                {
                    unique_lock<mutex> lock(currNode->accessGuard_);

                    //const vector<MSWNode*>& neighbor = currNode->getAllFriends();
                    const vector<MSWNode*>& neighbor = currNode->getAllFriends();
                    neighborQty = neighbor.size();
                    if (neighborQty > neighborCopy.size()) neighborCopy.resize(neighborQty);
                    for (size_t k = 0; k < neighborQty; ++k)
                        neighborCopy[k]=neighbor[k];
                }*/

                // Can't access curEv anymore! The reference would become invalid
                candidateSet.pop();

                // calculate distance to each neighbor
                //LOG(LIB_INFO) << "size neighbors:::" << ElList_[currEv.first]->getNeighbors().size();
                for (HNodeSet_eval<dist_t> *e : ElList_[currEv.first]->getNeighbors()) {
                   // LOG(LIB_INFO) << "id neighbor::" << e->getHNodeSet()->getId();
                  //  if (tempNodesSet.find(e->getHNodeSet()->getId()) != tempNodesSet.end()) {
                        if(tempNodesSet.find(e->getHNodeSet()->getId()) == tempNodesSet.end()){
                            continue;
                        }
                        size_t neighborId = e->getHNodeSet()->getId();
                        if (neighborId >= data_.size()) {
                            stringstream err;
                            err << "Bug: nodeId > data_size()";
                            LOG(LIB_INFO) << err.str();
                            throw runtime_error(err.str());
                        }
                        if (!visitedBitset[neighborId]) {
                            visitedBitset[neighborId] = true;
                        }

                        d = space_.IndexTimeDistance(queryObj, e->getHNodeSet()->getData());
                        if (closestDistQueue.size() < efConstruction_ || d < closestDistQueue.top()) {
                            closestDistQueue.push(d);
                            if (closestDistQueue.size() > efConstruction_) {
                                closestDistQueue.pop();
                            }
                            candidateSet.emplace(neighborId, d);
                        }

                        if ((resultSet.size() < NN_ || resultSet.top().second > d) && alreadyInserted.find(neighborId) == alreadyInserted.end()) {
                            alreadyInserted.insert(neighborId);
                            resultSet.emplace(neighborId, d);
                            if (resultSet.size() > NN_) {
                                resultSet.pop();
                            }
                        }

                  //  }
                }
                }
            }
        }



    template<typename dist_t>
    bool HGraphSet<dist_t>::shouldConnect(pair<int, dist_t> evNode, int idAtual, vector<int> Xs){
        if(idAtual != evNode.first){
            for(int i : Xs){
                dist_t dNewElement = space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData());
                dist_t dEvaluated = space_.IndexTimeDistance(ElList_[evNode.first]->getData(), ElList_[i]->getData());
                if(dNewElement < evNode.second && dEvaluated < evNode.second){
                    return false;
                    break;
                }
            }
            return true;
        }
        return false;
    }

    template<typename dist_t>
    bool HGraphSet<dist_t>::shouldConnectK(pair<int, dist_t> evNode, int idAtual, vector<int> Xs, int K){
        if(idAtual != evNode.first){
            int count = 0;
            for(int i : Xs){
                dist_t dNewElement = space_.IndexTimeDistance(ElList_[idAtual]->getData(), ElList_[i]->getData());
                dist_t dEvaluated = space_.IndexTimeDistance(ElList_[evNode.first]->getData(), ElList_[i]->getData());
                if(dNewElement < evNode.second && dEvaluated < evNode.second){
                    count++;
                    if(count > K) return false;
                    //cpnta numero de k
                    //return false;
                    //break;
                }
            }
            if (count <= K) return true;
            else return false;
        }
        return false;
    }

    template <typename dist_t>
    void HGraphSet<dist_t>::Search(RangeQuery<dist_t>* query, IdType) const {
        if (bDoSeqSearch_) {
            for (size_t i = 0; i < data_.size(); ++i) {
                query->CheckAndAddToResult(data_[i]);
            }
        } else {
            for (int i =0; i < 100000; ++i);
        }
    }

    template <typename dist_t>
    void HGraphSet<dist_t>::Search(KNNQuery<dist_t>* query, IdType) const {
        //LOG(LIB_INFO) << "gnns";
        if (ElList_.empty()) {
            LOG(LIB_INFO) << "Empty graph!!!!";
            return;
        }else if(searchAlgoType_ == gnns) {
            return GNNS(query);
        }else if(searchAlgoType_ == swsearch) {
            return SWSearch(query);
        }
    }

    template<typename dist_t>
    void HGraphSet<dist_t>::GNNS(similarity::KNNQuery<dist_t> *query) const {
        for (size_t i = 0; i < initSearchAttempts_; i++) {
#if START_PIVOTS
            int random = RandomInt() % levelSeeds_.size();
            int j=0;
            int curr;
            for (std::set<int>::iterator it=levelSeeds_.begin(); it!=levelSeeds_.end(); ++it){
                if(j == random){
                    curr = *it;
                    LOG(LIB_INFO) << curr;
                    break;
                }
                j++;
            }
#else
            size_t curr = RandomInt() % ElList_.size();
            //seeds to start search
#endif
            dist_t currDist = query->DistanceObjLeft(ElList_[curr]->getData());
            query->CheckAndAddToResult(currDist, ElList_[curr]->getData());
            size_t currOld;
            do {
                currOld = curr;
                //LOG(LIB_INFO) << "current::" << curr;
                for (HNodeSet_eval<dist_t> *e : ElList_[currOld]->getNeighbors()) {
                    size_t currNew = e->getHNodeSet()->getId();
                    //if(currNew != bad){
                    dist_t currDistNew = query->DistanceObjLeft(ElList_[currNew]->getData());
                    query->CheckAndAddToResult(currDistNew, ElList_[currNew]->getData());
                    if (currDistNew < currDist) {
                        curr = currNew;
                        currDist = currDistNew;
                    }
                    //}
                }
            } while (currOld != curr);
        }
    }

    template<typename dist_t>
    void HGraphSet<dist_t>::SWSearch(similarity::KNNQuery<dist_t> *query) const {
        vector<bool>                        visitedBitset(ElList_.size());

        for (size_t i = 0; i < initSearchAttempts_; i++) {
#if START_PIVOTS
            int random = RandomInt() % levelSeeds_.size();
            int j=0;
            int curr;
            for (std::set<int>::iterator it=levelSeeds_.begin(); it!=levelSeeds_.end(); ++it){
                if(j == random){
                    curr = *it;
                    LOG(LIB_INFO) << curr;
                    break;
                }
                j++;
            }
#else
            size_t curr = RandomInt() % ElList_.size();
            //seeds to start search
#endif
            const Object* currObj = ElList_[curr]->getData();
            dist_t currDist = query->DistanceObjLeft(ElList_[curr]->getData());
            query->CheckAndAddToResult(currDist, ElList_[curr]->getData());
            size_t currOld;
        //todo --> priority queue
            priority_queue <dist_t>                          closestDistQueue; //The set of all elements which distance was calculated
            priority_queue<HNodeSet_eval<dist_t>*,vector<HNodeSet_eval<dist_t>*>,HNodeSetEvalCompareReverse<dist_t>> candidateQueue;
            candidateQueue.push(new HNodeSet_eval<dist_t>(ElList_[curr], currDist));
            closestDistQueue.emplace(currDist);
            //the set of elements which we can use to evaluate
            size_t nodeId = ElList_[curr]->getId();
            // data_.size() is guaranteed to be equal to ElList_.size()
            if (nodeId >= data_.size()) {
                stringstream err;
                err << "Bug: nodeId > data_size()";
                LOG(LIB_INFO) << err.str();
                throw runtime_error(err.str());
            }
            visitedBitset[nodeId] = true;
            while(!candidateQueue.empty()){

                auto iter = candidateQueue.top(); // This one was already compared to the query
                HNodeSet_eval<dist_t>* currEv = iter;

                // Did we reach a local minimum?
                if (currEv->getDistance() > closestDistQueue.top()) {
                    break;
                }
//#ifdef NOT_MAC

          /*     for (HNodeSet_eval<dist_t>* neighbor : (currEv->getHNodeSet()->getNeighbors())) {
                    _mm_prefetch(reinterpret_cast<const char*>(const_cast<const Object*>(neighbor->getHNodeSet()->getData())), _MM_HINT_T0);
                }
                for (HNodeSet_eval<dist_t>* neighbor : (currEv->getHNodeSet()->getNeighbors())) {
                    _mm_prefetch(const_cast<const char*>(neighbor->getHNodeSet()->getData()->data()), _MM_HINT_T0);
                }*/
//#endif

                const set<HNodeSet_eval<dist_t>*,HNodeSetEvalCompare<dist_t>> neighbor = (currEv->getHNodeSet()->getNeighbors());

                // Can't access curEv anymore! The reference would become invalid
                candidateQueue.pop();

                //calculate distance to each neighbor
                for (auto iter = neighbor.begin(); iter != neighbor.end(); ++iter){
                    nodeId = (*iter)->getHNodeSet()->getId();
                    // data_.size() is guaranteed to be equal to ElList_.size()
                    if (nodeId >= data_.size()) {
                        stringstream err;
                        err << "Bug: nodeId > data_size()";
                        LOG(LIB_INFO) << err.str();
                        throw runtime_error(err.str());
                    }
                    if (!visitedBitset[nodeId]) {
                        currObj = (*iter)->getHNodeSet()->getData();
                        currDist = query->DistanceObjLeft(currObj);
                        visitedBitset[nodeId] = true;

                        if (closestDistQueue.size() < efSearch_ || currDist < closestDistQueue.top()) {
                            closestDistQueue.emplace(currDist);
                            if (closestDistQueue.size() > efSearch_) {
                                closestDistQueue.pop();
                            }

                            candidateQueue.push(new HNodeSet_eval<dist_t>((*iter)->getHNodeSet(), currDist));
                            //candidateQueue.emplace(HNodeSet_eval<dist_t>((*iter)->getHnodeSet(), currDist));
                        }

                        query->CheckAndAddToResult(currDist, currObj);
                    }
                }
            }
        }
    }

    template <typename dist_t>
    void
    HGraphSet<dist_t>::SetQueryTimeParams(const AnyParams& QueryTimeParams) {
        // Check if a user specified extra parameters, which can be also misspelled variants of existing ones
        AnyParamManager pmgr(QueryTimeParams);
        pmgr.GetParamOptional("initSearchAttempts", initSearchAttempts_, 1);
        pmgr.GetParamOptional("efSearch", efSearch_, 1);
        string tmp;
        pmgr.GetParamOptional("algoType", tmp, "gnns");
        ToLower(tmp);
        if (tmp == "gnns") searchAlgoType_ = gnns;
        else if (tmp == "sw") searchAlgoType_ = swsearch;
        else {
            throw runtime_error("algoType should be one of the following:gnns, sw");
        }
        pmgr.CheckUnused();
        LOG(LIB_INFO) << "Set Hgraph query-time params";
    }

    template <typename dist_t>
    void HGraphSet<dist_t>::printGraph(string file) {
        ofstream outFile(file);
        CHECK_MSG(outFile, "Cannot open file");
        outFile.exceptions(std::ios::badbit);
        for (HNodeSet<dist_t> *node : ElList_) {
            for(auto it : node->getNeighbors()){
                outFile << node->getId() << " ";
                outFile << it->getHNodeSet()->getId() << endl;
            }
        }
        outFile.close();
    }


    template<typename dist_t>
    void HGraphSet<dist_t>::writeGephi(string file) { //arrumar
        ofstream outFile(file);
        CHECK_MSG(outFile, "Cannot open file '" + file + "' for writing");
        outFile.exceptions(std::ios::badbit);
        outFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                << "<gexf xmlns=\"http://www.gexf.net/1.2draft\" version=\"1.2\">\n"
                << "<meta>\n"
                << "<creator>larissa.nmslib</creator>\n"
                << "<description></description>\n"
                << "</meta>\n"
                << "<graph mode=\"static\" defaultedgetype=\"directed\">\n";
        outFile << "<nodes>\n";
        for (HNodeSet<dist_t> *pNode: ElList_) {
            size_t nodeID = pNode->getId();
            CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                      "Bug: unexpected node ID " + ConvertToString(nodeID) +
                      " for object ID " + ConvertToString(pNode->getData()->id()) +
                      "data_.size() = " + ConvertToString(data_.size()));
            outFile << "<node id=\"" << nodeID
                    << "\" label=\"" << pNode->getData()->id() << "\" />\n";
        }
        outFile << "</nodes>\n";
        outFile << "<edges>\n";
        int i = 0;
        for (HNodeSet<dist_t> *pNode: ElList_) {
            size_t nodeId = pNode->getId();
            for (HNodeSet_eval<dist_t> *pNodeFriend: pNode->getNeighbors()) {
                IdType nodeFriendID = pNodeFriend->getHNodeSet()->getId();
                CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                          "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                          " for object ID " + ConvertToString(pNodeFriend->getHNodeSet()->getData()->id()) +
                          "data_.size() = " + ConvertToString(data_.size()));
                outFile << "<edge id=\"" << i++ << "\" source=\"" << nodeId << "\" "
                        << "target=\"" << nodeFriendID << "\" />\n";
            }
        }
        outFile << "</edges>\n";
        outFile << "</graph>\n";
        outFile << "</gexf>";
        outFile.close();
    }

    template<typename dist_t>
    void HGraphSet<dist_t>::SaveIndex(const string &location) {
        string gephi = location + "gephi.gexf";
        writeGephi(gephi);
        ofstream outFile(location);
        CHECK_MSG(outFile, "Cannot open file '" + location + "' for writing");
        outFile.exceptions(std::ios::badbit);
        size_t lineNum = 0;

        WriteField(outFile, METHOD_DESC, StrDesc());
        lineNum++;
        WriteField(outFile, "NN", NN_); lineNum++;
        WriteField(outFile, "seeds", S_); lineNum++;
        WriteField(outFile, "levels", L_); lineNum++;
        for (HNodeSet<dist_t> *pNode: ElList_) {
            size_t nodeID = pNode->getId();
            CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                      "Bug: unexpected node ID " + ConvertToString(nodeID) +
                      " for object ID " + ConvertToString(pNode->getData()->id()) +
                      "data_.size() = " + ConvertToString(data_.size()));
            outFile << nodeID << ":" << pNode->getData()->id() << ":";
            for (HNodeSet_eval<dist_t> *pNodeFriend: pNode->getNeighbors()) {
                IdType nodeFriendID = pNodeFriend->getHNodeSet()->getId();
                CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                          "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                          " for object ID " + ConvertToString(pNodeFriend->getHNodeSet()->getData()->id()) +
                          "data_.size() = " + ConvertToString(data_.size()));
                outFile << ' ' << nodeFriendID;
            }
            outFile << endl;
            lineNum++;
        }
        outFile << endl;
        lineNum++; // The empty line indicates the end of data entries
        //WriteField(outFile, LINE_QTY, lineNum+1 /* including this line */);
        outFile << LINE_QTY << ":" << lineNum+1;
        outFile.close();
    }


    template<typename dist_t>
    void HGraphSet<dist_t>::LoadIndex(const string &location) {
        cout << "LOAD INDEX " << endl;
        LOG(LIB_INFO) << "LOAD INDEX";
        vector<HNodeSet<dist_t> *> ptrMapper(data_.size());

        for (unsigned pass = 0; pass < 2; ++pass) {
            ifstream inFile(location);
            CHECK_MSG(inFile, "Cannot open file '" + location + "' for reading");
            inFile.exceptions(std::ios::badbit);

            size_t lineNum = 1;
            string methDesc;
            ReadField(inFile, METHOD_DESC, methDesc);
            lineNum++;
            CHECK_MSG(methDesc == StrDesc(),
                      "Looks like you try to use an index created by a different method: " + methDesc);
            ReadField(inFile, "NN", NN_);
            lineNum++;
            ReadField(inFile, "seeds", S_);
            lineNum++;
            ReadField(inFile, "levels", L_);
            lineNum++;

            string line;
            while (getline(inFile, line)) {
                if (line.empty()) {
                    lineNum++;
                    break;
                }
                stringstream str(line);
                str.exceptions(std::ios::badbit);
                char c1, c2;
                IdType nodeID, objID;
                CHECK_MSG((str >> nodeID) && (str >> c1) && (str >> objID) && (str >> c2),
                          "Bug or inconsitent data, wrong format, line: " + ConvertToString(lineNum)
                );
                CHECK_MSG(c1 == ':' && c2 == ':',
                          string("Bug or inconsitent data, wrong format, c1=") + c1 + ",c2=" + c2 +
                          " line: " + ConvertToString(lineNum)
                );
                CHECK_MSG(nodeID >= 0 && nodeID < data_.size(),
                          DATA_MUTATION_ERROR_MSG + " (unexpected node ID " + ConvertToString(nodeID) +
                          " for object ID " + ConvertToString(objID) +
                          " data_.size() = " + ConvertToString(data_.size()) + ")");
                CHECK_MSG(data_[nodeID]->id() == objID,
                          DATA_MUTATION_ERROR_MSG + " (unexpected object ID " + ConvertToString(data_[nodeID]->id()) +
                          " for data element with ID " + ConvertToString(nodeID) +
                          " expected object ID: " + ConvertToString(objID) + ")"
                );
                if (pass == 0) {
                    unique_ptr<HNodeSet<dist_t>> node(new HNodeSet<dist_t>(data_[nodeID], nodeID));
                    ptrMapper[nodeID] = node.get();
                    ElList_.push_back(node.release());
                } else {
                    HNodeSet<dist_t> *pNode = ptrMapper[nodeID];
                    CHECK_MSG(pNode != NULL,
                              "Bug, got NULL pointer in the second pass for nodeID " + ConvertToString(nodeID));
                    IdType nodeFriendID;
                    while (str >> nodeFriendID) {
                        CHECK_MSG(nodeFriendID >= 0 && nodeFriendID < data_.size(),
                                  "Bug: unexpected node ID " + ConvertToString(nodeFriendID) +
                                  "data_.size() = " + ConvertToString(data_.size()));
                        HNodeSet<dist_t> *pFriendNode = ptrMapper[nodeFriendID];
                        CHECK_MSG(pFriendNode != NULL,
                                  "Bug, got NULL pointer in the second pass for nodeID " +
                                  ConvertToString(nodeFriendID));
                        pNode->addNeighbor(pFriendNode, false, space_.IndexTimeDistance(data_[nodeID], data_[nodeFriendID]));//, false /* don't check for duplicates */);
                    }
                    CHECK_MSG(str.eof(),
                              "It looks like there is some extract erroneous stuff in the end of the line " +
                              ConvertToString(lineNum));
                }
                ++lineNum;
            }
            //LOG(LIB_INFO) << "ElList size" << ElList_.size();
            size_t ExpLineNum;
            ReadField(inFile, LINE_QTY, ExpLineNum);
            CHECK_MSG(lineNum == ExpLineNum,
                      DATA_MUTATION_ERROR_MSG + " (expected number of lines " + ConvertToString(ExpLineNum) +
                      " read so far doesn't match the number of read lines: " + ConvertToString(lineNum) + ")");
            inFile.close();
        }
    }

    template class HGraphSet<float>;
    template class HGraphSet<double>;
    template class HGraphSet<int>;

}
