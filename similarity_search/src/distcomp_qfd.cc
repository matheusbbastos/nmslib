#include "distcomp.h"
#include "string.h"
#include "logging.h"
#include "utils.h"
#include "pow.h"
#include "portable_intrinsics.h"

#include <cstdlib>
#include <limits>
#include <algorithm>
#include <cmath>
#include <boost/assign/std/vector.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/moment.hpp>
#include <boost/accumulators/statistics/covariance.hpp>
#include <boost/accumulators/statistics/variates/covariate.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

//using namespace boost::accumulators;
//using namespace boost::assign;
//using namespace boost::numeric::ublas;

typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean> > localMean;
typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::covariance<double, boost::accumulators::tag::covariate1> > > covariance_XY;
typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean, boost::accumulators::tag::variance> > zscore;


namespace similarity {
    using namespace std;
/*
 * Mahalanobis
 *
 */
    template<class T>
    bool InvertMatrixQFD (boost::numeric::ublas::matrix<T>& input, boost::numeric::ublas::matrix<T>& inverse){
        typedef boost::numeric::ublas::permutation_matrix<std::size_t> pmatrix;
        boost::numeric::ublas::matrix<T> A(input);
        pmatrix pm(A.size1());
        /*for(int i=0; i < A.size1(); i++){
            for(int j=0; j < A.size2();j++){
                LOG(LIB_INFO) << "input matrix::::" << A.operator()(i,j);
            }
        }*/
        int res = lu_factorize(A,pm);
        LOG(LIB_INFO) << "res::" << res;
        if( res != 0 )
            return false;

        inverse.assign(boost::numeric::ublas::identity_matrix<T>(A.size1()));
        lu_substitute(A, pm, inverse);

        return true;
    }

    template<class T>
    T QFD(
            const T *p1,
            const T *p2,
            size_t qty)
    {
        T result,matrix_tmp;
        std::vector<T> dif_mean(qty), tmp_vector(qty), dif_meanM(qty);
        boost::numeric::ublas::matrix<T> matrix(qty,qty), inverted_matrix(qty,qty);
        vector<T> p11(qty),p22(qty), dif_vector(qty);
        T num_FV = 2;
        T max = 0, min = INFINITY;

        for (size_t i = 0; i < qty; i++) {
            //dif_mean[i] = p2[i] - mean_vector[i];
            dif_mean[i] = p2[i] - p1[i];
            //LOG(LIB_INFO) << "dif mean:::" << dif_mean[i];
            //dif_meanM[i] = abs(dif_mean[i]);
            if(abs(dif_mean[i]) > max){
                max = abs(dif_mean[i]);
            }
            if(abs(dif_mean[i]) < min){
                min = abs(dif_mean[i]);
            }
        }

        //if(max == 0) max = 1;

        //create matrix

        for (size_t l = 0; l < qty; l++) { //lines
            for (size_t c = 0; c < qty && c <= l; c++) { //columns
                matrix_tmp = (abs(p2[l]-p1[c])/max);
                //LOG(LIB_INFO) << "matrix tmp before:" << matrix_tmp;
                //matrix_tmp = 1 - matrix_tmp;
                //if(matrix_tmp < 0){
                //    LOG(LIB_INFO) << "matrix tmp" << matrix_tmp;
                //}
                matrix(l,c) = matrix_tmp;  // symmetric covariance matrix
                matrix(c,l) = matrix_tmp;
            }
        }

        //LOG(LIB_INFO) << "normalize values";

        T max_min = max - min;
        for (size_t l = 0; l < qty; l++) { //lines
            for (size_t c = 0; c < qty && c <= l; c++) { //columns
                matrix(l,c) = 1 - ((matrix(l,c)- min)/(max_min));
                matrix(c,l) = 1 - ((matrix(l,c)- min)/(max_min));
            }
        }

        //multiply with matrix

        for (size_t i = 0; i < qty; i++) {
            tmp_vector[i] = 0;
            for (size_t j = 0; j < qty; j++) {
                tmp_vector[i] += (dif_mean[j] * matrix(j,i));
            }
        }
        result = 0;
        for (size_t i = 0; i < qty; i++) {
            result += (tmp_vector[i] * dif_mean[i]);
        }

        return sqrt(result);

    }

/*
    template<class T>
    T QFD(
            const T *p1,
            const T *p2,
            size_t qty)
    {
        T result,covariance_tmp;
        std::vector<T> dif_mean(qty), tmp_vector(qty);
        boost::numeric::ublas::matrix<T> covariance_matrix(qty,qty), inverted_matrix(qty,qty);
        vector<T> p11(qty),p22(qty);
        T num_FV = 2;

        for(size_t i = 0; i < qty; i++) {
            p11[i] = p1[i];
            //LOG(LIB_INFO) << "p1" << p1[i] << "p11[i]" << p11[i];
            p22[i] = p2[i];
            //LOG(LIB_INFO) << "p2" << p1[i] << "p22[i]" << p11[i];
        }

        double zscore_derivation;
        zscore norm;
        for(size_t i = 0; i < qty; i++) {
            norm = zscore();
            norm(p11[i]);
            norm(p22[i]);

            /* variance with N-1 (sample variance) */
 /*           zscore_derivation = sqrt(boost::accumulators::variance(norm) * (T)qty/((T)qty-1));
            //LOG(LIB_INFO) << "zscore:::" << zscore_derivation;
            if(zscore_derivation == 0){
                p11[i] = 0;
                p22[i] = 0;
            }else{
                p11[i] = (p11[i] - boost::accumulators::mean(norm)) / zscore_derivation;
                p22[i] = (p22[i] - boost::accumulators::mean(norm)) / zscore_derivation;
            }
            //LOG(LIB_INFO) << "p11" << p11[i] << " p22" << p22[i];
        }

        for (size_t i = 0; i < qty; i++) {
            localMean M;
            M(p11[i]);
            M(p22[i]);
            dif_mean.push_back(p11[i] - boost::accumulators::mean(M));
        }

        for (size_t l = 0; l < qty; l++) {               //lines
            for (size_t c = 0; c < qty && c <= l; c++) { //columns
                covariance_XY cov;
                cov(p11[l], boost::accumulators::covariate1 = p11[c]);
                cov(p22[l], boost::accumulators::covariate1 = p22[c]); // loop through feature vectors
                covariance_tmp = boost::accumulators::covariance(cov) * ( ((T) num_FV) / ((T) num_FV - 1) );     // sample variance
                if(covariance_tmp == 0 || covariance_tmp == -0) covariance_tmp=0;
                covariance_matrix(l, c) = covariance_tmp;
                covariance_matrix(c, l) = covariance_tmp;   // symmetric covariance matrix
            }
        }

        //LOG(LIB_INFO) << "cov matrix:" << covariance_matrix.size1() << " cov matrix 2:" << covariance_matrix.size2();


        bool inv = InvertMatrixQFD(covariance_matrix, inverted_matrix);
        if (inv == false) {
            throw std::length_error("Not invertible matrix.");
        }

        for (size_t i = 0; i < qty; i++) {
            tmp_vector[i] = 0;
            for (size_t j = 0; j < qty; j++) {
                tmp_vector[i] += dif_mean[j] * inverted_matrix(j,i);//*inverted_matrix2)(j,i);
            }
        }
        result = 0;
        for (size_t i = 0; i < qty; i++) {
            result += tmp_vector[i] * dif_mean[i];
        }

        return sqrt(result);

    }
*/

    template float QFD<float>(const float* pVect1, const float* pVect2, size_t qty);
    template double QFD<double>(const double* pVect1, const double* pVect2, size_t qty);

}